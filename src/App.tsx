import React, { FC }         from 'react';
import './App.scss';

import AppRoutes from 'routes/appRoutes'


const  App:FC = ()=> {
  return (
    <AppRoutes/>
  );
}

export default App;
