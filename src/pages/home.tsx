import './scss/home.scss'
import { FC } from 'react'

import { HomeMenu }             from 'components'
import { HomeContextProvider }  from 'contexts/homeContext'


export const HomePage:FC = ()=>{
    return (
        <HomeContextProvider>
            <HomeMenu/>
        </HomeContextProvider>
    )
}