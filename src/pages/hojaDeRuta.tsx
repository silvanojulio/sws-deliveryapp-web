import { FC } from 'react'
import { HojaDeRutaProvider } from 'contexts'
import { HojaDeRuta } from 'components/hojaDeRuta'

export const HojaDeRutaPage:FC = ()=>{

    return (
        <HojaDeRutaProvider>
            <HojaDeRuta />
        </HojaDeRutaProvider>
    )
}