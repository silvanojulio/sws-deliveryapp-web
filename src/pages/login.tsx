import React, { FC } from 'react'
import { LoginContextProvider } from 'contexts/loginContext'
import { LoginForm } from 'components'

import './scss/login.scss'

export const LoginPage:FC = ()=>{
    return (
        <LoginContextProvider>
            <LoginForm/>
        </LoginContextProvider>
    )
}