import React, { FC, useState }  from 'react'
import { useContext }           from 'react'
import { useEffect }            from 'react'
 

import { AppData_HojaDeRuta }   from 'entities/app'
import { HojasDeRuta }          from 'services/DB'
 
import { GestionCliente, IGestionCliente }      from 'dialogs/gestionCliente'
import { ComprobanteFisico }                    from 'dialogs/comprobanteFisico'
import { IComprobanteFisico }                   from 'dialogs/comprobanteFisico'
import { Entrega, IEntrega }                    from 'dialogs/entrega'
import { Efectivo, IEfectivo }                  from 'dialogs/cobros'
import { Cheque, ICheque }                      from 'dialogs/cobros'
import { Retencion, IRetencion }                from 'dialogs/cobros'
import { Credito, ICredito }                    from 'dialogs/cobros'
import { Debito, IDebito }                      from 'dialogs/cobros'
import { Cobros, ICobros }                      from 'dialogs/cobros'
import { Alertas, IAlertas }                    from 'dialogs/alertas'
import { Devoluciones, IDevoluciones }          from 'dialogs/devoluciones'
import { TomarCoordenadas, ITomarCoordenadas }  from 'dialogs/tomarCoordenadas'
import { ServicioTecnico, IServicioTecnico }    from 'dialogs/servicioTecnico'
import { Transacciones, ITransacciones }        from 'dialogs/transacciones'
import { ControlDePlaya, IControlDePlaya }      from 'dialogs/controlDePlaya'
import { GastosDeReparto, IGastosDeReparto }    from 'dialogs/gastosDeReparto'
import { CierreDeRuta, ICierreDeRuta }          from 'dialogs/cierreDeRuta'
import { FichaInfoCliente, IFichaInfoCliente }  from 'dialogs/fichaInfoCliente'
import { WhatsappShare, IWhatsappShare }        from 'dialogs/whatsappShare'


type HojaDeRutaContextType={
    hojaDeRuta?:AppData_HojaDeRuta
    comprobanteFisicoRef: React.RefObject<IComprobanteFisico>
    entregaDialog:React.RefObject<IEntrega>
    gestionDialog:React.RefObject<IGestionCliente>
    efectivoDialog:React.RefObject<IEfectivo>
    chequeDialog:React.RefObject<ICheque>
    retencionDialog:React.RefObject<IRetencion>
    creditoDialog:React.RefObject<ICredito>
    debitoDialog:React.RefObject<IDebito>
    alertasDialog:React.RefObject<IAlertas>
    cobrosDialog:React.RefObject<ICobros>
    devolucionesDialog:React.RefObject<IDevoluciones>
    tomarCoordenadasDialog:React.RefObject<ITomarCoordenadas>
    servicioTecnicoDialog:React.RefObject<IServicioTecnico>
    transaccionesDialog:React.RefObject<ITransacciones>
    controlDePlayaDialog:React.RefObject<IControlDePlaya>
    gastosDialog:React.RefObject<IGastosDeReparto>
    cierreDeRutaDialog:React.RefObject<ICierreDeRuta>
    fichaInfoClienteDialog:React.RefObject<IFichaInfoCliente>
    whatsappShareDialog:React.RefObject<IWhatsappShare>
}

export const HojaDeRutaContext = React.createContext<HojaDeRutaContextType>({
    gestionDialog:React.createRef<IGestionCliente>(),
    entregaDialog:React.createRef<IEntrega>(),
    comprobanteFisicoRef: React.createRef<IComprobanteFisico>(),
    cobrosDialog:React.createRef<ICobros>(),
    efectivoDialog:React.createRef<IEfectivo>(),
    chequeDialog:React.createRef<ICheque>(),
    retencionDialog:React.createRef<IRetencion>(),
    creditoDialog:React.createRef<ICredito>(),
    debitoDialog:React.createRef<IDebito>(),
    alertasDialog:React.createRef<IAlertas>(),
    devolucionesDialog:React.createRef<IDevoluciones>(),
    tomarCoordenadasDialog:React.createRef<ITomarCoordenadas>(),
    servicioTecnicoDialog:React.createRef<IServicioTecnico>(),
    transaccionesDialog:React.createRef<ITransacciones>(),
    controlDePlayaDialog:React.createRef<IControlDePlaya>(),
    gastosDialog:React.createRef<IGastosDeReparto>(),
    cierreDeRutaDialog:React.createRef<ICierreDeRuta>(),
    fichaInfoClienteDialog:React.createRef<IFichaInfoCliente>(),
    whatsappShareDialog:React.createRef<IWhatsappShare>()
})

export const HojaDeRutaProvider:FC = (props)=>{
    const { children }                      = props
    const context                           = useContext(HojaDeRutaContext)
    const [ hojaDeRuta, setHojaDeRuta ]     = useState<AppData_HojaDeRuta>()
    
    useEffect(()=>{
        HojasDeRuta.GetActual().then(setHojaDeRuta) 
    },[])
    
    return <HojaDeRutaContext.Provider value={{
        ...context,
        hojaDeRuta,
    }}>
        <Alertas            ref={context.alertasDialog}         />
        <GestionCliente     ref={context.gestionDialog}         />
        <ComprobanteFisico  ref={context.comprobanteFisicoRef}  />
        <Entrega            ref={context.entregaDialog}         />
        <Efectivo           ref={context.efectivoDialog}        />
        <Cheque             ref={context.chequeDialog}          />
        <Retencion          ref={context.retencionDialog}       />
        <Credito            ref={context.creditoDialog}         />
        <Debito             ref={context.debitoDialog}          />
        <Cobros             ref={context.cobrosDialog}          />
        <Devoluciones       ref={context.devolucionesDialog}    />
        <TomarCoordenadas   ref={context.tomarCoordenadasDialog}/>
        <ServicioTecnico    ref={context.servicioTecnicoDialog} />
        <Transacciones      ref={context.transaccionesDialog}   />
        <ControlDePlaya     ref={context.controlDePlayaDialog}  />
        <GastosDeReparto    ref={context.gastosDialog}          />
        <CierreDeRuta       ref={context.cierreDeRutaDialog}    />
        <FichaInfoCliente   ref={context.fichaInfoClienteDialog}/>
        <WhatsappShare      ref={context.whatsappShareDialog}   />
        {children}
    </HojaDeRutaContext.Provider>
}