import React                        from 'react' 
import { useState, useEffect }      from 'react' 
import { useContext, useCallback }  from 'react'
import { toast as toastify }        from 'react-toastify'
import { ToastContainer}            from 'react-toastify'
import { ToastOptions}              from 'react-toastify'
import { ToastContent }             from 'react-toastify'

import { FnQueue, AppStorage }      from 'utils'
import { Sync }                     from 'utils'
import { GPSTracker }               from 'utils'
import { AppData_UsuariosApp }      from 'entities/app'
import { LoadingModal }             from 'dialogs/loading'
import { Choose, IChoose }          from 'dialogs/choose' 
import { Confirm, IConfirm }        from 'dialogs/confirm'
import { SessionService }           from 'services/session'


type AppContextType={
    onLine?:        boolean
    ToastError:     (content: ToastContent, options?: ToastOptions)=> React.ReactText
    ToastSuccess:   (content: ToastContent, options?: ToastOptions)=> React.ReactText
    ToastInfo:      (content: ToastContent, options?: ToastOptions)=> React.ReactText
    logout:         () => Promise<void>
    user?:          AppData_UsuariosApp
    setUser?:       React.Dispatch<React.SetStateAction<AppData_UsuariosApp | undefined>>
    printerOn:     boolean
    setPrinterOn?:  React.Dispatch<React.SetStateAction<boolean>>
    ConfigServer:   (domain:string, movilId:string) => Promise<void>
    ChooseRef:      React.RefObject<IChoose>
    ConfirmRef:     React.RefObject<IConfirm>
}

export const AppContext=React.createContext<AppContextType>({
    ToastError:     toastify.error,
    ToastSuccess:   toastify.success,
    ToastInfo:      toastify.info,
    logout:         async () => {},
    ConfigServer:   async (_domain:string, _movilId: string) =>{},
    ChooseRef:      React.createRef<IChoose>(),
    ConfirmRef:     React.createRef<IConfirm>(),
    printerOn:      true
})
export interface IAppContext{
    askForRefresh:(registration:ServiceWorkerRegistration)=>void
}
export const AppContextProvider:React.FC = (props)=>{
    const app                           = useContext(AppContext)
    const [ onLine, setOnLine ]         = useState<boolean>(true)
    const [ user, setUser ]             = useState<AppData_UsuariosApp>()
    const [ printerOn, setPrinterOn ]   = useState<boolean>(true)
    window.addEventListener('online',  ()=>setOnLine(navigator.onLine))
    window.addEventListener('offline',  ()=>setOnLine(navigator.onLine))
    const loadData=useCallback(async()=>{
        const localSession=await SessionService.GetCurrentLocalSession()
        setUser(localSession?.userApp)
        const printerOn = await AppStorage.getValue<boolean>("askPrinter",true)
        setPrinterOn(printerOn??true)
    },[])
    const ConfigServer=useCallback(async(domain?:string, movilId?:string):Promise<void> =>{
        AppStorage.clear()
        await AppStorage.setValue("SERVERDOMAIN", domain)
        await AppStorage.setValue("MOVILID", movilId)
    },[])
    const logout = useCallback(async():Promise<void> =>{
        Sync.StopSync()
        GPSTracker.StopTracker()
        setUser(undefined)
    },[])
    useEffect(()=>FnQueue.setOnLine(onLine),[onLine])
    useEffect(()=>{loadData()},[loadData])
    useEffect(()=>{AppStorage.setValue("askPrinter",printerOn??true)},[printerOn])
    return (
        <AppContext.Provider value={{
            ...app,
            onLine,
            logout,
            user,           setUser,
            printerOn,      setPrinterOn,
            ConfigServer,
        }}>
            <LoadingModal/> 
            <Choose ref={app.ChooseRef} />
            <Confirm ref={app.ConfirmRef} />
            <ToastContainer {...{
                position:"bottom-right",
                autoClose:5000,
                closeButton:false,
                newestOnTop:true,
                closeOnClick:true,
                pauseOnFocusLoss:false,
                draggable:true
            }}/>
            {props.children}
        </AppContext.Provider>
    )
}