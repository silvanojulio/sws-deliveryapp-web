import React, { FC, useContext } from 'react'
type HomeContextType = {
}

export const HomeContext = React.createContext<HomeContextType>({
})

export const HomeContextProvider:FC = ({children})=>{
    const context = useContext(HomeContext)
    return <HomeContext.Provider value={{
        ...context,
    }}>
        {children}
    </HomeContext.Provider>
}