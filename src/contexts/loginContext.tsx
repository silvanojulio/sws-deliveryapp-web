import React, { FC, useContext }    from 'react'

type LoginContextType={}

export const LoginContext = React.createContext<LoginContextType>({})

export const LoginContextProvider:FC = ({children})=>{
    const context = useContext(LoginContext)
    return (
        <LoginContext.Provider value={{...context}}>
            {children}
        </LoginContext.Provider>
    )
}