import { concatUint8Arrays } from 'utils'
const FontBold={
    normal  : new Uint8Array([0x1B, 0x45, 0x00]),
    bold    : new Uint8Array([0x1B, 0x45, 0x01])
}
const FontAlign={
    left      : new Uint8Array([0x1B,0x61,0x00]),
    center    : new Uint8Array([0x1B,0x61,0x01]),
    right     : new Uint8Array([0x1B,0x61,0x02])
}
export class ReceiptText{
    public static Separador = new ReceiptText("================================")
    public static Feed = new ReceiptText("\n")
    public static Aligns = FontAlign
    public static Bold = FontBold.bold
    constructor(
        public content:string,
        public type:"text"|"image"|"qr" = "text",
        public align:"left"|"center"|"right"="left",
        public bold?:boolean,
        public imageWidth?:number,
        public imageHeight?:number
    ){
        if(this.type==="text") this.content=content
                .replaceAll('á','a')
                .replaceAll('é','e')
                .replaceAll('í','i')
                .replaceAll('ó','o')
                .replaceAll('ú','u')
                .replaceAll('Á','A')
                .replaceAll('É','E')
                .replaceAll('Í','I')
                .replaceAll('Ó','O')
                .replaceAll('Ú','U')
                .replaceAll('ñ','n')
                .replaceAll('Ñ','N')
    }
    public toUint8Array = ():Uint8Array=>{

        return concatUint8Arrays(
            this.bold?FontBold.bold:FontBold.normal,
            FontAlign[this.align],
            new TextEncoder().encode(this.content),
            new Uint8Array(this.content.length!==32?[10]:[])
        )
    }
}
export type Receipt=ReceiptText[]