import { AppData_Clientes, AppData_ResumenClientes } from 'entities/app'
export type HojaDeRutaFiltroOptions="search"|"all"|"pendientes"|"repasos"|"pedidos"|"srvtecs"|"nosyncs"
export type PromiseCallbacks<T> = {
    resolve: (value: T | PromiseLike<T>) => void
    reject: (reason?: any) => void
}
export type SelectOption<T> = {
    label:string,
    value:T
}
export { ReceiptText } from './receipt'
export type { Receipt } from './receipt'
export type Transaccion = {
    tipo:"entrega" | "devolucion" | "prestamo" | "cobro" | "pnc"
    descripcion:string
    cantidad?:number 
}
export type HojaDeRutaFiltroResult=Partial<AppData_Clientes> & Partial<AppData_ResumenClientes> & {
    cliente_id:number
    store:"Clientes" | "ResumenClientes"
    alertaFue?:boolean
    preventivo?:boolean
    bloqueado?:boolean
    mostrarTelefono:boolean
    mostrarWhatsapp:boolean
}
export type SyncResultados={
    RegistrarVisitasResult:boolean
    ObtenerNuevosPedidosResult:boolean
    InformarUbicacionResult:boolean
    ObtenerComandosParaEjecutarResult:boolean
}
export const GoogleMapApiKey="AIzaSyBnlJXjmcS1KhSAOdlINLyfetNY6Fe7paw"
