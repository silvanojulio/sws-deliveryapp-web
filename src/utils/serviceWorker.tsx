import { showNotification } from './notifications'

export class ServiceWorkerUtils{
    private static interval?:NodeJS.Timeout
    public static async startPeriodicUpdate(){
        if(this.interval!==undefined) return
        const update=async ()=>{
            const registration = await navigator.serviceWorker.getRegistration()
            if(!registration) return
            registration.update()
        }
        update()
        this.interval=setInterval(update,600000)
    }
    public static updateSW(registration:ServiceWorkerRegistration ){
        if(registration?.waiting){
            showNotification("Nueva Version Disponible",{
                tag:"new-version",
                vibrate:[500,200,500],
                actions:[
                    {title:"ACTUALIZAR",action:"actualizar"}
                ]
            })
        }
    }
}
