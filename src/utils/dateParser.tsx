import moment from 'moment'
export const dateParser = (date?:string|null, format?:string):string=>{
    if(!date || date==="") return ""
    if(!format || format==="") format="YYYY/MM/DD"
    switch(true){
        case date.substr(0,6)==="/Date(":
            return moment(parseInt(date.substring(6, date.length-2))).format(format)
        default:
            return moment(date).format(format)
    }
}