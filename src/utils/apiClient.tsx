import axios, { AxiosRequestConfig} from 'axios';
import AppStorage from 'utils/appStorage'

const gateways = [
    "southamerica-east1-waterserviceweb.cloudfunctions.net/swsgateway",
    "gateway.sistemaws.com:3001",
]

export type AppErrorBase= {
    error:number,
    message?:string,
}

export default class ApiClient {

    static async call<T>(request: AxiosRequestConfig):Promise<AppErrorBase & T> {
        const token         = await AppStorage.getString("CURRENTTOKENVALUE")
        var serverDomain    = await AppStorage.getString("SERVERDOMAIN")
        var gatewayID       = parseInt(await AppStorage.getString("GATEWAYID","0"))
        var currentLatitud  = await AppStorage.getString("CURRENTLATITUD","")
        var currentLongitud = await AppStorage.getString("CURRENTLONGITUD","")
        
        if(!serverDomain) serverDomain="test.app.sistemaws.com:8070"

        request.headers = {
            urldestiny: `http://${serverDomain}/${request.url}`,
            ...request.headers,
        }
        
        request.baseURL = `https://${gateways[gatewayID]}/`
        request.url = "";
        
        if (token) request.headers = {
            ...request.headers,
            CURRENTTOKENVALUE: token,
        }
        if(currentLatitud!=="" && currentLongitud!=="") request.headers={
            ...request.headers,
            latitud:currentLatitud,
            longitud:currentLongitud
        }
        
        var axRes = await axios(request).catch(
            (error)=>{ 
                if( error.response ){
                    const result=error.response.data
                    result.message=`WS Error: ${result.message}`
                    throw result;
                }else{
                    //gatewayID++
                    if(gatewayID>=gateways.length) gatewayID=0
                    AppStorage.setValue("GATEWAYID",gatewayID.toString())
                    throw JSON.stringify(error);
                }
            }
        );
        var {data:response} = axRes;
        if(!response) return {error:1, message:"Null response received"} as AppErrorBase & T
        if(request.responseType==='json' && response.error) response.error=parseInt(response.error) 
        return response
    }
}
export { ApiClient }