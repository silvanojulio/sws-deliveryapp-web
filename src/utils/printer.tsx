
import { Receipt, ReceiptText }         from 'types'
import { AppData_Clientes }             from 'entities/app'
import { AppData_ItemsDeRecibos }       from 'entities/app'
import { AppData_HojaDeRuta }           from 'entities/app'
import { AppData_ArticulosEntregados }  from 'entities/app'
import { AppData_FirmasRemitos }        from 'entities/app'
import moment                           from 'moment'
export class PrinterTemplates{
    public static cabecera(nombre_fantasia:string,web_empresa:string, telefono:String):Receipt{
        const format_tel=telefono.replace(".","\n")
        return [
            ReceiptText.Separador,
            new ReceiptText(`${nombre_fantasia}\n${web_empresa}\nTel: ${format_tel}`,"text","center"),
            ReceiptText.Separador
        ]
    }
    public static clienteInfo(cliente:AppData_Clientes, hojaDeRuta:AppData_HojaDeRuta):Receipt{
        var titulo=""
        switch(true){
            case cliente.ausente:titulo="COMPROBANTE DE VISITA";break;
            default: titulo="COMPROBANTE DE GESTION"
        }
        const receipt=[
            new ReceiptText(titulo,"text","center"),
            new ReceiptText(`Nro Cliente: ${cliente.cliente_id}`),
            new ReceiptText(`Cliente: ${cliente.nombreCliente}`),
            new ReceiptText(`Fecha: ${moment(new Date()).format("DD/MM/YYYY HH:mm:ss")}`),
            new ReceiptText(`Hoja de ruta: #${hojaDeRuta.nombreReparto}`),
        ]
        if(!cliente.ausente) receipt.push(new ReceiptText(`Comprobante #${hojaDeRuta.id}-${cliente.cliente_id}`))
        receipt.push(ReceiptText.Separador)
        return receipt
    }
    public static pie(urlBase:string, user?:string, pass?:string):Receipt{
        const url=urlBase.substr(0,7)==="http://"?urlBase:`http://${urlBase}`
        const receipt= [
            new ReceiptText("===== Accede a tu Cuenta =====","text","center"),
            new ReceiptText("=====  Escaneando el QR  =====","text","center"),
            new ReceiptText(url, "qr","center"),
            new ReceiptText(`\n${url}`,"text","center"),
        ]
        if(user) receipt.push(new ReceiptText(`Usuario: ${user}`,"text","center"))
        if(pass) receipt.push(new ReceiptText(`Contraseña: ${pass}`,"text","center"))
        return receipt
    }
    public static mensaje(msg:string):Receipt{
        return [
            new ReceiptText("\n--- Mensaje ---\n", "text","center"),
            ...this.splitMessage(msg)
        ]
    }
    private static splitMessage(text:string, align:"left"|"center"|"right"="left"):Receipt{
        const receipt:Receipt = []
        const palabras = text.split(" ")
        var buffer=palabras[0]??""
        for(let p=1;p<palabras.length;p++){
            if(buffer.length+1+palabras[p].length>32){
                receipt.push(new ReceiptText(buffer, "text",align))
                buffer=palabras[p]
            }else{
                buffer+=` ${palabras[p]}`
            }
        }
        receipt.push(new ReceiptText(buffer,"text",align))
        return receipt;
    }
    public static cuentaSuspendidaFacturas(params:{desde:string, cantVencidos:number, montoVencido:number, cantVigente:number, montoVigente:number}):Receipt{
        return [
            new ReceiptText(`-------------------------------`,"text","center"),
            new ReceiptText("Cuenta Suspendida", "text","center"),
            new ReceiptText(`-------------------------------`,"text","center"),
            new ReceiptText(`Estimado Cliente,`,"text","left"),
            ...this.splitMessage(`Le informamos que desde el dia ${params.desde} la cuenta esta suspendida por superar el limite de comprobantes vencidos permitidos.`),
            ...this.splitMessage(`Actualmente posee ${params.cantVencidos} comprobantes vencidos por el monto de $${params.montoVencido.toFixed(2)} y ${params.cantVigente} comprobantes por vencer por el monto de $${params.montoVigente.toFixed(2)}.`),
            ...this.splitMessage(`Regularice su situacion para poder continuar con el servicio`),
        ]
    }
    public static cuentaSuspendidaSaldos(params:{saldo:number,limite:number}):Receipt{
        return [
            new ReceiptText(`-------------------------------`,"text","center"),
            new ReceiptText("Cuenta Suspendida", "text","center"),
            new ReceiptText(`-------------------------------`,"text","center"),
            new ReceiptText(`Estimado Cliente,`,"text","left"),
            ...this.splitMessage(`Le informamos que su cuenta se encuentra suspendida por superar el limite de credito.`),
            ...this.splitMessage(`El saldo actual es de $${params.saldo.toFixed(2)} y su límite de crédito es de $${params.limite.toFixed(2)}.`),
            ...this.splitMessage(`Regularice su situacion para poder continuar con el servicio`),
        ]
    }
    public static cuentaPreventivoSaldo(params:{saldo:number, limite:number}):Receipt{
        return [
            new ReceiptText(`-------------------------------`,"text","center"),
            new ReceiptText(`Proximidad suspension de cuenta`,"text","center"),
            new ReceiptText(`-------------------------------`,"text","center"),
            new ReceiptText(`Estimado cliente,`,"text","left"),
            ...this.splitMessage(`Le informamos que la cuenta se encuentra proxima a suspenderse.`),
            ...this.splitMessage(`El saldo actual es de $${params.saldo.toFixed(2)} y su limite de credito es de $${params.limite.toFixed(2)}.`),
            ...this.splitMessage(`Por favor regularice su situacion para evitar la suspension del servicio`)
        ]
    }
    public static cuentaPreventivoFacturas(params:{desde:string, cantVencidos:number, montoVencido:number, cantVigente:number, montoVigente:number}):Receipt{
        return [
            new ReceiptText(`-------------------------------`,"text","center"),
            new ReceiptText(`Proximidad suspension de cuenta`,"text","center"),
            new ReceiptText(`-------------------------------`,"text","center"),
            new ReceiptText(`Estimado cliente,`,"text","left"),
            ...this.splitMessage(`Le informamos que la cuenta se encuentra proxima a suspenderse.`),
            ...this.splitMessage(`Actualmente posee ${params.cantVencidos} comprobantes vencidos por el monto de $${params.montoVencido.toFixed(2)} y ${params.cantVigente} comprobantes por vencer por el monto de $${params.montoVigente.toFixed(2)}.`),
            ...this.splitMessage(`Por favor regularice su situacion antes del ${params.desde} para evitar la suspension del servicio`)
        ]
    }
    public static saldos(cliente:AppData_Clientes,cobrado:number,entrega:number):Receipt{
        if(!cliente ) return []
        const receipt:Receipt = [
            new ReceiptText("\n--- Saldo del cliente ---\n", "text","center"),
            new ReceiptText(`Saldo consumos:     $${cliente.saldoConsumos.toFixed(2)}`),
            new ReceiptText(`Saldo facturacion:  $${cliente.saldoFacturacion.toFixed(2)}`),
        ]
        cobrado>0 && receipt.push(new ReceiptText(`Cobros registrados: $${cobrado.toFixed(2)}`))
        entrega>0 && receipt.push(new ReceiptText(`Consumo de visita:  $${entrega.toFixed(2)}`))
        receipt.push(new ReceiptText(`Saldo al momento:   $${(cliente.saldoConsumos + cliente.saldoFacturacion + entrega - cobrado).toFixed(2)}`))
        receipt.push(ReceiptText.Separador)
        return receipt
    }
    public static articulosEntregados(articulos:AppData_ArticulosEntregados[]):Receipt{
        if(!articulos.some(art=>art.cantidadEntregada>0)) return []
        const total=articulos.reduce((t,art)=>t+=art.subtotal,0)
        return [
            new ReceiptText("\n --- Productos ---\n", "text","center"),
            ...articulos.filter(art=>art.cantidadEntregada>0).map<ReceiptText>(art=>{
                const desc      = `> ${art.nombreDelArticulo}`.slice(0,31)
                const cantidad  = `\n  (${art.cantidadEntregada}x$${art.precioUnitario})`
                const subtotal  = `$${art.subtotal.toString()}`
                const venta     = (cantidad+"                                ".slice(cantidad.length,32)).slice(0, -1 + -1 * subtotal.length)+subtotal
                return new ReceiptText(desc+venta)
            }),
            new ReceiptText(`Total: $${total}`, "text","right")
        ]
    }
    public static envasesDevueltos(articulos:AppData_ArticulosEntregados[]):Receipt{
        if(!articulos.some(art=>art.cantidadEnvasesDevueltos>0)) return []
        return [
            new ReceiptText("\n --- Devoluciones ---", "text","center"),
            ...articulos.filter(art=>art.cantidadEnvasesDevueltos>0).map<ReceiptText>(
                art=>new ReceiptText(`\n> (X ${art.cantidadEnvasesDevueltos.toString()}) ${art.nombreDelArticulo}`.slice(0,31))
            )
        ]
    }
    public static envasesPrestados(articulos:AppData_ArticulosEntregados[]):Receipt{
        if(!articulos.some(art=>art.cantidadEnvasesPrestados>0)) return []
        return [
            new ReceiptText("\n --- Envases Prestados ---", "text","center"),
            ...articulos.filter(art=>art.cantidadEnvasesPrestados>0).map<ReceiptText>(
                art=>new ReceiptText(`\n> (X ${art.cantidadEnvasesPrestados.toString()}) ${art.nombreDelArticulo}`.slice(0,31))
            )
        ]
    }
    public static firmaRecibo(firma:AppData_FirmasRemitos):Receipt{        
        return [
            new ReceiptText("\n--- Firma de receptor ---\n", "text","center"),
            new ReceiptText(`Receptor: ${firma.firmante}\n`),
            new ReceiptText(firma.firmaRemito, "image"),
            ReceiptText.Separador
        ]
    }
    public static cobros(items:AppData_ItemsDeRecibos[]):Receipt{      
        if(!items.some(item=>item.importe>0)) return []  
        return [
            new ReceiptText("\n --- Cobros ---\n", "text","center"),
            ...items.filter(item=>item.importe>0).map<ReceiptText>(
                item=>{
                    var formaDePago=""
                    switch (true){
                        case !!item.chequeId:           formaDePago="Cheque";break;
                        case !!item.retencionId:        formaDePago="Retencion";break;
                        case !!item.tarjetaDeCreditoId: formaDePago="Tarjeta de Credito";break;
                        case !!item.tarjetaDeDebitoId:  formaDePago="Tarjeta de Debito";break;
                        default:formaDePago="Efectivo"
                    }
                    const l1=`> ${formaDePago}`
                    const l2=item.descripcion
                    const l3=`  Importe: $${item.importe}`
                    return new ReceiptText(`${l1}\n${l2?`  ${l2}\n`:""}${`${l3}`}`)
                }
            )
        ]
    }
    public static QR(text:string):Receipt{
        return[
            new ReceiptText(text,"qr")
        ]
    }
}