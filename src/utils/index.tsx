export { ApiClient }    	from './apiClient'
export { AppStorage }   	from './appStorage'
export { FnQueue }      	from './fnQueue'
export { DB }           	from './indexedDB'
export { Sync }         	from './sync'
export { dateParser }   	from './dateParser'
export { PrinterTemplates }	from './printer'
export { ImageUtil } 		from './image'
export { ServiceWorkerUtils }	from './serviceWorker'
export { GPSTracker } 		from './gpsTracker'
export { whatsappShare } 	from './whatsappShare'

export const concatUint8Arrays=(...bufs:Uint8Array[])=>{
	const result = new Uint8Array(bufs.reduce((totalSize, buf)=>totalSize+buf.byteLength,0));
	bufs.reduce((offset, buf)=>{
		result.set(buf,offset)
		return offset+buf.byteLength
	},0)
	return result
}
export const transpose=(arr:any[], original_width:number)=>{
	let ret=[]
	for(let x=0;x<original_width;x++){
		for(let y=0;y<arr.length/original_width;y++){
			ret[x+y*original_width]=arr[y+x*original_width]
			ret[y+x*original_width]=arr[x+y*original_width]
		}
	}
	return ret.filter(v=>v!==undefined)
}
export const ofuscatePassword=async (txt:string):Promise<string> =>{
	const enc=new TextEncoder();
	const data=await crypto.subtle.digest("SHA-256",new Uint8Array(enc.encode(txt)));
	const arr=Array.from(new Uint16Array(data))
	return arr.map(b=>b.toString(16).padStart(2,'0')).join("")
}