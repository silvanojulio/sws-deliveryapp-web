export default class AppStorage {
    
    static async setValue(key:string, value: any) {
        localStorage.setItem(key, typeof value === 'object'? JSON.stringify(value) : value);
    }
    static async getObject<T>(key:string, defaultValue?:T):Promise<T|undefined>{
        try {
            return JSON.parse(localStorage[key]) as T;
        }catch(error){
            return defaultValue
        }
    }
    static async getString(key:string, defaultValue?:string):Promise<string>{
        return localStorage[key]??defaultValue??"";
    }
    static async getValue<T>(key:string, defaultValue?:T):Promise<T | null>{
        return localStorage[key]??defaultValue??null
    }
    static removeValue(key:string){
        return localStorage.removeItem(key);
    }
    static clear(){
        localStorage.clear()
    }
}
export { AppStorage }