import moment                   from 'moment'

import { SyncServices }         from 'services/sync'
import { MovilesServices }      from 'services/moviles'
import { IncidentesService }    from 'services/incidentes'
import { SessionService }       from 'services/session'
import * as DB                  from 'services/DB'
import { AppStorage }           from './appStorage'
import { Comandos }             from './comandos'
import { SyncResultados }       from 'types'

export class Sync{
    private static IntervalID?:NodeJS.Timeout = undefined
    private static runningSync=false
    private static delay=10000
    public static callback?:(resultados:SyncResultados)=>Promise<void>
    public static async StartSync(delay?:number){
        if(this.IntervalID) return
        const CANT_SEG_SYNC = parseInt(await DB.Configuraciones.GetConfig("CANT_SEG_SYNC","10"))*1000
        this.delay=delay??CANT_SEG_SYNC??this.delay
        this.IntervalID=setInterval(()=>this.Sync(),this.delay)
    }
    public static StopSync(){
        if(!this.IntervalID) return
        clearInterval(this.IntervalID)
        this.IntervalID=undefined
    }
    public static async Sync(){
        const hojaDeRutaId  = parseInt(await AppStorage.getString("HojaDeRutaId"))
        const codigoMovil   = await AppStorage.getString("MOVILID")
        if(!navigator.onLine) return
        if(!this.runningSync){
            this.runningSync=true
            try{
                const [
                    RegistrarVisitasResult, 
                    ObtenerNuevosPedidosResult,
                    InformarUbicacionResult,
                    ObtenerComandosParaEjecutarResult
                ] = await Promise.all([
                    this.RegistrarVisitas(),
                    this.ObtenerNuevosPedidos(hojaDeRutaId,codigoMovil),
                    this.InformarUbicacion(hojaDeRutaId,codigoMovil),
                    this.ObtenerComandosParaEjecutar(codigoMovil),
                    //this.ObtenerIncidentes()
                ])
                if(this.callback){
                    this.callback({                        
                        RegistrarVisitasResult, 
                        ObtenerNuevosPedidosResult,
                        InformarUbicacionResult,
                        ObtenerComandosParaEjecutarResult
                    })
                }
            }catch(error){}
            this.runningSync=false
        }        
    }
    private static async RegistrarVisitas(){
        const clientes=await DB.Clientes.GetPendientesSync()
        if(clientes.length===0) return false
        const resultados = await Promise.all<boolean>(clientes.map(async (cliente)=>{
            try{
                const DateTransferido = await SyncServices.RegistrarVisitaClienteMobile(cliente.cliente_id)
                cliente.fechaHoraTransferido=moment(DateTransferido).format("DD/MM/YYYY HH:mm:ss")
                await DB.Clientes.SaveCliente(cliente)
            }catch(error){
                if((error as Error).message==="Intenta cargar más de una visita sobre el mismo cliente. "){
                    cliente.fechaHoraTransferido=moment(new Date()).format("DD/MM/YYYY HH:mm:ss")
                    await DB.Clientes.SaveCliente(cliente)
                    return true
                }
                return false;
            }
            return true
        }))
        return resultados.reduce((ret,res)=>res || ret, false)
    }
    private static async ObtenerNuevosPedidos(hojaDeRutaId:number,codigoMovil:string){
        const pedidos       = await SyncServices.ObtenerNuevosPedidos(hojaDeRutaId, codigoMovil)
        const sonNuevos = await Promise.all<boolean>(pedidos.clientes
            .map<Promise<boolean>>(async (pedido)=>{
                const cliente=await DB.Clientes.GetCliente(pedido.cliente.cliente_id)
                if(!!cliente){
                    var result=false
                    if(!!pedido.ordenesDeTrabajo && pedido.ordenesDeTrabajo.length>0 && (await DB.OrdenesDeTrabajo.Get(pedido.cliente.cliente_id)).length>0){
                        result=true
                    }

                    if(pedido.cliente.esPedido){
                        cliente.esPedido=true
                        //cliente.esSync=true
                        cliente.tipoDeVisitaId  = pedido.cliente.tipoDeVisitaId
                        cliente.comunicacion1   = pedido.cliente.comunicacion1
                        cliente.comunicacion2   = pedido.cliente.comunicacion2
                        cliente.comunicacion3   = pedido.cliente.comunicacion3
                        cliente.comunicacion4   = pedido.cliente.comunicacion4
                        await DB.Clientes.SaveCliente(cliente)
                        result=true
                    }
                    return result
                }else{
                    try{
                        await DB.AgregarPedido(pedido)
                    }catch(error){
                        return false
                    }
                    return true
                }
            })
        )
        const idsClNuevos   = pedidos.clientes.map(cl=>cl.cliente.cliente_id)
        if(idsClNuevos.length>0) await SyncServices.ConfirmarClienteAgregadoALaRuta({
            clientesIds:idsClNuevos,
            codigoDeMovil:codigoMovil,
            hojaDeRutaId:hojaDeRutaId
        })
        return sonNuevos.reduce((ret,nuevo)=>ret || nuevo, false)
    }
    private static async InformarUbicacion(hojaDeRutaId:number,codigoMovil:string ):Promise<boolean>{
        const latitud   = await AppStorage.getString("CURRENTLATITUD","")
        const longitud  = await AppStorage.getString("CURRENTLONGITUD","")
        if(latitud==="" || longitud==="") return true
        try{
            await MovilesServices.InformarUbicacion(
                hojaDeRutaId,
                codigoMovil,
                latitud,
                longitud
            )
            return true
        }catch(error){
            return false
        }
    }
    private static async ObtenerComandosParaEjecutar(codigoMovil:string){
        try{
            const comandos = await SyncServices.ObtenerComandosParaEjecutar(codigoMovil)
            await Promise.all(comandos.map<Promise<void>>(async comando=>{
                comando.ejecutado=true
                switch(comando.comando){
                    case "EliminarAusenteNoCompr"   : return Comandos.EliminarAusenteNoCompra(comando.valor1)
                    case "Desbloquear"              : return Comandos.Desbloquear(comando.valor1)
                    default: comando.ejecutado=false
                }
            }))
            if(comandos.some(comando=>comando.ejecutado)){
                await SyncServices.ConfirmarComandosMobile(
                    comandos
                    .filter(comando=>comando.ejecutado)
                    .map<string>(comando=>comando._id)
                )
            }
            return true
        }catch(error){
            return false
        }
    }
    private static async ObtenerIncidentes(){
        const session = await SessionService.GetCurrentLocalSession()
        if(!session) return
        const incidentes=await IncidentesService.ObtenerIncidentesCliente(session.userApp.usuario_id)
        DB.Incidentes.UpdateList(incidentes)
    }
}
