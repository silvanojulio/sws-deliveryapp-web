export class ImageUtil{
    public static ConvertToDotsArray(base64:string):Promise<{width:number, height:number, dots:number[]}>{
        const p = new Promise<{width:number, height:number, dots:number[]}>(resolve=>{
            const img=new Image()        
            const cnv=document.createElement("canvas")
            img.onload=()=>{
                const width=380 //img.width - (img.width % 8)
                const height=380 //img.height - (img.height % 8)
                cnv.width=width
                cnv.height=height
                const cntx = cnv.getContext('2d')
                if(!cntx) throw new Error("No se pudo procesar la imagen")
                cntx.drawImage(img,0,0,width, height)
                const data=cntx.getImageData(0,0,width, height).data
                const dots=new Array<number>(data.length/4)
                for(let px=0;px<data.length;px+=4){
                    const pixel=data.slice(px,px+4)
                    const luma = pixel[0]*0.299 + pixel[1]*0.587 + pixel[2]*0.114
                    dots[px/4]=luma<200?1:0
                }
                resolve({width, height, dots})
            }
            img.src="data:image/png;base64, "+base64

        })
        return p
    }
}