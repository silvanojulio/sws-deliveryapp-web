export const showNotification=async(title:string, options?:NotificationOptions )=>{
    if(!await Notification.requestPermission()) return
    const registration = await navigator.serviceWorker.getRegistration()
    if(!registration) return
    registration.showNotification(title, options)
}