import * as DB from 'services/DB'

export class Comandos{
    public static async EliminarAusenteNoCompra(cliente_id:number){
        const cliente=await DB.Clientes.GetCliente(cliente_id)
        if(!cliente) return
        cliente.ausente=false
        cliente.visitado=false
        cliente.visita_longitud=""
        cliente.visita_altitud=""
        DB.Clientes.SaveCliente(cliente)        
    }
    public static async Desbloquear(cliente_id:number){
        await DB.LimitesDeClientes.Desbloquear(cliente_id)
    }
}