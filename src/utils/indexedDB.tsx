import { AppData_HojaDeRuta } from 'entities/app'
import { AppStorage } from 'utils'
import moment from 'moment'

export class DB{
    public static async openDB(databaseName?:string):Promise<IDBDatabase>{
        const HojaDeRutaId=await AppStorage.getObject<number>("HojaDeRutaId")
        if(!HojaDeRutaId) throw new Error("No Database selected")
        return new Promise<IDBDatabase>((resolve, reject)=>{
            var req = indexedDB.open(databaseName??`HojaDeRuta-${HojaDeRutaId}`,7)
            req.onupgradeneeded=function(ev){
                const db=this.result
                switch(ev.oldVersion){
                    case 0:
                    case 1: 
                    case 2:
                    case 3:
                        DB.CreateStoreWithIndexes(db,"AlertasDeCliente",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"cliente_id",keyPath:"cliente_id"},
                        ])
                        DB.CreateStoreWithIndexes(db,"ArticulosDeAbonos",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"cliente_id",keyPath:"cliente_id"}
                        ])
                        DB.CreateStoreWithIndexes(db,"ArticulosDeComodatos",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"cliente_id",keyPath:"cliente_id"},
                        ])
                        DB.CreateStoreWithIndexes(db,"ArticulosDeLista",{keyPath:'id',autoIncrement:true},[{name:"id",keyPath:"id",options:{unique:true}}])
                        DB.CreateStoreWithIndexes(db,"ArticulosEntregados",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"clienteId",keyPath:"clienteId"},
                        ])
                        DB.CreateStoreWithIndexes(db,"Cheques",{keyPath:'id',autoIncrement:true},[{name:"id",keyPath:"id",options:{unique:true}}])
                        DB.CreateStoreWithIndexes(db,"Clientes",{keyPath:'cliente_id',autoIncrement:true},[{name:"cliente_id",keyPath:"cliente_id",options:{unique:true}}])
                        DB.CreateStoreWithIndexes(db,"ComprobantesEntregados",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"cliente_id",keyPath:"cliente_id"},
                        ])
                        DB.CreateStoreWithIndexes(db,"ComprobantesFisicos",{keyPath:'idRango',autoIncrement:true},[
                            {name:"idRango",keyPath:"idRango",options:{unique:true}},
                            {name:"idTipoComprobante",keyPath:"idTipoComprobante"},
                        ])
                        DB.CreateStoreWithIndexes(db,"ComprobantesFisicosUtilizados",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"cliente_id",keyPath:"cliente_id"},
                        ])
                        DB.CreateStoreWithIndexes(db,"Configuraciones",{keyPath:'id',autoIncrement:true},[{name:"id",keyPath:"id",options:{unique:true}}])
                        
                        DB.CreateStoreWithIndexes(db,"DeclaracionesEfectivo",{keyPath:'id',autoIncrement:true},[{name:"id",keyPath:"id",options:{unique:true}}])
                        DB.CreateStoreWithIndexes(db,"DescuentosPorCantidad",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"listaDePrecioId",keyPath:"listaDePrecioId"},
                        ])
                        DB.CreateStoreWithIndexes(db,"DevolucionesArticulos",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"cliente_id",keyPath:"cliente_id"},
                        ])
                        DB.CreateStoreWithIndexes(db,"Dispensers",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"cliente_id",keyPath:"cliente_id"},
                        ])
                        DB.CreateStoreWithIndexes(db,"DispensersAsociados",{keyPath:'id',autoIncrement:true},[{name:"id",keyPath:"id",options:{unique:true}}])
                        DB.CreateStoreWithIndexes(db,"EnvasesDisponibles",{keyPath:'id',autoIncrement:true},[{name:"id",keyPath:"id",options:{unique:true}}])
                        DB.CreateStoreWithIndexes(db,"Facturas",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"cliente_id",keyPath:"cliente_id"},
                        ])
                        DB.CreateStoreWithIndexes(db,"FirmantesDisponibles",{keyPath:'id',autoIncrement:true},[
                            {name:"id", keyPath:"id",options:{unique:true}},
                            {name:"cliente_id",keyPath:"cliente_id"},
                        ])
                        DB.CreateStoreWithIndexes(db,"FirmasRemitos",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"cliente_id",keyPath:"cliente_id"},
                        ])
                        DB.CreateStoreWithIndexes(db,"GastosDeReparto",{keyPath:'id',autoIncrement:true},[{name:"id",keyPath:"id",options:{unique:true}}])
                        DB.CreateStoreWithIndexes(db,"HojasDeRuta",{keyPath:'id',autoIncrement:true},[{name:"id",keyPath:"id",options:{unique:true}}])
                        DB.CreateStoreWithIndexes(db,"ItemsDeRecibos",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"reciboId",keyPath:"reciboId"},
                        ])
                        DB.CreateStoreWithIndexes(db,"LimitesDeClientes",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"clienteId",keyPath:"clienteId"},
                        ])
                        DB.CreateStoreWithIndexes(db,"ListasDePrecios",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"listaId",keyPath:"listaId"},
                        ])
                        DB.CreateStoreWithIndexes(db,"Logs",{keyPath:'id',autoIncrement:true},[{name:"id",keyPath:"id",options:{unique:true}}])
                        DB.CreateStoreWithIndexes(db,"MantenimientosDeDispensers",{keyPath:'id',autoIncrement:true},[{name:"id",keyPath:"id",options:{unique:true}}])
                        DB.CreateStoreWithIndexes(db,"NumerosDeComprobantesDisponibles",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"utilizado",keyPath:["idRango","utilizado"]},
                        ])
                        DB.CreateStoreWithIndexes(db,"OrdenesDeTrabajo",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"cliente_id",keyPath:"cliente_id"},
                        ])
                        DB.CreateStoreWithIndexes(db,"PreciosEspeciales",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"clienteId",keyPath:"clienteId"},
                        ])
                        DB.CreateStoreWithIndexes(db,"PrestamosPendientes",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"cliente_id",keyPath:"cliente_id"},
                        ])
                        DB.CreateStoreWithIndexes(db,"ProximasVisitas",{keyPath:'clienteId',autoIncrement:true},[
                            {name:"clienteId",keyPath:"clienteId",options:{unique:true}},
                        ])
                        DB.CreateStoreWithIndexes(db,"Recibos",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"clienteId",keyPath:"clienteId"},
                        ])
                        DB.CreateStoreWithIndexes(db,"RepuestosActividades",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"mantenimiento_id",keyPath:"mantenimiento_id"},
                        ])
                        DB.CreateStoreWithIndexes(db,"ResumenClientes",{keyPath:'clienteId',autoIncrement:true},[{name:"clienteId",keyPath:"clienteId",options:{unique:true}}])
                        DB.CreateStoreWithIndexes(db,"Retenciones",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"reciboId",keyPath:"reciboId"},                            
                        ])
                        DB.CreateStoreWithIndexes(db,"StocksDeClientes",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"clienteId",keyPath:"clienteId"}
                        ])
                        DB.CreateStoreWithIndexes(db,"TarjetasDeDebito",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"reciboId", keyPath:"reciboId"}
                        ])
                        DB.CreateStoreWithIndexes(db,"TarjetasDeCredito",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"reciboId", keyPath:"reciboId"}
                        ])
                        DB.CreateStoreWithIndexes(db,"UsuariosApp",{keyPath:'usuario_id',autoIncrement:true},[
                            {name:"usuario_id",keyPath:"usuario_id",options:{unique:true}},
                            {name:"username",keyPath:"username",options:{unique:true}},
                        ])
                        DB.CreateStoreWithIndexes(db,"ValoresSatelites",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"tabla_id", keyPath:"tabla_id"}
                        ])
                        DB.CreateStoreWithIndexes(db,"Contactos",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}},
                            {name:"cliente_id",keyPath:"cliente_id"},
                        ])
                        DB.CreateStoreWithIndexes(db,"Declaraciones",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}}
                        ])
                        DB.CreateStoreWithIndexes(db,"Incidentes",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}}
                        ])
                        DB.CreateStoreWithIndexes(db,"RubrosInternosArticulos",{keyPath:'idArticulo', autoIncrement:true},[
                            {name:"idArticulo",keyPath:"idArticulo", options:{unique:false}},
                            {name:"rubro", keyPath:"rubro",options:{unique:false}}
                        ])
                        break;
                    case 4:
                    case 5:
                        DB.CreateStoreWithIndexes(db,"Incidentes",{keyPath:'id',autoIncrement:true},[
                            {name:"id",keyPath:"id",options:{unique:true}}
                        ])
                        DB.CreateStoreWithIndexes(db,"RubrosInternosArticulos",{keyPath:'idArticulo', autoIncrement:true},[
                            {name:"idArticulo",keyPath:"idArticulo", options:{unique:false}},
                            {name:"rubro", keyPath:"rubro",options:{unique:false}}
                        ])
                        break;
                    case 6:
                        DB.CreateStoreWithIndexes(db,"RubrosInternosArticulos",{keyPath:'idArticulo', autoIncrement:true},[
                            {name:"idArticulo",keyPath:"idArticulo", options:{unique:false}},
                            {name:"rubro", keyPath:"rubro",options:{unique:false}}
                        ])
                        break;
                }
            }
            req.onsuccess = function(){
                 resolve(this.result)
            }
            req.onerror = ()=>{
                reject("Error al conectar a indexedDB"+req.error?.message)
            }
        })
    }
    private static CreateStoreWithIndexes(db:IDBDatabase, StoreName:string, StoreOptions?:IDBObjectStoreParameters, indexes?:{name: string, keyPath: string | string[], options?: IDBIndexParameters}[]){
        const store = db.createObjectStore(StoreName,StoreOptions)
        if(indexes) indexes.forEach(index=>{
            store.createIndex(index.name, index.keyPath,index.options)
        })
    }
    public static async Clear(StoreNames:string[]):Promise<void>{
        const db = await DB.openDB()
        return new Promise((resolve, reject)=>{
            const tr = db.transaction(StoreNames,"readwrite")            
            tr.onerror=()=>{
                reject("Error en transaccion")
            }
            tr.oncomplete=()=>{
                resolve()
            }
            StoreNames.forEach(StoreName=>{
                const req   = tr.objectStore(StoreName).clear()
                req.onerror=()=>{
                    reject(`Error en el vaciado del Store ${StoreName}`)
                }
            })

        })
    }
    public static async Get<T>(StoreName:string, indexName:string, key:any[]|any):Promise<T | undefined>{
        const db = await DB.openDB()
        return new Promise<T | undefined>((resolve, reject)=>{
            const tr = db.transaction(StoreName,"readonly")
            tr.onerror=()=>{
                reject("Error en transaccion")
            }
            const store = tr.objectStore(StoreName)
            if(!store.indexNames.contains(indexName)) return reject(`Indice ${indexName} de store ${StoreName} invalido`)
            const req:IDBRequest<T> = store.index(indexName).get(key)
            
            
            req.onsuccess = ()=>resolve(req.result)
            req.onerror = ()=>reject(`Error en Query:${req.error?.message}`)            
        })
    }
    public static async GetAll<T>(StoreName:string, indexName?:string, indexValues?:any[]|any):Promise<T[]>{
        const db = await DB.openDB()
        return new Promise<T[]>((resolve, reject)=>{
            const tr = db.transaction(StoreName)
            tr.onerror=()=>{
                reject("Error en transaccion")
            }
            const store = tr.objectStore(StoreName)
            if(!indexName){
                const req=store.getAll()
                req.onerror=()=>reject(`Error:${req.error?.message}`)
                req.onsuccess=()=>resolve(req.result as T[])
                    
            }else{
                if(!store.indexNames.contains(indexName)) return reject(`Indice ${indexName} de store ${StoreName} invalido`)
                const req = store.index(indexName).getAll(IDBKeyRange.only(Array.isArray(indexValues)?indexValues.filter((value:any)=>!!value):indexValues))
                req.onerror=()=>reject(`Error en Query:${req.error?.message}`)
                req.onsuccess=()=>resolve(req.result as T[])
            }
        })
    }
    public static async Put(StoreName:string, data:object):Promise<IDBValidKey>{
        const db = await DB.openDB()
        return new Promise<IDBValidKey>((resolve, reject)=>{
            const tr = db.transaction(StoreName,"readwrite")
            tr.onerror=()=>{
                reject("Error en transaccion")
            }
            const req   = tr.objectStore(StoreName).put(data)
            req.onsuccess=function(){
                resolve(this.result)
            }
            req.onerror=()=>{
                reject("Error agregando el registro")
            }
        })
    }
    public static async Delete<T>(StoreName:string, key:any):Promise<T>{
        const db = await DB.openDB()
        return new Promise<T>((resolve, reject)=>{
            const store = db.transaction(StoreName,"readwrite")
            store.onerror=()=>{
                reject("Error en transaccion")
            }
            const obj = store.objectStore(StoreName).get(key)
            obj.onsuccess = ()=>{
                const req   = store.objectStore(StoreName).delete(key)
                req.onsuccess=()=>resolve(obj.result as T)
                req.onerror=_error=>reject("Error al eliminar registro")
            }
            obj.onerror = ()=>reject("No se encuentra registro a eliminar")            
        })
    }
    public static async Fill(data:{[key:string]:any[]}){
        //this.DropOlds(7)
        const db=await DB.openDB()
        const StoreNames=Object.keys(data).filter(StoreName=>StoreName!=="sqlite_sequence")
        const tr = db.transaction(StoreNames,"readwrite")
        tr.onerror=ev=>{throw new Error("Error en transaccion: "+ev.type)}

        StoreNames.forEach(StoreName=>{
            if(!tr.objectStoreNames.contains(StoreName)) return;
            const store=tr.objectStore(StoreName)
            //store.clear()
            data[StoreName].forEach(row=>{
                var req=store.add(row)
                req.onerror=(ev)=>{
                    ev.stopPropagation()
                    ev.preventDefault()
                }
            })
        })
        db.close()
    }
    private static async DropOlds(days:number){
        const HojaDeRutaId=await AppStorage.getObject<number>("HojaDeRutaId")
        const dbs   = (await indexedDB.databases())
                        .filter(db=>{
                            return !!db.name && db.name.substr(0,10)==="HojaDeRuta" && db.name.substr(11)!==HojaDeRutaId?.toString()
                        })
        dbs.forEach(db=>{
            this.DropDB(db.name!,days)
        })
    }
    private static async DropDB(dbName:string, days:number){
        const db=await this.openDB(dbName)
        const tr = db.transaction("HojasDeRuta", "readonly")
        tr.onerror=()=>{}
        const store = tr.objectStore("HojasDeRuta")
        const req  = store.get(0)
        req.onsuccess=()=>{
            const item = req.result as AppData_HojaDeRuta
            db.close()
            const fecha = moment(parseInt(item.fechaReparto.substring(6, item.fechaReparto.length-2)))
            if(fecha.diff(moment(),"days")>7) indexedDB.deleteDatabase(dbName)
        }
    }
}
export default DB