
type queueItemOption={
    requireOnLine?:boolean,
    blocking?:boolean,
    retryMaxCount?:number,
    label?:string
}
type queueItemCallbacks={
    catchFn?:(error:any)=>void,
    doneFn?:(data:any)=>void,
}

export type queueItem={
    fn:()=>Promise<any>
} & queueItemOption & queueItemCallbacks

type queueStatus={
    pending:number,
    running?:queueItem,
}
export abstract class FnQueue{
    private static queue:queueItem[] = []
    private static options:{
        onLine:boolean, 
        running?:queueItem,
        defaultItemOptions:queueItemOption
    } = {
        onLine:true, 
        defaultItemOptions:{
            requireOnLine:false,
            blocking:false,
            retryMaxCount:0,
            label:"Loading...",
        }
    }
    private static onRunningChange:((item:queueItem, event:"start"|"success"|"error")=>void) | null = null
    public static async Run<T>(fnToAdd?:()=>Promise<T>, options?:queueItemOption, callbacks?:queueItemCallbacks):Promise<void>{

        if(fnToAdd) this.queue.push({fn:fnToAdd,...this.options.defaultItemOptions, ...options, ...callbacks})

        if(this.options.running) return

        const fnToRun=this.queue.shift();
        if(!fnToRun) return

        if(this.onRunningChange) this.onRunningChange(fnToRun, "start")
        if(!this.options.onLine && fnToRun.requireOnLine){
            setTimeout(()=>FnQueue.Run(), 5000)
            this.queue.unshift(fnToRun)
            return
        }
        this.options.running=fnToRun
        fnToRun.fn()
        .then((data)=>{
            if(fnToRun.doneFn) fnToRun.doneFn(data)
            if(this.onRunningChange) this.onRunningChange(fnToRun, "success")
        })
        .catch(error=>{
            
            if (fnToRun.retryMaxCount && fnToRun.retryMaxCount>0){
                fnToRun.retryMaxCount--
                this.queue.unshift(fnToRun)
                return
            }
            if(fnToRun.catchFn){
                fnToRun.catchFn(error)
                return
            }
            if(this.onRunningChange) this.onRunningChange(fnToRun, "error")
            throw(error)
        })
        .finally(()=>{
            this.options.running=undefined
            this.Run()
        })

    }
    public static addButNotRun(fnToAdd:queueItem){ this.queue.push(fnToAdd) }
    public static setOnLine(onLine:boolean) { this.options.onLine=onLine }
    public static needWait():boolean{ return this.queue.some(item=>item.blocking) || (this.options.running?.blocking??false) }
    public static setDefaultOptions(options:queueItemOption){
        this.options.defaultItemOptions=options
    }
    public static status():queueStatus{
        return {
            pending:this.queue.length,
            running:this.options.running
        }
    }
    public static setSpy(spy:((item:queueItem, event:"start"|"success"|"error")=>void) | null){
        this.onRunningChange=spy
    }
}