import { AppStorage } from './appStorage'

export abstract class GPSTracker{
    private static trackerID=0

    public static async StartTracker(){
        if(this.trackerID!==0) return
        this.trackerID=navigator.geolocation.watchPosition(
            async (position:GeolocationPosition)=>{
                try{
                    await AppStorage.setValue("CURRENTLATITUD",position.coords.latitude)
                    await AppStorage.setValue("CURRENTLONGITUD",position.coords.longitude)
                }catch(error){
                }
            },
            error=>{},
            {timeout:5000})
    }
    public static async StopTracker(){
        navigator.geolocation.clearWatch(this.trackerID)
        this.trackerID=0
    }
    
}