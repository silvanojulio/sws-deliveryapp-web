import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorkerRegistration from './serviceWorkerRegistration';
import reportWebVitals from './reportWebVitals';
import { AppContextProvider } from 'contexts/appContext'
import { ServiceWorkerUtils } from 'utils/serviceWorker'

const vp = document.getElementById("myViewport")
// eslint-disable-next-line no-restricted-globals
vp && vp.setAttribute("content",`width=412,user-scalable=no,initial-scale=${screen.width/412}`)

ReactDOM.render(
    <AppContextProvider>
      <App />
    </AppContextProvider>,
  document.getElementById('root')
);
Notification.requestPermission()
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA
serviceWorkerRegistration.register({
  onUpdate:ServiceWorkerUtils.updateSW
});

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
