import { VisitaClienteMobile }      from 'entities/clientes'
import { ClienteHojaDeRutaMobile }  from 'entities/hojasDeRuta'
import { AlertaDeClienteMobile }    from 'entities/alertas'
import { ArticuloDeAbonoMobile, ArticuloDeComodatoMobile }    from 'entities/articulos'
import { DispenserDeClienteMobile } from 'entities/dispensers'
import { FacturaMobile }            from 'entities/facturas'
import { OrdenDeTrabajoMobile }     from 'entities/ordenesDeTrabajo'
import { StockEnvasesMobile }       from 'entities/envases'

export type RegistrarVisitaClienteMobileRequest={
    visita:VisitaClienteMobile,
    firmaBase64:string|null,
    hojaDeRutaId:number,
    codigoMovil:string,
    garbage:string,
}
export type RegistrarServicioTecnicoRequest={
    servicioTecnico:ServicioTecnico_R,
    hojaDeRutaId:number,
    identificadorMovil:string,
    latitud:string,
    longitud:string,
    usuario_id:number,
}
export type AgregarClienteAHojaDeRutaRequest={
    hojaDeRutaId:number,
    clienteId:number,
    codigoDeMovil:string,
}
export type ConfirmarClienteAgregadoALaRutaRequest={
    hojaDeRutaId:number ,
    clientesIds:number[] ,
    codigoDeMovil:string ,
}
export type Detalle_R={
    articulo_id:number,
    cantidad:number,
    tipo_id:number,
    mantenimiento_id:number,
    nombreDelItem:string,
}
export type Mantenimiento_R={
    nroDispenser:string,
    sintomaReal:number,
    comentarios:string,
    fechaHora:Date,
    danosAtribuidosAlCliente:boolean,
    latitud:string,
    longitud:string,
    usuario_id:number,
    accionPrincipal_id:number,
    detalles:Detalle_R[],
}
export type DispenserAsociado_R={
    ordenDeTrabajoServer_id?:number,
    nroDispenser:string,
    dispenser_id?:number,
    deCliente:boolean,
    ordenDeTrabajoId?:number,
    mantenimiento:Mantenimiento_R,
}
export type ServicioTecnico_R={
    ordenDeTrabajoServer_id?:number ,
    cliente_id:number ,
    sintoma_id:number ,
    comentarios:string ,
    motivoDeCierreId:number ,
    usuario_id:number ,
    firma:string ,
    firmante:string ,
    nroRemitoFisico?:number ,
    nroRemitoFisicoPrefijo?:number ,
    dispensers:DispenserAsociado_R[] ,
}
export type PrecioEspecialMobile={
    cliente_id:number,
    articulo_id:number,
    precio:number,
}
export type ClientePedidoMobile={
    cliente:ClienteHojaDeRutaMobile,
    alertasDeCliente:AlertaDeClienteMobile[],
    articulosDeAbonos:ArticuloDeAbonoMobile[],
    comodatos:ArticuloDeComodatoMobile[],
    dispensers:DispenserDeClienteMobile[],
    facturas:FacturaMobile[],
    ordenesDeTrabajo:OrdenDeTrabajoMobile[],
    stock:StockEnvasesMobile[],
    preciosEspeciales:PrecioEspecialMobile[],
}
export type SyncNuevosPedidos={
    clientes:ClientePedidoMobile[],
}