import { TiposOperacionesEnvasesMobile } from 'entities/envases'
export type VIEW_Articulos_DTO={
    id:number,
    codigoInterno:string,
    rubro_ids:number ,
    nombreArticulo:string ,
    unidad_ids:number ,
    cantidadXPack:number ,
    tipoEnvase_ids:number ,
    comisionFamilia:number ,
    comisionEmpresa:number ,
    precioMaximo:number ,
    precioMinimo:number ,
    rubroArticulo:string ,
    tipoEnvase:string ,
    unidadArticulo:string ,
    medidaUnidad:number ,
    exportarAMovil:boolean ,
    porcIVAFacA:number|null ,
    factIVAFacA:number|null ,
    porcIVAConsF:number|null ,
    factIVAConsF:number|null ,
    controlableEnPlaya:boolean ,
    eliminado:boolean ,
    tipoDeIva_consumidorFinal:number ,
    tipoDeIva_facturaA:number ,
    pesoKilos:number ,
    rubroFiscal_ids:number ,
    tipoArticulo_ids:number ,
    modelo:string ,
    marca_ids:number ,
    stockActual:number ,
    stockMinimo:number ,
    stockMaximo:number ,
    rubroFiscal:string ,
    tipoArticulo:string ,
    marcaArticulo:string ,
    esFrecuente:boolean  ,
    permiteDecimales:boolean  ,
    noFacturable:boolean|null ,
}
export type ArticuloEntregadoMobile_DTO={
    id:number,
    clienteId:number,
    articulo_id:number,
    cantidadEnvasesRelevados:number,
    cantidadEnvasesPrestados:number,
    cantidadEnvasesDevueltos:number,
    motivoPrestamoDevolucionId:number,
    cantidadEntregada:number,
    descuentoPorCantidad:number,
    descuentoManual:number,
    precioUnitario:number,
}
export type DevolucionArticuloMobile_DTO={
    id:number ,
    articulo_id:number ,
    cantidad:number ,
    fecha:string ,
    cliente_id:number ,
    esCambioDirecto:boolean ,
    motivoDolucion_ids:number ,
    esReutilizable:boolean ,
    tipoOperacion:TiposOperacionesEnvasesMobile ,
}
export type ArticuloDeAbonoMobile={
    cliente_id:number,
    articulo_id:number,
    cantidad:number,
    precioExcendente:number,
}
export type ArticuloDeComodatoMobile={
    cliente_id:number
    articulo_id:number
    cantidad:number
    precio:number
}
export type VIEW_ArticulosXAbonoDTO={
    id:number,
    articulo_id:number,
    nombreArticulo:string,
    cantidad:number,
    precioExcendente:number,
    abono_id:number,
    cliente_id:number,
    cicloActualInicio:Date|null,
    cicloActualFin:Date|null,
    cantidadAbonos:number,
    fechaVigenciaHasta:Date|null,
    fechaVigencia:Date,
    leyendaFacturaAbono:string,
    precioAbono:number,

    //campos adicionales
    CantidadConsumida?:number,
    CantidadDisponibleSinCosto:number,

}