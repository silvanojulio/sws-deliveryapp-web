import { RolesDeUsuario } from 'entities/roles'

export type User_DTO={
    usuario_id:number,
    nombreApellido:string,
    userName:string,
    email:string,
    telefono:string,
    dni:string,
    usuarioActivo:boolean,
    cliente_id?:number,
    esPrimerAcceso:boolean,
    centroDistribucion_id:number|null,
    rolesDeUsuario:RolesDeUsuario,
}