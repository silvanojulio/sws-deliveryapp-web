export type ComprobanteEntregado_DTO={
    id:number,
    cliente_id:number,
    tipoComprobanteInterno_id:number,
    idEntidad:number,
}

export type ComprobanteFisicoUtilizadoMobile_DTO={
    id:number,
    cliente_id:number,
    tipoDeComprobanteFisico_ids:number,
    nroPrefijo:number,
    nroComprobante:number,
}