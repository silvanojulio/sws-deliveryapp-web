import { DispenserAsociadoAOrdenDeTrabajoMobile } from 'entities/dispensers'
export type OrdenDeTrabajoMobile={
    ordenDeTrabajoServer_id:number,
    cliente_id:number,
    sintoma_id:number,
    sintoma:string,
    prioridad:string,
    comentarios:string,
    cantDispensers:string,
    sectorUbicacion:string,
    responsableEnCliente:string,
    telefonoResponsable:string,
    franjaHoraria:string,

    dispensers:DispenserAsociadoAOrdenDeTrabajoMobile[],
}