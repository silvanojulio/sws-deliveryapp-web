export type VIEW_Moviles_DTO={
    id:number,
    descripcion:string,
    numeroTelefono:string,
    activo:boolean,
    ultimaUbicacionLatitud:string,
    ultimaUbicacionLongitud:string,
    ultimaUbicacionFechaHora:Date|null,
    ultimaUbicacionEstado_ids:number|null,
    estadoMovil:string,
    repartidor_id:number|null,
    centroDistribucion_id:number,
    centroDeDistribucion:string,
}