import { AppData_ArticulosDeLista,
    AppData_Dispensers }             from 'entities/app'


export type ArticulosXControlDePlaya_DTO={
    id:number
    controlDePlaya_id:number
    articulo_id:number
    cantidadVacios:number
    cantidadLlenos:number
    cantidadFallados:number
}
export type ArticuloControlDePlaya={
    articulo_id:number
    dispenser_id:number
    cantidadVaciosPack:number
    cantidadLlenosPack:number
    cantidadFalladosPack:number
    cantidadVaciosUni:number
    cantidadLlenosUni:number
    cantidadFalladosUni:number
    articulo:AppData_ArticulosDeLista|null
    dispenser:AppData_Dispensers|null
}

export type ControlDePlaya={
    kilometros:number,
    tipoEventoId:number,
    hojaDeRutaId:number,
    usuarioControlaId:number,
    articulos:ArticuloControlDePlaya[],
    dispensersIds:number[]
}