export type VIEW_ServiciosTecnicos_DTO={
    id:number,
    cantidadDispensers:number,
    sectorUbicacion:string,
    responsableEnCliente:string,
    telefonoResponsable:string,
    comentariosDeCierre:string,
    usuarioTecnicoId:number | null,
    fechaVisitaPlanificada:Date | null,
    cumplido:boolean,
    fechaRealVisita:Date | null,
    esSanitizacionPlanificada:boolean,
    precio:number | null,
    RazonesDeCierre_ids:number | null,
    orden:number | null,
    usuarioTecnico:string,
    incidente_id:number | null,
    cliente_id:number | null,
    nombreCliente:string,
    domicilioCompleto:string,
    altitud:string,
    longitud:string,
    clientePadre:number | null,
    sintoma:string,
    dispenserNumeroInterno:string,
    dispenserNumeroSerie:string,
    marcaDispenser:string,
    dispenser_id:number | null,
    sintoma_ids:number,
    fechaCreacion:Date,
    prioridad_ids:number,
    estadoServicioTecnico_ids:number,
    comentarios:string,
    estadoServicioTecnico:string,
    franjaHoraria_ids:number|null,
    franjaHoraria:string,
    prioridad:string,
    reparto_id:number | null,
    nombreReparto:string,
    archivoFirmaComprobanteDeServicio:string,
    firmaRemito_id:number | null,
    nroRemitoFisicoPrefijo:number | null,
    nroRemitoFisicoComprobante:number | null,
    centroDistribucion_id:number,
    centroDeDistribucion:string,
}
export type VIEW_MantenimientosDispensers_DTO={
    id:number 
    dispenser_id:number 
    dispenserAsociadoAServicioTecnico_id:number|null 
    estadoDispenserActual_ids:number 
    condicionActual_ids:number 
    ubicacionActual_ids:number 
    comentarios:string 
    tecnico_id:number 
    fechaHora:Date 
    eliminado:boolean 
    estadoDispenserActual:string 
    condicionActual:string 
    ubicacionActual:string 
    trabajos:string 
    condicionAnterior_ids:number|null 
    estadoDispenserAnterior_ids:number|null 
    clienteInstalado_id:number|null 
    clienteFacturado_id:number|null 
    esCobrable:boolean 
    cliente_id:number|null 
    liquidado:boolean 
    condicionAnterior:string 
    estadoAnterior:string 
    tecnico:string 
    clienteInstalado:string 
    accionPrincipal_ids:number 
    leyendaParaFactura:string 
    totalPrecioEnFactura:number|null 
    cobrarAlCliente:boolean|null 
    mostrarSoloLeyendaEnFactura:boolean|null 
    sintomaReal_ids:number 
    accionPrincipal:string 
    sintomaReal:string 
    esRecambio:boolean 
    final:Date|null 
    inicio:Date|null 
    ultimoClienteInstalado:string 
    numeroInterno:string 
    costoActividad:number 
    costoRepuestos:number 
    costoTotal:number|null 
    detalles:VIEW_DetallesDeMantenimientos_DTO[] 
    mediciones:MedicionesDeMantenimiento_DTO[]
}

export type VIEW_DetallesDeMantenimientos_DTO={
    id:number   
    mantenimiento_id:number   
    articulo_id:number   
    cantidad:number   
    costo:number   
    precioAlCliente:number   
    cobrarAlCliente:boolean   
    nombreArticulo:string   
    codigoInterno:string   
    modelo:string   
    tipoArticulo:string   
    tipoArticulo_ids:number   
    servicioTecnico_id:number|null   
    dispenser_id:number   
    numeroSerie:string   
    numeroInterno:string   
    marcaDispenser:string   
    tipoDispenser:string   
}

export type MedicionesDeMantenimiento_DTO={
    id:number
    descripcionMedicion:string
    valorMedicion:string
    observaciones:string
}