export enum TiposOperacionesEnvasesMobile{
    Devolucion, 
    Prestamo
}
export type StockEnvasesMobile={
    clienteId:number,
    articulo_id:number,
    nombreArticulo:string,
    stockActual:number,
    consumoPromedio:number,
    cantidadPermitida:number,
    cantidadARecuperar:number,
}