export type ComandoMobile={
    _id:string,
    comando:string,
    mobileId:number,
    ejecutado:boolean,
    fechaHoraEjecutado?:Date,
    fechaHoraCreado:Date,
    extras1:string,
    extras2:string,
    extras3:string,
    valor1:number,
    valor2:number,
    valor3:number,
}