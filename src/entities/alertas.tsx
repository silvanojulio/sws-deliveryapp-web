
export type AlertaDeClienteMobile={
    id:number,
    cliente_id:number,
    tipoDeAlerta_ids:number,
    tipoDeAlerta:string,
    comentarios:string,
    esBloqueante:boolean,
}