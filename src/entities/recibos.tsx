export type ReciboMobile_DTO={
    id:number,
    clienteId:number,
    clienteFacturaId:number,
    cerrado:boolean,
    fechaHora:string,
    comprobanteFisicoPrefijo?:number,
    comprobanteFisicoNro?:number,

    items:ItemDeReciboMobile_DTO[]

}
export type ItemDeReciboMobile_DTO={
    id:number,
    reciboId:number,
    importe:number,
    formaDePagoId:number,
    chequeId?:number,
    tarjetaDeCreditoId?:number,
    tarjetaDeDebitoId?:number,
    retencionId?:number,

    cheque:ChequeMobile_DTO,
    tarjetaDeCredito:TarjetaDeCreditoMobile_DTO,
    tarjetaDeDebito:TarjetaDeDebitoMobile_DTO,
    retencion:RetencionesMobile_DTO,
}
export type ChequeMobile_DTO={
    id:number,
    librador:string,
    nroCheque:string,
    banco_id:number,
    importe:number,
    fechaCobro:string,
    nroSucursalBanco:string,
    
}

export type TarjetaDeCreditoMobile_DTO={
    id:number,
    lote:string,
    cupon:string,
    codigoAutorizacion:string,
    banco_id:number,
    marcaTarjeta_id:number,
    cuotas:number,
    importe:number,
}

export type TarjetaDeDebitoMobile_DTO={
    id:number,
    lote:string,
    cupon:string,
    codigoAutorizacion:string,
    banco_id:number,
    importe:number,
}

export type RetencionesMobile_DTO={
    id:number,
    tipoRetencionId:number,
    descripcion:string,
    banco_id:number,
    importe:number,
    fecha:string,
}