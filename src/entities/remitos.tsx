export type FirmaRemitoMobile_DTO={
    cliente_id:number,
    firmaRemito?:string,
    firmante:string,
    fechaHora:String,
}