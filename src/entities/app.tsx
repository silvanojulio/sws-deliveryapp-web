export type AppData={
    AlertasDeCliente:AppData_AlertasDeCliente[]
    ArticulosDeAbonos:AppData_ArticulosDeAbonos[]
    ArticulosDeComodatos:AppData_ArticulosDeComodatos[]
    ArticulosDeLista:AppData_ArticulosDeLista[]
    ArticulosEntregados:AppData_ArticulosEntregados[]
    Cheques:AppData_Cheques[]
    Clientes:AppData_Clientes[]
    ComprobantesEntregados:AppData_ComprobantesEntregados[]
    ComprobantesFisicos:AppData_ComprobantesFisicos[]
    ComprobantesFisicosUtilizados:AppData_ComprobantesFisicosUtilizados[]
    Configuraciones:AppData_Configuraciones[]
    Contactos:AppData_Contactos[]
    DeclaracionesEfectivo:AppData_DeclaracionesEfectivo[]
    DescuentosPorCantidad:AppData_DescuentosPorCantidad[]
    DevolucionesArticulos:AppData_DevolucionesArticulos[]
    Dispensers:AppData_Dispensers[]
    DispensersAsociados:AppData_DispensersAsociados[]
    EnvasesDisponibles:AppData_EnvasesDisponibles[]
    Facturas:AppData_Facturas[]
    FirmantesDisponibles:AppData_FirmantesDisponibles[]
    FirmasRemitos:AppData_FirmasRemitos[]
    GastosDeReparto:AppData_GastosDeReparto[]
    HojasDeRuta:AppData_HojaDeRuta[]
    ItemsDeRecibos:AppData_ItemsDeRecibos[]
    LimitesDeClientes:AppData_LimitesDeClientes[]
    ListasDePrecios:AppData_ListasDePrecios[]
    Logs:AppData_Logs[]
    MantenimientosDeDispensers:AppData_MantenimientosDeDispensers[]
    NumerosDeComprobantesDisponibles:AppData_NumerosDeComprobantesDisponibles[]
    OrdenesDeTrabajo:AppData_OrdenesDeTrabajo[]
    PreciosEspeciales:AppData_PreciosEspeciales[]
    PrestamosPendientes:AppData_PrestamosPendientes[]
    ProximasVisitas:AppData_ProximasVisitas[]
    Recibos:AppData_Recibos[]
    RepuestosActividades:AppData_RepuestosActividades[]
    ResumenClientes:AppData_ResumenClientes[]
    Retenciones:AppData_Retenciones[]
    StocksDeClientes:AppData_StocksDeClientes[]
    TarjetasDeCredito:AppData_TarjetasDeCredito[]
    TarjetasDeDebito:AppData_TarjetasDeDebito[]
    UsuariosApp:AppData_UsuariosApp[]
    ValoresSatelites:AppData_ValoresSatelites[]
}
export type AppData_AlertasDeCliente={
    id:number
    cliente_id:number
    tipoDeAlerta_ids:number
    tipoDeAlerta:string
    comentarios:string
    esBloqueante:boolean
}
export type AppData_ArticulosDeAbonos={
    id?:number
    articulo_id:number
    cliente_id:number
    vigenciaDesde?:string
    vigenciaHasta?:string
    cantidad:number
    precioExcendente:number    
}
export type AppData_ArticulosDeComodatos={
    id?:number
    articulo_id:number
    cliente_id:number
    cantidad:number
    precio:number
}
export type AppData_ArticulosDeLista={
    id:number
    codigoInterno:string
    nombreArticulo:string
    precio:number
    tipoDeEnvase_ids:number
    controlableEnPlaya:boolean
    cantidadPorPack:number
    tipoArticulo_ids:number
    permiteDecimales:boolean
}
export type AppData_ArticulosEntregados={
    id?:number
    nombreDelArticulo:string
    codigoArticulo:string
    articulo_id:number
    cantidadEntregada:number
    cantidadEnvasesRelevados:number
    cantidadEnvasesPrestados:number
    cantidadEnvasesDevueltos:number
    motivoPrestamoDevolucionId:number
    envasesEnCliente:number
    envasesPermitidos:number
    envasesARecuperar:number
    precioUnitario:number
    esRetornable:boolean
    cantidadDisponibles:number
    descuentoPorCantidad:number
    descuentoManual:number
    subtotal:number
    esPredeterminado:boolean
    orden:number
    clienteId:number
}
export type AppData_Cheques={
    id?:number
    librador:string
    nroCheque:string
    banco_id:number
    importe:number
    fechaCobro:string
    nroSucursalBanco:string
}
export type AppData_Clientes={
    cliente_id:number
    nombreCliente:string
    tipoCliente_ids:number
    estadoCliente_ids:number
    nombreProvincia:string
    nombreCiudad:string
    nombreBarrio:string
    domicilioCompleto:string
    torre:string
    piso:string
    depto:string
    manzana:string
    lote:string
    numeroPuerta:string
    nombreCalle:string
    actividadCliente:string
    fechaIngreso:string
    altitud:string
    longitud:string
    fechaUtlimaEntrega:string
    fechaUltimoCobroFactura:string
    fechaUltimaEnvases:string
    fechaUltimaDevoluciones:string
    orden:number
    esVentaExtra:boolean
    saldoFacturacion:number
    saldoConsumos:number
    permiteVender:boolean
    permiteCobrar:boolean
    permitePrestarEnvases:boolean
    enviarSMS:boolean
    esPedido:boolean
    comunicacion1:string
    comunicacion2:string
    comunicacion3:string
    comunicacion4:string
    creditoDisponible:number
    visitado:boolean
    ausente:boolean
    ventaEntrega:boolean
    cobroConsumo:boolean
    cobroFactura:boolean
    devolucionEnvases:boolean
    prestamoEnvases:boolean
    devolucionArticulo:boolean
    visita_altitud:string
    visita_longitud:string
    visita_fechaHora:string
    pendiente:boolean
    descargado:boolean
    tieneAlertas:boolean
    haVistoAlertas:boolean
    saldoNotasDeCredito:number
    requiereRemito:boolean
    tipoDeVisitaId:number
    relevarCoordenadas:boolean
    relevamientoCoordenadas_latitud:string
    relevamientoCoordenadas_longitud:string
    listaDePrecioId:number | null
    clienteFactura_id:number
    esSync:boolean
    esRepaso:boolean
    fechaHoraTransferido:string | null
    cerrado:boolean
    desbloqueado:boolean
    totalEntregadoActual:number
    totalCobradoActual:number
    formaPagoHabitual?:string | null
}
export type AppData_ComprobantesEntregados={
    id:number
    tipoComprobanteInterno_id:number
    idEntidad:number
    cliente_id:number
}
export type AppData_ComprobantesFisicos={
    idRango:number
    tipoDeComprobante:string
    idTipoComprobante:number
    prefijo:number
}
export type AppData_ComprobantesFisicosUtilizados={
    id?:number
    cliente_id:number
    tipoDeComprobanteFisico_ids:number
    nroPrefijo:number
    nroComprobante:number
}
export type AppData_Configuraciones={
    id:number
    clave:string
    valor:string
}
export type AppData_Contactos={
    id:number
    cliente_id:number
    nombreContacto:string
    tipoContacto:string
    telefono:string
    celular:string
    mail:string
}
export type AppData_Declaraciones={
    id:number
    tipo:number
    monto:number
}
export type AppData_DeclaracionesEfectivo={
    id:number
    valorNominal:number
    esMoneda:boolean
    valorDisplay:string
    cantidad:number
}
export type AppData_DescuentosPorCantidad={
    id:number
    listaDePrecioId:number
    articuloId:number
    cantidad:number
    descuento:number
}
export type AppData_DevolucionesArticulos={
    id?:number
    articulo_id:number
    cantidad:number
    fecha:string
    cliente_id:number
    esCambioDirecto:boolean
    motivoDevolucion_ids:number
    esReutilizable:boolean
    motivoDevolucion:string
    nombreArticulo:string
}
export type AppData_Dispensers={
    id?:number
    nroDispenser:string
    marca:string
    tipo:string
    color:string
    cliente_id:number|null
}
export type AppData_DispensersAsociados={
    id?:number
    nroDispenser:string
    dispenser_id:number
    deCliente:boolean
    ordenDeTrabajoId:number
    mantemientoCargado:boolean
    OrdenDeTrabajoServer_id:number
}
export type AppData_EnvasesDisponibles={
    id:number
    codigo:string
    articuloContenidoId:number
    articuloContenidoNombre:string
    cantidadDeCarga:number
    unidadDeCarga:string
    enCliente:boolean
    clienteEntregadoId:number
}
export type AppData_Facturas={
    id?:number
    cliente_id:number
    nroFactura:string
    fechaFactura:string
    tipoFactura:string
    montoFacturaTotal:number
    fechaVencimiento1:string
    fechaVencimiento2:string
    fechaVencimiento3:string
    cobradoActual:number
    saldoActual:number
    entregada:boolean
    imputado:number
}
export type AppData_FirmantesDisponibles={
    id:number
    nombreFirmante:string
    cliente_id:number
}
export type AppData_FirmasRemitos={
    id?:number
    cliente_id:number
    firmaRemito:string
    firmante:string
    fechaHora:string
    esServicioTecnico:number
}
export type AppData_GastosDeReparto={
    id:number
    tipoGasto_id:number
    descripcion:string
    montoGasto:number
}
export type AppData_HojaDeRuta={
    id:number
    usuario:string
    password:string
    nombreReparto:string
    fechaReparto:string
    montoDeclarado:number
    cerrada:boolean
    codigoReparto:string
    usuarioRepartidor_id:number
    centroDeDistribucionId:number
    centroDeDistribucion:string
    reparto_id:number
}
export type AppData_ItemsDeRecibos={
    id?:number
    reciboId:number
    importe:number
    formaDePagoId:number
    descripcion:string | null
    chequeId:number | null
    tarjetaDeCreditoId:number | null
    tarjetaDeDebitoId:number | null
    retencionId:number | null
}
export type AppData_LimitesDeClientes={
    id:number
    clienteId:number
    validaLimiteFacturas:boolean
    validaLimiteDeSaldo:boolean
    limiteDeFacturas:number
    limiteDeSaldo:number
}
export type AppData_ListasDePrecios={
    id:number
    listaId:number
    articuloId:number
    precio:number
}
export type AppData_Logs={
    id:number
    mensaje:string
    funcionalidad:string
    entidad:string
    idEntidad:number
    fechaHora:string
}
export type AppData_MantenimientosDeDispensers={
    id:number
    nroDispenser:string
    sintomaReal:number
    comentarios:string
    fechaHora:string
    danosAtribuidosAlCliente:boolean
    latitud:string
    longitud:string
    usuario_id:number
    accionPrincipal_ids:number
}
export type AppData_NumerosDeComprobantesDisponibles={
    id:number
    idRango:number
    nroComprobante:number
    utilizado:boolean
}
export type AppData_OrdenesDeTrabajo={
    id?:number
    ordenDeTrabajoServer_id:number
    cliente_id:number
    sintoma_id:number
    sintoma:string
    prioridad:string
    comentarios:string
    cantDispensers:string
    sectorUbicacion:string
    responsableEnCliente:string
    telefonoResponsable:string
    motivoDeCierreId?:number
    franjaHoraria:string
    fechaHoraTransferido?:string
    usuarioCierra_id?:number
    nroRemitoFisico?:number
    nroRemitoFisicoPrefijo?:number
}
export type AppData_PreciosEspeciales={
    id?:number
    articuloId:number
    clienteId:number
    precio:number
}
export type AppData_PrestamosPendientes={
    id:number
    articulo_id:number
    nombreArticulo:string
    cantidadAdeudada:number
    fechaPrestamo:string
    fechaVencimiento:string
    cliente_id:number
}
export type AppData_ProximasVisitas={
    clienteId:number
    fechaVisita1:string
    fechaVisita2:string
    fechaVisita3:string
}
export type AppData_Recibos={
    id?:number
    nombreCliente:string
    clienteId:number
    fechaHora:string
    cerrado:boolean
    comprobanteFisicoPrefijo:number | null
    comprobanteFisicoNro:number | null
    clienteFacturaId:number
}
export type AppData_RepuestosActividades={
    id:number
    articulo_id:number
    cantidad:number
    tipo_id:number
    mantenimiento_id:number
    nombreDelItem:string
}
export type AppData_ResumenClientes={
    clienteId:number
    nombreCliente:string
    domicilioCompleto:string
    tipoCliente:string
    reparto:string
    repartoId:number
    latitud:string
    longitud:string
    usuario?:string
    clave?:string
}
export type AppData_Retenciones={
    id?:number
    reciboId:number
    tipoRetencionId:number
    descripcion:string
    importe:number
    fecha:string
}
export type AppData_StocksDeClientes={
    id?:number
    articuloId:number
    nombreArticulo:string
    stockActual:number
    consumoPromedio:number
    cantidadPermitida:number
    cantidadARecuperar:number
    clienteId:number
}
export type AppData_TarjetasDeCredito={
    id?:number
    reciboId:number
    lote:string
    cupon:string
    codigoAutorizacion:string
    banco_id:number
    marcaTarjeta_id:number
    cuotas:number
    importe:number
}
export type AppData_TarjetasDeDebito={
    id?:number
    reciboId:number
    lote:string
    cupon:string
    codigoAutorizacion:string
    banco_id:number
    importe:number
}
export type AppData_UsuariosApp={
    usuario_id:number
    nombreApellido:string
    username:string
    password:string
    esSupervisor:boolean
}
export type AppData_ValoresSatelites={
    id:number
    tabla_id:number
    valor_id:number
    valor_texto:string
}