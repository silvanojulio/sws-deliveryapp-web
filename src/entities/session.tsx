import { User_DTO } from 'entities/usuarios'
import { OpcionesMenuXRol } from 'entities/menuOptions'
import { AppData_UsuariosApp } from 'entities/app'

export type CurrentSession_DTO={
    tokenValido:string,
    inicioToken:Date,
    vencimientoToken:Date,
    User:User_DTO,
    PermisosActuales:string[],
    OpcionesMenu:OpcionesMenuXRol,
    CentroDistribucionId:number|null,
    SessionActiva:boolean
}
export type CurrentLocalSession={
    usuario:string
    expire:number
    userApp:AppData_UsuariosApp
    token?:string
}
export type LoginResponse={
    session_id:string,
    tokenValido:string,
    usuario:User_DTO
}