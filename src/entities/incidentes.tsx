export type Incidentes_Totales_DTO={
    totalIncidentes:number,
    cantidadIncidentes:number,
    cantidadIncidentesGrupales:number,
    cantidadIncidentesSeguidos:number,

    IncidentesGrupales:Incidentes_Grupos_DTO[],

    IncidentesSeguidos:VIEW_Incidente_SeguimientoDTO[],
}
export type Incidentes_Grupos_DTO={
    grupoResponsable:string ,
    cantidad:number ,
}
export type VIEW_Incidente_SeguimientoDTO={
    id:number,
    fechaHoraRegistro:string,
    usuarioRegistraId:number,
    cliente_id:number | null,
    cliente:string,
    titulo:string,
    descripcion:string,
    tipoIncidente_ids:number,
    severidad_ids:number,
    pedido_id:number | null,
    usuarioResponsable_id:number,
    fechaCierreEstimado:string | null,
    fechaCierreReal:string | null,
    estadoIncidente_ids:number,
    eliminado:boolean,
    usuarioSeguidor_id:number,
    notificar:boolean,

}
export type VIEW_Incidente_DTO={
    id:number,
    fechaHoraRegistro:string,
    usuarioRegistraId:number,
    cliente_id:number | null,
    repartoCliente:string,
    titulo:string,
    descripcion:string,
    tipoIncidente_ids:number,
    severidad_ids:number,
    edido_id:number | null,
    usuarioResponsable_id:number,
    echaCierreEstimado:string | null,
    fechaCierreReal:string | null,
    estadoIncidente_ids:number,
    eliminado:boolean,
    usuarioRegistra:string,
    usuarioResponsable:string,
    repartoRealAtencion:string,
    fechaRealAtencion:string | null,
    estadoPedido:string,
    fechaPlanificadaAtencion:string | null,
    repartoPlanifiicado:string,
    cliente:string,
    severidad:string,
    tipoIncidente:string,
    estadoIncidente:string,
    servicioTecnico_id:number | null,
    subTipoIncidente_ids:number,
    clientePadre_id:number | null,
    subtipoIncidente:string,
    esParaAprobacion:boolean,
    solTomaCoordenadas_id:number | null,
    incidenteRelacionado:number | null,
    domicilioResumen:string,
    fechaUltimaEnvases:string | null,
    fechaUltimaDevoluciones:string | null,
    fechaUltimoCobroFactura:string | null,
    fechaUtlimaEntrega:string | null,
    grupoResponsable:string,
    grupoResponsable_ids:number | null,
    fechaCierreEstimadoReplanificada:string | null,
    centroDistribucion_id:number,
    centroDeDistribucion:string,
    statusColor:string,
    usuariosSeguimiento:VIEW_UsuariosSeguimientoXIncidentesDTO[],
    seguidor:boolean,
    esResponsable:boolean,
    notificar:boolean,
    usuarioCierre_id:number,
    usuarioCierre:string,
    repartoCierre_id:number,
    repartoCierre:string,
   
}
export type VIEW_UsuariosSeguimientoXIncidentesDTO={
    id:number,
    usuario_id:number,
    incidente_id:number,
    nombreApellido:string,

}