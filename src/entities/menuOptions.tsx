export type OpcionMenuXRol={
    id: number,
    opcion_id: number,
    nivel: number,
    displayText: string,
    codigoOpcion: string,
    href: string,
    iconoCss: string,
    parent_menu_codigo: string,
    rol_id: number,
    orden: number,
    visible: boolean,

}
export type OpcionesMenuXRol=OpcionMenuXRol[]