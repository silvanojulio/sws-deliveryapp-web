export type RolDeUsuario={
    nombreRol:string,
    descripcionRol:string,
    rol_id:number
}
export type RolesDeUsuario=RolDeUsuario[]