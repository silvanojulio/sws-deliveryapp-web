import { VIEW_MantenimientosDispensers_DTO } from 'entities/serviciosTecnicos'

export type DispenserDeClienteMobile={
    dispenserId:number,
    nroDispenser:string,
    marca:string,
    tipo:string,
    color:string,
    cliente_id:number,
}
export type DispenserAsociadoAOrdenDeTrabajoMobile={
    nroDispenser:string,
    dispenser_id:number,
    deCliente:boolean,
    ordenDeTrabajoServer_id:number,
}
export type VIEW_Dispensers_DTO={
    id:number 
    numeroSerie:string 
    numeroInterno:string 
    tipoDispenser_ids:number 
    tipoDispenser:string 
    codigoMarca:number 
    nombreCliente:string|null 
    clienteActual_id:number|null 
    estadoDispenserActual_ids:number 
    estadoDispenser:string 
    tipoContratoActual_ids:number 
    tipoContrato:string 
    abono_id:number|null 
    clienteLocacion_id:number|null 
    clienteFactura_id:number|null 
    marcaDispenser:string 
    ubicacionActual:string 
    ubicacionActual_ids:number 
    condicionActual:string 
    condicionActual_ids:number 
    fechaDeNuevo:Date|null
    fechaDeCompra:Date|null
    fechaUltimaReparacion:Date|null
    fechaUltimaRestauracion:Date|null
    fechaInstalacionCliente:Date|null
    fechaUltimaSanitización:Date|null
    mesesEntreSanitizaciones:number 
    sanitizacionPorCliente:boolean 
    diasEntreSanitizacionesPorCliente:number|null 
    fechaProximaSanitizacion:Date|null
    color:string 
    color_ids:number 
    fechaUltimoCambioDeEstado:Date|null
    centroDistribucion_id:number 
    centroDeDistribucion:string 
    mantenimientos:VIEW_MantenimientosDispensers_DTO[]|null

}