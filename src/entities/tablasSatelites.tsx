export type ValoresTablasSateliteDTO={
    id:number
    valor_ids:number
    valorTexto:string
    codigoExterno:number|null
    codigo:string
    descripcion:string
    tablaSatelites_id?:number
}