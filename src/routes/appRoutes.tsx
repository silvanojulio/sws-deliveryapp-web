import { FC, Fragment }        from 'react'
import { 
    Switch, 
    Route, 
    Redirect, 
    HashRouter 
}                    from 'react-router-dom'
import * as Pages    from 'pages'
import { useAppContext }            from 'hooks/app'

const AppRoutes:FC = ()=>{
    const {user} = useAppContext()
    return (
        <Fragment>            
            <HashRouter >
                <Switch>
                    {!user && (<Route path="/login" component={Pages.LoginPage} />)}
                    {!user && (<Redirect to="/login" />)}
                    <Route path="/home" component={Pages.HomePage} />
                    <Route path="/hojaDeRuta" component={Pages.HojaDeRutaPage} />
                    {user && (<Redirect to="/home" />)}
                    
                </Switch>
            </HashRouter>
        
        </Fragment>
    )
}
export default AppRoutes