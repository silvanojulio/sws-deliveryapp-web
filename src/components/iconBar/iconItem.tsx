import { FC }           from 'react'
import { IconType }     from 'react-icons'
import { NavItem }      from 'react-bootstrap'
import { Form }         from 'react-bootstrap'


type IconItemProps={
    label:string
    onClick:(option:string)=>void
    selected:boolean
    Icon:IconType
}
export const IconItem:FC<IconItemProps> = (props)=>{
    const { onClick }   = props
    const { selected }  = props
    const { label }     = props
    const { Icon }      = props
    
    return (    
        <NavItem onClick={()=>onClick(label)} className={selected?"selected":""}>
            <Icon/><Form.Label>{label}</Form.Label>
        </NavItem>
    )
}