import './iconBar.scss'

export { IconBar }          from './iconBar'
export type { IconOption }  from './iconBar'