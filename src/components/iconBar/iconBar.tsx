import { FC, useState } from 'react'
import { Form }         from 'react-bootstrap'
import { IconType }     from 'react-icons'

import { IconItem }     from './iconItem'

export type IconOption={
    label:string
    Icon:IconType
}
type IconBarProps={
    options:IconOption[]
    onChange:(optionLabel:string)=>void
}
export const IconBar:FC<IconBarProps> = ({options, onChange})=>{
    const [ selected, setSelected ] = useState<string>(options[0]?.label??"")
    return (
        <Form.Row className="iconBar">
            {options.map((option, index)=>(
                <IconItem 
                    key={index}
                    Icon={option.Icon}
                    label={option.label}
                    onClick={(option:string)=>{
                        onChange(option)
                        setSelected(option)
                    }}
                    selected={selected===option.label}
                />
            ))}
        </Form.Row>
    )
}