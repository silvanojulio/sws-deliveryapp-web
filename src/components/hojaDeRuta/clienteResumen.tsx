import { FC, useCallback, useState }    from 'react'
import { Card, Form, Col, Spinner }     from 'react-bootstrap'
 
import { SyncServices }             from 'services'
import { AppStorage, Sync }         from 'utils'
import { useHojaDeRutaContext }     from 'hooks'
import { HojaDeRutaFiltroResult }   from 'types'

type ClienteResumenProps=HojaDeRutaFiltroResult & {
    onUpdateNeeded:()=>Promise<void>,
    style?:React.CSSProperties
}

export const ClienteResumen:FC<ClienteResumenProps> = props=>{
    const { hojaDeRuta } = useHojaDeRutaContext()
    const [ agregando, setAgregando ] = useState<boolean>(false)
    const agregarCliente=useCallback(async()=>{
        if(!hojaDeRuta || agregando || !props.clienteId) return
        const hojaDeRutaId = hojaDeRuta.id
        const codigoDeMovil = await AppStorage.getString("MOVILID")
        if(!codigoDeMovil) return
        try{
            setAgregando(true)
            await SyncServices.AgregarClienteAHojaDeRuta({clienteId:props.clienteId,hojaDeRutaId,codigoDeMovil})
            await Sync.Sync()
            props.onUpdateNeeded()
        }catch(error){
            setAgregando(false)
        }
    },[hojaDeRuta, props, agregando])
    return (
        <Card className="cliente-resumen" style={props.style}>
            <Form.Row>
                <Col xs={3}>Nro cliente:</Col>
                <Col xs={3}>{props.clienteId}</Col>
                <Col xs={6}>{props.reparto}</Col>
                <Col xs={12}>{props.nombreCliente}</Col>
                <Col xs={12}>{props.domicilioCompleto}</Col>
                <Col xs={3}>Tipo:</Col>
                <Col xs={3}>{props.tipoCliente}</Col>
                <Col xs={6} onClick={agregarCliente}>
                    {agregando?(<Spinner animation="border"></Spinner>):"+ Agregar"}
                </Col>
            </Form.Row>
        </Card>
    )
}