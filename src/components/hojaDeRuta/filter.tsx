import React, {FC} from 'react'
import { NavItem } from 'react-bootstrap'
import { Form } from 'react-bootstrap'
import { IconType } from 'react-icons'
import { HojaDeRutaFiltroOptions } from 'types'

type FilterProps={
    option:HojaDeRutaFiltroOptions, 
    label:string, 
    selected:boolean,
    Icon:IconType,
    onClick:(option:HojaDeRutaFiltroOptions)=>void
}
export const Filter:FC<FilterProps> = (props)=>{
    const { option, label, selected, Icon, onClick } = props
    return (
        <NavItem onClick={()=>onClick(option)} className={selected?"selected":""}>
            <Icon/><Form.Label>{label}</Form.Label>
        </NavItem>
    )
}