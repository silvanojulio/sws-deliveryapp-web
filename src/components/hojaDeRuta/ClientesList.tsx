import { forwardRef, useImperativeHandle }  from 'react'
import { useEffect, useState }              from 'react'
import { useCallback, useRef }              from 'react'
import { Container }                        from 'react-bootstrap'
import { 
    List,
    AutoSizer,
    CellMeasurer,
    CellMeasurerCache
}                                           from 'react-virtualized'

import { ClienteHojaDeRuta }        from './cliente' 
import { HojaDeRutaFiltroOptions }  from 'types'
import { SyncResultados }           from 'types'
import { HojaDeRutaFiltroResult }   from 'types'
import { useHojaDeRutaContext }     from 'hooks'
import { useAppContext }            from 'hooks'
import * as DB                      from 'services/DB'
import { Sync }                     from 'utils'


type ClientesListProps={
    pageSize?:number
    onStatsChanged?:(repaso:number, pendientes:number)=>void
}
export interface IClientesList{
    buscar:(texto:string, tipo:HojaDeRutaFiltroOptions)=>void
}
export const ClientesList=forwardRef<IClientesList,ClientesListProps>((props,ref)=>{
    const cacheRowHeight            = useRef(new CellMeasurerCache({
        fixedWidth:true,
        defaultWidth:412,
        defaultHeight:130
    }))
    const { hojaDeRuta }            = useHojaDeRutaContext()
    const { ConfirmRef }            = useAppContext()
    const { onStatsChanged }        = props
    const [ listado, setListado ]   = useState<HojaDeRutaFiltroResult[]>([])
    const [ showing, setShowing ]   = useState<HojaDeRutaFiltroResult[]>([])
    const [ tab,     setTab     ]   = useState<HojaDeRutaFiltroOptions>("pendientes")


    const show=useCallback((item:HojaDeRutaFiltroResult,text:string,filtro:HojaDeRutaFiltroOptions)=>{        
        switch(filtro){
            case "all": return item.store==="Clientes";
            case "pendientes": return item.store==="Clientes" && item.visitado===false && item.esRepaso===false;
            case "pedidos": return item.store==="Clientes" && item.esPedido===true;
            case "repasos": return item.store==="Clientes" && item.esRepaso===true;
            case "srvtecs": return item.store==="Clientes" && item.tipoDeVisitaId===4;
            case "nosyncs": return item.store==="Clientes" && item.visitado===true && !item.fechaHoraTransferido;
            case "search" : 
                return (item.domicilioCompleto?.toLowerCase().indexOf(text.toLowerCase())??-1)>=0 || (item.nombreCliente?.toLowerCase().indexOf(text.toLowerCase())??-1)>=0
            default: return false;
        }
    },[])
    
    const buscar=useCallback((text:string,filtro:HojaDeRutaFiltroOptions=tab)=>{
        if(filtro!==tab)setTab(filtro)
        setShowing(listado.filter(item=>show(item,text,filtro)))
    },[listado, show, tab])
    useImperativeHandle(ref,()=>({buscar}),[buscar])
    const loadData=useCallback(async()=>{
        const alertas   = await DB.AlertasDeCliente.GetAll()
        const clientes  = await DB.Clientes.GetClientes()
        const resumenes = (await DB.ResumenClientes.BuscarClientes("")).filter(resumen=>!clientes.some(cliente=>cliente.cliente_id===resumen.clienteId))
        const contactos = await DB.Contactos.GetAll()
        const vExtra=parseInt(await DB.Configuraciones.GetConfig("VENTA_EXTRA_PERMITIDA","1"))
        const mostrarContactos=await DB.Configuraciones.GetConfig("PERMITE_CONTACTOS","TRUE")
        const data=[
            ...clientes.map<HojaDeRutaFiltroResult>(cliente=>({
                ...cliente,
                store:"Clientes",
                alertaFue:alertas.filter(al=>al.cliente_id===cliente.cliente_id).some(al=>al.tipoDeAlerta_ids===8),
                preventivo:alertas.filter(al=>al.cliente_id===cliente.cliente_id).some(al=>al.tipoDeAlerta_ids===3 ||al.tipoDeAlerta_ids===4),
                bloqueado:alertas.filter(al=>al.cliente_id===cliente.cliente_id).some(al=>al.esBloqueante),
                mostrarTelefono:mostrarContactos==="TRUE" && contactos.some(con=>con.cliente_id===cliente.cliente_id && con.telefono!==""),
                mostrarWhatsapp:mostrarContactos==="TRUE" && contactos.some(con=>con.cliente_id===cliente.cliente_id && con.celular!==""),
            })),
            ...resumenes.filter(r=>vExtra===3 || (vExtra===2 && r.repartoId===hojaDeRuta?.reparto_id) ).map<HojaDeRutaFiltroResult>(resumen=>({
                ...resumen,
                cliente_id:resumen.clienteId,
                store:"ResumenClientes",
                mostrarTelefono:contactos.some(con=>con.cliente_id===resumen.clienteId && con.telefono!==""),
                mostrarWhatsapp:contactos.some(con=>con.cliente_id===resumen.clienteId && con.celular!==""),
            }))
        ]
        setListado(data)
    },[hojaDeRuta])
    useEffect(()=>{
        loadData()
    },[loadData])
    const onSyncResult = useCallback(async(resultados:SyncResultados)=>{
        if(resultados.ObtenerNuevosPedidosResult){
            if(ConfirmRef.current) ConfirmRef.current.prompt("OkOnly","Nuevo cliente agregado a la ruta","Cliente recibido")
            buscar("", "pendientes")
        }
        if(resultados.RegistrarVisitasResult){
            buscar("")
        }
    },[ConfirmRef, buscar])
    useEffect(()=>{
        Sync.callback=onSyncResult
        return ()=>{
            Sync.callback=undefined
        }
    },[onSyncResult])

    useEffect(()=>{
        if(onStatsChanged) onStatsChanged(
            listado.filter(cliente=>cliente.store==="Clientes" && !cliente.visitado).length,
            listado.filter(cliente=>cliente.store==="Clientes" && cliente.esRepaso).length
        )
        buscar("")
    },[listado,onStatsChanged, buscar])
    return(
        <Container fluid className="hojaDeRuta-listadoClientes">
            <AutoSizer>
                {({width,height})=>(
                    <List 
                        width={width}
                        height={height}
                        rowHeight={cacheRowHeight.current.rowHeight}
                        deferredMeasurementCache={cacheRowHeight.current}
                        rowCount={showing.length}
                        rowRenderer={({key,index,style,parent})=>{
                            const cliente=showing[index]
                            return (
                                <CellMeasurer key={cliente.cliente_id} cache={cacheRowHeight.current} parent={parent} columnIndex={0} rowIndex={index}>
                                    <ClienteHojaDeRuta {...cliente} onUpdateNeeded={loadData} style={style}/>
                                </CellMeasurer>
                            )
                        }}
                    />
                )}
            </AutoSizer>
        </Container>
    )/*
    return (
        <InfiniteScroll
            className="hojaDeRuta-listadoClientes"
            loadMore={nextPage}
            pageStart={0}
            hasMore={page*pageSize<listado.filter(show).length}
            loader={<Container className="cargando" key={0}><Spinner animation="border" /></Container>}
            useWindow={false}
            initialLoad={true}
            threshold={150}
        >
            {listado.filter(show).slice(0, page*pageSize).map((cl, index)=>(
                <ClienteHojaDeRuta key={cl.cliente_id} {...cl} onUpdateNeeded={loadData}/>
            ))}
        </InfiniteScroll>
    )*/
})