import { FC, useCallback }              from 'react'
import { Container, Form, Col }         from 'react-bootstrap'

import { FiCoffee, FiBell }             from 'react-icons/fi'
import { FiCheckCircle }                from 'react-icons/fi'
import { ImCancelCircle }               from 'react-icons/im'
import { FcHome, FcDepartment }         from 'react-icons/fc'
import { FcExpired }                    from 'react-icons/fc'
import { FaRoute, FaPhone,FaWhatsapp }  from 'react-icons/fa'
import { FaRegListAlt }                 from 'react-icons/fa'
import { FiCheck }                      from 'react-icons/fi'
import { MdPhoneAndroid }               from 'react-icons/md'
 
import { dateParser }                   from 'utils' 
import { AppStorage }                   from 'utils' 
import { whatsappShare }                from 'utils' 
import { useHojaDeRutaContext }         from 'hooks'
import { useAppContext }                from 'hooks'
import { HojaDeRutaFiltroResult }       from 'types'
import * as DB                          from 'services/DB'
import { ClienteResumen } from './clienteResumen'

 
type ClienteHojaDeRutaProps=HojaDeRutaFiltroResult & {
    onUpdateNeeded:()=>Promise<void>,
    style?:React.CSSProperties
}
export const ClienteHojaDeRuta:FC<ClienteHojaDeRutaProps> = (props)=>{
    const { cliente_id, }                       = props
    const { gestionDialog, transaccionesDialog }= useHojaDeRutaContext()
    const { ToastError, ConfirmRef, ChooseRef } = useAppContext()
    const VisitaClick = useCallback(async ()=>{
        if(!gestionDialog?.current) return
        const cliente=await DB.Clientes.GetCliente(props.cliente_id)
        if(!cliente) return
        if(cliente.fechaHoraTransferido){
            if(!ConfirmRef.current) return ToastError("Cliente ya sincronizado")
            if(await ConfirmRef.current.prompt("YesNo","El cliente ya fue sincronizado. Desea ir a VentaWeb?","Cliente Sincronizado")){
                const url = await AppStorage.getString("SERVERDOMAIN")
                const hojaDeRuta_id = await AppStorage.getString("HojaDeRutaId")
                
                window.open(`http://${url}/TransaccionesTemporales/Create?clienteId=${cliente.cliente_id}&hojaDeRutaId=${hojaDeRuta_id}`,"_blank")
            }
            return
        }
        if(cliente.esSync){
            if(!transaccionesDialog.current || !ConfirmRef.current) return ToastError("El cliente fue marcado para sincronizarse...")
            if(await ConfirmRef.current.prompt("YesNo","El cliente fue marcado para sincronizarse. Desea ver las transacciones?","Cliente con sincronismo programado")){
                transaccionesDialog.current.show(cliente.cliente_id)
            }
            return
        }
        try {
            await gestionDialog.current.open(cliente)
        }catch(error){
        }finally{
            await props.onUpdateNeeded()    
        }
    },[gestionDialog, props, ToastError, ConfirmRef, transaccionesDialog])  
    const RepasoClick = useCallback(async()=>{
        try{
            await DB.Clientes.MarcarRepaso(props.cliente_id)
        }catch(error){
        }finally{
            props.onUpdateNeeded()
        }
    },[props])
    const IrClick = useCallback(async ()=>{
        const cliente=await DB.Clientes.GetCliente(cliente_id)
        if(!cliente || !cliente.altitud || !cliente.longitud) return
        window.open(`https://www.google.com/maps/dir/?api=1&destination=${cliente.altitud},${cliente.longitud}&travelmode=driving&dir_action=navigate`,"_blank")
    },[cliente_id])

    const llamar=useCallback(async()=>{
        const contactos=await DB.Contactos.GetContactosCliente(cliente_id)
        if(contactos.principal && contactos.principal.telefono!==""){
            window.open(`tel:${contactos.principal.telefono}`)
            return
        }
        if(contactos.alternativo && contactos.alternativo.telefono!==""){
            window.open(`tel:${contactos.alternativo.telefono}`)
            return
        }
    },[cliente_id])
    const whatsapp=useCallback(async()=>{
        if(!ChooseRef.current) return
        const contactos=await DB.Contactos.GetContactosCliente(cliente_id)
        const phone =  (contactos.principal && contactos.principal.celular!=="")
                            ?parseFloat("54" + contactos.principal.celular.replaceAll("-",""))
                            :(contactos.alternativo && contactos.alternativo.celular!=="")
                                ?parseFloat("54" + contactos.alternativo.celular.replaceAll("-",""))
                                :null  
        if(!phone) return
        switch(await ChooseRef.current.choose([
            {label:"Llamar al cliente",value:"llamar",Icon:FaPhone},
            {label:"Enviar Whatsapp",value:"whatsapp",Icon:FaWhatsapp},
        ],"Contactar al celular")){
            case "llamar":  
                window.open(`tel:${phone}`)                
                break;
            case "whatsapp":
                whatsappShare(phone,""); 
                break;
        }        
    },[cliente_id, ChooseRef])
    const verTransacciones = useCallback(async()=>{
        if(!transaccionesDialog.current) return
        await transaccionesDialog.current.show(cliente_id)
    },[cliente_id, transaccionesDialog])

    return (props.store==="Clientes"?
        <Container key={cliente_id} className="cliente" style={props.style}>
            <Form.Row className="cliente_info">
                <Col xs={7}>
                    <Form.Label column xs={5}>Nro cliente:</Form.Label> 
                    <Form.Label column xs={7}>{cliente_id}</Form.Label> 
                    <Form.Label column xs={12}>
                        {props.tipoCliente_ids===1?<FcHome/>:<FcDepartment/>}{props?.nombreCliente}
                    </Form.Label>
                </Col>
                <Col xs={5}>
                    {props.tieneAlertas?<FiBell color="red"/>:undefined}
                    {props.mostrarTelefono && <FaPhone className="text-primary" onClick={llamar}/>}
                    {props.mostrarWhatsapp && <MdPhoneAndroid className="text-success" onClick={whatsapp}/>}
                    <Form.Label>{props.orden}</Form.Label>
                </Col>
                <Col xs={12}>{props.domicilioCompleto}</Col>
            </Form.Row>
            <Form.Row className="cliente_status">
                <Col className={props.alertaFue?"bg-warning":""}>FUE: {dateParser(props.fechaUtlimaEntrega??null,"DD/MM/YYYY")}</Col>
                <Col xs={5} className={["","Cliente regular", undefined, null].some(item=>item===props.formaPagoHabitual)?"":"formaDePago"}>
                    {props.formaPagoHabitual!=="Cliente regular"?props.formaPagoHabitual:""}
                </Col>
                <Col>FUC: {dateParser(props.fechaUltimoCobroFactura??null,"DD/MM/YYYY")}</Col>
                <Col xs={6}>Saldo Facturas: ${((props.saldoFacturacion??0)-(props.totalCobradoActual??0)).toFixed(2)}</Col>
                <Col xs={6}>Consumos: ${((props.saldoConsumos??0)+(props.totalEntregadoActual??0)).toFixed(2)}</Col>
                <Col xs={12}>Saldo Final:</Col>
                <Col xs={12}>${(((props.saldoFacturacion??0)-(props.totalCobradoActual??0)) + ((props.saldoConsumos??0)+(props.totalEntregadoActual??0))).toFixed(2)}</Col>
                <Col xs={4}>
                    {props.preventivo?<Form.Label className="preventivo">Preventivo</Form.Label>:undefined}
                </Col>
                <Col xs={4}>
                    {props.visitado?(<FiCheckCircle color="green"/>):(<ImCancelCircle color="red"/>)}
                    {props.esRepaso?(<FcExpired />):undefined}
                    {props.ausente?(<FiCoffee />):undefined}
                </Col>
                <Col xs={4}>
                    {props.bloqueado?<Form.Label className="bloqueado">Bloqueado</Form.Label>:undefined}
                </Col>
            </Form.Row>
            <Form.Row className="cliente_actions">
                <Col onClick={IrClick}><FaRoute/>Ir</Col>
                {!props.visitado && <Col onClick={RepasoClick}><FcExpired/>Repaso</Col>}
                {props.visitado && <Col xs={6} onClick={verTransacciones}><FaRegListAlt/>Transacciones</Col>}
                <Col onClick={VisitaClick}><FiCheck/>Visita</Col>
            </Form.Row>
        </Container>:<ClienteResumen {...props} />
    )
}
