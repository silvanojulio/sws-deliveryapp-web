import { FC, useCallback, useRef }  from 'react'
import { useState, useEffect }      from 'react'
import { Button, Col }              from 'react-bootstrap'
import { Form, Collapse, Card }     from 'react-bootstrap'
import { FcAssistant }              from 'react-icons/fc'
import { FcDatabase }               from 'react-icons/fc'
import { FcProcess }                from 'react-icons/fc'
import { FaAmbulance }              from 'react-icons/fa'
import { FcExpired }                from 'react-icons/fc'
import { FcSearch }                 from 'react-icons/fc'
import { FcDeleteDatabase }         from 'react-icons/fc'
import { ImMenu }                   from 'react-icons/im'
import { ImBell }                   from 'react-icons/im'

import { useHojaDeRutaContext }     from 'hooks'
import { useAppContext }            from 'hooks'
import { Sync, AppStorage }         from 'utils'
import { GPSTracker }               from 'utils'
import { HojaDeRutaFiltroOptions }  from 'types'
import { AppData_UsuariosApp }      from 'entities/app'
import { VIEW_Incidente_DTO }       from 'entities/incidentes'
import { SessionService }           from 'services'
import * as DB from 'services/DB'

import { ClientesList, IClientesList }  from './ClientesList'
import { Filter }                       from './filter'

import './hojaDeRuta.scss'


export const HojaDeRuta:FC = ()=>{
    const { controlDePlayaDialog }          = useHojaDeRutaContext()
    const { gastosDialog }                  = useHojaDeRutaContext()
    const { cierreDeRutaDialog }            = useHojaDeRutaContext()
    const { hojaDeRuta }                    = useHojaDeRutaContext()
    const { user, logout }                  = useAppContext()
    const { printerOn, setPrinterOn }       = useAppContext()
    const { ToastError, ToastSuccess }      = useAppContext()
    
    const [ filtro,     setFiltro     ]     = useState<HojaDeRutaFiltroOptions>("all")
    const [ menuOpen,   setMenuOpen   ]     = useState<boolean>(false)
    const [ repaso,     setRepaso     ]     = useState<number>(0)
    const [ pendientes, setPendientes ]     = useState<number>(0)
    const [ incidentes, setIncidentes ]     = useState<VIEW_Incidente_DTO[]>([])
    const buscarRef                         = useRef<HTMLInputElement>(null)
    const clientesListRef                   = useRef<IClientesList>(null)

    const buscar = useCallback(async(texto:string)=>{
        if(!!clientesListRef.current) clientesListRef.current.buscar(texto,filtro)
    },[filtro])
    useEffect(()=>{
        if(filtro==="search" && buscarRef.current) buscarRef.current.value=""
        if(!!clientesListRef.current) clientesListRef.current.buscar("",filtro)
    },[filtro])
    const loadIncidentes=useCallback(async()=>{setIncidentes(await DB.Incidentes.GetList())},[])
    
    useEffect(()=>{
        Sync.StartSync()
        GPSTracker.StartTracker()
        setFiltro("pendientes")
    },[loadIncidentes])
    //#region Menu Events
    const crearControlDePlaya = useCallback(async ()=>{
        if(!user || !controlDePlayaDialog.current || !hojaDeRuta) return
        setMenuOpen(false)
        await controlDePlayaDialog.current.new(user.usuario_id, hojaDeRuta.id).catch(()=>{})
    },[user, controlDePlayaDialog, hojaDeRuta])
    const verGastos = useCallback(async()=>{
        if(!gastosDialog?.current) return
        setMenuOpen(false)
        await gastosDialog.current.show()
    },[gastosDialog])
    const cerrarRuta = useCallback(async()=>{
        if(!cierreDeRutaDialog.current) return
        setMenuOpen(false)
        const pendientes = await DB.Clientes.GetPendientesSync()
        if(pendientes.length>0){
            ToastError("Existen clientes pendientes de sincronizar")
            return
        }
        if(await cierreDeRutaDialog.current.cerrarRuta()) {
            ToastSuccess("Hoja de Ruta Cerrada")
            logout()
        }        
    },[cierreDeRutaDialog, logout, ToastSuccess, ToastError])
    const abrirAltaTemprana = useCallback(async()=>{
        const CURRENT_LOCAL_SESSION = await AppStorage.getObject<{
            expire:number,
            usuario:string,
            userApp:AppData_UsuariosApp
        }>("CURRENT_LOCAL_SESSION")
        const base_url = await AppStorage.getString("SERVERDOMAIN")
        if(!CURRENT_LOCAL_SESSION) return
        const {username, password}=CURRENT_LOCAL_SESSION.userApp
        const usuario = await SessionService.IniciarSessionExterno(username, password)
        window.open(`http://${base_url}/React/#/components/altaCliente/${usuario.tokenValido}`,"_blank")
    },[])
    const switchPrinter = useCallback(async()=>{
        if(!setPrinterOn) return
        setPrinterOn(!printerOn)
    },[printerOn, setPrinterOn])
    const updateStats = useCallback((pendientes:number,repaso:number)=>{
        setRepaso(repaso)
        setPendientes(pendientes)
    },[])
    //#endregion
    return (
        <Card className="hojaDeRuta-Layout">
            <Card.Header>
                <Col xs={5}>Pendientes: {pendientes}</Col>
                <Col xs={2}>
                    {incidentes.length>0?(
                        <ImBell color="orange" 
                            onClick={()=>setMenuOpen(!menuOpen)}
                            aria-controls="menuItems"
                            aria-expanded={menuOpen}
                        />
                    ):(
                        <ImMenu 
                            onClick={()=>setMenuOpen(!menuOpen)}
                            aria-controls="menuItems"
                            aria-expanded={menuOpen}
                        />
                    )}
                </Col>
                <Col xs={5}>En Repaso: {repaso}</Col>
                <Collapse in={menuOpen}>
                    <Col xs={12} id="menuItems" className="menu-items" >
                        <Button variant="light" onClick={crearControlDePlaya} >Control de Playa</Button>
                        <Button variant="light" onClick={verGastos}>Gastos de Reparto</Button>
                        <Button variant="light" onClick={abrirAltaTemprana}>Alta Temprana de Cliente</Button>
                        <Button variant="light" onClick={cerrarRuta}>Cerrar Hoja de Ruta</Button>
                        {incidentes.length>0 && <Button variant="warning">Ver Incidentes</Button>}
                        <Button variant={printerOn?"success":"danger"} onClick={switchPrinter}>Sugerir impresion: {printerOn?"[SI]":"[NO]"}</Button>
                        <Form.Label>Version {process.env.REACT_APP_VERSION}</Form.Label>
                    </Col>
                </Collapse>
                <Col 
                    xs={{offset:4,span:4}}
                    className="menu-toggle" 
                    onClick={()=>setMenuOpen(!menuOpen)}
                    onDragStart={()=>setMenuOpen(!menuOpen)}
                    aria-controls="menuItems" 
                    aria-expanded={menuOpen}
                />
            </Card.Header>
            <Card.Body>
                <ClientesList 
                    ref={clientesListRef}
                    onStatsChanged={updateStats}
                />                    
            </Card.Body>
            <Card.Footer>
                <Collapse in={filtro==="search"}>
                    <Form.Row id="buscar" className="w-100">
                        <Col xs={{offset:1, span:10}}>
                            <Form.Control size="sm" type="text" 
                                ref={buscarRef}
                                onChange={ev=>buscar(ev.target.value)} 
                            />
                        </Col>                            
                    </Form.Row>
                </Collapse>
                <Filter Icon={FcSearch}         option="search"     selected={filtro==="search"}        label="Buscar"      onClick={setFiltro}/>
                <Filter Icon={FcDatabase}       option="all"        selected={filtro==="all"}           label="Todos"       onClick={setFiltro}/>
                <Filter Icon={FcDeleteDatabase} option="pendientes" selected={filtro==="pendientes"}    label="Pendientes"  onClick={setFiltro}/>
                <Filter Icon={FcAssistant}      option="pedidos"    selected={filtro==="pedidos"}       label="Peidos"      onClick={setFiltro}/>
                <Filter Icon={FcExpired}        option="repasos"    selected={filtro==="repasos"}       label="Repaso"      onClick={setFiltro}/>
                <Filter Icon={FaAmbulance}      option="srvtecs"    selected={filtro==="srvtecs"}       label="Srv Tec"     onClick={setFiltro}/>
                <Filter Icon={FcProcess}        option="nosyncs"    selected={filtro==="nosyncs"}       label="No Sync"     onClick={setFiltro}/>
            </Card.Footer>
        </Card>
    )
}
