import { FC, useRef, useCallback, useState }    from 'react'
import { Container, Form, Button, Col, Image }  from 'react-bootstrap'

import { useAppContext }            from 'hooks'
import { ConfigServer }             from 'dialogs'
import { DBDownload, IDBDownload }  from 'dialogs/DBDownload'
import * as DB from 'services/DB'

import './loginForm.scss'
import { AppStorage } from 'utils'
export const LoginForm:FC = ()=>{
    const [bussy, setBussy]             = useState<boolean>(false)
    const { setUser }                   = useAppContext()
    const { ToastError, ToastSuccess }  = useAppContext()

    const userRef=useRef<HTMLInputElement>(null) 
    const passRef=useRef<HTMLInputElement>(null)
    const dbDownloader = useRef<IDBDownload>(null)
    const loginClick=useCallback(async ()=>{
        setBussy(true)
        try{
            const u=await DB.UsuariosApp.Login(userRef.current?.value??"", passRef.current?.value??"")
            setUser!(u)
        }catch(error){
            ToastError((error as Error).message) 
            setBussy(false) 
        }
    },[ToastError, setUser, setBussy])
    const downloadClick=useCallback(async()=>{
        if(!dbDownloader.current) return
        try{
            await dbDownloader.current?.descargarBase()
            ToastSuccess("Base de Datos descargada",{autoClose:2000})
            AppStorage.setValue("CURRENTLATITUD","")
            AppStorage.setValue("CURRENTLONGITUD","")
        }catch(error){}
    },[ToastSuccess])
    return (
        <Container fluid className="loginForm" >
            <DBDownload ref={dbDownloader}/>
            <Form >
                <Form.Row>
                    <Col>
                        <Image src="images/logo_sws.png" className="logo" />
                    </Col>
                </Form.Row>
                <Form.Row>
                    <Col xs={10}>
                        <Form.Control disabled={bussy} name="usuario" ref={userRef} placeholder="Usuario" />
                    </Col>
                </Form.Row>
                <Form.Row>
                    <Col xs={10}>
                        <Form.Control disabled={bussy} name="password" type="password" ref={passRef} placeholder="Contraseña" />
                    </Col>
                </Form.Row>
                <Form.Row >
                    <Col xs={10} >
                        <Button disabled={bussy} variant="primary" onClick={loginClick} >Ingresar</Button>
                    </Col>
                </Form.Row>
                <Form.Row>
                    <Col xs={10}>
                        <Button disabled={bussy} variant="primary" onClick={downloadClick} >Descargar Base de Datos</Button>
                    </Col>
                </Form.Row>  
                <Form.Row>
                    <Col xs={10}>
                        <ConfigServer disabled={bussy} />
                    </Col>
                </Form.Row>
                <Form.Row>
                    <Col xs={10}>
                        <Form.Label>Version {process.env.REACT_APP_VERSION}</Form.Label>
                    </Col>
                </Form.Row>

            </Form>
        </Container>
    )
}