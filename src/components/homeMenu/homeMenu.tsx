import React, { FC, useRef, useCallback }       from 'react'
import { useState, useEffect }                  from 'react'
import { Container, Button, Image, Form, Col }  from 'react-bootstrap'
import { useHistory }                           from 'react-router-dom'

import { useAppContext }                        from 'hooks/app'
import { ControlDePlaya, IControlDePlaya }      from 'dialogs/controlDePlaya'
import { AppStorage }                           from 'utils'
import { dateParser }                           from 'utils'
import * as DB                                  from 'services/DB'
import { AppData_HojaDeRuta }                   from 'entities/app'
//import { ReceiptText }                          from 'types'
import './homeMenu.scss'

export const HomeMenu:FC = ()=>{
    const history           = useHistory()
    const { logout, user }  = useAppContext()
    const controlDePlayaRef = useRef<IControlDePlaya>(null)
    const [ hojaDeRuta, setHojaDeRuta ] = useState<AppData_HojaDeRuta>()

    const controDePlayaClick=useCallback(async()=>{
        DB.Logs.AddLog({mensaje:"Touch Control de Playa", funcionalidad:"HomeMenu", entidad:"hojaDeRuta", idEntidad:hojaDeRuta?.id})
        if(!controlDePlayaRef.current || !user) return
        if(!hojaDeRuta) throw new Error("Hoja de ruta no establecida")
        await controlDePlayaRef.current.new(user.usuario_id, hojaDeRuta.id).catch(()=>{})
    },[ user, hojaDeRuta ])
    const hojaDeRutaClick=useCallback(()=>{
        DB.Logs.AddLog({mensaje:"Touch Hoja De Ruta", funcionalidad:"HomeMenu", entidad:"hojaDeRuta", idEntidad:hojaDeRuta?.id})
        history.push('/hojaDeRuta')
    },[history, hojaDeRuta])
    const salirClick=useCallback(()=>{
        DB.Logs.AddLog({mensaje:"Touch Salir", funcionalidad:"HomeMenu", entidad:"hojaDeRuta", idEntidad:hojaDeRuta?.id})
        logout()
    },[hojaDeRuta, logout])
    const loadData=useCallback(async ()=>{
        const hojaDeRutaId = await AppStorage.getString("HojaDeRutaId")
        if(!hojaDeRutaId) return setHojaDeRuta(undefined)
        setHojaDeRuta(await DB.HojasDeRuta.Get(parseInt(hojaDeRutaId)))
    },[])
    useEffect(()=>{loadData()},[loadData])
    return (
        <Container fluid className="home-menu"> 
            <ControlDePlaya ref={controlDePlayaRef}/>
                <Form.Row>
                    <Col><Image src="images/logo_sws.png" className="logo" /></Col>    
                </Form.Row>
                <Form.Row className="RepartoData">
                    <Col xs={12}>{hojaDeRuta?.nombreReparto}</Col>
                    <Col xs={12}>({hojaDeRuta?.id}) {dateParser(hojaDeRuta?.fechaReparto??null,"DD/MM/YYYY")}</Col>
                </Form.Row>
                <Form.Row>
                    <Col><Button variant="primary" onClick={controDePlayaClick} >Control DePlaya</Button></Col>
                </Form.Row>    
                <Form.Row>
                    <Col><Button variant="primary" onClick={hojaDeRutaClick} >Hoja de ruta</Button></Col>
                </Form.Row>
                <Form.Row>
                    <Col><Button variant="primary" onClick={salirClick}>Salir</Button></Col>
                </Form.Row>
                <Form.Row className="flex-column">
                    <Col>
                        <Form.Label>Version {process.env.REACT_APP_VERSION}</Form.Label>
                    </Col>
                    {window.location.hostname !== "prod.pwa.sistemaws.com" && <Col>
                        {window.location.hostname === "localhost" && <Form.Label>[ Localhost ]</Form.Label>}
                        {window.location.hostname === "test.pwa.sistemaws.com" && <Form.Label>[ Testing ]</Form.Label>}
                        {window.location.hostname === "dev.pwa.sistemaws.com" && <Form.Label>[ Development ]</Form.Label>}
                    </Col>}
                </Form.Row>
        </Container>
    )
}