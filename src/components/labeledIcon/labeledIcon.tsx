import React, { FC, useCallback }   from 'react'
import { Form, NavItem }            from 'react-bootstrap'
import { IconType }                 from 'react-icons'




type LabeledIconProps={
    label:string
    Icono:IconType
    hideLabel?:boolean
    onClick:(arg?:any)=>void
    iconColor?:string
    disabled?:boolean
}
export const LabeledIcon:FC<LabeledIconProps> = (props)=>{
    const { label, hideLabel, iconColor }   = props
    const { Icono, onClick }                = props
    const { disabled }                      = props
    const click = useCallback(async()=>{
        if(!onClick) return
        if(disabled) return
        onClick()
    },[onClick, disabled])
    return (
        <NavItem className="labeled-icon" onClick={click}>
            <Icono color={disabled?"gray":iconColor}/>
            <Form.Label hidden={hideLabel}>{label}</Form.Label>
        </NavItem>
    )
    
}