import React, { forwardRef, useCallback, useImperativeHandle, useRef } from 'react'
import { Form, Col } from 'react-bootstrap'
import { FormControlProps, ColProps } from 'react-bootstrap'

type NumericFieldProps= {
    colProps?:ColProps
    hidden?:boolean
    formControlProps?:FormControlProps & {min?:number}
    defaultValue?:number
    onChange?:(value:number)=>void
} 
export interface INumericField{
    getValue:()=>number
    setValue:(value:number)=>void
}
export const NumericField=forwardRef<INumericField,NumericFieldProps> ((props,ref)=>{
    const inputRef=useRef<HTMLInputElement>(null)
    const decrease = useCallback(async()=>{
        if(!inputRef.current) return
        inputRef.current.valueAsNumber && inputRef.current.valueAsNumber--
        if(props.onChange) props.onChange(inputRef.current.valueAsNumber)
    },[props])
    const increase = useCallback(async()=>{
        if(!inputRef.current) return
        inputRef.current.valueAsNumber++
        if(props.onChange) props.onChange(inputRef.current.valueAsNumber)
    },[props])
    const valueChanged = useCallback(()=>{
        if(!inputRef.current) return
        if(inputRef.current.value==="") inputRef.current.valueAsNumber=0
        inputRef.current.value=inputRef.current.valueAsNumber.toString()
        if(props.onChange) props.onChange(inputRef.current.valueAsNumber)
    },[props])
    useImperativeHandle(ref,()=>({
        getValue:()=>inputRef.current?.valueAsNumber??0,
        setValue:(value:number)=>inputRef.current!.valueAsNumber=value
    }))
    return (
        <Col {...props.colProps} hidden={props.hidden} className="numericField">
            <Form.Label onClick={decrease}>-</Form.Label>
            <Form.Control {...props.formControlProps} type="number" size="sm"
                ref={inputRef} defaultValue={props.defaultValue??0}
                onChange={valueChanged}/>            
            <Form.Label onClick={increase}>+</Form.Label>
        </Col>
    )
})