import './numericField.scss'

export { NumericField } from './numericField'
export type { INumericField } from './numericField'