import { forwardRef, useImperativeHandle }  from 'react'
import { useState, useCallback }            from 'react'
import { Modal, Button, Form }              from 'react-bootstrap'

import { PromiseCallbacks }         from 'types'
import { AppData_AlertasDeCliente } from 'entities/app'

export interface IAlertas{
    show:(alertas:AppData_AlertasDeCliente[])=>Promise<boolean>
}
export const Alertas = forwardRef<IAlertas>((_props, ref)=>{
    const [ alertas, setAlertas ] = useState<AppData_AlertasDeCliente[]>([])
    const [ callbacks, setCallbacks] = useState<PromiseCallbacks<boolean>>()
    useImperativeHandle(ref, ()=>({
        show:async (alertas:AppData_AlertasDeCliente[])=>{
            const p = new Promise<boolean>((resolve, reject)=>setCallbacks({resolve,reject}))
            p.finally(()=>setCallbacks(undefined))
            setAlertas(alertas)
            return p
        }
    }))
    const vistas=useCallback(async()=>{
        if(!callbacks) return
        callbacks.resolve(true)
    },[callbacks])
    return (
        <Modal show={!!callbacks} size="sm" backdrop="static" centered keyboard={false} className="gestion-cliente-menu">
            <Modal.Header><Modal.Title>Alertas</Modal.Title></Modal.Header>
            <Modal.Body>
                {alertas.map(alerta=>(
                    <Form.Label key={alerta.id} column xs={12}>
                        {alerta.comentarios}
                    </Form.Label>
                ))}
            </Modal.Body>
            <Modal.Footer>
                <Button size="sm" variant="info" onClick={vistas}>Entendido</Button>
            </Modal.Footer>
        </Modal>
    )
})