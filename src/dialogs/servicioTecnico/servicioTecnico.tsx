import { forwardRef, useImperativeHandle }  from 'react'
import { useState, useCallback,  }    from 'react'
import { Modal,  Tabs, Tab }   from 'react-bootstrap'

import { PromiseCallbacks } from 'types'
import { Orden }            from './orden'
import { AppData_OrdenesDeTrabajo } from 'entities/app'
import * as DB from 'services/DB'

export interface IServicioTecnico{
    show:(cliente_id:number)=>Promise<void>
}
export const ServicioTecnico = forwardRef<IServicioTecnico>((_,ref)=>{
    const [ callbacks, setCallbacks ]   = useState<PromiseCallbacks<void>>()
    const [ , setCliente_id ]           = useState<number>()
    const [ orden, setOrden ]           = useState<AppData_OrdenesDeTrabajo>()
    const show = useCallback(async (cliente_id:number)=>{
        setCliente_id(cliente_id)
        setOrden((await DB.OrdenesDeTrabajo.Get(cliente_id))[0])
        const p = new Promise<void>((resolve, reject)=>setCallbacks({resolve, reject}))
        p.finally(()=>setCallbacks(undefined))
        return p
    },[])
    useImperativeHandle(ref, ()=>({show}),[show])
    const cerrar = useCallback(async()=>{
        if(!callbacks)return
        callbacks.resolve()
    },[callbacks])
    return <Modal centered show={!!callbacks} onHide={cerrar} className="serviciosTecnicos">
        <Modal.Header closeButton>Servicios Técnicos</Modal.Header>
        <Modal.Body>
            <Tabs defaultActiveKey="orden">
                <Tab title="Orden" eventKey="orden">
                    <Orden orden={orden} />
                </Tab>
                <Tab title="Dispensers" eventKey="dispensers"></Tab>
            </Tabs>
        </Modal.Body>
    </Modal>
})