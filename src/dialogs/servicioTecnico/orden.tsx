import { FC }           from 'react'
import { Form, Badge }  from 'react-bootstrap'


import { AppData_OrdenesDeTrabajo } from 'entities/app'

const OrderField:FC<{label:string, value:string, variant?:string, subVariant?:string, colSize?:number}> = ({label, value, variant, subVariant, colSize})=>{
    return (
        <Form.Label column xs={colSize}>
            <Badge variant={variant}>
                <Form.Label>{label}</Form.Label>
                <Badge variant={subVariant}>{value}</Badge>
            </Badge>
        </Form.Label>
    )
}
type OrdenProps={
    orden?:AppData_OrdenesDeTrabajo
}
export const Orden:FC<OrdenProps> = ({orden})=>{
    if(!orden) return (<></>)
    return (

        <Form.Row className="orden">
            <OrderField colSize={12} variant="info" subVariant="light" label="Síntoma"          value={orden.sintoma} />
            <OrderField colSize={3}  variant="info" subVariant="light" label="Cant Disp"        value={orden.cantDispensers} />
            <OrderField colSize={4}  variant="info" subVariant="light" label="Prioridad"        value={orden.prioridad} />
            <OrderField colSize={5}  variant="info" subVariant="light" label="Horario"          value={orden.franjaHoraria} />
            <OrderField colSize={12} variant="info" subVariant="light" label="Responsable"      value={orden.responsableEnCliente} />
            <OrderField colSize={12} variant="info" subVariant="light" label="Sector/Ubicación" value={orden.sectorUbicacion} />
            <OrderField colSize={12} variant="info" subVariant="light" label="Comentarios"      value={orden.comentarios} />
            
        </Form.Row>
    )
}

