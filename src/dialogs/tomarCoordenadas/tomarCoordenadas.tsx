import { forwardRef, useImperativeHandle }  from 'react'
import { FC, useState, useCallback }        from 'react'
import { Modal, Button }                    from 'react-bootstrap'
import { MdLayers }                         from 'react-icons/md'
import { MdControlPoint  }                  from 'react-icons/md'
import { RiMapPinUserFill }                 from 'react-icons/ri'
import { FaTruck }                          from 'react-icons/fa'
import GoogleMapReact                       from 'google-map-react'
import { render }                           from 'react-dom';

import { PromiseCallbacks } from 'types'
import { GoogleMapApiKey }  from 'types'
import { AppData_Clientes } from 'entities/app'
import * as DB from 'services/DB'

const Marker:FC<{lat:number, lng:number, tipo:"cliente"|"reparto"}> = ({lat, lng, tipo})=>{
    switch (tipo){
        case "cliente":return <RiMapPinUserFill className="marker-cliente"/>
        case "reparto":return <FaTruck className="marker-reparto"/>
        default:return <></>
    }
}
export interface ITomarCoordenadas{
    obtenerCoordenadas:(cliente_id:number,ubicacion:GoogleMapReact.Coords)=>Promise<void>
}
export const TomarCoordenadas = forwardRef<ITomarCoordenadas>((_, ref)=>{
    const [ callbacks, setCallbacks]    = useState<PromiseCallbacks<void>>()
    const [ center, setCenter ]         = useState<GoogleMapReact.Coords>()
    const [ ubicacion, setUbicacion ]   = useState<GoogleMapReact.Coords>()
    const [ cliente_id, setCliente_id ] = useState<number>()
    const [ cliente, setCliente ]       = useState<AppData_Clientes>()
    const obtenerCoordenadas = useCallback(async(cliente_id:number, ubicacion:GoogleMapReact.Coords)=>{
        setCliente_id(cliente_id)
        setCliente(await DB.Clientes.GetCliente(cliente_id))
        const p = new Promise<void>((resolve, reject)=>{
            setCallbacks({resolve, reject})
        })
        setCenter(ubicacion)
        setUbicacion(ubicacion)
        p.finally(()=>{
            setCenter(undefined)
            setCliente_id(undefined)
            setCliente(undefined)
            setCallbacks(undefined)
        })
        return p
    },[])
    useImperativeHandle(ref, ()=>({obtenerCoordenadas}),[obtenerCoordenadas])
    const close = useCallback(async()=>{
        if(!callbacks) return
        callbacks.resolve()
    },[callbacks])
    const save = useCallback(async()=>{
        if(!callbacks) return
        if(!center || !cliente_id) return callbacks.resolve()
        try{
            const cliente = await DB.Clientes.GetCliente(cliente_id)
            await DB.Clientes.SaveCliente({
                ...cliente, 
                relevamientoCoordenadas_latitud:center.lat.toString(), 
                relevamientoCoordenadas_longitud:center.lng.toString()
            } as AppData_Clientes)
        }catch(error){return }
        callbacks.resolve()
    },[cliente_id, center, callbacks])
    
    const mapStyles=[
        {"featureType": "all", "stylers": [
            {"saturation": 0},
            {"hue": "#e7ecf0"}
        ]},
        {"featureType": "road", "stylers": [{"saturation": -70}]},
        {"featureType": "transit", "stylers": [{"visibility": "off"}]},
        {"featureType": "poi", "stylers": [{"visibility": "off"}]},
        {"featureType": "water", "stylers": [
            {"visibility": "simplified"},
            {"saturation": -60}
        ]}
    ]
    const defaultOptions:GoogleMapReact.MapOptions={
        fullscreenControl:false,
        mapTypeControlOptions:{
            style:3
        },
        controlSize:32,
        styles:mapStyles,
    }
    const switchMapType=(map:any)=>{
        //const {map_, maps_} = mapRef.current
        map.setMapTypeId(map.getMapTypeId()==="roadmap"?"hybrid":"roadmap")
    }
    const centerChanged=(value:GoogleMapReact.ChangeEventValue)=>{setCenter(value.center)}
    const initMapControls = (_google:any)=>{
        
        const {map, maps}=_google
        
        // Crear boton para intercambiar vista satelite
        const toggleButtonDiv = document.createElement('div');
        render(<Button size="sm" variant="info" className="map-btn" onClick={()=>switchMapType(map)} ><MdLayers /></Button>, toggleButtonDiv);
        map.controls[maps.ControlPosition.RIGHT_BOTTOM].push(toggleButtonDiv);
        
        

        const pin = render(<div><MdControlPoint className="PINMAP"/></div>, document.createElement("div"))
        map.controls[maps.ControlPosition.CENTER].push(pin)
    }
    return (
        <Modal show={!!callbacks} onHide={close} size="sm" backdrop="static" centered keyboard={false} className="tomarCoordenadas" >
            <Modal.Header closeButton>Tomar Coordenadas</Modal.Header>
            <Modal.Body>
                <GoogleMapReact 
                    options={defaultOptions}
                    center={center}
                    defaultZoom={15}
                    bootstrapURLKeys={{ key: GoogleMapApiKey}}
                    yesIWantToUseGoogleMapApiInternals
                    onGoogleApiLoaded={initMapControls}
                    onChange={centerChanged}
                >
                    {!!ubicacion?<Marker tipo="reparto" {...ubicacion} />:undefined}
                    {!!cliente && !!cliente.altitud && !!cliente.longitud
                        ?<Marker tipo="cliente" lat={parseFloat(cliente.altitud)} lng={parseFloat(cliente.longitud)} />
                        :undefined
                    }
                </GoogleMapReact>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={save}>Guardar</Button>
            </Modal.Footer>
        </Modal>
    )
})