import React, { forwardRef, useImperativeHandle }   from 'react'
import { useRef, useState, useCallback }            from 'react'
import { Modal, Form, Col, Button, Spinner }        from 'react-bootstrap'

import { PromiseCallbacks }         from 'types'
import { ArticuloControlDePlaya }   from 'entities/controlDePlaya'
import DispensersService            from 'services/dispensers'
import { useAppContext }            from 'hooks'
import * as DB from 'services/DB'


type AddDispenserProps={
    disabled:boolean
}
export interface IAddDispenser{
    new:()=>Promise<ArticuloControlDePlaya>
}
export const AddDispenser = forwardRef<IAddDispenser, AddDispenserProps>((props, ref)=>{
    const [ callbacks, setCallbacks ]   = useState<PromiseCallbacks<ArticuloControlDePlaya>>()
    const [ bussy, setBussy ]           = useState<boolean>(false)
    const numRef                        = useRef<HTMLInputElement>(null)
    const { ToastError }                = useAppContext()

    const cancelarClick=useCallback(()=>{
        DB.Logs.AddLog({mensaje:"Touch Cancelar", funcionalidad:"ControlDePlaya-AddDispensers"})
        if(!callbacks) return
        callbacks.reject()
        setCallbacks(undefined)
    },[callbacks])
    const saveClick=useCallback(async()=>{
        DB.Logs.AddLog({mensaje:"Touch Guardar", funcionalidad:"ControlDePlaya-AddDispensers"})
        if(!callbacks || !numRef.current) return
        setBussy(true)
        try{
            const dispenser=await DispensersService.ObtenerDispenserPorNumeroInterno(numRef.current.value)
            const articulo:ArticuloControlDePlaya = {
                dispenser_id:dispenser.id,
                cantidadLlenosUni:1,
                cantidadLlenosPack:0,
                cantidadVaciosUni:0,
                cantidadVaciosPack:0,
                cantidadFalladosUni:0,
                cantidadFalladosPack:0,
                dispenser:{
                    cliente_id:dispenser.clienteActual_id,
                    color:dispenser.color,
                    id:dispenser.id,
                    marca:dispenser.marcaDispenser,
                    nroDispenser:dispenser.numeroInterno,
                    tipo:dispenser.tipoDispenser
                },
                articulo_id:0,
                articulo:null                
            } 
            DB.Logs.AddLog({mensaje:`nroDispenser:${articulo.dispenser?.nroDispenser}`, funcionalidad:"ControlDePlaya-AddDispensers", entidad:"Dispenser", idEntidad:articulo.dispenser_id})
            callbacks.resolve(articulo)
        }catch(error){
            ToastError((error as Error).message)
            setBussy(false) 
        }finally{}

    },[callbacks, ToastError])
    useImperativeHandle(ref, ()=>({
        new:()=>{
            DB.Logs.AddLog({mensaje:"Iniciando Guardar", funcionalidad:"ControlDePlaya-AddDispensers"})
            const p = new Promise<ArticuloControlDePlaya>((resolve, reject)=>setCallbacks({resolve, reject}))
            p.finally(()=>{
                DB.Logs.AddLog({mensaje:"Finalizando Guardar", funcionalidad:"ControlDePlaya-AddDispensers"})
                setCallbacks(undefined)
                setBussy(false)
            })
            return p
        }
    }))
    return (        
        <Modal show={!!callbacks} backdrop="static" centered keyboard={false} >
            <Modal.Header><Modal.Title>Agregar Dispenser</Modal.Title></Modal.Header>
            <Modal.Body>
                <Form.Row>
                    <Col>
                        <Form.Label column>Numero de dispenser</Form.Label>
                        <Form.Control disabled={bussy || props.disabled} type="number" ref={numRef}/>
                    </Col>
                </Form.Row>
            </Modal.Body>
            <Modal.Footer>
                <Button disabled={bussy || props.disabled} variant="secondary" onClick={cancelarClick}>Cancelar</Button>
                <Button disabled={bussy || props.disabled} variant="primary" onClick={saveClick} >{bussy && <Spinner animation={"border"} size="sm"/>} Agregar</Button>
            </Modal.Footer>
        </Modal>
    )
})