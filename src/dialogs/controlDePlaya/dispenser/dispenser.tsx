import React, { FC } from 'react'
import { Form, Button, Image, Col } from 'react-bootstrap'
import { MdDelete } from 'react-icons/md'

import { ArticuloControlDePlaya } from 'entities/controlDePlaya'
import { AppData_Dispensers } from 'entities/app'


export type ArticuloControlDePlayaWithDispenser=ArticuloControlDePlaya & {
    dispenser:AppData_Dispensers
}
type DispenserProps = {
    disabled:boolean
    dispenser:ArticuloControlDePlayaWithDispenser
    delClick:(dispenser_id:number) => void
}
export const Dispenser:FC<DispenserProps> = props=>{

    return (
        <Form.Row key={props.dispenser.dispenser_id} className="dispenserItem ">
            <Col xs={1}><Button disabled={props.disabled} variant="transparent" onClick={()=>props.delClick(props.dispenser.dispenser_id)}><MdDelete color="red"/></Button></Col>
            <Col xs={1}><Image src="images/dispenser.png" height="36px" /></Col>
            <Col xs={10}>
                <Form.Label column>{`Nro: ${props.dispenser.dispenser.nroDispenser} (${props.dispenser.dispenser.marca})`}</Form.Label>
                <Form.Label column>{`Tipo: ${props.dispenser.dispenser.tipo} - Color: ${props.dispenser.dispenser.color}`}</Form.Label>
            </Col>
        </Form.Row>
    )
}