import React, { forwardRef, useImperativeHandle } from 'react'
import { useState, useCallback } from 'react'

import { ArticuloControlDePlaya } from 'entities/controlDePlaya'
import { Dispenser, ArticuloControlDePlayaWithDispenser} from './dispenser'
import * as DB from 'services/DB'

type DispenserListProps={
    disabled:boolean
}
export interface IDispenserList{
    getDispensers:()=>ArticuloControlDePlaya[]
    addDispenser:(dispenser:ArticuloControlDePlaya)=>void
}
export const DispenserList = forwardRef<IDispenserList, DispenserListProps>((props, ref)=>{
    const [ dispensers, setDispensers ] = useState<ArticuloControlDePlayaWithDispenser[]>([])

    const delClick=useCallback((dispenser_id:number)=>{
        DB.Logs.AddLog({mensaje:"Touch Quitar Dispenser", funcionalidad:"ControlDePlaya-ListDispensers"})
        setDispensers(dispensers.filter(d=>d.dispenser_id!==dispenser_id))
    },[dispensers])
    const addDispenser=useCallback((dispenser:ArticuloControlDePlaya)=>{
        setDispensers([...dispensers, dispenser as ArticuloControlDePlayaWithDispenser])
    },[dispensers])
    const getDispensers=useCallback(()=>{
        return dispensers.map(d=>d as ArticuloControlDePlaya)
    },[dispensers])
    useImperativeHandle(ref, ()=>({getDispensers, addDispenser}))
    return <>{
        dispensers.map((dispenser, index)=>(
            <Dispenser key={index} delClick={delClick} disabled={props.disabled} dispenser={dispenser} />
        ))
    }</>
})