import React, { forwardRef, useImperativeHandle }   from 'react'
import { useCallback, useRef }            from 'react'
import { Form, Button } from 'react-bootstrap'


import { ArticuloControlDePlaya } from 'entities/controlDePlaya'
import { AddDispenser, IAddDispenser } from './add'
import { DispenserList, IDispenserList } from './list'
import * as DB from 'services/DB'

type DispenserPanelProps={
    disabled:boolean
}
export interface IDispenserPanel{
    getDispensers:()=>ArticuloControlDePlaya[]
}
export const  DispenserPanel= forwardRef<IDispenserPanel, DispenserPanelProps>((props,ref)=>{
    const addRef=useRef<IAddDispenser>(null)
    const listRef=useRef<IDispenserList>(null)
    const addClick=useCallback(async()=>{
        DB.Logs.AddLog({mensaje:"Touch AddDispenser", funcionalidad:"ControlDePlaya-PanelDispensers"})
        if(!addRef.current || !listRef.current) return
        listRef.current.addDispenser(await addRef.current.new())
    },[])
    const getDispensers=useCallback(()=>{
        if(!listRef.current) return []
        return listRef.current.getDispensers()
    },[])
    useImperativeHandle(ref, ()=>({getDispensers}))
    return <>
        <Form.Row>
            <Button disabled={props.disabled} size="sm" onClick={addClick}>Agregar Dispenser</Button>
        </Form.Row>
        <AddDispenser  ref={addRef}  disabled={props.disabled} />
        <DispenserList ref={listRef} disabled={props.disabled} />
    </>
})