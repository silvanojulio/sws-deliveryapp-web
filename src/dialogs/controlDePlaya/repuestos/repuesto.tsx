import React, { FC } from 'react'
import { Form, Col } from 'react-bootstrap'
import { MdDelete }  from 'react-icons/md'

import { ArticuloControlDePlaya }   from 'entities/controlDePlaya'
import { AppData_ArticulosDeLista } from 'entities/app'

type RepuestoProps={
    disabled:boolean
    delRepuesto:()=>void
    repuesto:ArticuloControlDePlaya & {articulo:AppData_ArticulosDeLista}
}
export const Repuesto:FC<RepuestoProps> = props=>{
    return (        
        <Form.Row key={props.repuesto.articulo_id} className="repuestoItem">
            <Col xs={1}>
                <MdDelete onClick={props.disabled?undefined:props.delRepuesto} color="red"/>
            </Col>
            <Col xs={8}>{props.repuesto.articulo.nombreArticulo}</Col>
            <Col xs={3}>{props.repuesto.cantidadLlenosUni}</Col>
        </Form.Row>
    )
}