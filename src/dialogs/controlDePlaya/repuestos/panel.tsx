import { forwardRef, useImperativeHandle}   from 'react'
import { useCallback, useRef }              from 'react'
import { Form, Button,  }                   from 'react-bootstrap'

import { ArticuloControlDePlaya }           from 'entities/controlDePlaya'
import { AddRepuesto, IAddRepuesto }        from './add'
import { IRepuestosList, RepuestosList }    from './list'

import ArticulosDeLista                     from 'services/DB/articulosDeLista'
import Logs                                 from 'services/DB/logs'

type RepuestosPanelProps={
    disabled:boolean
}
export interface IRepuestosPanel{
    getRepuestos:()=>ArticuloControlDePlaya[]
}
export const RepuestosPanel=forwardRef<IRepuestosPanel, RepuestosPanelProps>((props,ref)=>{
    const { disabled } = props
    const addRepuestoRef = useRef<IAddRepuesto>(null)
    const listRepuestosRef = useRef<IRepuestosList>(null)
    const addRepuestoClick=useCallback(async()=>{
        Logs.AddLog({mensaje:"Touch AddRepuesto", funcionalidad:"ControlDePlaya-PanelRepuestos"})
        if(!addRepuestoRef.current || !listRepuestosRef.current) return
        const repDeLista    = await ArticulosDeLista.GetRepuestos()
        const repuestos     = listRepuestosRef.current.getRepuestos()
        const repuesto      = await addRepuestoRef.current.new(repDeLista.filter(rep=>!repuestos.some(crep=>crep.articulo_id===rep.id)))
        listRepuestosRef.current.addRepuesto(repuesto)
    },[])
    const getRepuestos=useCallback(()=>{
        if(!listRepuestosRef.current) return []
        return listRepuestosRef.current.getRepuestos()
    },[])
    useImperativeHandle(ref, ()=>({getRepuestos}))
    return <>
        <Form.Row>
            <Button disabled={disabled} onClick={addRepuestoClick} size="sm">Agregar Repuesto</Button>
        </Form.Row>
        <AddRepuesto ref={addRepuestoRef} disabled={disabled} />
        <RepuestosList ref={listRepuestosRef} disabled={disabled} />
    </>
})
