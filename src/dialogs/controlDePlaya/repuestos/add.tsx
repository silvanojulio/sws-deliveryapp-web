import React, { forwardRef, useImperativeHandle }   from 'react'
import { useState, useRef, useCallback }            from 'react'
import { Form, Modal, Col, Button }                 from 'react-bootstrap'
import Select from 'react-select'


import { AppData_ArticulosDeLista } from 'entities/app'
import { PromiseCallbacks } from 'types'
import { ArticuloControlDePlaya } from 'entities/controlDePlaya'

import Logs from 'services/DB/logs'


type AddRepuestoProps={
    disabled:boolean
}
export interface IAddRepuesto{
    new:(opciones:AppData_ArticulosDeLista[])=>Promise<ArticuloControlDePlaya>
}
export const AddRepuesto=forwardRef<IAddRepuesto, AddRepuestoProps>((props, ref)=>{
    const { disabled }    = props
    const [ opciones, setOpciones ] = useState<AppData_ArticulosDeLista[]>([])

    const [ callbacks, setCallbacks ] = useState<PromiseCallbacks<ArticuloControlDePlaya>>()

    const repuestoRef       = useRef<Select<{value:number, label:string, repuesto:AppData_ArticulosDeLista}>>(null)
    const cantidadRef       = useRef<HTMLInputElement>(null)
    const saveClick=useCallback(()=>{
        Logs.AddLog({mensaje:"Touch Guardar", funcionalidad:"ControlDePlaya-AddRepuestos"})
        if(!callbacks) return 
        if(!repuestoRef.current?.state.value) return callbacks.reject("Seleccione un repuesto")
        if(!cantidadRef.current?.valueAsNumber) return callbacks.reject("Ingrese la cantidad")
        const selected=repuestoRef.current.state.value
        const {repuesto}=Array.isArray(selected)?selected[0]:selected
        const articulo:ArticuloControlDePlaya = {
            articulo:repuesto,
            articulo_id:repuesto.id,
            cantidadFalladosPack:0,
            cantidadFalladosUni:0,
            cantidadVaciosPack:0,
            cantidadVaciosUni:0,
            cantidadLlenosPack:0,
            cantidadLlenosUni:cantidadRef.current.valueAsNumber,
            dispenser:null,
            dispenser_id:0
        }
        Logs.AddLog({mensaje:`cantidad:${articulo.cantidadLlenosUni}`, funcionalidad:"ControlDePlaya-AddRepuestos", entidad:"Repuesto",idEntidad:articulo.articulo_id})
        callbacks.resolve(articulo)
    },[callbacks])
    const cancelClick=useCallback(()=>{
        Logs.AddLog({mensaje:"Touch Cancelar", funcionalidad:"ControlDePlaya-AddRepuestos"})
        if(!callbacks) return
        callbacks.reject()
    },[callbacks])
    useImperativeHandle(ref, ()=>({
        new:(opciones:AppData_ArticulosDeLista[])=>{
            Logs.AddLog({mensaje:"Iniciando AddRepuestos", funcionalidad:"ControlDePlaya-AddRepuestos"})
            const p = new Promise<ArticuloControlDePlaya>((resolve, reject)=>setCallbacks({resolve, reject}))
            p.finally(()=>{
                Logs.AddLog({mensaje:"Finalizando AddRepuestos", funcionalidad:"ControlDePlaya-AddRepuestos"})
                setCallbacks(undefined)
            })
            setOpciones(opciones)
            return p
        }
    })) 
    return (
        <>
            <Modal show={!!callbacks} backdrop="static" centered keyboard={false} className="AddArticuloModal">
                <Modal.Header><Modal.Title>Agregar Repuesto</Modal.Title></Modal.Header>
                <Modal.Body>
                    <Form.Row>
                        <Col xs={3}><Form.Label>Articulo</Form.Label></Col>
                        <Col xs={9}><Select<{value:number, label:string, repuesto:AppData_ArticulosDeLista}> 
                            className="select" size="sm" isDisabled={disabled}
                            ref={repuestoRef}
                            options={opciones.map(op=>({value:op.id, label:op.nombreArticulo, repuesto:op}))}
                            
                        /></Col>
                    </Form.Row>
                    <Form.Row>
                        <Col xs={3}><Form.Label>Cantidad</Form.Label></Col>
                        <Col xs={9}><Form.Control type="number" ref={cantidadRef} /></Col>
                    </Form.Row>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" disabled={disabled} onClick={cancelClick}>Cancelar</Button>
                    <Button variant="primary"   disabled={disabled} onClick={saveClick} >Agregar</Button>
                </Modal.Footer>
            </Modal>
        </>
    )
})