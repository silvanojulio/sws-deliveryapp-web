import React, { forwardRef, useImperativeHandle }   from 'react'
import { useCallback, useState }                    from 'react'

import { AppData_ArticulosDeLista } from 'entities/app'
import { ArticuloControlDePlaya }   from 'entities/controlDePlaya'
import { Repuesto }                 from './repuesto'

import Logs from 'services/DB/logs'

type ArticuloControlDePlayaWithArticulo=ArticuloControlDePlaya & {
    articulo:AppData_ArticulosDeLista
}
export interface IRepuestosList{
    addRepuesto:(articulo:ArticuloControlDePlaya)=>void
    getRepuestos:()=>ArticuloControlDePlaya[]
}
type RepuestosListProps={
    disabled:boolean
}
export const RepuestosList=forwardRef<IRepuestosList, RepuestosListProps> ((props,ref)=>{
    const [ repuestos, setRepuestos] = useState<ArticuloControlDePlayaWithArticulo[]>([])
    const addRepuesto=useCallback((repuesto:ArticuloControlDePlaya)=>{
        setRepuestos([...repuestos, repuesto as ArticuloControlDePlayaWithArticulo])
    },[repuestos])
    const getRepuestos=useCallback(()=>{
        return repuestos.map<ArticuloControlDePlaya>(rep=>rep as ArticuloControlDePlaya)
    },[repuestos])
    const delRepuesto=useCallback((articulo_id:number)=>{
    Logs.AddLog({mensaje:"Touch QuitarRepuesto", funcionalidad:"ControlDePlaya-ListRepuestos", entidad:"Repuesto",idEntidad:articulo_id})
    setRepuestos(repuestos.filter(rep=>rep.articulo_id!==articulo_id))
    },[repuestos])
    useImperativeHandle(ref, ()=>({addRepuesto, getRepuestos}))
    return <>
        {repuestos.map(rep=>(  
            <Repuesto key={rep.articulo_id} disabled={props.disabled} repuesto={rep} delRepuesto={()=>delRepuesto(rep.articulo_id)}/>
        ))}
    </>
})