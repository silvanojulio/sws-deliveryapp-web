import { forwardRef,  useImperativeHandle }  from 'react'
import { useState, useCallback, useRef }            from 'react'
import { Modal, Button, Form, Spinner }             from 'react-bootstrap'
import { Tabs, Tab }                                from 'react-bootstrap'
import { FaTruckLoading }                           from 'react-icons/fa'

import { ControlDePlaya as CDP }        from 'entities/controlDePlaya'

import ControlesDePlayaService          from 'services/controlDePlaya'
import { useAppContext }                from 'hooks'

import { PromiseCallbacks }             from 'types'
import { SesionControladorPlaya }       from 'dialogs'
import { ISesionControladorPlaya }      from 'dialogs'
import * as DB                          from 'services/DB'

import { ArticulosPanel, IArticulosPanel } from './articulos/panel'
import { RepuestosPanel, IRepuestosPanel } from './repuestos/panel'
import { DispenserPanel, IDispenserPanel } from './dispenser/panel'

export interface IControlDePlaya {
    new:(usuario_id: number, hojaDeRutaID:number)=>Promise<void>
}
export const ControlDePlaya = forwardRef<IControlDePlaya>((_props, ref)=>{
    const { ToastSuccess }  = useAppContext()
    const [ callbacks, setCallbacks ]   = useState<PromiseCallbacks<void>>()
    const [ bussy, setBussy ]           = useState<boolean>(false)
    const [ control, setControl ]       = useState<CDP>({
        hojaDeRutaId:0,
        articulos:[],
        dispensersIds:[],
        kilometros:0,
        tipoEventoId:1,
        usuarioControlaId:0
    })
    const panelArticulosRef             = useRef<IArticulosPanel>(null)
    const panelRepuestosRef             = useRef<IRepuestosPanel>(null)
    const panelDispenserRef             = useRef<IDispenserPanel>(null)
    const sesionControladorPlayaRef     = useRef<ISesionControladorPlaya>(null)
    const kilometrosRef                 = useRef<HTMLInputElement>(null)

    useImperativeHandle(ref, ()=>({
        new:async (usuario_id:number, hojaDeRutaID:number)=>{
            DB.Logs.AddLog({mensaje:"Iniciando Control De Playa", funcionalidad:"ControlDePlaya", entidad:"hojaDeRuta", idEntidad:hojaDeRutaID})
            const p=new Promise<void>((resolve, reject)=>setCallbacks({resolve, reject}))
            p.finally(()=>{
                DB.Logs.AddLog({mensaje:"Finalizando Control De Playa", funcionalidad:"ControlDePlaya", entidad:"hojaDeRuta", idEntidad:hojaDeRutaID})
                setCallbacks(undefined)
            })
            setControl({
                hojaDeRutaId:hojaDeRutaID,
                articulos:[],
                dispensersIds:[],
                kilometros:0,
                tipoEventoId:1,
                usuarioControlaId:usuario_id
            })
            if(kilometrosRef.current) kilometrosRef.current.valueAsNumber=0
            return p
        }
    }))
    const saveClick=useCallback(async()=>{
        DB.Logs.AddLog({mensaje:"Touch Guardar", funcionalidad:"ControlDePlaya", entidad:"hojaDeRuta", idEntidad:control.hojaDeRutaId})
        if(!control || !callbacks || !sesionControladorPlayaRef.current) return
        setBussy(true)
        try{
            const aprobador=await sesionControladorPlayaRef.current.get()
            control.kilometros=kilometrosRef.current?.valueAsNumber??0
            control.articulos=[
                ...panelArticulosRef.current?.GetArticulos()??[],
                ...panelRepuestosRef.current?.getRepuestos()??[],
                ...panelDispenserRef.current?.getDispensers()??[]
            ]
            await ControlesDePlayaService.GuardarNuevoControlMobile(control, aprobador.tokenValido)
            setBussy(false)
            ToastSuccess("Control de playa enviado correctamente",{autoClose:3000})
            callbacks.resolve()
            setCallbacks(undefined)
        }catch(error){
        }finally{
            setBussy(false)
        }
    },[control, ToastSuccess, callbacks])
    return (
        <Modal className="control-de-playa-modal" show={!!callbacks} size="xl" backdrop="static" centered keyboard={false}>
            <SesionControladorPlaya ref={sesionControladorPlayaRef}/>
            <Modal.Header>  
                <Modal.Title><FaTruckLoading/> Control De Playa</Modal.Title>                    
            </Modal.Header>
            <Modal.Body> 
                <Form.Row className="justify-content-around d-flex">
                    <Form.Check name="tipoEventoId" inline type="radio" disabled={bussy}
                        label="Salida" 
                        onClick={()=>{
                            DB.Logs.AddLog({mensaje:"Touch Salida", funcionalidad:"ControlDePlaya", entidad:"hojaDeRuta", idEntidad:control.hojaDeRutaId})
                            setControl({...control, tipoEventoId:1} as CDP)
                        }}
                        defaultChecked />
                    <Form.Check name="tipoEventoId" inline type="radio" disabled={bussy}
                        label="Entrada" 
                        onClick={()=>{
                            DB.Logs.AddLog({mensaje:"Touch Entrada", funcionalidad:"ControlDePlaya", entidad:"hojaDeRuta", idEntidad:control.hojaDeRutaId})
                            setControl({...control, tipoEventoId:2} as CDP)
                        }}
                        />
                </Form.Row>
                <Form.Row>
                    <Form.Label column="sm" xs={4}>Kilometros</Form.Label>
                    <Form.Control size="sm" name="kilometros" type="number" 
                        disabled={bussy} 
                        ref={kilometrosRef} 
                    />
                </Form.Row>
                <Form.Row>
                    <Tabs defaultActiveKey="articulos">
                        <Tab disabled={bussy} eventKey="articulos" title="Articulos" className="ArticuloListItems">                            
                            <ArticulosPanel 
                                bussy={bussy}
                                tipoEventoId={control.tipoEventoId}
                                ref={panelArticulosRef}
                            />
                        </Tab>
                        <Tab disabled={bussy} eventKey="dispensers" title="Dispensers" className="DispenserListItems">
                            <DispenserPanel disabled={bussy} ref={panelDispenserRef} />
                        </Tab>
                        <Tab disabled={bussy} eventKey="repuestos" title="Repuestos" className="RepuestosListItems">
                            <RepuestosPanel disabled={bussy} ref ={panelRepuestosRef} />
                        </Tab>
                    </Tabs>
                </Form.Row>
            </Modal.Body>
            <Modal.Footer>
                <Button 
                    disabled={bussy} 
                    onClick={()=>{
                        if(callbacks) callbacks.resolve()
                    }}
                >Cancelar</Button>
                <Button disabled={bussy} onClick={saveClick}>
                    {bussy && <Spinner animation="border" size="sm" />}Guardar
                </Button>
            </Modal.Footer>
        </Modal>
    )
})