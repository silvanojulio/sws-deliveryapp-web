import { forwardRef, useImperativeHandle }      from 'react'
import { useState, useCallback }                from 'react'
import {Form, Col }                             from 'react-bootstrap'
import { BsFillStopFill, BsFillGrid3X2GapFill } from 'react-icons/bs'

import { ArticuloControlDePlaya }   from 'entities/controlDePlaya'
import { Articulo }                 from './articulo'
import * as DB from 'services/DB'

type ArticulosListProps={
    bussy:boolean
    tipoEventoId:number
}
export interface IArticulosList{
    GetArticulos:()=>ArticuloControlDePlaya[]
    addArticulo:(articulo:ArticuloControlDePlaya)=>void
}
export const ArticulosList = forwardRef<IArticulosList, ArticulosListProps>((props, ref)=>{
    const [ articulos, setArticulos ]   = useState<ArticuloControlDePlaya[]>([])
    const addArticulo = useCallback((articulo:ArticuloControlDePlaya)=>{
        setArticulos([...articulos, articulo])
    },[articulos])
    const delArticulo = useCallback((articulo_id:number)=>{
        DB.Logs.AddLog({mensaje:"Quitar Articulo", funcionalidad:"ControlDePlaya-ListArticulos", entidad:"Articulo",idEntidad:articulo_id})
        setArticulos([...articulos.filter(art=>art.articulo_id!==articulo_id)])
    },[articulos])
    const GetArticulos=useCallback(()=>articulos, [articulos])
    useImperativeHandle(ref, ()=>({
        GetArticulos, addArticulo
    }))
    return (     
        <>        
            <Form.Row>
                <Col xs={{offset:8, span:4}}>
                    <Form.Row className="titulos">
                        <Col xs={{offset:4,span:4}}><BsFillStopFill/></Col> 
                        <Col xs={{offset:0,span:4}}><BsFillGrid3X2GapFill/></Col>                        
                    </Form.Row>
                </Col>
            </Form.Row>                    
            {articulos.map((art, index)=>(
                <Articulo 
                    disabled={props.bussy}
                    articulo={art} 
                    tipoEventoId={props.tipoEventoId} 
                    key={index} 
                    delArticuloClick={()=>delArticulo(art.articulo_id)}
                />
            ))}
        </>
    )
})