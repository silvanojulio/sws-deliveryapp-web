import React, { forwardRef, useImperativeHandle }   from 'react'
import { useCallback, useRef }                      from 'react'
import { Form, Button }                             from 'react-bootstrap'

import { ArticuloControlDePlaya }           from 'entities/controlDePlaya'
import { ArticulosDeLista }                 from 'services/DB/articulosDeLista'
import * as DB from 'services/DB'
import { AddArticulo, IAddArticulo}         from './add'
import { ArticulosList, IArticulosList }    from './list'

export interface IArticulosPanel{
    GetArticulos:()=>ArticuloControlDePlaya[]
}
type ArticulosPanelProps={
    tipoEventoId:number
    bussy:boolean
}
export const ArticulosPanel = forwardRef<IArticulosPanel, ArticulosPanelProps>((props, ref)=>{
    const addArticuloRef                = useRef<IAddArticulo>(null)
    const listArticulosRef              = useRef<IArticulosList>(null)
    useImperativeHandle(ref,()=>({
        GetArticulos:()=>listArticulosRef.current?.GetArticulos()??[]
    }))
    const addArticuloClick=useCallback(async()=>{
        DB.Logs.AddLog({mensaje:"Touch AddArticulo", funcionalidad:"ControlDePlaya-PanelArticulos"})
        if(!addArticuloRef.current) return 
        const artDeLista  = await ArticulosDeLista.GetArticulos()
        const articulos = listArticulosRef.current?.GetArticulos()??[]
        const newArticulo = await addArticuloRef.current.new(artDeLista.filter(art=>!articulos.some(cart=>art.id===cart.articulo_id)),props.tipoEventoId)
        listArticulosRef.current?.addArticulo(newArticulo)
    },[props])
    return (
        <>
            <Form.Row>
                <AddArticulo ref={addArticuloRef} />
                <Button disabled={props.bussy} size="sm" onClick={addArticuloClick}>Agregar Articulo</Button>
            </Form.Row>
            <ArticulosList 
                bussy={props.bussy}
                tipoEventoId={props.tipoEventoId}
                ref={listArticulosRef}
            />
        </>
    )
})