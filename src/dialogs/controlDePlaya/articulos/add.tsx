import React, { forwardRef, useImperativeHandle }       from 'react'
import { useState, useRef, useCallback }                    from 'react'
import Select                                               from 'react-select'
import { Form, Col, Button, Modal }                         from 'react-bootstrap'
import { FcHighPriority, FcLowPriority, FcMediumPriority }  from 'react-icons/fc'
import { BsFillGrid3X2GapFill, BsFillStopFill }             from 'react-icons/bs'

import { AppData_ArticulosDeLista }     from 'entities/app'
import { ArticuloControlDePlaya }       from 'entities/controlDePlaya'
import { NumericField, INumericField }  from 'components/numericField'
import { useAppContext }                from 'hooks'
import { PromiseCallbacks }             from 'types'
import { SelectOption }                 from 'types'
import * as DB from 'services/DB'



type ArticuloOption=SelectOption<AppData_ArticulosDeLista>

type AddArticuloProps={
}
export interface IAddArticulo{
    new:(opciones: AppData_ArticulosDeLista[], tipoEventoId: number)=>Promise<ArticuloControlDePlaya>
}
export const AddArticulo=forwardRef<IAddArticulo,AddArticuloProps> ((props,ref)=>{
    const { ToastError }                        = useAppContext()

    const [ opciones, setOpciones ]             = useState<ArticuloOption[]>([])
    const [ selected, setSelected ]             = useState<ArticuloOption | null>(null)
    const [ tipoEventoId, setTipoEventoId ]     = useState<number>()
    const [ callbacks, setCallbacks ]           = useState<PromiseCallbacks<ArticuloControlDePlaya>>()

    useImperativeHandle(ref, ()=>({
        new:async (opciones: AppData_ArticulosDeLista[], tipoEventoId: number)=>{
            DB.Logs.AddLog({mensaje:"Iniciando AddArticulo", funcionalidad:"ControlDePlaya-AddArticulos"})
            const p = new Promise<ArticuloControlDePlaya>((resolve, reject)=>setCallbacks({resolve, reject}))
            p.finally(()=>{
                DB.Logs.AddLog({mensaje:"Finalizando AddArticulo", funcionalidad:"ControlDePlaya-AddArticulos"})
                setCallbacks(undefined)
            })
            setOpciones(opciones.map<ArticuloOption>(op=>({value:op,label:op.nombreArticulo})))
            setTipoEventoId(tipoEventoId)
            setSelected(null)
            return p
        }
    }))

    const art           = useRef<Select<ArticuloOption>>(null)
    const llenosUni     = useRef<INumericField>(null)
    const llenosPack    = useRef<INumericField>(null)
    const vaciosUni     = useRef<INumericField>(null)
    const vaciosPack    = useRef<INumericField>(null)
    const falladosUni   = useRef<INumericField>(null)
    const falladosPack  = useRef<INumericField>(null)

    const handleSave=useCallback(()=>{
        DB.Logs.AddLog({mensaje:"Touch Agregar", funcionalidad:"ControlDePlaya-AddArticulos", entidad:"Articulo",idEntidad:selected?.value.id})
        if(!callbacks) return
        if(!selected) return ToastError("Seleccione un Articulo", {autoClose:2000})
        if(
            (llenosUni.current?.getValue()??0)+(llenosPack.current?.getValue()??0)+
            (vaciosUni.current?.getValue()??0)+(vaciosPack.current?.getValue()??0)+
            (falladosUni.current?.getValue()??0)+(falladosPack.current?.getValue()??0)===0
        ) return ToastError("No ha ingresado cantidad en ningun campo", {autoClose:2000})
        const articulo:ArticuloControlDePlaya = {
            articulo:selected.value,
            articulo_id:selected.value.id,
            dispenser:null,
            dispenser_id:0,
            cantidadLlenosUni:llenosUni.current?.getValue()??0,
            cantidadLlenosPack:llenosPack.current?.getValue()??0,
            cantidadVaciosUni:vaciosUni.current?.getValue()??0,
            cantidadVaciosPack:vaciosPack.current?.getValue()??0,
            cantidadFalladosUni:falladosUni.current?.getValue()??0,
            cantidadFalladosPack:falladosPack.current?.getValue()??0,
        }
        DB.Logs.AddLog({mensaje:`cantidadLlenosUni:${articulo.cantidadLlenosUni}`, funcionalidad:"ControlDePlaya-AddArticulos", entidad:"Articulo",idEntidad:articulo.articulo_id})
        DB.Logs.AddLog({mensaje:`cantidadLlenosPack:${articulo.cantidadLlenosPack}`, funcionalidad:"ControlDePlaya-AddArticulos", entidad:"Articulo",idEntidad:articulo.articulo_id})
        DB.Logs.AddLog({mensaje:`cantidadVaciosUni:${articulo.cantidadVaciosUni}`, funcionalidad:"ControlDePlaya-AddArticulos", entidad:"Articulo",idEntidad:articulo.articulo_id})
        DB.Logs.AddLog({mensaje:`cantidadVaciosPack:${articulo.cantidadVaciosPack}`, funcionalidad:"ControlDePlaya-AddArticulos", entidad:"Articulo",idEntidad:articulo.articulo_id})
        DB.Logs.AddLog({mensaje:`cantidadFalladosUni:${articulo.cantidadFalladosUni}`, funcionalidad:"ControlDePlaya-AddArticulos", entidad:"Articulo",idEntidad:articulo.articulo_id})
        DB.Logs.AddLog({mensaje:`cantidadFalladosPack:${articulo.cantidadFalladosPack}`, funcionalidad:"ControlDePlaya-AddArticulos", entidad:"Articulo",idEntidad:articulo.articulo_id})
        callbacks.resolve(articulo)
    },[ callbacks, ToastError, selected])
    const handleClose=useCallback(()=>{
        DB.Logs.AddLog({mensaje:"Touch Cancelar", funcionalidad:"ControlDePlaya-AddArticulos", entidad:"Articulo",idEntidad:selected?.value.id})
        if(callbacks) callbacks.reject()
    },[callbacks, selected])
    return <>
        <Modal show={!!callbacks} backdrop="static" centered keyboard={false} className="AddArticuloModal">
            <Modal.Header><Modal.Title>Agregar Articulo</Modal.Title></Modal.Header>
            <Modal.Body>
                <Form.Row >
                    <Col xs={3}>Articulo</Col>
                    <Col xs={9}>
                        <Select<ArticuloOption> size="sm" className="select"
                            id="select_articulo"
                            ref={art}
                            options={opciones}
                            value={selected}
                            onChange={op=>{
                                DB.Logs.AddLog({mensaje:"Select Articulo", funcionalidad:"ControlDePlaya-AddArticulos", entidad:"articulo",idEntidad:op?.value.id})
                                setSelected(op)
                            }}
                        />
                    </Col>
                </Form.Row>
                {selected && <>
                    <Form.Row className="titulos">
                        <Col xs={{offset:selected.value.cantidadPorPack===1?8:4, span:4}}><BsFillStopFill        className="text-primary"/> Unidades</Col>
                        {selected.value.cantidadPorPack!==1 && <Col xs={{offset:0, span:4}} ><BsFillGrid3X2GapFill  className="text-primary"/> Packs</Col>}
                    </Form.Row>
                    <Form.Row className="dataFields llenos">
                        <Col xs={selected.value.cantidadPorPack===1?8:4}><FcLowPriority/> Llenos</Col>
                        <NumericField ref={llenosUni}
                            colProps={{xs:4}} 
                            formControlProps={{min:0}}
                        />
                        {selected?.value.cantidadPorPack===1 ||
                            <NumericField ref={llenosPack}
                                colProps={{xs:4}} 
                                formControlProps={{min:0}}
                            />
                        }
                    </Form.Row>
                    {tipoEventoId!==1 && <>
                        {selected.value.tipoDeEnvase_ids!==1 || 
                            <Form.Row className="dataFields vacios">
                                <Col xs={selected.value.cantidadPorPack===1?8:4}><FcMediumPriority/> Vacios</Col>
                                <NumericField ref={vaciosUni}
                                    colProps={{xs:4}} 
                                    formControlProps={{min:0}}
                                />
                                {selected.value.cantidadPorPack===1 ||
                                    <NumericField ref={vaciosPack}
                                        colProps={{xs:4}} 
                                        formControlProps={{min:0}}
                                    />
                                }
                            </Form.Row>
                        }
                        <Form.Row className="dataFields fallados">
                            <Col xs={selected.value.cantidadPorPack===1?8:4}><FcHighPriority/> Fallados</Col>
                            <NumericField ref={falladosUni}
                                colProps={{xs:4}} 
                                formControlProps={{min:0}}
                            />
                            {selected.value.cantidadPorPack===1 ||
                                <NumericField ref={falladosPack}
                                    colProps={{xs:4}} 
                                    formControlProps={{min:0}}
                                />
                            }
                        </Form.Row>
                    </>}
                </>}
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={handleClose}>Cancelar</Button>
                <Button onClick={handleSave}>Agregar</Button>
            </Modal.Footer>
        </Modal>
    </>
})