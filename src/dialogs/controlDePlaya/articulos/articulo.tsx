import React, { FC }            from 'react'
import { Form, Col, Button }    from 'react-bootstrap'

import { ArticuloControlDePlaya }               from 'entities/controlDePlaya'
import { MdDelete }                             from 'react-icons/md'
import { FcHighPriority, FcMediumPriority, FcLowPriority }  from 'react-icons/fc'

type ArticuloProps={
    articulo:ArticuloControlDePlaya
    tipoEventoId:number
    disabled:boolean
    delArticuloClick:()=>void
}
export const Articulo:FC<ArticuloProps> = (props)=>{
    const { articulo, tipoEventoId, disabled }  = props
    const { delArticuloClick }                  = props
    return (
        <Form.Row className="listItem">
            <Col xs={8}>
                <Button disabled={disabled} variant="transparent" onClick={delArticuloClick}><MdDelete color="red"/></Button>{articulo.articulo?.nombreArticulo}                
            </Col>
            <Col xs={4} className="cantidades">
                <Form.Row className="llenos">
                    <Col xs={4}><FcLowPriority/></Col>
                    <Col xs={4}>{articulo.cantidadLlenosUni}</Col>
                    <Col xs={4}>{articulo.cantidadLlenosPack}</Col>
                </Form.Row>
                {tipoEventoId!==1 && <>
                    <Form.Row className="vacios">
                        <Col xs={4}><FcMediumPriority/></Col>
                        <Col xs={4}>{articulo.cantidadVaciosUni}</Col>
                        <Col xs={4}>{articulo.cantidadVaciosPack}</Col>
                    </Form.Row>
                    <Form.Row className="fallados">
                        <Col xs={4}><FcHighPriority/></Col>
                        <Col xs={4}>{articulo.cantidadFalladosUni}</Col>
                        <Col xs={4}>{articulo.cantidadFalladosPack}</Col>
                    </Form.Row>
                </>}
            </Col>
        </Form.Row>
    )
}