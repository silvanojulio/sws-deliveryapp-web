import { forwardRef, useImperativeHandle }  from 'react'
import { useState, useCallback, useRef }    from 'react'
import { Modal, Form, Button }              from 'react-bootstrap'


import { PromiseCallbacks } from 'types'
import * as DB from 'services/DB'

export interface IDeclaracionTarjetas{
    total:(montoDefault?:number)=>Promise<void>
}
export const DeclaracionTarjetas = forwardRef<IDeclaracionTarjetas>((_,ref)=>{
    const [ monto, setMonto ] = useState<number>(0)
    const [ callbacks, setCallbacks] = useState<PromiseCallbacks<void>>()
    const montoRef = useRef<HTMLInputElement>(null)
    const total = useCallback(async(monto?:number)=>{
        setMonto(monto??0)
        const p = new Promise<void>((resolve,reject)=>{
            setCallbacks({resolve, reject})
        })
        p.finally(()=>{
            setCallbacks(undefined)
        })
        return p
    },[])
    useImperativeHandle(ref, ()=>({total}),[total])
    const guardar = useCallback(async ()=>{
        if(!callbacks) return
        const id=(await DB.Declaraciones.GetAll()).find(d=>d.tipo===3)?.id
        await DB.Declaraciones.Upsert({
            id,
            tipo:DB.Declaraciones.T_Tarjetas,
            monto
        })
        callbacks.resolve()
    },[callbacks, monto])
    return (
        <Modal size="sm" show={!!callbacks} backdrop="static" centered keyboard={false} className="declaracionTarjetas">
            <Modal.Header>Declaración de Cobro Total con Tarjetas</Modal.Header>
            <Modal.Body>
                <Form.Row>
                    <Form.Label column xs={6}>Monto total cobrado:</Form.Label>
                    <Form.Label column xs={6}>
                        <Form.Control type="number" size="sm"
                            ref={montoRef}
                            onChange={()=>{
                                if(!montoRef.current) return
                                const monto=montoRef.current.valueAsNumber
                                if(!isNaN(monto)) {}
                                montoRef.current.value=isNaN(monto)?"0":monto.toString()
                                setMonto(isNaN(monto)?0:monto)
                            }}
                            value={monto}
                        />
                    </Form.Label>
                </Form.Row>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={guardar}>Guardar</Button>
            </Modal.Footer>
        </Modal>
    )
})