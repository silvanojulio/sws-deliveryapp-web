import { forwardRef, useImperativeHandle }  from 'react'
import { useState, useCallback }            from 'react'
import { Modal, Card, Form, Button }        from 'react-bootstrap'

import { NumericField }                     from 'components'

import * as DB from 'services/DB'
import { useHojaDeRutaContext } from 'hooks'

import { AppData_DeclaracionesEfectivo }    from 'entities/app'
import { PromiseCallbacks }                 from 'types'


export interface IDeclaracionEfectivo{
    show:()=>Promise<void>
    total:number
}
export const DeclaracionEfectivo=forwardRef<IDeclaracionEfectivo>((_,ref)=>{
    const { hojaDeRuta } = useHojaDeRutaContext()
    const [ callbacks, setCallbacks ]           = useState<PromiseCallbacks<void>>()
    const [ declaraciones, setDeclaraciones ]   = useState<AppData_DeclaracionesEfectivo[]>([])
    const [ total, setTotal ]                   = useState<number>(0)
    const [ esMoneda, setEsMoneda ]             = useState<boolean>(false)

    
    const loadDeclaraciones = useCallback(async()=>{
        if(!hojaDeRuta) return
        const declaraciones=(await DB.DeclaracionesEfectivo.GetAll())
                            .map(declaracion=>declaracion.valorNominal<10
                                                ?{...declaracion,esMoneda:true}
                                                :declaracion
                                )
                            .sort((a,b)=>a.valorNominal-b.valorNominal)
        setTotal(declaraciones.reduce((t,d)=>t+d.cantidad*d.valorNominal,0))
        setDeclaraciones(declaraciones)
    },[hojaDeRuta])
    const show = useCallback(async()=>{
        const p = new Promise<void>((resolve, reject)=>{
            setCallbacks({resolve, reject})
            loadDeclaraciones()
        })
        p.finally(()=>{
            setCallbacks(undefined)
        })
        return p
    },[loadDeclaraciones])
    useImperativeHandle(ref,()=>({show, total}),[show, total])
    const cerrar = useCallback(async()=>{
        if(!!callbacks) callbacks.resolve()
    },[callbacks])
    const updateCantidad=useCallback(async(id:number, cantidad:number)=>{
        await DB.DeclaracionesEfectivo.Update(id, cantidad)
        loadDeclaraciones()
    },[loadDeclaraciones])
    return (
        <Modal size="sm" onHide={cerrar} show={!!callbacks} backdrop="static" centered keyboard={false} className="declaracionEfectivo">
            <Modal.Header closeButton>Declaración de efectivo</Modal.Header>
            <Modal.Body>
                <Form.Row>
                    <Button variant="primary" size="sm" onClick={()=>setEsMoneda(false)} disabled={!esMoneda} >Billetes</Button>
                    <Button variant="info"    size="sm" onClick={()=>setEsMoneda(true)}  disabled={esMoneda } >Monedas</Button>
                </Form.Row>
                {
                    declaraciones
                    .filter(declaracion=>esMoneda === declaracion.esMoneda)
                    .map(declaracion=>(
                        <Card key={declaracion.id}>
                            <Form.Row>
                                <Form.Label column xs={{offset:1,span:1}}>$</Form.Label>
                                <Form.Label column xs={{offset:0,span:3}}>{declaracion.valorNominal.toFixed(2)}</Form.Label>
                                <NumericField 
                                    colProps={{xs:{offset:1,span:5}}}
                                    defaultValue={declaracion.cantidad} 
                                    onChange={value=>updateCantidad(declaracion.id,value)}
                                />
                            </Form.Row>
                        </Card>
                    ))
                }
            </Modal.Body>
            <Modal.Footer><Button onClick={cerrar}>Cerrar</Button></Modal.Footer>
        </Modal>
    )
})