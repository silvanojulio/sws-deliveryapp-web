import { forwardRef, useImperativeHandle }  from 'react'
import { useState, useCallback, useRef }    from 'react'
import { Modal, Form, Button }              from 'react-bootstrap'
import { FaPencilAlt }                      from 'react-icons/fa'

import { DeclaracionEfectivo,    IDeclaracionEfectivo    }  from './declaracionEfectivo'
import { DeclaracionCheques,     IDeclaracionCheques     }  from './declaracionCheques'
import { DeclaracionTarjetas,    IDeclaracionTarjetas    }  from './declaracionTarjetas'
import { DeclaracionRetenciones, IDeclaracionRetenciones }  from './declaracionRetenciones'
import { GastosDeReparto, IGastosDeReparto }                from 'dialogs/gastosDeReparto'

import { AppData_Declaraciones }            from 'entities/app'
import { AppData_GastosDeReparto }          from 'entities/app'
import { AppData_DeclaracionesEfectivo }    from 'entities/app'
import { PromiseCallbacks }                 from 'types'
import * as DB                              from 'services/DB'
import { useAppContext } from 'hooks'

export interface ICierreDeRuta{
    cerrarRuta:()=>Promise<boolean>
}
export const CierreDeRuta = forwardRef<ICierreDeRuta>((_,ref)=>{
    const { ToastInfo }                         = useAppContext()
    const [ callbacks,     setCallbacks     ]   = useState<PromiseCallbacks<boolean>>()
    const [ declaraciones, setDeclaraciones ]   = useState<AppData_Declaraciones[]>([])
    const [ declaEfectivo, setDeclaEfectivo ]   = useState<AppData_DeclaracionesEfectivo[]>([])
    const [ declaGastos,   setDeclaGastos   ]   = useState<AppData_GastosDeReparto[]>([])
    const [ validado,      setValidado      ]   = useState<boolean>()
    const declaracionesEfectivo     = useRef<IDeclaracionEfectivo>(null)
    const declaracionesCheques      = useRef<IDeclaracionCheques>(null)
    const declaracionesTarjetas     = useRef<IDeclaracionTarjetas>(null)
    const declaracionesRetenciones  = useRef<IDeclaracionRetenciones>(null)
    const gastosDeRepartoDialog     = useRef<IGastosDeReparto>(null)
    const cancelar = useCallback(async()=>{
        if(!!callbacks) callbacks.resolve(false)
    },[callbacks])
    const loadData   = useCallback(async()=>{
        setDeclaGastos(await DB.GastosDeReparto.getGastosDeReparto())
        setDeclaEfectivo(await DB.DeclaracionesEfectivo.GetAll())
        setDeclaraciones(await DB.Declaraciones.GetAll())
        setValidado(undefined)
    },[])
    const cerrarRuta = useCallback(async()=>{
        const p = new Promise<boolean>((resolve, reject)=>{
            setCallbacks({resolve, reject})
            loadData()
        })
        p.finally(()=>{
            setCallbacks(undefined)
        })
        return p
    },[loadData])
    const validacion = useCallback(async()=>{

        /***************************
         * Verificacion de Efectivo Desactivada
         * Solicitud de Juan, validado por Ignacio
         * 26/05/2021
         */
        const items = await DB.ItemsDeRecibos.GetAll()
        //const totalCobrosEfectivo       = items.filter(i=>i.formaDePagoId===1).reduce((t,i)=>t+i.importe,0)
        const totalCobrosCheques        = items.filter(i=>i.formaDePagoId===2).reduce((t,i)=>t+i.importe,0)
        const totalCobrosRetenciones    = items.filter(i=>i.formaDePagoId===9).reduce((t,i)=>t+i.importe,0)
        const totalCobrosTarjetas       = items.filter(i=>i.formaDePagoId===10 || i.formaDePagoId===8).reduce((t,i)=>t+i.importe,0)
        
        const totalDeclaCheques     = (await DB.Declaraciones.GetTipo(DB.Declaraciones.T_Cheque)).reduce((t,c)=>t+c.monto,0)
        const totalDeclaReten       = (await DB.Declaraciones.GetTipo(DB.Declaraciones.T_Retencion)).reduce((t,c)=>t+c.monto,0)
        const totalDeclaTarjeta     = (await DB.Declaraciones.GetTipo(DB.Declaraciones.T_Tarjetas)).reduce((t,c)=>t+c.monto,0)
        //const totalDeclaEfectivo    = (await DB.DeclaracionesEfectivo.GetAll()).reduce((t,d)=>t+(d.valorNominal*d.cantidad),0)
        //const totalDeclaGastos      = (await DB.GastosDeReparto.getGastosDeReparto()).reduce((t,g)=>t+g.montoGasto,0)
        
        

        //if(totalCobrosEfectivo - totalDeclaGastos !== totalDeclaEfectivo) ToastInfo("Tiene diferencia en Efectivo")
        if(totalCobrosRetenciones !== totalDeclaReten) ToastInfo("Tiene diferencia en Retenciones")
        if(totalCobrosCheques !== totalDeclaCheques) ToastInfo("Tiene diferencia en Cheques")
        if(totalCobrosTarjetas!== totalDeclaTarjeta) ToastInfo("Tiene diferencia en Total de Tarjetas")
        
        setValidado(
            //totalCobrosEfectivo - totalDeclaGastos  === totalDeclaEfectivo  &&
            totalCobrosRetenciones                  === totalDeclaReten     &&
            totalCobrosCheques                      === totalDeclaCheques   && 
            totalCobrosTarjetas                     === totalDeclaTarjeta
        ) 

    },[ToastInfo])
    const continuar  = useCallback(async()=>{
        if(!callbacks) return
        await DB.HojasDeRuta.CerrarHojaDeRutaActual()
        callbacks.resolve(true)
    },[callbacks])
    useImperativeHandle(ref,()=>({cerrarRuta}),[cerrarRuta])
    //#region Show Handlers
    const showDeclaracionesEfectivo=useCallback(async()=>{
        if(!declaracionesEfectivo.current) return
        await declaracionesEfectivo.current.show()
        loadData()
    },[loadData])
    const showDeclaracionesCheques=useCallback(async()=>{
        if(!declaracionesCheques.current)return
        await declaracionesCheques.current.show()
        loadData()
    },[loadData])
    const showDeclaracionTotalTarjeta = useCallback(async()=>{
        if(!declaracionesTarjetas.current) return
        await declaracionesTarjetas.current.total(declaraciones.find(d=>d.tipo===3)?.monto)
        loadData()
    },[loadData, declaraciones])
    const showDeclaracionesRetenciones = useCallback(async()=>{
        if(!declaracionesRetenciones.current) return
        await declaracionesRetenciones.current.show()
        loadData()
    },[loadData])
    const showGastos = useCallback(async()=>{
        if(!gastosDeRepartoDialog.current) return
        await gastosDeRepartoDialog.current.show()
        loadData()
    },[loadData])
    //#endregion
    return(
        <>
            <GastosDeReparto        ref={gastosDeRepartoDialog}     />
            <DeclaracionEfectivo    ref={declaracionesEfectivo}     />
            <DeclaracionCheques     ref={declaracionesCheques}      />
            <DeclaracionTarjetas    ref={declaracionesTarjetas}     />
            <DeclaracionRetenciones ref={declaracionesRetenciones}  />
            <Modal size="sm" onHide={cancelar} show={!!callbacks} backdrop="static" centered keyboard={false} className="cierreRuta">
                <Modal.Header closeButton>Cierre de Ruta</Modal.Header>
                <Modal.Body>
                    <Form.Row>
                        <Form.Label column xs={7}>Gastos (x{declaGastos.length}): </Form.Label>
                        <Form.Label column xs={4}>$ {declaGastos.reduce((t,d)=>t+d.montoGasto,0).toFixed(2)}</Form.Label>
                        <Form.Label column xs={1}>
                            <FaPencilAlt onClick={showGastos}/>
                        </Form.Label>
                    </Form.Row>
                    <Form.Row>
                        <Form.Label column xs={7}>Efectivo: </Form.Label>
                        <Form.Label column xs={4}>$ {declaEfectivo.reduce((t,d)=>t+(d.cantidad*d.valorNominal),0).toFixed(2)}</Form.Label>
                        <Form.Label column xs={1}>
                            <FaPencilAlt onClick={showDeclaracionesEfectivo}/>
                        </Form.Label>
                    </Form.Row>
                    <Form.Row>
                        <Form.Label column xs={7}>Cheques (x{declaraciones.filter(d=>d.tipo===1).length}): </Form.Label>
                        <Form.Label column xs={4}>$ {declaraciones.filter(d=>d.tipo===1).reduce((t,d)=>t+d.monto,0).toFixed(2)}</Form.Label>
                        <Form.Label column xs={1}>
                            <FaPencilAlt onClick={showDeclaracionesCheques}/>
                        </Form.Label>
                    </Form.Row>
                    <Form.Row>
                        <Form.Label column xs={7}>Retenciones (x{declaraciones.filter(d=>d.tipo===2).length}): </Form.Label>
                        <Form.Label column xs={4}>$ {declaraciones.filter(d=>d.tipo===2).reduce((t,d)=>t+d.monto,0).toFixed(2)}</Form.Label>
                        <Form.Label column xs={1}>
                            <FaPencilAlt onClick={showDeclaracionesRetenciones}/>
                        </Form.Label>
                    </Form.Row>
                    <Form.Row>
                        <Form.Label column xs={7}>Tarjetas Total: </Form.Label>
                        <Form.Label column xs={4}>$ {(declaraciones.find(d=>d.tipo===3)?.monto??0).toFixed(2)}</Form.Label>
                        <Form.Label column xs={1}>
                            <FaPencilAlt onClick={showDeclaracionTotalTarjeta}/>
                        </Form.Label>
                    </Form.Row>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary"
                        disabled={validado!==undefined} 
                        onClick={validacion}
                        >Validar</Button>
                    <Button variant={validado===undefined?"warning":validado?"success":"danger"}
                        disabled={validado===undefined}
                        onClick={continuar}
                        >Continuar &gt;</Button>
                </Modal.Footer>
            </Modal>
        </>
    )
})