import { forwardRef, useImperativeHandle }  from 'react'
import { useState, useCallback, useRef }    from 'react'
import { Modal, Form }                      from 'react-bootstrap'
import { BsFillDashCircleFill }             from 'react-icons/bs'
import { BsFillPlusCircleFill }             from 'react-icons/bs'

import * as DB                      from 'services/DB'
import { PromiseCallbacks }         from 'types'
import { AppData_Declaraciones }    from 'entities/app'


export interface IDeclaracionRetenciones{
    show:()=>Promise<void>
}

export const DeclaracionRetenciones = forwardRef<IDeclaracionRetenciones>((_,ref)=>{
    const [ callbacks, setCallbacks ]       = useState<PromiseCallbacks<void>>()
    const [ declarados, setDeclarados ]     = useState<AppData_Declaraciones[]>([])
    const montoRef = useRef<HTMLInputElement>(null)
    const loadDeclarados = useCallback(async()=>{
        setDeclarados(await DB.Declaraciones.GetTipo(DB.Declaraciones.T_Retencion))
    },[])
    const show = useCallback(async()=>{
        const p = new Promise<void>((resolve, reject)=>{
            setCallbacks({resolve, reject})
            loadDeclarados()
        })
        p.finally(()=>{
            setCallbacks(undefined)
        })
        return p
    },[loadDeclarados])
    useImperativeHandle(ref, ()=>({show}),[show])
    const close = useCallback(async()=>{
        if(!callbacks) return
        callbacks.resolve()
    },[callbacks])
    const agregar = useCallback(async()=>{
        if(!montoRef.current) return
        await DB.Declaraciones.Upsert({
            tipo:DB.Declaraciones.T_Retencion,
            monto:montoRef.current.valueAsNumber
        })
        montoRef.current.value=""
        loadDeclarados()
    },[loadDeclarados])
    const eliminar = useCallback(async(id:number)=>{
        await DB.Declaraciones.Delete(id)
        loadDeclarados()
    },[loadDeclarados])
    return (
        <Modal onHide={close} show={!!callbacks} centered backdrop="static" keyboard={false} className="declaracionRetenciones">
            <Modal.Header closeButton>Declaracion de Retenciones</Modal.Header>
            <Modal.Body>
                
            <Form.Row>
                    <Form.Label column xs={{offset:1,span:8}}>
                        <Form.Control type="number" placeholder="Monto de Retencion" size="sm"
                            ref={montoRef}
                        />
                    </Form.Label>
                    <Form.Label column xs={{offset:0, span:2}}>
                        <BsFillPlusCircleFill onClick={agregar} />
                    </Form.Label>
                </Form.Row>
                {declarados.map(declaracion=>(
                    <Form.Row key={declaracion.id}>
                        <Form.Label column xs={{offset:1, span:8}}>$ {declaracion.monto}</Form.Label>
                        <Form.Label column xs={{offset:0, span:2}}>
                            <BsFillDashCircleFill onClick={()=>eliminar(declaracion.id)} />
                        </Form.Label>
                    </Form.Row>
                ))}
            </Modal.Body>
        </Modal>
    )
})