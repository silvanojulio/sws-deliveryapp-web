import React, { FC, useState, useCallback, useRef } from 'react'
import { forwardRef, useImperativeHandle }          from 'react'
import { Form, Button, Modal, Col }                 from 'react-bootstrap'


import { useAppContext }                            from 'hooks'
import { FnQueue, AppStorage, ofuscatePassword }    from 'utils'
import SyncServices                                 from 'services/sync'
import { PromiseCallbacks }                         from 'types'

type ValidatePasswordProps={
    saveName:string
}
interface IValidatePassword{
    validar:()=>Promise<boolean>
}
const ValidatePassword=forwardRef<IValidatePassword, ValidatePasswordProps>((props,ref)=>{
    const [ callbacks, setCallbacks ]   = useState<PromiseCallbacks<boolean>>()
    const [ password,  setPassword  ]   = useState<string>()
    const { ToastError, ToastSuccess }  = useAppContext()
    const passRef = useRef<HTMLInputElement>(null)
    const validar=useCallback(async()=>{
        const password = await AppStorage.getString(props.saveName)
        const p=new Promise<boolean>((resolve, reject)=>{
            setCallbacks({resolve, reject})
            setPassword(password)
        })
        p.finally(()=>{
            setCallbacks(undefined)
        })
        return p
    },[props])
    useImperativeHandle(ref,()=>({validar}),[validar])
    const guardar=useCallback(async()=>{
        if(!callbacks || !passRef.current)return
        const testPass=await ofuscatePassword(passRef.current.value)
        if(!password){
            if(passRef.current.value.length<4) return ToastError("La contraseña debe tener al menos 4 letras o números")
            await AppStorage.setValue(props.saveName, testPass)
            ToastSuccess("Contraseña configurada!")
            return callbacks.resolve(true)
        }
        if(testPass===password) return callbacks.resolve(true)
        ToastError("Contraseña incorrecta!")
    },[callbacks, password, ToastError, ToastSuccess, props])
    const cancelar=useCallback(async()=>{
        if(!callbacks)return
        callbacks.resolve(false)
    },[callbacks])
    return (
        <Modal show={!!callbacks} size="sm" backdrop="static" centered keyboard={false}>
            <Modal.Body>
                <Form.Control type="password" ref={passRef}  placeholder={password?"Intoruzca la contraseña configurada":"Configure una contraseña"} />
            </Modal.Body>
            <Modal.Footer>
                <Button size="sm" variant="danger" onClick={cancelar}>Cancelar</Button>
                <Button size="sm" variant="success" onClick={guardar}>Aceptar</Button>
            </Modal.Footer>
        </Modal>
    )
})


export type ConfigServerProps={
    disabled?:boolean
}
export const ConfigServer:FC<ConfigServerProps> = ({disabled})=>{
    const {ToastSuccess, ToastError}    = useAppContext()
    const domainRef                     = useRef<HTMLInputElement>(null)
    const movilidRef                    = useRef<HTMLInputElement>(null)
    const validatePass                  = useRef<IValidatePassword>(null)

    const [ show, setShow ]     = useState<boolean>(false)
    const [ config, setConfig]  = useState<{domain:string, movilid:string}>({domain:"",movilid:""})

    const handleClose = useCallback(() => setShow(false),[])
    const handleShow = useCallback(async () => {
        if(!validatePass.current || !await validatePass.current.validar()) return setShow(false)
        setConfig({
            domain:await AppStorage.getString("SERVERDOMAIN","test.app.sistemaws.com:8070"),
            movilid:await AppStorage.getString("MOVILID","")
        })
        setShow(true);
    },[])
    const saveConfig=useCallback(async ()=>{        
        FnQueue.Run(async()=>{
            if(!domainRef.current) return
            if(!movilidRef.current) return
            try{
                const msg=await SyncServices.VerificarConfiguracion(domainRef.current.value, movilidRef.current.value)
                
                const oldPass = await AppStorage.getString("configPassword","")
                if(config.domain!==domainRef.current.value || config.movilid!==movilidRef.current.value) AppStorage.clear()
                AppStorage.setValue("SERVERDOMAIN",domainRef.current.value)
                AppStorage.setValue("MOVILID", movilidRef.current.value)
                AppStorage.setValue("configPassword", oldPass)
                ToastSuccess(msg)
                setShow(false)
            }catch(error){
                ToastError((error as Error).message)
            }
        },{blocking:true, label:"Verificando Configuracion..."})
    },[ToastError, ToastSuccess, config])
    return ( 
        <>
            <Button disabled={disabled} variant="primary" onClick={handleShow}>
                Configurar Servidor
            </Button>
            <ValidatePassword saveName="configPassword" ref={validatePass} />
            <Modal show={show} size="sm" backdrop="static" centered keyboard={false} onHide={handleClose} >
                <Modal.Header>
                    <Modal.Title>Configuracion del Servidor</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Row>
                        <Col xs={12} sm={4}><Form.Label>Servidor</Form.Label></Col>
                        <Col xs={12} sm={8}><Form.Control 
                            size="sm" type={"text"} placeholder="URL:PUERTO (sin 'https://')"
                            ref={domainRef} 
                            defaultValue={config?.domain??"localhost"}
                        /></Col>
                    </Form.Row>
                    <Form.Row>
                        <Col xs={12} sm={4}><Form.Label>Codigo de Movil</Form.Label></Col>
                        <Col xs={12} sm={8}><Form.Control 
                            size="sm" type={"text"}
                            ref={movilidRef} 
                            defaultValue={config?.movilid??""}
                        /></Col>
                    </Form.Row>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={handleClose} variant="secondary">Cancelar</Button>
                    <Button onClick={saveConfig}>Guardar</Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}