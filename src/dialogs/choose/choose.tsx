import { forwardRef, useImperativeHandle}   from 'react'
import { useState, useCallback }            from 'react'
import { Modal, Button, Form }              from 'react-bootstrap'
import { IconType }                         from 'react-icons'
import { RiArrowRightSLine }                from 'react-icons/ri'

import { PromiseCallbacks } from 'types'

type OptionObject = {
    value:any,
    label:string,
    Icon?:IconType,
    callback?:()=>Promise<void>
}
export interface IChoose{
    choose:(chooseOptions:OptionObject[], title?:string)=>Promise<any>
}
export const Choose = forwardRef<IChoose>((_, ref)=>{
    const [ callbacks,  setCallbacks ]  = useState<PromiseCallbacks<string>>()
    const [ options,    setOptions  ]   = useState<OptionObject[]>([])
    const [ title,      setTitle    ]   = useState<string>()
    useImperativeHandle(ref, ()=>({
        choose:(chooseOptions, title)=>{
            setTitle(title)
            const p = new Promise<string>((resolve, reject)=>setCallbacks({resolve, reject}))
            p.finally(()=>setCallbacks(undefined))
            setOptions(chooseOptions)
            return p
        }
    }))
    const choose = useCallback(async(option:OptionObject, title?:string)=>{
        if(!callbacks) return
        if(option.callback) option.callback()
        callbacks.resolve(option.value)
    },[callbacks])
    const cancelar = useCallback(async()=>{
        if(!callbacks) return
        callbacks.resolve("")
    },[callbacks])
    return (
        <Modal show={!!callbacks} onHide={cancelar} size="sm" backdrop="static" centered keyboard={false} className="choose" >
            <Modal.Header closeButton>{title??"Elija una opción"}</Modal.Header>
            <Modal.Body>
                {options.map((opt, index)=>(
                    <Button key={index} onClick={()=>choose(opt)}>
                        {opt.Icon??(<RiArrowRightSLine/>)}
                        <Form.Label>{opt.label}</Form.Label>
                    </Button>
                ))}
            </Modal.Body>
        </Modal>
    )
})