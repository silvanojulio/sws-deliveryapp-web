import React, { forwardRef, useImperativeHandle  }  from 'react'
import { useCallback, useState, useRef, useEffect } from 'react'
import SignatureCanvas                              from 'react-signature-canvas'
import { Modal, Form, Button }                      from 'react-bootstrap'

import { PromiseCallbacks }     from 'types'
import { useAppContext } from 'hooks'

type SignatureReturn={
    aclaracion:string
    firma:string
}
export interface ISignature {
    show:()=>Promise<SignatureReturn>
}
type SignatureProps={
}
export const Signature=forwardRef<ISignature, SignatureProps>((_props, ref) =>{
    const [ callbacks, setCallbacks ]   = useState<PromiseCallbacks<SignatureReturn>>()
    const firmaRef                      = useRef<SignatureCanvas>(null)
    const aclaracionRef                 = useRef<HTMLInputElement>(null)
    const { ToastError }                = useAppContext()

    useImperativeHandle(ref,()=>({
        async show(){
            const p=new Promise<SignatureReturn>((resolve, reject)=>setCallbacks({resolve, reject}))
            p.finally(()=>setCallbacks(undefined))            
            if(firmaRef.current) {
                const canvas=firmaRef.current.getTrimmedCanvas()
                const context=canvas.getContext('2d')
                if(context){
                    context.globalCompositeOperation = "destination-over"
                    context.fillStyle = "white"
                    context.fillRect(0, 0, canvas.width, canvas.height)
                }
            }
            return p
        }
    }))
    const Save=useCallback(()=>{
        if(!firmaRef.current || !aclaracionRef.current ||!callbacks) return
        if(aclaracionRef.current.value==="") return ToastError("Ingrese la aclaracion de firma", {autoClose:2000})
        if(firmaRef.current.isEmpty()) return ToastError("Ingrese su firma en el recuadro", {autoClose:2000})
        callbacks.resolve({
            aclaracion:aclaracionRef.current.value,
            firma:firmaRef.current.toDataURL().split(",")[1]
        })
    },[callbacks, ToastError])
    const Limpiar=useCallback(()=>{
        if(!firmaRef.current) return
        const canvas=firmaRef.current.getCanvas()
        canvas.width=canvas.offsetWidth
        canvas.height=canvas.offsetHeight
        firmaRef.current.clear()
    },[])
    useEffect(()=>{
        Limpiar()
    },[callbacks, Limpiar])
    const Cancelar=useCallback(()=>callbacks!.reject(),[callbacks])
    return (
        <Modal show={!!callbacks} size="sm" backdrop="static" centered keyboard={false} className="signature">
            <Modal.Header><Modal.Title>Firma</Modal.Title></Modal.Header>
            <Modal.Body>
                <SignatureCanvas 
                    canvasProps={{
                        className:"sigPad",
                    }}
                    backgroundColor="white"
                    clearOnResize={false}
                    onEnd={()=>{
                        if(!aclaracionRef.current) return
                        aclaracionRef.current.blur()
                    }}
                    ref={firmaRef}
                />
                <Form.Control size="sm" type="text" ref={aclaracionRef} placeholder="Aclaracion"/>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={Cancelar}>Cancelar</Button>
                <Button onClick={Limpiar}>Limpiar</Button>
                <Button onClick={Save}>Aceptar</Button>
            </Modal.Footer>
        </Modal>
    )
})