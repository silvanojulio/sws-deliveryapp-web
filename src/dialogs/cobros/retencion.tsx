import React, { forwardRef, useImperativeHandle }   from 'react'
import { useState, useCallback, useRef, useEffect } from 'react'
import { Modal, Container, Form, Button }           from 'react-bootstrap'
import DatePicker                                   from 'react-datepicker'
import Select                                       from 'react-select'


import { AppData_Retenciones }      from 'entities/app'
import { AppData_ValoresSatelites } from 'entities/app'
import { PromiseCallbacks }         from 'types'
import * as DB                      from 'services/DB'

type TipoSelectOption={
    value:AppData_ValoresSatelites
    label:string
}
export interface IRetencion{
    nuevo:(recibo_id:number)=>Promise<AppData_Retenciones>
    editar:(retencion:AppData_Retenciones)=>Promise<AppData_Retenciones>
}

export const Retencion=forwardRef<IRetencion>((_,ref)=>{
    const [ reciboId, setReciboId ]     = useState<number>(0)
    const [ retencion, setRetencion ]   = useState<AppData_Retenciones>()
    const [ tipos, setTipos ]           = useState<TipoSelectOption[]>([])
    const [ tipoId, setTipoId ]         = useState<number>(0)
    const [ fecha, setFecha ]           = useState<Date>(new Date())
    const [ callbacks, setCallbacks ]   = useState<PromiseCallbacks<AppData_Retenciones>>()

    const importeRef        = useRef<HTMLInputElement>(null)
    const descripcionRef    = useRef<HTMLInputElement>(null)

    const cargarTipos = useCallback(async()=>{
        setTipos((await DB.ValoresSatelites.GetTablas([DB.ValoresSatelites.T_TIPOS_RETENCIONES])).map<TipoSelectOption>(tipo=>({
            value:tipo, label:tipo.valor_texto
        }))
    )},[])
    useEffect(()=>{cargarTipos()},[cargarTipos])

    const nuevo = useCallback(async (recibo_id:number)=>{
        setReciboId(recibo_id)
        const p = new Promise<AppData_Retenciones>((resolve, reject)=>setCallbacks({resolve, reject}))
        p.finally(()=>{
            setReciboId(0)
            setTipoId(0)
            setRetencion(undefined)
            setCallbacks(undefined)
        })
        return p
    },[])
    const editar = useCallback(async (retencion:AppData_Retenciones)=>{
        setRetencion(retencion)
        setTipoId(retencion.tipoRetencionId)
        const p = new Promise<AppData_Retenciones>((resolve, reject)=>setCallbacks({resolve, reject}))
        p.finally(()=>{
            setTipoId(0)
            setRetencion(undefined)
            setCallbacks(undefined)
        })
        return p
    },[])
    useImperativeHandle(ref, ()=>({nuevo, editar}))

    const handleClose=useCallback(async ()=>{
        if(!callbacks) return
        callbacks.reject()
    },[callbacks])
    const handleSave = useCallback(async ()=>{
        if(!callbacks) return
        if(!descripcionRef.current) throw new Error("Referencia no establecida (descripcionRef)")
        if(!importeRef.current)     throw new Error("Referencia no establecida (importeRef)")
        callbacks.resolve(await DB.Retenciones.Save({ ...retencion, reciboId,
            descripcion:descripcionRef.current.value,
            fecha:"",
            importe:importeRef.current.valueAsNumber,
            tipoRetencionId:tipoId
        }))
    },[callbacks, tipoId, reciboId, retencion])
    return (
        <Modal show={!!callbacks} size="sm" backdrop="static" centered onHide={handleClose} keyboard={false} className="cobro_retencion">
            <Modal.Header closeButton>Retención</Modal.Header>
            <Modal.Body>
                <Form.Row>
                    <Form.Label column xs={{offset:1, span:3}}>Importe $</Form.Label>
                    <Form.Label column xs={{offset:0, span:7}}>
                        <Form.Control type="number" size="sm" ref={importeRef} defaultValue={retencion?.importe??0}/>
                    </Form.Label>
                </Form.Row>
                <Container>
                    <Form.Row>
                        <Form.Label>Descripcion</Form.Label>
                        <Form.Control type="text" ref={descripcionRef} defaultValue={retencion?.descripcion} />
                    </Form.Row>
                    <Form.Row>
                        <Form.Label>Tipo</Form.Label>
                        <Select<TipoSelectOption> id="tipo"
                            value={tipos.find(tipo=>tipo.value.valor_id===tipoId)}
                            options={tipos}
                            onChange={option=>option && setTipoId(option.value.valor_id)}
                        />
                    </Form.Row>
                    <Form.Row>
                        <Form.Label>Fecha</Form.Label>
                        <DatePicker id="fecha"
                            onChange={(fecha:Date)=>setFecha(fecha)}
                            selected={fecha}
                            dateFormat="yyyy-MM-dd"
                        />
                    </Form.Row>
                </Container>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={handleSave}>Guardar</Button>
            </Modal.Footer>
        </Modal>
    )
})