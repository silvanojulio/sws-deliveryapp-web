import { FC }                               from 'react'
import { Card, Form, Button, Badge, Image } from 'react-bootstrap'
import { ImBin } from 'react-icons/im'

import { AppData_ItemsDeRecibos } from 'entities/app'

export type CobroProps={
    itemDeRecibo:AppData_ItemsDeRecibos
    onEditClick:(item:AppData_ItemsDeRecibos)=>void
    onDeleteClick:(item:AppData_ItemsDeRecibos)=>void
    visible:boolean
}

export const Cobro:FC<CobroProps> = ({itemDeRecibo, onEditClick, onDeleteClick, visible})=>{
    const { formaDePagoId } = itemDeRecibo
    const { importe }       = itemDeRecibo
    const { descripcion }   = itemDeRecibo

    return (
        <Card className={`cobro ${visible?"":" d-none"}`}>
            <Form.Row>
                <Badge>$ {importe}</Badge>
                <Form.Label>
                    {formaDePagoId===1 && "> Efectivo"} 
                    {formaDePagoId===2 && "> Cheque"} 
                    {formaDePagoId===8 && "> Tarjeta de Crédito"}
                    {formaDePagoId===9 && "> Retención"}
                    {formaDePagoId===10 && "> Tarjeta de Débito"}
                </Form.Label>
            </Form.Row>
            <Form.Row>
                {formaDePagoId===1 && <Image src="images/efectivo.png"/>}
                {formaDePagoId===2 && <Image src="images/cheque.png" />}
                {formaDePagoId===9 && <Image src="images/retencion.png" />}
                {(formaDePagoId===8 || formaDePagoId===10) && <Image src="images/tarjetas.png" />}
                <Button onClick={()=>onEditClick(itemDeRecibo)}>Editar</Button>
                <ImBin onClick={()=>onDeleteClick(itemDeRecibo)} className="text-danger" />
            </Form.Row>
            {!!descripcion && <Form.Row><Form.Label>{descripcion}</Form.Label></Form.Row>}
        </Card>
    )
}