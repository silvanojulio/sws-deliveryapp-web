import React, { useRef, useCallback, useState } from 'react'
import { forwardRef, useImperativeHandle} from 'react'
import { PromiseCallbacks } from 'types'
import { Modal, Form, Button } from 'react-bootstrap'

type EfectivoProps={
    monto?:number
}
export interface IEfectivo{
    cobrar:(monto?:number)=>Promise<number>
}

export const Efectivo = forwardRef<IEfectivo, EfectivoProps>((props, ref)=>{
    const [ defaultMonto, setDefaultMonto ] = useState<number>(0)
    const [ callbacks, setCallbacks ]       = useState<PromiseCallbacks<number>>()
    const montoRef                          = useRef<HTMLInputElement>(null)

    const cobrar = useCallback(async (monto?:number):Promise<number> => {
        setDefaultMonto(monto??0)
        const p = new Promise<number>((resolve, reject)=>setCallbacks({resolve, reject}))
        p.finally(()=>{
            setDefaultMonto(0)
            setCallbacks(undefined)
        })
        if(!!montoRef.current) montoRef.current.valueAsNumber=monto??0
        return p
    },[ ])
    const save=useCallback(async()=>{
        if(!callbacks) return 
        const monto=montoRef.current?.valueAsNumber??0
        callbacks.resolve(monto)
    },[callbacks])

    useImperativeHandle(ref, ()=>({cobrar}))

    return (
        <Modal show={!!callbacks} size="sm" backdrop="static" centered keyboard={false} className="cobro_efectivo">
            <Modal.Header>
                <Modal.Title>Importe</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form.Row>
                    <Form.Label column xs={{offset:2, span:1}}>$</Form.Label>
                    <Form.Label column xs={{offset:0, span:7}}>
                        <Form.Control type="number" size="sm" ref={montoRef} defaultValue={defaultMonto}
                            onChange={()=>{
                                if(!montoRef.current) return
                                montoRef.current.value=montoRef.current.valueAsNumber.toString()
                            }}
                        />
                    </Form.Label>
                </Form.Row>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={save}>Guardar</Button>
            </Modal.Footer>
        </Modal>
    )
})