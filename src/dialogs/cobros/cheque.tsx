import React, { forwardRef, useImperativeHandle }   from 'react'
import { useState, useCallback, useRef, useEffect } from 'react'
import { Modal, Container, Form, Button  }          from 'react-bootstrap'
import Select                                       from 'react-select'
import DatePicker                                   from 'react-datepicker'
import moment                                       from 'moment'

import { AppData_Cheques  }         from 'entities/app'
import { AppData_ValoresSatelites } from 'entities/app'
import { PromiseCallbacks }         from 'types'
import * as DB                      from 'services/DB'
import { useAppContext }            from 'hooks'

type BancoSelectOption={value:AppData_ValoresSatelites, label:string}
export interface ICheque{
    nuevo:()=>Promise<AppData_Cheques>
    editar:(cheque:AppData_Cheques)=>Promise<AppData_Cheques>
}
export const Cheque = forwardRef<ICheque>((_, ref)=>{
    const { ToastError }                = useAppContext()
    const [ cheque, setCheque ]         = useState<AppData_Cheques>()
    const [ bancos, setBancos ]         = useState<BancoSelectOption[]>([])
    const [ bancoId, setBancoId ]       = useState<number>()
    const [ fecha, setFecha ]           = useState<Date>(new Date())
    const [ callbacks, setCallbacks ]   = useState<PromiseCallbacks<AppData_Cheques>>()
    const montoRef = useRef<HTMLInputElement>(null)
    const nroChequeRef = useRef<HTMLInputElement>(null)
    const libradorRef = useRef<HTMLInputElement>(null)
    const sucursalRef = useRef<HTMLInputElement>(null)
    const bancoRef = useRef<Select<BancoSelectOption>>(null)

    const cargarBancos = useCallback(async()=>{
        setBancos(
            (await DB.ValoresSatelites.GetTablas([DB.ValoresSatelites.T_BANCOS]))
            .map<BancoSelectOption>(value=>({value, label:value.valor_texto}))
        )
    },[])
    useEffect(()=>{cargarBancos()},[cargarBancos])

    const nuevo=useCallback(async()=>{
        const p = new Promise<AppData_Cheques>((resolve, reject)=>setCallbacks({resolve, reject}))
        p.finally(()=>setCallbacks(undefined))
        return p
    },[])
    const editar=useCallback(async(cheque:AppData_Cheques)=>{
        setCheque(cheque)
        setBancoId(cheque.banco_id)
        const p = new Promise<AppData_Cheques>((resolve, reject)=>setCallbacks({resolve, reject}))
        p.finally(()=>{
            setCheque(undefined)
            setBancoId(0)
            setCallbacks(undefined)
        })
        return p
    },[])
    useImperativeHandle(ref, ()=>({nuevo, editar}))

    const closeHandle = useCallback(async()=>{
        if(!callbacks) return
        callbacks.reject()
    },[callbacks])
    const guardarHandle = useCallback(async()=>{
        if(!callbacks)              return
        if(!montoRef.current)       return callbacks.reject(new Error("Referencia no establecida (montoRef)"))
        if(!nroChequeRef.current)   return callbacks.reject(new Error("Referencia no establecida (nroChequeRef)"))
        if(!libradorRef.current)    return callbacks.reject(new Error("Referencia no establecida (libradorRef)"))
        if(!sucursalRef.current)    return callbacks.reject(new Error("Referencia no establecida (sucursalRef)"))
        if(!fecha)                  return callbacks.reject(new Error("No ha seleccionado la Fecha de cobro"))
        if(
            libradorRef.current.value==="" ||
            nroChequeRef.current.value==="" ||
            montoRef.current.value==="" ||
            sucursalRef.current.value==="" ||
            !bancoId
        ) return ToastError("Debe completar todos los datos")
        callbacks.resolve(await DB.Cheques.SaveCheque({ ...cheque,
            librador:libradorRef.current.value,
            nroCheque:nroChequeRef.current.value,
            importe:montoRef.current.valueAsNumber,
            nroSucursalBanco:sucursalRef.current.value,
            banco_id:bancoId,
            fechaCobro: moment(fecha).format("DD/MM/YYYY")
        } as AppData_Cheques))
    },[callbacks, cheque, bancoId, fecha, ToastError])
    return (
        <Modal show={!!callbacks} size="sm" backdrop="static" centered keyboard={false} className="cobro_cheque" onHide={closeHandle}>
            <Modal.Header closeButton>Cheque</Modal.Header>
            <Modal.Body>
                <Form.Row>
                    <Form.Label column xs={{offset:1, span:3}}>Importe $</Form.Label>
                    <Form.Label column xs={{offset:0, span:7}}>
                        <Form.Control type="number" size="sm" ref={montoRef} defaultValue={cheque?.importe}/>
                    </Form.Label>
                </Form.Row>
                <Container>
                    <Form.Row>
                        <Form.Label>Nro de Cheque</Form.Label>
                        <Form.Control type="number" size="sm" ref={nroChequeRef} defaultValue={cheque?.nroCheque}/>
                    </Form.Row>
                    <Form.Row>
                        <Form.Label>Librador</Form.Label>
                        <Form.Control type="text" size="sm" ref={libradorRef} defaultValue={cheque?.librador} />
                    </Form.Row>
                    <Form.Row>
                        <Form.Label>Banco</Form.Label>
                        <Select<BancoSelectOption> id="banco"
                            value={bancos.find(item=>item.value.valor_id===bancoId)}
                            options={bancos}
                            ref={bancoRef}
                            onChange={(option)=>option && setBancoId(option.value.valor_id)}
                        />
                    </Form.Row>
                    <Form.Row>
                        <Form.Label>Sucursal</Form.Label>
                        <Form.Control type="number" size="sm" ref={sucursalRef} defaultValue={cheque?.nroSucursalBanco} />
                    </Form.Row>
                    <Form.Row>
                        <Form.Label>Fecha de Cobro</Form.Label>
                        <DatePicker 
                            onChange={(fecha:Date)=>setFecha(fecha)}
                            selected={fecha}
                        />
                    </Form.Row>
                </Container>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={guardarHandle}>Guardar</Button>
            </Modal.Footer>
        </Modal>
    )
})