import { forwardRef, useImperativeHandle }          from 'react'
import { useState, useCallback, useEffect, useRef } from 'react'
import { Modal, Container, Form, Button }           from 'react-bootstrap'
import Select                                       from 'react-select'

import { AppData_TarjetasDeCredito }    from 'entities/app'
import { AppData_ValoresSatelites }     from 'entities/app'
import { PromiseCallbacks }             from 'types'
import * as DB                          from 'services/DB'


type TarjetaSelectOption={
    value:AppData_ValoresSatelites
    label:string
}
type BancoSelectOption={
    value:AppData_ValoresSatelites
    label:string
}

export interface ICredito{
    nuevo:(reciboId:number)=>Promise<AppData_TarjetasDeCredito>
    editar:(credito:AppData_TarjetasDeCredito)=>Promise<AppData_TarjetasDeCredito>
}

export const Credito=forwardRef<ICredito>((_,ref)=>{
    const importeRef        = useRef<HTMLInputElement>(null)
    const loteRef           = useRef<HTMLInputElement>(null)
    const cuponRef          = useRef<HTMLInputElement>(null)
    const autorizacionRef   = useRef<HTMLInputElement>(null)
    const cuotasRef         = useRef<HTMLInputElement>(null)
    
    const [ reciboId, setReciboId ]     = useState<number>(0)
    const [ bancoId, setBancoId ]       = useState<number>(0)
    const [ tarjetaId, setTarjetaId ]   = useState<number>(0)
    const [ credito, setCredito ]       = useState<AppData_TarjetasDeCredito>()
    const [ callbacks, setCallbacks ]   = useState<PromiseCallbacks<AppData_TarjetasDeCredito>>()
    const [ bancos, setBancos ]         = useState<BancoSelectOption[]>([])
    const [ tarjetas, setTarjetas ]     = useState<TarjetaSelectOption[]>([])

    const cargarValoresSatelites = useCallback(async ()=>{
        const satelites=await DB.ValoresSatelites.GetTablas([
            DB.ValoresSatelites.T_BANCOS,
            DB.ValoresSatelites.T_MARCAS_TARJETAS
        ])
        setBancos(
            satelites
            .filter(valor=>valor.tabla_id===DB.ValoresSatelites.T_BANCOS)
            .map<BancoSelectOption>(banco=>({value:banco, label:banco.valor_texto}))
        )
        setTarjetas(
            satelites
            .filter(valor=>valor.tabla_id===DB.ValoresSatelites.T_MARCAS_TARJETAS)
            .map<TarjetaSelectOption>(tarjeta=>({value:tarjeta, label:tarjeta.valor_texto}))
        )
    },[])
    useEffect(()=>{cargarValoresSatelites()},[cargarValoresSatelites])
    const clearState = useCallback(()=>{
        setReciboId(0)
        setTarjetaId(0)
        setBancoId(0)
        setCredito(undefined)
        setCallbacks(undefined)
    },[])
    const nuevo = useCallback(async(reciboId:number)=>{
        setReciboId(reciboId)
        const p = new Promise<AppData_TarjetasDeCredito>((resolve, reject)=>setCallbacks({resolve, reject}))
        p.finally(clearState)
        return p
    },[clearState])
    const editar = useCallback(async(credito:AppData_TarjetasDeCredito)=>{
        setReciboId(credito.reciboId)
        setCredito(credito)
        setTarjetaId(credito.marcaTarjeta_id)
        setBancoId(credito.banco_id)
        const p = new Promise<AppData_TarjetasDeCredito>((resolve, reject)=>setCallbacks({resolve, reject}))
        p.finally(clearState)
        return p
    },[clearState])
    useImperativeHandle(ref, ()=>({nuevo, editar}))

    const handleSave=useCallback(async () =>{
        if(!callbacks)      return
        if(tarjetaId<=0)    return
        if(reciboId<=0)     return
        if(bancoId<=0)      return

        if(!importeRef.current)         return callbacks.reject(new Error("Referencia no establecida (importeRef)"))
        if(!loteRef.current)            return callbacks.reject(new Error("Referencia no establecida (loteRef)"))
        if(!cuponRef.current)           return callbacks.reject(new Error("Referencia no establecida (cuponRef)"))
        if(!autorizacionRef.current)    return callbacks.reject(new Error("Referencia no establecida (autorizacionRef)"))
        if(!cuotasRef.current)          return callbacks.reject(new Error("Referencia no establecida (cuotasRef)"))
        try{
            callbacks.resolve(await DB.TarjetasDeCredito.Save({...credito, reciboId,
                lote:loteRef.current.value,
                cupon:cuponRef.current.value,
                codigoAutorizacion:autorizacionRef.current.value,
                banco_id:bancoId,
                marcaTarjeta_id:tarjetaId,
                cuotas:cuotasRef.current.valueAsNumber,
                importe:importeRef.current.valueAsNumber,
            }))
        }catch(_){}
    },[callbacks, credito, tarjetaId, bancoId, reciboId])
    const handleClose=useCallback(async () =>{
        if(!callbacks) return
        callbacks.reject()
    },[callbacks])
    return (
        <Modal show={!!callbacks} size="sm" backdrop="static" centered onHide={handleClose} keyboard={false} className="cobro_credito">
            <Modal.Header closeButton>Tarjeta de Crédito</Modal.Header>
            <Modal.Body>
                <Form.Row>
                    <Form.Label column xs={{offset:1, span:3}}>Importe $</Form.Label>
                    <Form.Label column xs={{offset:0, span:7}}>
                        <Form.Control type="number" size="sm" ref={importeRef} defaultValue={credito?.importe??0}/>
                    </Form.Label>
                </Form.Row>
                <Container>
                    <Form.Row>
                        <Form.Label>Lote</Form.Label>
                        <Form.Control type="text" size="sm" ref={loteRef} defaultValue={credito?.lote}/>
                    </Form.Row>
                    <Form.Row>
                        <Form.Label>Cupón</Form.Label>
                        <Form.Control type="text" size="sm" ref={cuponRef} defaultValue={credito?.cupon}/>
                    </Form.Row>
                    <Form.Row>
                        <Form.Label>Cod de Autorización</Form.Label>
                        <Form.Control type="text" size="sm" ref={autorizacionRef} defaultValue={credito?.codigoAutorizacion}/>
                    </Form.Row>
                    <Form.Row>
                        <Form.Label>Banco</Form.Label>
                        <Select<BancoSelectOption> id="banco"
                            options={bancos}
                            value={bancos.find(banco=>banco.value.valor_id===bancoId)}
                            onChange={option=>!!option && setBancoId(option.value.valor_id)}
                        />
                    </Form.Row>
                    <Form.Row>
                        <Form.Label>Tarjeta</Form.Label>
                        <Select<TarjetaSelectOption> id="tarjeta"
                            options={tarjetas}
                            value={tarjetas.find(tarjeta=>tarjeta.value.valor_id===tarjetaId)}
                            onChange={option=>!!option && setTarjetaId(option.value.valor_id)}
                            
                        />
                    </Form.Row>
                    <Form.Row>
                        <Form.Label>Cuotas</Form.Label>
                        <Form.Control type="number" size="sm" ref={cuotasRef} defaultValue={credito?.cuotas}/>
                    </Form.Row>
                </Container>
            </Modal.Body>          
            <Modal.Footer>
                <Button onClick={handleSave}>Guardar</Button>
            </Modal.Footer>
        </Modal>
    )
})