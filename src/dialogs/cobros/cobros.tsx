import { forwardRef, useImperativeHandle }  from 'react'
import { useState, useCallback }            from 'react'
import { useRef, useEffect }                from 'react'
import { Modal, Button, Card }              from 'react-bootstrap'
import { Badge, Form }                      from 'react-bootstrap'
import { FcPrint }                          from 'react-icons/fc'
import { BsFillPlusCircleFill }             from 'react-icons/bs'
import { FaRegListAlt }                     from 'react-icons/fa'
import { RiMoneyDollarCircleLine }          from 'react-icons/ri'
import { RiCheckboxBlankLine }              from 'react-icons/ri'
import { RiCheckboxLine }                   from 'react-icons/ri'

import { IconBar, IconOption }              from 'components/iconBar'
import { PromiseCallbacks }                 from 'types'
import { Cobro }                            from './cobro'
import { Factura, IFactura }                from './factura'
import { AppData_ItemsDeRecibos }           from 'entities/app'
import { AppData_Recibos }                  from 'entities/app'
import { AppData_Facturas }                 from 'entities/app'
import { useAppContext }                    from 'hooks'
import { useHojaDeRutaContext }             from 'hooks'
import * as DB                              from 'services/DB'
import { ItemsDeRecibos, TarjetasDeDebito } from 'services/DB'
import { Cheques }                          from 'services/DB'
import { Retenciones }                      from 'services/DB'
import { TarjetasDeCredito }                from 'services/DB'


export interface ICobros{
    show:(cliente_id:number)=>Promise<void>
}
export const Cobros = forwardRef<ICobros>((_, ref)=>{
    const appContext                = useAppContext() 
    const hojaDeRutaContext         = useHojaDeRutaContext()
    const { ChooseRef }             = appContext
    const { ToastError }            = appContext
    const { efectivoDialog }        = hojaDeRutaContext
    const { chequeDialog }          = hojaDeRutaContext
    const { retencionDialog }       = hojaDeRutaContext
    const { creditoDialog }         = hojaDeRutaContext
    const { debitoDialog }          = hojaDeRutaContext
    const { comprobanteFisicoRef }  = hojaDeRutaContext

    const iconOptions:IconOption[]=[
        {label:"Recibos", Icon:RiMoneyDollarCircleLine},
        {label:"Facturas",Icon:FaRegListAlt}
    ]

    const [ vista,          setVista ]          = useState<string>()
    const [ itemsDeRecibo,  setItemsDeRecibo ]  = useState<AppData_ItemsDeRecibos[]>([])
    const [ recibo,         setRecibo ]         = useState<AppData_Recibos>()
    const [ facturas,       setFacturas ]       = useState<AppData_Facturas[]>([])
    const [ callbacks,      setCallbacks ]      = useState<PromiseCallbacks<void>>()
    
    const [ recibido,       setRecibido ]       = useState<number>(0)
    const [ imputado,       setImputado ]       = useState<number>(0)
    const [ adeudado,       setAdeudado ]       = useState<number>(0)
    const [ aCuenta,        setACuenta ]        = useState<number>(0)
    const [ autoImp,        setAutoImp ]        = useState<boolean>(true)
    
    const facRefs = useRef<IFactura[]>([])
    
    //#region Add Edit y Delete Actions
    const addEfectivo = useCallback(async()=>{
        if(itemsDeRecibo.some(item=>item.formaDePagoId===1)) {
            ToastError!("Ya hay ingresado un cobro de efectivo")
            return
        }
        if(!efectivoDialog.current) return
        if(!recibo?.id) return        
        try {
            const importe = await efectivoDialog.current.cobrar()
            setItemsDeRecibo(await ItemsDeRecibos.AddItemDeRecibo({
                reciboId:recibo.id,
                importe:importe,
                formaDePagoId:1,
                descripcion:null,
                retencionId:null,
                chequeId:null,
                tarjetaDeCreditoId:null,
                tarjetaDeDebitoId:null
            } as AppData_ItemsDeRecibos))
        }catch(_){}
    },[efectivoDialog, itemsDeRecibo, recibo, ToastError])
    const editEfectivo = useCallback(async(item:AppData_ItemsDeRecibos)=>{
        if(!efectivoDialog.current) return
        const importe = await efectivoDialog.current.cobrar(item.importe)
        setItemsDeRecibo(await ItemsDeRecibos.EditItemDeRecibo({...item, importe}))
    },[efectivoDialog])
    const delEfectivo = useCallback(async(item:AppData_ItemsDeRecibos)=>{
        setItemsDeRecibo(await ItemsDeRecibos.DeleteItemDeRecibo(item))
    },[])

    const addCheque=useCallback(async()=>{
        if(!chequeDialog.current) return
        if(!recibo?.id) return
        try{
            const cheque = await chequeDialog.current.nuevo()
            setItemsDeRecibo(await ItemsDeRecibos.AddItemDeRecibo({
                reciboId:recibo.id,
                importe:cheque.importe,
                formaDePagoId:2,
                descripcion:null,
                chequeId:cheque.id!,
                retencionId:null,
                tarjetaDeCreditoId:null,
                tarjetaDeDebitoId:null
            }))
        }
        catch(_){}
        
    },[chequeDialog, recibo])
    const editCheque=useCallback(async(item:AppData_ItemsDeRecibos)=>{
        if(!chequeDialog.current) return
        if(!item.chequeId) return
        try{
            const cheque = await DB.Cheques.GetCheque(item.chequeId)
            if(!cheque) return
            const editedCheque = await chequeDialog.current.editar(cheque)
            setItemsDeRecibo(await ItemsDeRecibos.EditItemDeRecibo({...item, importe:editedCheque.importe}))
        }
        catch(_){}
    },[chequeDialog])
    const delCheque=useCallback(async(item:AppData_ItemsDeRecibos)=>{
        if(!item.chequeId) return
        try{
            await Cheques.DeleteCheque(item.chequeId)
            setItemsDeRecibo(await ItemsDeRecibos.DeleteItemDeRecibo(item))
        }catch(_){}

    },[])

    const addRetencion=useCallback(async()=>{
        if(!retencionDialog.current) return
        if(!recibo?.id) return 
        try{
            const retencion=await retencionDialog.current.nuevo(recibo.id)
            setItemsDeRecibo(await ItemsDeRecibos.AddItemDeRecibo({
                reciboId:recibo.id,
                importe:retencion.importe,
                formaDePagoId:9,
                descripcion:null,
                chequeId:null,
                retencionId:retencion.id!,
                tarjetaDeCreditoId:null,
                tarjetaDeDebitoId:null
            }))
        }catch(_){}
    },[retencionDialog, recibo])
    const editRetencion=useCallback(async(item:AppData_ItemsDeRecibos)=>{
        if(!retencionDialog.current) return
        if(!item.retencionId) return
        try{
            const retencion = await Retenciones.GetRetencion(item.retencionId)
            if(!retencion) return
            const editedRetencion = await retencionDialog.current.editar(retencion)
            setItemsDeRecibo(await ItemsDeRecibos.EditItemDeRecibo({...item, importe:editedRetencion.importe}))
        }catch(error){}
        
    },[retencionDialog])
    const delRetencion=useCallback(async(item:AppData_ItemsDeRecibos)=>{
        if(!item.retencionId) return
        try{
            await Retenciones.DeleteRetencion(item.retencionId)
            setItemsDeRecibo(await ItemsDeRecibos.DeleteItemDeRecibo(item))
        }catch(_){}     
    },[])


    const addTCredito=useCallback(async()=>{
        if(!creditoDialog.current) return
        if(!recibo?.id) return 
        try{
            const credito = await creditoDialog.current.nuevo(recibo.id)
            setItemsDeRecibo(await ItemsDeRecibos.AddItemDeRecibo({
                reciboId:recibo.id,
                importe:credito.importe,
                formaDePagoId:8,
                descripcion:null,
                chequeId:null,
                retencionId:null,
                tarjetaDeCreditoId:credito.id!,
                tarjetaDeDebitoId:null
            }))
        }catch(_){}
    },[creditoDialog, recibo])
    const editTCredito=useCallback(async(item:AppData_ItemsDeRecibos)=>{
        if(!creditoDialog.current)      return
        if(!item.tarjetaDeCreditoId)    return
        try{
            const credito = await TarjetasDeCredito.Get(item.tarjetaDeCreditoId)
            if(!credito) return
            const editedCredito = await creditoDialog.current.editar(credito)
            setItemsDeRecibo(await ItemsDeRecibos.EditItemDeRecibo({...item, importe:editedCredito.importe}))
        }catch(error){}
    },[creditoDialog])
    const delTCredito = useCallback(async(item:AppData_ItemsDeRecibos)=>{
        if(!item.tarjetaDeCreditoId) return
        try{
            await TarjetasDeCredito.Delete(item.tarjetaDeCreditoId)
            setItemsDeRecibo(await ItemsDeRecibos.DeleteItemDeRecibo(item))
        }catch(_){}
    },[])

    const addTDebito=useCallback(async()=>{        
        if(!debitoDialog.current) return
        if(!recibo?.id) return 
        try{
            const debito = await debitoDialog.current.nuevo(recibo.id)
            setItemsDeRecibo(await ItemsDeRecibos.AddItemDeRecibo({
                reciboId:recibo.id,
                importe:debito.importe,
                formaDePagoId:10,
                descripcion:null,
                chequeId:null,
                retencionId:null,
                tarjetaDeCreditoId:null,
                tarjetaDeDebitoId:debito.id!
            }))
        }catch(_){}

    },[debitoDialog, recibo])
    const editTDebito = useCallback(async(item:AppData_ItemsDeRecibos)=>{
        if(!debitoDialog.current)   return
        if(!item.tarjetaDeDebitoId) return
        try{
            const debito = await TarjetasDeDebito.Get(item.tarjetaDeDebitoId)
            if(!debito) return
            const editedDebito = await debitoDialog.current.editar(debito)
            setItemsDeRecibo(await ItemsDeRecibos.EditItemDeRecibo({...item, importe:editedDebito.importe}))
        }catch(error){}
    },[debitoDialog])
    const delTDebito = useCallback(async(item:AppData_ItemsDeRecibos)=>{
        if(!item.tarjetaDeDebitoId) return
        try{
            await TarjetasDeDebito.Delete(item.tarjetaDeDebitoId)
            setItemsDeRecibo(await ItemsDeRecibos.DeleteItemDeRecibo(item))
        }catch(_){}
    },[])

    const handleAddClick=useCallback(async()=>{
        if(!ChooseRef.current) return
        await ChooseRef.current.choose([
            {label:"Efectivo", value:1, callback:addEfectivo},
            {label:"Cheque", value:2, callback:addCheque},
            {label:"Retención", value:9, callback:addRetencion},
            {label:"Tarjeta de Crédito", value:8, callback:addTCredito},
            {label:"Tarjeta de Débito", value:10, callback:addTDebito}
        ])
    },[ChooseRef, addEfectivo, addCheque, addRetencion, addTCredito, addTDebito])
    const handleEditClick=useCallback(async(item:AppData_ItemsDeRecibos)=>{
        switch(item.formaDePagoId){
            case 1:editEfectivo(item); break;
            case 2:editCheque(item); break;
            case 9:editRetencion(item); break;
            case 8:editTCredito(item);break;
            case 10:editTDebito(item);break;
        }
    },[editEfectivo, editCheque, editRetencion, editTCredito, editTDebito])
    const handleDeleteClick = useCallback(async(item:AppData_ItemsDeRecibos)=>{
        switch(item.formaDePagoId){
            case 1: delEfectivo(item);break;
            case 2: delCheque(item);break;
            case 9: delRetencion(item);break;
            case 8: delTCredito(item);break;
            case 10:delTDebito(item);break;
        }
    },[delEfectivo, delCheque, delRetencion, delTCredito, delTDebito])
    //#endregion

    const reciboFisicoClick=useCallback(async()=>{
        if(!comprobanteFisicoRef.current || !recibo)return
        try{
            await comprobanteFisicoRef.current.completar(2,recibo.clienteId)
        }catch(error){}
    },[recibo,comprobanteFisicoRef])
    
    const handleClose = useCallback(async()=>{
        if(!callbacks) return
        callbacks.resolve()
    },[callbacks])
    const show=useCallback(async (cliente_id:number)=>{
        const recibo=await DB.Recibos.GetReciboCliente(cliente_id)
        if(recibo){
            setRecibo(recibo)
        }else{
            setRecibo(await DB.Recibos.CreateRecibo(cliente_id))
        }
        setItemsDeRecibo(await DB.ItemsDeRecibos.GetItemsDeReciboCliente(cliente_id))
        setFacturas(await DB.Facturas.Get(cliente_id))
        setVista("Recibos")
        const p = new Promise<void>((resolve, reject)=>setCallbacks({resolve, reject}))
        p.finally(()=>setCallbacks(undefined))
        return p
    },[])
    useImperativeHandle(ref, ()=>({show}))
    const calcularTotales=useCallback(()=>{
        const recibido=itemsDeRecibo.reduce<number>((total, item)=>total+item.importe,0)
        const adeudado=facturas.reduce<number>((total, factura)=>total+factura.saldoActual,0)
        const imputado=facturas.reduce<number>((total, factura)=>total+factura.imputado,0)
        const aCuenta =recibido-imputado
        setRecibido(recibido)
        setAdeudado(adeudado)
        setImputado(imputado)
        setACuenta(aCuenta)
    },[facturas, itemsDeRecibo])
    const autoImputar=useCallback(async (monto:number)=>{
        if(!recibo) return
        var disponible=monto
        facturas.forEach(factura=>{
            const imputado=Math.min(factura.saldoActual, disponible)
            disponible-=imputado
            factura.imputado=imputado
        })
        await Promise.all(facturas.map(async (factura,index)=>{
            await DB.Facturas.Save(factura)
            if(!!facRefs.current[index]) await facRefs.current[index].imputar(factura.imputado)
        }))
        calcularTotales()
    },[facturas, recibo, facRefs,calcularTotales])
    useEffect(()=>{if(autoImp) autoImputar(recibido)},[autoImp, autoImputar, recibido])
    useEffect(calcularTotales,[calcularTotales])
    const handleImputacionChange=useCallback(async(factura:AppData_Facturas)=>{
        if(!recibo) return
        await DB.Facturas.Save(factura)
        setFacturas(await DB.Facturas.Get(recibo.clienteId))
    },[recibo])
    return (
        <Modal show={!!callbacks} onHide={handleClose} size="sm" backdrop="static" centered keyboard={false} className="cobros_dialog">
            <Modal.Header closeButton><Modal.Title>Cobros</Modal.Title></Modal.Header>
            <Modal.Body>
                <Card className="recibo_resumen">
                    <Form.Row>
                        <Form.Label>Total recibo: <Badge>$ {recibido}</Badge></Form.Label>
                        <Form.Label>Total imputado: <Badge>$ {imputado}</Badge></Form.Label>
                    </Form.Row>
                    <Form.Row>
                        <Form.Label>Total adeudado: <Badge>$ {adeudado}</Badge></Form.Label>
                        <Form.Label>A cuenta: <Badge>$ {aCuenta}</Badge></Form.Label>
                    </Form.Row>
                    <Form.Row>
                        <FcPrint />
                        <Button onClick={reciboFisicoClick}>Recibo fisico</Button>
                    </Form.Row>
                </Card>
                <Form.Label onClick={()=>setAutoImp(!autoImp)}>
                    {autoImp?<RiCheckboxLine/>:<RiCheckboxBlankLine/>}
                    <Form.Label>Imputaciones automáticas</Form.Label>
                </Form.Label>
                {itemsDeRecibo.map(item=>(
                    <Cobro key={item.id!} 
                        itemDeRecibo={item} 
                        onDeleteClick={handleDeleteClick} 
                        onEditClick={handleEditClick} 
                        visible={vista==="Recibos"}
                    />
                ))}
                {facturas.map((factura, index)=>(
                    <Factura key={factura.id!}
                        factura={factura}
                        onChange={handleImputacionChange}
                        visible={vista==="Facturas"}
                        readonly={autoImp}
                        ref={el=>{
                            facRefs.current.length=index
                            return facRefs.current[index]=el!
                        }}
                        disponible={aCuenta}
                    />
                ))}
            </Modal.Body>
            <Modal.Footer>
                <BsFillPlusCircleFill onClick={handleAddClick} className={vista==="Recibos"?"":"d-none"}/>
                <IconBar options={iconOptions} onChange={opcion=>setVista(opcion)} />
                <Button onClick={handleClose}>Cerrar</Button>
            </Modal.Footer>
        </Modal>
    )
})