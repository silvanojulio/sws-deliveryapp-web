import { forwardRef, useImperativeHandle }  from 'react'
import { useState, useCallback }            from 'react'
import { useEffect, useRef }                from 'react'
import { Card, Form, Col }                  from 'react-bootstrap'
import { RiMoneyDollarCircleLine }          from 'react-icons/ri'
import { FcApproval }                       from 'react-icons/fc'
import { FcViewDetails }                    from 'react-icons/fc'
import moment from 'moment'

import { AppData_Facturas } from 'entities/app'
import { useAppContext }    from 'hooks'

type FacturaProps={
    factura:AppData_Facturas
    onChange:(factura:AppData_Facturas)=>void
    visible:boolean
    readonly:boolean
    disponible:number
}

export interface IFactura{
    imputar:(monto:number)=>Promise<void>
}

export const Factura = forwardRef<IFactura, FacturaProps>((props, ref)=>{
    const { ConfirmRef }        = useAppContext()
    const [ monto, setMonto ]   = useState<number>(props.factura.imputado)
    const montoRef = useRef<HTMLInputElement>(null)
    const imputar=useCallback(async(monto:number)=>{
        setMonto(monto)
    },[])
    useImperativeHandle(ref, ()=>({imputar}),[imputar])
    const handleChange=useCallback(async()=>{
        if(!montoRef.current) return setMonto(0)
        if(montoRef.current.value==="") return setMonto(0)
        const newmonto  = montoRef.current.valueAsNumber + 0
        const max       = props.disponible + monto
        const imputado  = Math.min(newmonto, max)
        setMonto(imputado)
        props.onChange({...props.factura, imputado})
    },[props, monto])
    useEffect(()=>{
        if(!montoRef.current) return
        montoRef.current.value=monto.toString()
    },[monto])
    const cubrirSaldo=useCallback(async()=>{
        const disponible=props.disponible+monto
        const saldo=props.factura.saldoActual
        const imputado=Math.min(saldo,disponible)
        props.onChange({...props.factura, imputado})
        setMonto(imputado)
    },[props, monto])
    const askFacturaEntregada=useCallback(async()=>{
        if(!ConfirmRef.current) return
        if(await ConfirmRef.current.prompt("YesNo","Confirma que se ha entregado la factura?","Factura Entregada")){
            props.onChange({...props.factura, entregada:true})
        }
    },[ConfirmRef, props])
    return (
        <Card className={`cobro_factura${props.visible?"":" d-none"}`}>
            <Form.Row className="cabecera">
                <Col xs={5}>
                    <Form.Label>Fecha: {moment(props.factura.fechaFactura).format("DD/MM/YYYY")}</Form.Label>
                    <Form.Label>Importe: ${props.factura.montoFacturaTotal}</Form.Label>

                </Col>
                <Col xs={2}><FcViewDetails/></Col>
                <Col xs={5}>
                    <Form.Label>Nro: {props.factura.nroFactura}</Form.Label>
                    <Form.Label>Saldo: ${props.factura.saldoActual}</Form.Label>
                </Col>
            </Form.Row>
            <Form.Row className="imputacion">
                <Col xs={{offset:0,span:12}}>Importe a imputar</Col>
                <Col xs={{offset:0,span:1}}>$</Col>
                <Col xs={{offset:0,span:4}}>
                    <Form.Control type="number" size="sm" disabled={props.readonly}
                        ref={montoRef}
                        value={monto}
                        onChange={handleChange}
                        onKeyPress={handleChange}
                    />
                </Col>
                <Col xs={{offset:0,span:2}}>
                    <RiMoneyDollarCircleLine color="blue" 
                        style={{fontSize:"2em"}} 
                        onClick={cubrirSaldo}
                    />
                </Col>
                <Col xs={{offset:3,span:2}}>
                    <FcApproval 
                        style={{fontSize:"2em"}}
                        onClick={askFacturaEntregada}
                    />
                </Col>

            </Form.Row>
        </Card>
    )
})