import { forwardRef, useImperativeHandle }          from 'react'
import { useState, useCallback, useEffect, useRef } from 'react'
import { Modal, Container, Form, Button }           from 'react-bootstrap'
import Select                                       from 'react-select'

import { AppData_TarjetasDeDebito }     from 'entities/app'
import { AppData_ValoresSatelites }     from 'entities/app'
import { PromiseCallbacks }             from 'types'
import * as DB                          from 'services/DB'

type BancoSelectOption={
    value:AppData_ValoresSatelites
    label:string
}

export interface IDebito{
    nuevo:(reciboId:number)=>Promise<AppData_TarjetasDeDebito>
    editar:(credito:AppData_TarjetasDeDebito)=>Promise<AppData_TarjetasDeDebito>
}

export const Debito=forwardRef<IDebito>((_,ref)=>{
    const importeRef        = useRef<HTMLInputElement>(null)
    const loteRef           = useRef<HTMLInputElement>(null)
    const cuponRef          = useRef<HTMLInputElement>(null)
    const autorizacionRef   = useRef<HTMLInputElement>(null)
    
    const [ reciboId, setReciboId ]     = useState<number>(0)
    const [ bancoId, setBancoId ]       = useState<number>(0)
    const [ debito, setDebito ]         = useState<AppData_TarjetasDeDebito>()
    const [ callbacks, setCallbacks ]   = useState<PromiseCallbacks<AppData_TarjetasDeDebito>>()
    const [ bancos, setBancos ]         = useState<BancoSelectOption[]>([])

    const cargarValoresSatelites = useCallback(async ()=>{
        setBancos((await DB.ValoresSatelites.GetTablas([DB.ValoresSatelites.T_BANCOS]))
            .map<BancoSelectOption>(banco=>({value:banco, label:banco.valor_texto}))
        )
    },[])
    useEffect(()=>{cargarValoresSatelites()},[cargarValoresSatelites])
    const clearState = useCallback(()=>{
        setReciboId(0)
        setBancoId(0)
        setDebito(undefined)
        setCallbacks(undefined)
    },[])
    const nuevo = useCallback(async(reciboId:number)=>{
        setReciboId(reciboId)
        const p = new Promise<AppData_TarjetasDeDebito>((resolve, reject)=>setCallbacks({resolve, reject}))
        p.finally(clearState)
        return p
    },[clearState])
    const editar = useCallback(async(debito:AppData_TarjetasDeDebito)=>{
        setReciboId(debito.reciboId)
        setDebito(debito)
        setBancoId(debito.banco_id)
        const p = new Promise<AppData_TarjetasDeDebito>((resolve, reject)=>setCallbacks({resolve, reject}))
        p.finally(clearState)
        return p
    },[clearState])
    useImperativeHandle(ref, ()=>({nuevo, editar}))

    const handleSave=useCallback(async () =>{
        if(!callbacks)      return
        if(reciboId<=0)     return
        if(bancoId<=0)      return

        if(!importeRef.current)         return callbacks.reject(new Error("Referencia no establecida (importeRef)"))
        if(!loteRef.current)            return callbacks.reject(new Error("Referencia no establecida (loteRef)"))
        if(!cuponRef.current)           return callbacks.reject(new Error("Referencia no establecida (cuponRef)"))
        if(!autorizacionRef.current)    return callbacks.reject(new Error("Referencia no establecida (autorizacionRef)"))
        try{
            callbacks.resolve(await DB.TarjetasDeDebito.Save({...debito, reciboId,
                lote:loteRef.current.value,
                cupon:cuponRef.current.value,
                codigoAutorizacion:autorizacionRef.current.value,
                banco_id:bancoId,
                importe:importeRef.current.valueAsNumber,
            }))
        }catch(error){}
    },[callbacks, debito, bancoId, reciboId])
    const handleClose=useCallback(async () =>{
        if(!callbacks) return
        callbacks.reject()
    },[callbacks])
    return (
        <Modal show={!!callbacks} size="sm" backdrop="static" centered onHide={handleClose} keyboard={false} className="cobro_debito">
            <Modal.Header closeButton>Tarjeta de Débito</Modal.Header>
            <Modal.Body>
                <Form.Row>
                    <Form.Label column xs={{offset:1, span:3}}>Importe $</Form.Label>
                    <Form.Label column xs={{offset:0, span:7}}>
                        <Form.Control type="number" size="sm" ref={importeRef} defaultValue={debito?.importe??0}/>
                    </Form.Label>
                </Form.Row>
                <Container>
                    <Form.Row>
                        <Form.Label>Lote</Form.Label>
                        <Form.Control type="text" size="sm" ref={loteRef} defaultValue={debito?.lote}/>
                    </Form.Row>
                    <Form.Row>
                        <Form.Label>Cupón</Form.Label>
                        <Form.Control type="text" size="sm" ref={cuponRef} defaultValue={debito?.cupon}/>
                    </Form.Row>
                    <Form.Row>
                        <Form.Label>Cod de Autorización</Form.Label>
                        <Form.Control type="text" size="sm" ref={autorizacionRef} defaultValue={debito?.codigoAutorizacion}/>
                    </Form.Row>
                    <Form.Row>
                        <Form.Label>Banco</Form.Label>
                        <Select<BancoSelectOption> id="banco"
                            options={bancos}
                            value={bancos.find(banco=>banco.value.valor_id===bancoId)}
                            onChange={option=>!!option && setBancoId(option.value.valor_id)}
                        />
                    </Form.Row>
                </Container>
            </Modal.Body>          
            <Modal.Footer>
                <Button onClick={handleSave}>Guardar</Button>
            </Modal.Footer>
        </Modal>
    )
})