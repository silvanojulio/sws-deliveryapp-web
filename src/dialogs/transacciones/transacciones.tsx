import { forwardRef, useImperativeHandle }  from 'react'
import { useState, useCallback }            from 'react'
import { Modal, Button }                    from 'react-bootstrap'
import { FcPrint }                          from 'react-icons/fc'

import { TransaccionCard }  from './transaccionCard'

import { PromiseCallbacks } from 'types'
import { Transaccion }      from 'types'
import { AppData_Clientes } from 'entities/app'

import { useAppContext }    from 'hooks'

import * as DB from 'services/DB'


export interface ITransacciones{
    show:(cliente_id:number)=>Promise<void>
}
export const Transacciones = forwardRef<ITransacciones>((_,ref)=>{
    const app       = useAppContext()
    const [ callbacks, setCallbacks ]           = useState<PromiseCallbacks<void>>()
    const [ transacciones, setTransacciones ]   = useState<Transaccion[]>([])
    const [ cliente, setCliente ]               = useState<AppData_Clientes>()
    const { ToastError }                = app

    const clear=useCallback(async()=>{
        DB.Logs.AddLog({mensaje:"Close Transacciones",funcionalidad:"Transacciones", entidad:"cliente",idEntidad:cliente?.cliente_id})
        setCallbacks(undefined)
        setCliente(undefined)
        setTransacciones([])
    },[cliente])
    const show = useCallback(async(cliente_id:number)=>{
        DB.Logs.AddLog({mensaje:"Open Transacciones",funcionalidad:"Transacciones", entidad:"cliente",idEntidad:cliente_id})
        setTransacciones(await DB.Clientes.GetTransacciones(cliente_id))
        setCliente(await DB.Clientes.GetCliente(cliente_id))
        const p = new Promise<void>((resolve, reject)=>setCallbacks({resolve, reject}))
        p.finally(clear)
        return p
    },[clear])
    useImperativeHandle(ref,()=>({show}),[show])
    const close=useCallback(()=>{
        if(!callbacks) return
        callbacks.resolve()
    },[callbacks])
    const imprimirTransacciones = useCallback(async()=>{
        DB.Logs.AddLog({mensaje:"Touch Imprimir",funcionalidad:"Transacciones", entidad:"cliente",idEntidad:cliente?.cliente_id})
        if(!cliente) return
        try{
            await DB.Clientes.ImprimirComprobante(cliente.cliente_id)
        }catch(error){
            console.error(error)
            ToastError && ToastError((error as Error).message)
        }
    },[cliente, ToastError])
    return(
        <Modal show={!!callbacks} onHide={close} centered size="sm" backdrop="static" keyboard={false} className="transacciones">
            <Modal.Header closeButton>Transacciones</Modal.Header>
            <Modal.Body>
                <Button onClick={imprimirTransacciones}><FcPrint /> Imprimir</Button>
                {cliente && transacciones.map((transaccion,index)=>(<TransaccionCard key={index} transaccion={transaccion} cliente={cliente} />))}
            </Modal.Body>
        </Modal>
    )
})