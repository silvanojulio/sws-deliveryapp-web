import { FC } from 'react'
import { Card, Form, Col } from 'react-bootstrap'
import { MdAddShoppingCart } from 'react-icons/md'
import { MdRemoveShoppingCart } from 'react-icons/md'
import { MdStarBorder } from 'react-icons/md'
import { RiHandCoinLine } from 'react-icons/ri'

import { Transaccion } from 'types'
import { AppData_Clientes } from 'entities/app'

type TransaccionProps={
    transaccion:Transaccion
    cliente:AppData_Clientes
}
export const TransaccionCard:FC<TransaccionProps> = ({transaccion, cliente})=>{
    
    return (
        <Card className="transaccion"> 
            <Form.Row>
                <Col xs={3}>
                    {transaccion.tipo==="entrega"       && <MdAddShoppingCart className="text-success" />}
                    {transaccion.tipo==="prestamo"      && <MdStarBorder className="text-warning" />}
                    {transaccion.tipo==="devolucion"    && <MdRemoveShoppingCart className="text-danger" />}
                    {transaccion.tipo==="pnc"           && <MdRemoveShoppingCart className="text-danger" />}
                    {transaccion.tipo==="cobro"         && <RiHandCoinLine className="text-info"/>}
                </Col>
                <Col>
                    <Form.Label className="w-100">{cliente.nombreCliente} ({cliente.cliente_id})</Form.Label>
                    <Form.Label className="w-100">{transaccion.descripcion}</Form.Label>
                    <Form.Label className="w-100">
                        {transaccion.tipo==="entrega"       && `Entrega x ${transaccion.cantidad}`}
                        {transaccion.tipo==="devolucion"    && `Devolución x ${transaccion.cantidad}`}
                        {transaccion.tipo==="pnc"           && `PNC $${transaccion.cantidad}`}
                        {transaccion.tipo==="prestamo"      && `Prestamo x ${transaccion.cantidad}`}
                        {transaccion.tipo==="cobro"         && `Cobro $${transaccion.cantidad}`}
                    </Form.Label>
                </Col>
            </Form.Row>
        </Card>
    )
}