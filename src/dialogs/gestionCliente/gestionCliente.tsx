import { forwardRef, useImperativeHandle }  from 'react'
import { useCallback, useState }            from 'react'
import { Modal, Button }                    from 'react-bootstrap'
import { Form }                             from 'react-bootstrap'
import { FiCoffee, FiBell, FiMapPin }       from 'react-icons/fi'
import { RiMoneyDollarCircleLine }          from 'react-icons/ri'
import { GiShoppingCart }                   from 'react-icons/gi'
import { FaAmbulance, FaRegListAlt }        from 'react-icons/fa'
import { FcCancel, FcGlobe }                from 'react-icons/fc'
import { HiOutlineSwitchHorizontal }        from 'react-icons/hi'
import { RiChatPollLine }                   from 'react-icons/ri'
import { BsInfoCircleFill }                 from 'react-icons/bs'

import { AppData_Clientes }                 from 'entities/app'
import { useHojaDeRutaContext }             from 'hooks'
import { useAppContext }                    from 'hooks'
import { LabeledIcon }                      from 'components/labeledIcon'
import { PromiseCallbacks }                 from 'types'
import * as DB                              from 'services/DB'
import { AppStorage }                       from 'utils'

type GestionClienteProps={
}
export interface IGestionCliente{
    open:(cliente:AppData_Clientes)=>Promise<void>
}
export const GestionCliente = forwardRef<IGestionCliente, GestionClienteProps>((_, ref)=>{
    const context   = useHojaDeRutaContext()
    const app       = useAppContext()
    const { entregaDialog }             = context
    const { alertasDialog }             = context
    const { cobrosDialog  }             = context
    const { devolucionesDialog }        = context
    const { tomarCoordenadasDialog }    = context
    const { servicioTecnicoDialog }     = context
    const { transaccionesDialog }       = context
    const { fichaInfoClienteDialog }    = context
    const { ChooseRef }                 = app
    const { ConfirmRef }                = app
    const { ToastError }                = app
    const { printerOn }                 = app
    const [ cliente, setCliente ]       = useState<AppData_Clientes>()
    const [ callbacks, setCallbacks ]   = useState<PromiseCallbacks<void>>()
    
    useImperativeHandle(ref, ()=>({
        open: async (cliente:AppData_Clientes)=>{
            const p = new Promise<void>((resolve, reject)=>setCallbacks({resolve, reject}))
            p.finally(()=>{
                setCliente(undefined)
                setCallbacks(undefined)
            })
            setCliente({...cliente})
            return p
        }
    }))

    const verificarAlertas=useCallback(async ()=>{
        DB.Logs.AddLog({mensaje:"Verificar Alertas",funcionalidad:"GestionCliente", entidad:"cliente",idEntidad:cliente?.cliente_id})
        if(!cliente || !alertasDialog?.current) return true
        if(cliente.haVistoAlertas || !cliente.tieneAlertas) return true
        const alertas = await DB.AlertasDeCliente.GetAlertas(cliente.cliente_id)
        if(await alertasDialog.current.show(alertas)){
            cliente.haVistoAlertas=true
            return true
        }
        return false
    },[cliente, alertasDialog])
    const guardarGestion=useCallback(async ()=>{
        if(!callbacks || !cliente) return
        DB.Logs.AddLog({mensaje:"Iniciar guardado de gestión",funcionalidad:"GestionCliente", entidad:"cliente",idEntidad:cliente.cliente_id})
        await DB.Clientes.MarcarCerrado(cliente.cliente_id)     
        if(!ConfirmRef.current) return callbacks.resolve()
        if(printerOn && await ConfirmRef.current.prompt("YesNo","Desea imprimir comprobante?","Confirmar Impresion")) {
            DB.Clientes.ImprimirComprobante(cliente.cliente_id).catch(error=>{
                console.error(error)
                ToastError && ToastError((error as Error).message)
            })
        } 
        callbacks.resolve()
        DB.Logs.AddLog({mensaje:"Fin guardado de gestión",funcionalidad:"GestionCliente", entidad:"cliente",idEntidad:cliente.cliente_id})
    },[callbacks, cliente, ConfirmRef, ToastError, printerOn])
    const marcarAusente=useCallback(async()=>{
        DB.Logs.AddLog({mensaje:"Touch Ausente",funcionalidad:"GestionCliente", entidad:"cliente",idEntidad:cliente?.cliente_id})
        if(!cliente || !callbacks) return        
        if(!ConfirmRef.current) return
        const entregados = await DB.ArticulosEntregados.GetArticulosEntregadosACliente(cliente.cliente_id)
        const cobros     = await DB.ItemsDeRecibos.GetItemsDeReciboCliente(cliente.cliente_id)
        if(entregados.length+cobros.length>0){
            await ConfirmRef.current.prompt("OkOnly","El cliente tiene cobros o entregas cargadas. Debe revertir estas gestiones si desea marcar ausente.","Gestiones Cargadas")
            return
        }
        if(!await ConfirmRef.current.prompt("YesNo","Desea marcar el cliente como ausente?","Confirmar Ausente")) return
        DB.Logs.AddLog({mensaje:"Iniciar marcar ausente",funcionalidad:"GestionCliente", entidad:"cliente",idEntidad:cliente.cliente_id})
        await DB.Clientes.MarcarAusente(cliente.cliente_id)               
        guardarGestion()
        DB.Logs.AddLog({mensaje:"Fin marcar ausente",funcionalidad:"GestionCliente", entidad:"cliente",idEntidad:cliente.cliente_id})
    },[cliente, callbacks, guardarGestion, ConfirmRef])
    const nuevaEntrega=useCallback(async ()=>{ 
        DB.Logs.AddLog({mensaje:"Touch Entrega",funcionalidad:"GestionCliente", entidad:"cliente",idEntidad:cliente?.cliente_id})

        if(!cliente) return
        if(cliente.fechaHoraTransferido) {
            if(!ConfirmRef.current) return
            if(await ConfirmRef.current.prompt("YesNo","El cliente ya fue sincronizado. Imprimir comprobante?","Confirmar Impresion")){
                await DB.Clientes.ImprimirComprobante(cliente.cliente_id)
            }
            return
        }
        if(!await verificarAlertas()) return
        if(!await DB.LimitesDeClientes.ValidarLimites(cliente.cliente_id)) return ToastError("El cliente excede los limites de Facturas o Saldos pendientes",{autoClose:2000})
        if(!entregaDialog?.current || !cliente) return
        try{
            await entregaDialog.current.nueva(cliente)
        }catch(error){}
    },[cliente, entregaDialog, verificarAlertas, ToastError, ConfirmRef])
    const verCobros=useCallback(async()=>{
        DB.Logs.AddLog({mensaje:"Touch Cobros",funcionalidad:"GestionCliente", entidad:"cliente",idEntidad:cliente?.cliente_id})

        if(!cobrosDialog.current || !cliente) return
        try{
            await cobrosDialog.current.show(cliente.cliente_id)
        }catch(error){}
    },[cobrosDialog, cliente])
    const verDevoluciones = useCallback(async()=>{
        DB.Logs.AddLog({mensaje:"Touch Devoluciones",funcionalidad:"GestionCliente", entidad:"cliente",idEntidad:cliente?.cliente_id})
        if(!devolucionesDialog.current) return
        if(!cliente) return
        await devolucionesDialog.current.show(cliente.cliente_id)
    },[devolucionesDialog, cliente])
    const relevarCoords = useCallback(async()=>{
        DB.Logs.AddLog({mensaje:"Touch Coordenadas",funcionalidad:"GestionCliente", entidad:"cliente",idEntidad:cliente?.cliente_id})
        if(!tomarCoordenadasDialog.current) return
        if(!cliente) return
        const lat = parseFloat(await AppStorage.getString("CURRENTLATITUD","0"))
        const lng = parseFloat(await AppStorage.getString("CURRENTLONGITUD","0"))
        await tomarCoordenadasDialog.current.obtenerCoordenadas(cliente.cliente_id, {lat,lng})
        /*
        try{
            navigator.geolocation.getCurrentPosition(async loc=>{   
                if(!tomarCoordenadasDialog.current) return
                if(!cliente) return              
                await tomarCoordenadasDialog.current.obtenerCoordenadas(cliente.cliente_id, {
                    lat:loc.coords.latitude,
                    lng:loc.coords.longitude
                })
            },error=>{ToastError("Error al acceder a la ubicacion del movil")},{timeout:2000})
        }catch(error){}
        */
    },[tomarCoordenadasDialog, cliente])
    const verServicioTecnico = useCallback(async()=>{
        if(!servicioTecnicoDialog.current) return
        if(!cliente) return
        await servicioTecnicoDialog.current.show(cliente.cliente_id)
    },[servicioTecnicoDialog, cliente])
    const ventaWebClick=useCallback(async()=>{ 
        DB.Logs.AddLog({mensaje:"Touch Venta Web",funcionalidad:"GestionCliente", entidad:"cliente",idEntidad:cliente?.cliente_id})
        if(!cliente) return
        const url = await AppStorage.getString("SERVERDOMAIN")
        const hojaDeRuta_id = await AppStorage.getString("HojaDeRutaId")
        
        window.open(`http://${url}/TransaccionesTemporales/Create?clienteId=${cliente.cliente_id}&hojaDeRutaId=${hojaDeRuta_id}`,"_blank")
    },[cliente])
    const marcarSinOperaciones = useCallback(async()=>{
        DB.Logs.AddLog({mensaje:"Touch Sin Operaciones",funcionalidad:"GestionCliente", entidad:"cliente",idEntidad:cliente?.cliente_id})
        if(!ConfirmRef.current) return
        if(!cliente) return
        const entregados = await DB.ArticulosEntregados.GetArticulosEntregadosACliente(cliente.cliente_id)
        const cobros     = await DB.ItemsDeRecibos.GetItemsDeReciboCliente(cliente.cliente_id)
        if(entregados.length+cobros.length>0){
            await ConfirmRef.current.prompt("OkOnly","El cliente tiene cobros o entregas cargadas. Debe revertir estas gestiones si desea marcar sin operaciones.","Gestiones Cargadas")
            return
        }
        if(!await ConfirmRef.current.prompt("YesNo", "Registrar visita sin operaciones?","Confirmar Sin Operaciones")) return
        guardarGestion()
    },[ConfirmRef, guardarGestion, cliente])
    const handleIncidentes = useCallback(async()=>{
        DB.Logs.AddLog({mensaje:"Touch Incidentes",funcionalidad:"GestionCliente", entidad:"cliente",idEntidad:cliente?.cliente_id})
        if(!cliente) return
        if(!ChooseRef.current) return
        const url = await AppStorage.getString("SERVERDOMAIN")
        switch(await ChooseRef.current.choose([
            {label:"Crear nuevo incidente", value:"nuevo"},
            {label:"Historial de incidentes", value:"historial"},
        ])){
            case "nuevo":
                window.open(`http://${url}/Incidentes/Create?cliente_id=${cliente.cliente_id}`,"_blank")
                break;
            case "historial":
                window.open(`http://${url}/Incidentes/Historial?cliente_id=${cliente.cliente_id}`,"_blank")
                break;
        }
    },[cliente, ChooseRef])
    const verTransacciones = useCallback(async()=>{
        DB.Logs.AddLog({mensaje:"Touch Transacciones",funcionalidad:"GestionCliente", entidad:"cliente",idEntidad:cliente?.cliente_id})
        if(!cliente) return
        if(!transaccionesDialog.current) return
        await transaccionesDialog.current.show(cliente.cliente_id)
    },[cliente, transaccionesDialog])
    const cancelarGestion = useCallback(async()=>{
        if(!cliente || !callbacks) return
        await DB.Clientes.ResetCliente(cliente.cliente_id)
        callbacks.resolve()
    },[cliente, callbacks])
    const marcarRepaso = useCallback(async()=>{
        if(!cliente || !callbacks) return
        await DB.Clientes.MarcarRepaso(cliente.cliente_id)
        callbacks.resolve()
    },[cliente, callbacks])
    const close=useCallback(async()=>{
        DB.Logs.AddLog({mensaje:"Touch Close",funcionalidad:"GestionCliente", entidad:"cliente",idEntidad:cliente?.cliente_id})
        if(!callbacks || !cliente || !ChooseRef.current) return
        const transacciones = await DB.Clientes.GetTransacciones(cliente.cliente_id)
        if(transacciones.length>0){
            await ChooseRef.current.choose([
                {
                    value:"cancelar",
                    label:"Cancelar gestion",
                    callback:cancelarGestion
                },
                {
                    value:"repaso",
                    label:"Marcar para Repaso",
                    callback:marcarRepaso
                }
            ])
        }else{
            cancelarGestion()
        }
    },[callbacks, cliente, cancelarGestion, marcarRepaso, ChooseRef])
    const verFichaInfo = useCallback(async()=>{
        DB.Logs.AddLog({mensaje:"Touch Ver Ficha",funcionalidad:"GestionCliente", entidad:"cliente",idEntidad:cliente?.cliente_id})
        if(!fichaInfoClienteDialog.current || !cliente) return
        await fichaInfoClienteDialog.current.show(cliente.cliente_id)
    },[cliente, fichaInfoClienteDialog])
    return (
        <Modal show={!!callbacks} size="sm" onHide={close} backdrop="static" centered keyboard={false} className="gestion-cliente-menu" >
            <Modal.Header closeButton>
                <BsInfoCircleFill fontSize="1.5rem" className="text-info" onClick={verFichaInfo}/>
                <Form.Row>
                    <Form.Label xs={12} column>{cliente?.nombreCliente??"Cliente"} ({cliente?.cliente_id??0})</Form.Label>
                    <Form.Label xs={12} column>{cliente?.domicilioCompleto??"Domicilio"}</Form.Label>
                </Form.Row>
            </Modal.Header>
            <Modal.Body>
                <Form.Row>
                    <LabeledIcon iconColor="green" label="Entregas"         onClick={nuevaEntrega}                  Icono={GiShoppingCart}              />
                    <LabeledIcon iconColor="black" label="Ventas Web"       onClick={ventaWebClick}                 Icono={FcGlobe}                     />
                </Form.Row>
                <Form.Row>
                    <LabeledIcon iconColor="darkviolet" label="Cobros"      onClick={verCobros}                     Icono={RiMoneyDollarCircleLine}     />
                    <LabeledIcon iconColor="darkred" label="Devoluciones"   onClick={verDevoluciones}               Icono={HiOutlineSwitchHorizontal}   />
                    <LabeledIcon iconColor="green" label="Incidentes"       onClick={handleIncidentes}              Icono={RiChatPollLine}              />
                </Form.Row>
                <Form.Row>
                    <LabeledIcon iconColor="darkred" label="Ausente"        onClick={marcarAusente}                 Icono={FiCoffee}                    />
                    <LabeledIcon iconColor="orange" label="Sin Operaciones" onClick={marcarSinOperaciones}          Icono={FcCancel}                    />
                    <LabeledIcon iconColor="darkred" label="Alertas"        onClick={verificarAlertas}              Icono={FiBell}                      />
                </Form.Row>
                <Form.Row>
                    <LabeledIcon iconColor="darkcyan" label="Coordenadas"    onClick={relevarCoords}                Icono={FiMapPin}                    />
                    <LabeledIcon iconColor="lightblue" label="Transacciones" onClick={verTransacciones}             Icono={FaRegListAlt}                />
                    <LabeledIcon iconColor="darkcyan" label="Serv Técnico"   onClick={verServicioTecnico} disabled  Icono={FaAmbulance}                 />
                </Form.Row>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={guardarGestion}>Guardar y Sincronizar</Button>
            </Modal.Footer>
        </Modal>
    )
})