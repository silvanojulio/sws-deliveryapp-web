import { forwardRef, useImperativeHandle }  from 'react'
import { useCallback, useState, useRef }    from 'react'
import { Modal, Button }                    from 'react-bootstrap'
import { BsFillPlusCircleFill }             from 'react-icons/bs'

import { Devolucion } from './devolucion'


import { PromiseCallbacks }                     from 'types'
import { useAppContext }                        from 'hooks'
import * as DB                                  from 'services/DB'
import { AppData_DevolucionesArticulos }        from 'entities/app'

import { NuevaDevolucion, INuevaDevolucion }    from './nuevaDevolucion'

export interface IDevoluciones{
    show:(cliente_id:number)=>Promise<void>
}
export const Devoluciones = forwardRef<IDevoluciones>((_,ref)=>{
    const { ChooseRef }                         = useAppContext()
    const [ cliente_id, setCliente_id ]         = useState<number>()
    const [ callbacks, setCallbacks ]           = useState<PromiseCallbacks<void>>()
    const [ devoluciones, setDevoluciones ]     = useState<AppData_DevolucionesArticulos[]>([])
    const nuevaDevolucionRef                    = useRef<INuevaDevolucion>(null)

    const show = useCallback(async(cliente_id:number)=>{
        setCliente_id(cliente_id)
        setDevoluciones(await DB.DevolucionesArticulos.Get(cliente_id))
        const p = new Promise<void>((resolve, reject)=>setCallbacks({resolve,reject}))
        p.finally(()=>setCallbacks(undefined))
        return p
    },[])
    useImperativeHandle(ref, ()=>({show}),[show])
    const handleAddClick = useCallback(async()=>{
        if(!ChooseRef.current) return
        if(!nuevaDevolucionRef.current) return
        if(!cliente_id) return
        try{
            const devolucion=await nuevaDevolucionRef.current.nuevo(cliente_id,await ChooseRef.current.choose([
                {label:"Producto Conforme",      value:"Reutilizable"},
                {label:"Producto No Conforme",   value:"Fallado"     }
            ], "Nueva Devolución"))
            setDevoluciones(await DB.DevolucionesArticulos.Save(devolucion))
        }catch(error){}
    },[ChooseRef, cliente_id])
    const handleCloseClick = useCallback(async ()=>{
        if(!callbacks) return
        callbacks.resolve()
    },[callbacks])
    const eliminarDevolucion = useCallback(async (devolucion:AppData_DevolucionesArticulos)=>{
        setDevoluciones(await DB.DevolucionesArticulos.Delete(devolucion))
    },[])
    const acutalizarDevolucion = useCallback(async(devolucion:AppData_DevolucionesArticulos)=>{
        setDevoluciones(await DB.DevolucionesArticulos.Save(devolucion))
    },[])

    return (
        <Modal show={!!callbacks} size="sm" onHide={handleCloseClick} backdrop="static" centered keyboard={false} className="PNCDialog" >
            <NuevaDevolucion ref={nuevaDevolucionRef}/>
            <Modal.Header closeButton >Devolucion de Productos</Modal.Header>
            <Modal.Body>
                {devoluciones.map(devolucion=>(
                    <Devolucion key={devolucion.id} devolucion={devolucion} onChange={acutalizarDevolucion} onDelete={eliminarDevolucion}/>
                ))}
            </Modal.Body>
            <Modal.Footer>
                <BsFillPlusCircleFill onClick={handleAddClick} />
            </Modal.Footer>
            <Modal.Footer><Button onClick={handleCloseClick}>Cerrar</Button></Modal.Footer>
        </Modal>
    )
})