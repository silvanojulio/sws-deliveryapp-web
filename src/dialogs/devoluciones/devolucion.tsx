import { FC, useEffect, useRef }        from 'react'
import { useState, useCallback }        from 'react'
import { Card, Form, Button, Badge }    from 'react-bootstrap'


import { NumericField, INumericField }      from 'components'
import { AppData_DevolucionesArticulos }    from 'entities/app'


type DevolucionProps={
    devolucion:AppData_DevolucionesArticulos
    onDelete:(devolucion:AppData_DevolucionesArticulos)=>Promise<void>
    onChange:(devolucion:AppData_DevolucionesArticulos)=>Promise<void>
}

export const Devolucion:FC<DevolucionProps> = ({devolucion, onChange, onDelete})=>{
    const cantidadRef               = useRef<INumericField>(null)
    const [ cantidad, setCantidad ] = useState<number>(devolucion.cantidad)
    
    //useEffect(()=>{setCantidad(props.devolucion.cantidad)},[props.devolucion])
    //useEffect(()=>{props.onChange({...props.devolucion, cantidad})},[props, cantidad])
    useEffect(()=>{
        if(!cantidadRef.current) return
        cantidadRef.current.setValue(devolucion.cantidad)
    },[devolucion])
    const update=useCallback(async(cantidad:number)=>{
        if(cantidadRef.current) cantidadRef.current.setValue(cantidad)
        setCantidad(cantidad)
        onChange({...devolucion, cantidad})
    },[devolucion, onChange])
    return (
        <Card className="PNCItem">
            <Card.Header >
                <Form.Label>{devolucion.nombreArticulo}</Form.Label>
                <Button variant="transparent" className="close" onClick={()=>{onDelete(devolucion)}}>{String.fromCharCode(215)}</Button>
            </Card.Header>
            <Card.Body>
                <Form.Row>
                    <Form.Label column xs={4}>Motivo</Form.Label>
                    <Form.Label column xs={8}><Badge variant="info">{devolucion.motivoDevolucion}</Badge></Form.Label>
                    <Form.Label column xs={4}>Cantidad</Form.Label>
                    <NumericField
                        colProps={{xs:4}}
                        defaultValue={cantidad}
                        onChange={update}
                        />
                    <Form.Label column xs={6}><Badge variant={devolucion.esReutilizable?"success":"danger"}>{devolucion.esReutilizable?"":"NO "}Reutilizable</Badge></Form.Label>
                    <Form.Label column xs={6}><Badge variant={devolucion.esCambioDirecto?"success":"danger"}>{devolucion.esCambioDirecto?"Con":"Sin"} Cambio Directo</Badge></Form.Label>
                </Form.Row>
            </Card.Body>
        </Card>
    )
}