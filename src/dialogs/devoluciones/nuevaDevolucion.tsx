import { forwardRef, useImperativeHandle }  from 'react'
import { useState, useCallback }            from 'react'
import { Modal, Button, Form, Badge }       from 'react-bootstrap'
import { RiCheckboxLine, RiCheckboxBlankLine } from 'react-icons/ri'
import Select from 'react-select'
import moment from 'moment'

import { AppData_DevolucionesArticulos }    from 'entities/app'
import { AppData_ArticulosDeLista }         from 'entities/app'
import { AppData_ValoresSatelites }         from 'entities/app'
import { PromiseCallbacks, SelectOption }   from 'types'
import * as DB                              from 'services/DB'
import { useAppContext }                    from 'hooks'


type MotivoOption   = SelectOption<AppData_ValoresSatelites>
type ArticuloOption = SelectOption<AppData_ArticulosDeLista>
type TipoDevolucion = "Fallado" | "Reutilizable"

export interface INuevaDevolucion{
    nuevo:(cliente_id:number, tipo:TipoDevolucion)=>Promise<AppData_DevolucionesArticulos>
}
export const NuevaDevolucion = forwardRef<INuevaDevolucion>((_,ref)=>{
    const { ToastError }                    = useAppContext()
    const [ callbacks, setCallbacks ]       = useState<PromiseCallbacks<AppData_DevolucionesArticulos>>()
    const [ motivo_id, setMotivo_id ]       = useState<number>()
    const [ articulo_id, setArticulo_id ]   = useState<number>()
    const [ cliente_id, setCliente_id ]     = useState<number>()
    const [ recambio, setRecambio ]         = useState<boolean>(false)
    const [ articulos, setArticulos ]       = useState<ArticuloOption[]>([])
    const [ motivos, setMotivos ]           = useState<MotivoOption[]>([])
    const [ tipoDev, setTipoDev ]           = useState<TipoDevolucion>()

    const loadArticulos = useCallback(async()=>{
        const articulos = await DB.ArticulosDeLista.GetArticulos()
        setArticulos(articulos.map<ArticuloOption>(value=>({label:value.nombreArticulo, value})))
    },[])
    const loadMotivos = useCallback(async()=>{
        const motivos = await DB.ValoresSatelites.GetTablas([DB.ValoresSatelites.T_MOTIVOSDEDEVOLUCION])
        setMotivos(motivos.map<MotivoOption>(value=>({label:value.valor_texto, value})))
    },[])
    const nuevo = useCallback(async(cliente_id:number, tipo:TipoDevolucion)=>{
        setTipoDev(tipo)
        setArticulo_id(undefined)
        setCliente_id(cliente_id)
        switch(tipo){
            case "Fallado":
                setRecambio(true)
                setMotivo_id(undefined)
                break;
            case "Reutilizable":
                setRecambio(false)
                setMotivo_id(1)
                break;
        }
        await loadArticulos()
        await loadMotivos()
        const p=new Promise<AppData_DevolucionesArticulos>((resolve, reject)=>setCallbacks({resolve, reject}))
        p.finally(()=>setCallbacks(undefined))
        return p
    },[loadMotivos, loadArticulos])
    useImperativeHandle(ref,()=>({nuevo}),[nuevo])

    const save=useCallback(async()=>{
        if(!callbacks) return
        if(!articulo_id) return ToastError("Debe especificar un Articulo")
        if(!motivo_id)   return ToastError("Debe especificar un Motivo")
        if(!cliente_id)  return ToastError("No se recibió el id del cliente")
        const motivo=motivos.find(motivo=>motivo.value.valor_id===motivo_id)
        const articulo=articulos.find(articulo=>articulo.value.id===articulo_id)
        if(!motivo)     return ToastError("Error Interno: motivo no encontrado en listado")
        if(!articulo)   return ToastError("Error Interno: articulo no encontrado en listado")
        callbacks.resolve({
            articulo_id, cliente_id,
            esCambioDirecto:recambio,
            motivoDevolucion_ids:motivo_id,
            cantidad:1,
            esReutilizable:motivo_id===1,
            fecha:moment( new Date()).format("DD/MM/YYYY"),
            motivoDevolucion:motivo.label??"",
            nombreArticulo:articulo.label??""
        })
    },[callbacks, articulo_id, motivo_id, articulos, motivos, cliente_id, recambio, ToastError])
    const cancel=useCallback(()=>{
        if(!callbacks) return
        callbacks.reject()
    },[callbacks])
    return (
        <Modal show={!!callbacks} size="sm" onHide={cancel} backdrop="static" centered keyboard={false} className="PNCNuevo" >
            <Modal.Header closeButton>{`Devolucion de producto ${tipoDev==="Fallado"?"NO ":""}Conforme`}</Modal.Header>
            <Modal.Body>
                <Form.Row>
                    <Form.Label column xs={4}>Articulo</Form.Label>
                    <Form.Label column xs={8}>
                        <Select<ArticuloOption> id="articulo" 
                            options={articulos}
                            selected={articulos.find(articulo=>articulo.value.id===articulo_id)}
                            onChange={articulo=>setArticulo_id(articulo?.value.id)}
                            />
                    </Form.Label>
                </Form.Row>
                {tipoDev==="Fallado" &&
                    <Form.Row>
                        <Form.Label column xs={4}>Motivo</Form.Label>
                        <Form.Label column xs={8}>
                            <Select<MotivoOption> id="motivo"
                                options={motivos.filter(motivo=>motivo.value.valor_id!==1)}
                                selected={motivos.find(motivo=>motivo.value.valor_id===motivo_id)}
                                onChange={motivo=>setMotivo_id(motivo?.value.valor_id)}
                                />
                        </Form.Label>
                        <Form.Label column xs={5}>Cambio Directo?</Form.Label>
                        <Form.Label column xs={1} onClick={()=>{setRecambio(!recambio)}}>
                            {recambio?<RiCheckboxLine/>:<RiCheckboxBlankLine/>}
                        </Form.Label>
                        <Form.Label column xs={6}>
                            <Badge variant={recambio?"success":"danger"} onClick={()=>{setRecambio(!recambio)}}>
                                {`${recambio?"CON":"SIN"} Cambio Directo`}
                            </Badge>
                        </Form.Label>
                    </Form.Row>
                }
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={save}  >Guardar</Button>
            </Modal.Footer>
        </Modal>
    )
})