import { FC } from 'react'
import { Card, Form, Col } from 'react-bootstrap'

import { AppData_Contactos } from 'entities/app'

type ContactoCardProps=AppData_Contactos

export const ContactoCard:FC<ContactoCardProps> = contacto=>{

    return (
        <Card className="contacto">
            <Card.Header>
                <Form.Row>
                    <Col xs={12}>{contacto.nombreContacto}</Col>
                </Form.Row>
            </Card.Header>
            <Card.Body>
                <Form.Row>
                    <Col xs={3}>Tipo: </Col>
                    <Col xs={9}>{contacto.tipoContacto}</Col>
                    {contacto.mail!=="" && (
                        <>
                            <Col xs={3}>Email </Col>
                            <Col xs={9}>{contacto.mail}</Col>
                        </>
                    )}
                    {contacto.celular!=="" && (
                        <>
                            <Col xs={3}>Celular </Col>
                            <Col xs={9}>{contacto.celular}</Col>
                        </>
                    )}
                    {contacto.telefono!=="" && (
                        <>
                            <Col xs={3}>Telefono: </Col>
                            <Col xs={9}>{contacto.telefono}</Col>
                        </>
                    )}
                </Form.Row>
            </Card.Body>
        </Card>
    )

}