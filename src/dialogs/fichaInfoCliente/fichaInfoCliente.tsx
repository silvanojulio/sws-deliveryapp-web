import { forwardRef, useImperativeHandle }  from 'react'
import { useCallback, useState, useEffect } from 'react'
import { Modal, Row, Col, Card, Button }    from 'react-bootstrap'
import { RiArrowDownSLine, RiArrowUpSLine } from 'react-icons/ri'
import { FiShare2 }                         from 'react-icons/fi'
import { FaWhatsapp }                       from 'react-icons/fa'    

import * as DB from 'services/DB'
import { AppData_Clientes }             from 'entities/app'
import { AppData_ItemsDeRecibos }       from 'entities/app'
import { AppData_ArticulosEntregados }  from 'entities/app'
import { AppData_Contactos }            from 'entities/app'
import { AppData_ResumenClientes }      from 'entities/app'

import { PromiseCallbacks }             from 'types'
import { dateParser }                   from 'utils'
import { AppStorage }                   from 'utils'
import { useHojaDeRutaContext }         from 'hooks'

import { ContactoCard }                 from './contacto'

const Secciones = {
    Hide:0,
    Contactos:1,
    Saldos:2,
    Acceso:4
}

export interface IFichaInfoCliente{
    show:(cliente_id:number)=>Promise<void>
}

export const FichaInfoCliente=forwardRef<IFichaInfoCliente>((_,ref)=>{
    const context                       = useHojaDeRutaContext()
    const { whatsappShareDialog }       = context
    const [ callbacks,  setCallbacks]   = useState<PromiseCallbacks<void>>()
    const [ cliente,    setCliente  ]   = useState<AppData_Clientes>()
    const [ resumen,    setResumen  ]   = useState<AppData_ResumenClientes>()
    const [ cobros,     setCobros   ]   = useState<AppData_ItemsDeRecibos[]>([])
    const [ articulos,  setArticulos]   = useState<AppData_ArticulosEntregados[]>([])
    const [ contactos,  setContactos]   = useState<AppData_Contactos[]>([])
    const [ saldo,      setSaldo    ]   = useState<number>(0)
    const [ credito,    setCredito  ]   = useState<number>(0)
    const [ seccion,    setSeccion  ]   = useState<number>(Secciones.Hide)
    const [ url,        setUrl      ]   = useState<string>("")

    const show=useCallback(async(cliente_id:number)=>{
        const p = new Promise<void>((resolve, reject)=>{
            setCallbacks({resolve, reject})
        })
        setCliente(await DB.Clientes.GetCliente(cliente_id))
        setResumen(await DB.ResumenClientes.GetResumenCliente(cliente_id))
        setCobros(await DB.ItemsDeRecibos.GetItemsDeReciboCliente(cliente_id))
        setArticulos(await DB.ArticulosEntregados.GetArticulosEntregadosACliente(cliente_id))
        setContactos(await DB.Contactos.GetAllCliente(cliente_id))
        setUrl("http://" + await AppStorage.getString("SERVERDOMAIN"))
        p.finally(()=>{
            setCallbacks(undefined)
            setCliente(undefined)
            setCobros([])
            setArticulos([])      
            setContactos([])      
            setSeccion(Secciones.Hide)
        })
        return p
    },[])
    const close = useCallback(()=>{
        if(!callbacks) return
        callbacks.resolve()
    },[callbacks])
    useImperativeHandle(ref, ()=>({show}),[show])
    useEffect(()=>{
        if(!cliente) {
            setSaldo(0)
            setCredito(0)
            return
        }
        const temp =    cliente.saldoConsumos + 
                        cliente.saldoFacturacion + 
                        articulos.reduce<number>((total,art)=>{
                            return total + art.subtotal
                        },0) -
                        cobros.reduce<number>((total, cobro)=>{
                            return total + cobro.importe
                        },0)
        setSaldo(Math.max(0,temp))
        setCredito(Math.abs(Math.min(0,temp)))
    },[cliente, cobros, articulos])
    const compartirWhatsapp=useCallback(async(data:string)=>{
        if(!whatsappShareDialog.current || !cliente) return
        whatsappShareDialog.current.share(data, cliente.cliente_id)
    },[whatsappShareDialog, cliente])
    return (
        <Modal show={!!callbacks && !!cliente} onHide={close} size="sm" backdrop="static" centered keyboard={false} className="fichaInfo" >
            <Modal.Header closeButton className="bg-light">
                Ficha del Cliente
            </Modal.Header>
            <Modal.Body>
                <Card>
                    <Card.Body><Row>
                        <Col xs={12} >Código: </Col>
                        <Col xs={{span:11, offset:1}} >{cliente?.cliente_id}</Col>
                        <Col xs={12} >Nombre: </Col> 
                        <Col xs={{span:11, offset:1}} >{cliente?.nombreCliente}</Col>
                        <Col xs={12} >Dirección: </Col>
                        <Col xs={{span:11, offset:1}} >{cliente?.domicilioCompleto}</Col>
                    </Row></Card.Body>
                    <Card.Header onClick={()=>setSeccion(seccion ^ Secciones.Contactos)}>
                        Contactos
                        {(seccion & Secciones.Contactos)!==Secciones.Hide?<RiArrowUpSLine/>:<RiArrowDownSLine/>}
                    </Card.Header>
                    <Card.Body hidden={(seccion & Secciones.Contactos)===Secciones.Hide}>
                        {contactos.map(contacto=><ContactoCard {...contacto} key={contacto.id}/>)}
                    </Card.Body>
                    <Card.Header onClick={()=>setSeccion(seccion ^ Secciones.Saldos)}>
                        Saldos
                        {(seccion & Secciones.Saldos)!==Secciones.Hide?<RiArrowUpSLine/>:<RiArrowDownSLine/>}
                    </Card.Header>
                    <Card.Body hidden={(seccion & Secciones.Saldos)===Secciones.Hide}>
                        <Row>
                            <Col xs={6} >Tipo: </Col>
                            <Col xs={6} >{cliente?.tipoCliente_ids===1?"Familia":"Empresa"}</Col>
                            <Col xs={6} >Saldo: </Col>
                            <Col xs={6} >$ {saldo.toFixed(2)}</Col>
                            <Col xs={6} >Crédito: </Col>
                            <Col xs={6} >$ {credito.toFixed(2)}</Col>
                            <Col xs={6} >Útltima venta: </Col>
                            <Col xs={6} >{dateParser(cliente?.fechaUtlimaEntrega)}</Col>
                            <Col xs={6} >Último cobro: </Col>
                            <Col xs={6} >{dateParser(cliente?.fechaUltimoCobroFactura)}</Col>
                            <Col xs={6} >Último préstamo: </Col>
                            <Col xs={6} >{dateParser(cliente?.fechaUltimaEnvases)}</Col>
                        </Row>
                    </Card.Body>                    
                    <Card.Header onClick={()=>setSeccion(seccion ^ Secciones.Acceso)}>
                        Datos de acceso
                        {(seccion & Secciones.Acceso)!==Secciones.Hide?<RiArrowUpSLine/>:<RiArrowDownSLine/>}
                    </Card.Header>
                    <Card.Body hidden={(seccion & Secciones.Acceso)===Secciones.Hide}>
                        <Row>
                            <Col xs={12}>URL</Col>
                            <Col xs={{span:11, offset:1}}>{url}</Col>
                            <Col xs={12}>Usuario</Col>
                            <Col xs={{span:11, offset:1}}>{resumen?.usuario}</Col>
                            <Col xs={12}>Contraseña</Col>
                            <Col xs={{span:11, offset:1}}>{resumen?.clave}</Col>
                            <Col xs={6}>
                                <Button className="w-100" onClick={()=>{
                                    compartirWhatsapp(`Le enviamos la informacion para acceder a su cuenta\n\n${url}\nUsuario:${resumen?.usuario}\nContraseña:${resumen?.clave}`)
                                }}>WhatsApp <FaWhatsapp/></Button>
                            </Col>
                            <Col xs={6}>
                                <Button className="w-100" onClick={async()=>{
                                    if(!navigator.share) return
                                    try{
                                        await navigator.share({
                                            text:`Le enviamos la informacion para acceder a su cuenta\n\n${url}\nUsuario:${resumen?.usuario}\nContraseña:${resumen?.clave}\n`
                                        })
                                    }catch(error){}
                                }}>Compartir <FiShare2/></Button>
                            </Col>
                        </Row>
                    </Card.Body>
                </Card>
            </Modal.Body>
        </Modal>
    )
})