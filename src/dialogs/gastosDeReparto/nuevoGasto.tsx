import { forwardRef, useImperativeHandle }  from 'react'
import { useState, useCallback, useRef }    from 'react'
import { Modal, Form, Button }              from 'react-bootstrap'
import Select from 'react-select'

import * as DB                      from 'services/DB'

import { AppData_GastosDeReparto }  from 'entities/app'
import { AppData_ValoresSatelites } from 'entities/app'
import { PromiseCallbacks }         from 'types'
import { SelectOption }             from 'types'

export interface INuevoGasto{
    show:()=>Promise<AppData_GastosDeReparto>
}
export const NuevoGasto = forwardRef<INuevoGasto>((_,ref)=>{
    const [ callbacks, setCallbacks ]   = useState<PromiseCallbacks<AppData_GastosDeReparto>>()
    const [ tipos, setTipos ]           = useState<AppData_ValoresSatelites[]>([])
    const [ tipo, setTipo ]             = useState<AppData_ValoresSatelites>()
    const descripcion = useRef<HTMLInputElement>(null)
    const monto = useRef<HTMLInputElement>(null)
    const show = useCallback(async()=>{
        const p = new Promise<AppData_GastosDeReparto>((resolve, reject)=>{
            setCallbacks({resolve, reject})
        })
        p.finally(()=>{
            setCallbacks(undefined)
        })
        setTipos(await DB.ValoresSatelites.GetTablas([DB.ValoresSatelites.T_TIPOSDEGASTOS]))
        return p
    },[])
    useImperativeHandle(ref,()=>({show}),[show])
    const cancelar = useCallback(()=>{
        if(!callbacks)return
        callbacks.reject(false)
    },[callbacks])
    const guardar = useCallback(async()=>{
        if(!callbacks || !descripcion.current || !monto.current || !tipo) return
        try{
            const gasto = await DB.GastosDeReparto.addGastosDeReparto({
                descripcion:descripcion.current.value,
                montoGasto:monto.current.valueAsNumber,
                tipoGasto_id:tipo.valor_id
            })
            callbacks.resolve(gasto)
        }catch(error){}
    },[ callbacks, tipo])
    const noLeading0 = useCallback(async()=>{
        if(!monto.current || !descripcion.current) return
        const m = isNaN(monto.current.valueAsNumber)?0:monto.current.valueAsNumber
        monto.current.value=m.toString()
    },[])
    return (
        <Modal show={!!callbacks} onHide={cancelar} size="sm" centered backdrop="static" keyboard={false} className="nuevoGasto">
            <Modal.Header closeButton>Declarar Nuevo Gasto</Modal.Header>
            <Modal.Body>
                <Form.Row>
                    <Form.Label column xs={12}>Tipo</Form.Label>
                    <Select<SelectOption<AppData_ValoresSatelites>>
                        options={tipos.map<SelectOption<AppData_ValoresSatelites>>(tipo=>({
                            value:tipo,
                            label:tipo.valor_texto
                        }))}
                        id="tipo"
                        selected={tipo}
                        onChange={(option:SelectOption<AppData_ValoresSatelites> | null)=>setTipo(option?.value??undefined)}
                        />
                    <Form.Label column xs={12}>Descripción</Form.Label>
                    <Form.Control id="descripcion" 
                        ref={descripcion}
                        />
                    <Form.Label column xs={12}>Monto</Form.Label>
                    <Form.Control id="monto" type="number"
                        ref={monto}
                        onChange={noLeading0}
                        />
                </Form.Row>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="info" onClick={guardar}>Guardar</Button>
            </Modal.Footer>
        </Modal>
    )
})