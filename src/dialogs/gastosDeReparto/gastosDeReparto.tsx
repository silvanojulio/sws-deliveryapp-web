import { forwardRef, useImperativeHandle }  from 'react'
import { useState, useCallback, useRef }    from 'react'
import { useEffect }                        from 'react'
import { Modal, Card, Form }                from 'react-bootstrap'
import { BsFillPlusCircleFill }             from 'react-icons/bs'

import * as DB                      from 'services/DB'
import { NuevoGasto, INuevoGasto }  from './nuevoGasto'

import { PromiseCallbacks }         from 'types'
import { AppData_GastosDeReparto }  from 'entities/app'
import { AppData_ValoresSatelites } from 'entities/app'

export interface IGastosDeReparto{
    show:()=>Promise<void>
    nuevo:()=>Promise<void>
}
export const GastosDeReparto = forwardRef<IGastosDeReparto>((_,ref)=>{
    const [ callbacks, setCallbacks ]   = useState<PromiseCallbacks<void>>()
    const [ gastos, setGastos ]         = useState<AppData_GastosDeReparto[]>([])
    const [ tipos, setTipos ]           = useState<AppData_ValoresSatelites[]>([])
    const nuevoGastoRef = useRef<INuevoGasto>(null)
    const loadTipos=useCallback(async()=>{
        setTipos(await DB.ValoresSatelites.GetTablas([DB.ValoresSatelites.T_TIPOSDEGASTOS]))
    },[])
    useEffect(()=>{loadTipos()},[loadTipos])
    const nuevo = useCallback(async()=>{
        if(!nuevoGastoRef.current) return
        try{
            const gasto=await nuevoGastoRef.current.show()
            setGastos([...gastos, gasto])
        }
        catch(error){}
    },[gastos])
    const show = useCallback(async()=>{
        const p = new Promise<void>((resolve, reject)=>setCallbacks({resolve, reject}))
        p.finally(()=>setCallbacks(undefined))
        setGastos(await DB.GastosDeReparto.getGastosDeReparto())
        return p
    },[])
    useImperativeHandle(ref,()=>({show, nuevo}),[show, nuevo])
    const cerrar = useCallback(async()=>{
        if(!!callbacks) callbacks.resolve()
    },[callbacks])
    return (
        <>
            <NuevoGasto ref={nuevoGastoRef} />
            <Modal show={!!callbacks} onHide={cerrar} size="sm" backdrop="static" centered keyboard={false} className="gastos">
                <Modal.Header closeButton>Gastos de Reparto</Modal.Header>
                <Modal.Body>
                    {gastos.map(gasto=>{
                        const tipo=tipos.find(tipo=>tipo.valor_id===gasto.tipoGasto_id)?.valor_texto??"Tipo indefinido"
                        return(
                            <Card key={gasto.id}>
                                <Form.Row>
                                    <Form.Label column xs={8}>{tipo}</Form.Label>
                                    <Form.Label column xs={4}>$ {gasto.montoGasto}</Form.Label>
                                    <Form.Label column xs={12}>{gasto.descripcion}</Form.Label>
                                </Form.Row>
                            </Card>
                        )
                    })}
                </Modal.Body>
                <Modal.Footer>
                    <BsFillPlusCircleFill onClick={nuevo}/>
                </Modal.Footer>
            </Modal>
        </>
    )
})