import './gastosDeReparto.scss'

export { GastosDeReparto }  from './gastosDeReparto'
export { NuevoGasto }       from './nuevoGasto'

export type { INuevoGasto}          from './nuevoGasto'
export type { IGastosDeReparto }    from './gastosDeReparto'