import React, { forwardRef, useImperativeHandle  }  from 'react'
import { useState, useCallback, useRef }            from 'react'
import { Modal, Form, Col, Button, Spinner }        from 'react-bootstrap'

import { SessionService, LoginSuccess } from 'services/session'
import { PromiseCallbacks }             from 'types'
import { useAppContext }                from 'hooks'

type SesionControladorPlayaProps={
}
export interface ISesionControladorPlaya{
    get:()=>Promise<LoginSuccess>
}
export const SesionControladorPlaya=forwardRef<ISesionControladorPlaya, SesionControladorPlayaProps > ((props, ref)=>{
    const [ callbacks, setCallbacks ]   = useState<PromiseCallbacks<LoginSuccess>>()
    const [bussy, setBussy]             = useState<boolean>(false)
    const { ToastError }                = useAppContext()
    
    const userRef = useRef<HTMLInputElement>(null)
    const passRef = useRef<HTMLInputElement>(null)
    const aprobar   = useCallback(async()=>{
        if(!userRef.current) return
        if(!passRef.current) return
        if(!callbacks) return
        try{
            setBussy(true)
            callbacks.resolve(await SessionService.IniciarSesionControladorPlaya(userRef.current.value, passRef.current.value))
        }catch(error){
            ToastError((error as Error).message)
        }finally{ setBussy(false)}
    },[callbacks, ToastError])

    const volver    = useCallback(async()=>{
        if(!callbacks) return
        callbacks.reject()
    },[callbacks])
    useImperativeHandle(ref, ()=>({
        get:async():Promise<LoginSuccess> =>{
            const p = new Promise<LoginSuccess>((resolve, reject)=>setCallbacks({resolve, reject}))
            p.finally(()=>setCallbacks(undefined))

            return p
        }
    }))
    return (
        <Modal show={!!callbacks} backdrop="static" centered keyboard={false}>
            <Modal.Header><Modal.Title>Usuario Aprobador</Modal.Title></Modal.Header>
            <Modal.Body>
                <Form.Row>
                    <Col>Usuario:</Col>
                    <Col><Form.Control type="text"
                        disabled={bussy}
                        ref={userRef}
                    /></Col>
                </Form.Row>
                <Form.Row>
                    <Col>Contraseña:</Col>
                    <Col><Form.Control type="password"
                        disabled={bussy}
                        ref={passRef}
                    /></Col>
                </Form.Row>
            </Modal.Body>
            <Modal.Footer>
                <Button disabled={bussy} onClick={volver}>Volver</Button>
                <Button disabled={bussy} onClick={aprobar}>{bussy && <Spinner animation="border" size="sm" />}Aprobar</Button>
            </Modal.Footer>
        </Modal>
    )
})