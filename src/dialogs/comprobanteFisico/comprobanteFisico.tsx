import React,{ forwardRef, useImperativeHandle }    from 'react'
import { useState, useCallback, useRef}             from 'react'
import { Modal, Form, Button, Badge }               from 'react-bootstrap'

import { AppData_ComprobantesFisicosUtilizados }    from 'entities/app'
import { PromiseCallbacks }                         from 'types'
import { useAppContext }                            from 'hooks'
import * as DB from 'services/DB'

export interface IComprobanteFisico{
    completar:(tipo:number, cliente_id:number, id?:number)=>Promise<AppData_ComprobantesFisicosUtilizados>
}

export const ComprobanteFisico = forwardRef<IComprobanteFisico>((_props, ref)=>{
    const { ToastError }                            = useAppContext()
    const [ callbacks, setCallbacks ]               = useState<PromiseCallbacks<AppData_ComprobantesFisicosUtilizados>>()
    const [ comprobanteTipo, setComprobanteTipo ]   = useState<number>()
    const [ cliente_id, setCliente_id ]             = useState<number>()

    const prefijoRef        = useRef<HTMLInputElement>(null)
    const comprobanteRef    = useRef<HTMLInputElement>(null)

    const volver    = useCallback(()=>{
        if(!callbacks) return
        callbacks.reject()
    },[callbacks])
    const guardar   = useCallback(async()=>{
        if(!callbacks || !comprobanteTipo || !cliente_id) return
        if(!prefijoRef.current || !comprobanteRef.current) return      
        try{
            const comprobante = await DB.ComprobantesFisicosUtilizados.AddComprobanteFisicoUtilizado({
                tipoDeComprobanteFisico_ids:comprobanteTipo,
                nroPrefijo:prefijoRef.current.valueAsNumber,
                nroComprobante:comprobanteRef.current.valueAsNumber,
                cliente_id,
            })
            callbacks.resolve(comprobante)
        }catch(error){
            ToastError((error as Error).message)
        }
    },[callbacks, comprobanteTipo, cliente_id, ToastError]) 


    useImperativeHandle(ref, ()=>({
        completar:async (tipo:number, cliente_id:number)=>{
            setComprobanteTipo(tipo)
            setCliente_id(cliente_id)
            const p = new Promise<AppData_ComprobantesFisicosUtilizados>((resolve, reject)=>setCallbacks({resolve, reject}))
            p.finally(()=>setCallbacks(undefined))
            return p
        }
    }))
    return (
        <Modal show={!!callbacks} size="sm" backdrop="static" centered keyboard={false} >
            <Modal.Header><Modal.Title>Comprobante Fisico</Modal.Title></Modal.Header>
            <Modal.Body>
                <Form.Row>
                    <Form.Label>Tipo: <Badge>{comprobanteTipo===1?'Remito':'Recibo'}</Badge></Form.Label>
                </Form.Row>
                <Form.Row>
                    <Form.Label>Nro Prefijo</Form.Label>
                    <Form.Control size="sm" ref={prefijoRef} type="number"/>
                </Form.Row>
                <Form.Row>
                    <Form.Label>Nro Comprobante</Form.Label>
                    <Form.Control size="sm" ref={comprobanteRef} type="number"/>
                </Form.Row>
            </Modal.Body>
            <Modal.Footer>
                <Button size="sm" onClick={guardar}>Guardar</Button>
                <Button size="sm" onClick={volver}>Volver</Button>
            </Modal.Footer>
        </Modal>
    )
})
