import { forwardRef, useImperativeHandle }  from 'react'
import { useState, useCallback, useRef }    from 'react'
import { Modal, Card, Form, Col, Button }   from 'react-bootstrap'
import { FiSend }                           from 'react-icons/fi'

import * as DB              from 'services/DB'
import { AppData_Contactos} from 'entities/app'
import { PromiseCallbacks } from 'types'
import { whatsappShare }    from 'utils'

export interface IWhatsappShare{
    share:(content:string, cliente_id:number)=>Promise<void>
}

export const WhatsappShare = forwardRef<IWhatsappShare>((_,ref)=>{
    const [ callbacks, setCallbacks ]   = useState<PromiseCallbacks<void>>()
    const [ contactos, setContactos ]   = useState<AppData_Contactos[]>([])
    const otherCellRef = useRef<HTMLInputElement>(null)
    const [ content, setContent ]       = useState<string>("")
    const share = useCallback(async(content:string, cliente_id:number)=>{
        const p = new Promise<void>((resolve, reject)=>setCallbacks({resolve,reject}))
        p.finally(()=>setCallbacks(undefined))
        setContactos(await DB.Contactos.GetAllCliente(cliente_id))
        setContent(content)
        return p
    },[])
    useImperativeHandle(ref, ()=>({share}),[share])
    const close = useCallback(()=>{
        if(!callbacks) return
        callbacks.resolve()
    },[callbacks])
    const send=useCallback(async(celnumber:number)=>{
        if(!callbacks || content==="") return
        try{
            await whatsappShare(celnumber, content)
            callbacks.resolve()
        }catch(error){
            console.log(error)
        }
    },[callbacks, content])
    return (
        <Modal show={!!callbacks} onHide={close} centered size="sm" backdrop="static" keyboard={false} className="whatsappShare" animation={false}>
            <Modal.Header closeButton>Compartir por whatsapp</Modal.Header>
            <Modal.Body>
                <Card>                    
                    {contactos.filter(c=>c.celular && c.celular!=="").map(c=>(
                        <Card.Body key={c.id}>
                            <Form.Row>
                                <Col xs={6}>
                                    {c.nombreContacto}</Col>
                                <Col xs={6}>
                                    <Button size="sm" className="w-100" onClick={()=>{send(parseFloat(c.celular.replaceAll("-","")))}}>
                                        {c.celular}
                                    </Button>
                                    <FiSend onClick={()=>{send(parseFloat(c.celular.replaceAll("-","")))}}/>
                                </Col>
                            </Form.Row>
                        </Card.Body>
                    ))}
                    <Card.Body>
                        <Form.Row>
                            <Col xs={4}>Otro </Col>
                            <Col xs={8}>
                                <Form.Control size="sm" type="number" ref={otherCellRef} />
                                <FiSend onClick={()=>{
                                    if(!otherCellRef.current) return
                                    send(otherCellRef.current.valueAsNumber)
                                }} />
                            </Col>
                        </Form.Row>
                    </Card.Body>                
                </Card>
            </Modal.Body>
        </Modal>
    )
})