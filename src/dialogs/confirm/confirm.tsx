import { forwardRef, useImperativeHandle }  from 'react'
import { useState, useCallback }            from 'react'
import { Modal, Button }                    from 'react-bootstrap'

import { PromiseCallbacks } from 'types'

export interface IConfirm{
    prompt:(buttons:"YesNo"|"RetryCancel"|"OkOnly", message:string, title:string)=>Promise<boolean>
}

export const Confirm = forwardRef<IConfirm>((_, ref)=>{
    const [ callbacks, setCallbacks ]   = useState<PromiseCallbacks<boolean>>()
    const [ buttons, setButtons ]       = useState<"YesNo"|"RetryCancel"|"OkOnly">("YesNo")
    const [ message, setMessage ]       = useState<string>("")
    const [ title, setTitle ]           = useState<string>("")
    
    useImperativeHandle(ref, ()=>({
        prompt:(buttons:"YesNo"|"RetryCancel"|"OkOnly", message:string, title:string)=>{
            setButtons(buttons)
            setMessage(message)
            setTitle(title)
            const p = new Promise<boolean>((resolve, reject)=>{setCallbacks({resolve, reject})})
            p.finally(()=>setCallbacks(undefined))
            return p
        }
    }))
    const resolve=useCallback(()=>{
        if(!callbacks) return
        callbacks.resolve(true)
    },[callbacks])
    const reject=useCallback(()=>{
        if(!callbacks) return
        callbacks.resolve(false)
    },[callbacks])
    return (
        <Modal show={!!callbacks} size="sm" backdrop="static" centered keyboard={false} className="gestion-cliente-menu">
            <Modal.Header>{title}</Modal.Header>
            <Modal.Body>{message}</Modal.Body>
            <Modal.Footer>
                <Button onClick={resolve} className="w-25">{buttons==="YesNo"?"Si":buttons==="OkOnly"?"Recibido":"Reintentar"}</Button>
                <Button onClick={reject} hidden={buttons==="OkOnly"} className="w-25">{buttons==="YesNo"?"No":"Cancelar"}</Button>
            </Modal.Footer>
        </Modal> 
    )
})