import React, { FC, useState, useCallback } from 'react'
import { Modal, ProgressBar } from 'react-bootstrap'
import { FnQueue, queueItem } from 'utils/fnQueue'
import './loading.scss'

export const LoadingModal:FC = ()=>{
    const [runningFn, setRunningFn] = useState<queueItem>()
    const spy = useCallback((item:queueItem, event:"start"|"success"|"error")=>{
        if(event==="start") setRunningFn(item); else setRunningFn(undefined);
    },[setRunningFn])
    FnQueue.setSpy(spy)
    return (
        <Modal show={runningFn && runningFn.blocking} backdrop="static" centered keyboard={false} className="loading" >
            <Modal.Body>
                <ProgressBar striped variant="info" animated now={100} label={runningFn?.label??"Loading..."}  />
            </Modal.Body>
        </Modal> 
    )
}