import React, { forwardRef, useImperativeHandle }   from 'react'
import { useState, useCallback }                    from 'react'
import { Modal, Button, Spinner }                   from 'react-bootstrap'
import DatePicker                                   from 'react-datepicker'

import { PromiseCallbacks }     from 'types'
import { AppData }              from 'entities/app'
import { HojasDeRutaSerivce }   from 'services/hojasDeRuta'
import { useAppContext }        from 'hooks'
 
export interface IDBDownload{
    descargarBase:()=>Promise<AppData>
}

export const DBDownload=forwardRef<IDBDownload>((_,ref)=>{
    const { ToastError }                = useAppContext()
    
    const [ callbacks, setCallbacks ]   = useState<PromiseCallbacks<AppData>>()
    const [ bussy, setBussy ]           = useState<boolean>(false)
    const [ date, setDate ]             = useState<Date>(new Date())
    const descargarBase = useCallback(async()=>{
        const p=new Promise<AppData>((resolve, reject)=>{
            setCallbacks({resolve, reject})
        })
        p.finally(()=>{
            setCallbacks(undefined)
        })
        return p
    },[])
    useImperativeHandle(ref, ()=>({descargarBase}),[descargarBase])
    const onSave=useCallback(async ()=>{
        if(!callbacks) return
        setBussy(true)
        try{
            const data=await HojasDeRutaSerivce.DescargarBaseDeDatosEnJSON(date)
            callbacks.resolve(data)
        }catch(error){
            ToastError("Error al descargar base de datos!")
        }finally{
            setBussy(false)
        }
        
    },[callbacks, date, ToastError])
    const onCancel=useCallback(async()=>{
        if(!callbacks) return
        callbacks.reject()
    },[callbacks])
    return (
        <Modal show={!!callbacks} size="sm" backdrop="static" centered keyboard={false} >
            <Modal.Header>
                <Modal.Title>Seleccione la fecha de reparto</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <DatePicker 
                    onChange={(d:Date)=>setDate(d)}
                    selected={date}
                    disabled={bussy}
                    dateFormat="yyyy-MM-dd" />
            </Modal.Body>
            <Modal.Footer>
                <Button disabled={bussy} variant="red" onClick={onCancel}>Cancelar</Button>
                <Button disabled={bussy} variant="green" onClick={onSave}>{bussy && <Spinner animation="border" size="sm" />} Aceptar</Button>
            </Modal.Footer> 
        </Modal>
    )
})