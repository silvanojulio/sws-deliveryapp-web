import { useRef, useCallback, useState }            from 'react'
import { forwardRef, useImperativeHandle  }         from 'react'
import { Modal, Form, Button, Badge }               from 'react-bootstrap'
import { FiCheckCircle }                            from 'react-icons/fi'

import { AppData_Clientes }                         from 'entities/app'
import { AppData_DescuentosPorCantidad }            from 'entities/app'
import { AppData_ArticulosEntregados }              from 'entities/app'
import { AppData_ItemsDeRecibos }                   from 'entities/app'
import { PromiseCallbacks }                         from 'types'
import { AddArticulo, IAddArticulo}                 from './addArticulo'
import { ArticuloEntregado }                        from './articuloEntregado'
import { ConfirmarVenta, IConfirmarVenta}           from './confirmarVenta'
import * as DB                                      from 'services/DB'

import { useAppContext, useHojaDeRutaContext }      from 'hooks'

export interface IEntrega{
    nueva:(cliente:AppData_Clientes)=>Promise<void>
}
export const Entrega=forwardRef<IEntrega>((_, ref)=>{ 
    const { efectivoDialog }            = useHojaDeRutaContext()
    const { ToastError, ConfirmRef }    = useAppContext()
    const [ descXCant, setDescXCant ]   = useState<AppData_DescuentosPorCantidad[]>([])
    const [ callbacks, setCallbacks ]   = useState<PromiseCallbacks<void>>()
    const [ articulos, setArticulos ]   = useState<AppData_ArticulosEntregados[]>([])
    const [ cobrado, setCobrado ]       = useState<number>(0)
    const [ total, setTotal ]           = useState<number>(0)
    const [ cliente, setCliente ]       = useState<AppData_Clientes>()
    const [ original, setOriginal ]     = useState<AppData_ArticulosEntregados[]>([])
    const addArticuloDialog             = useRef<IAddArticulo>(null)
    const confirmarRef                  = useRef<IConfirmarVenta>(null)
    const contadoRef                    = useRef<HTMLInputElement>(null)

    
    const nueva = useCallback(async(cliente:AppData_Clientes)=>{
        setCliente(cliente)
        const original=await DB.ArticulosEntregados.GetArticulosEntregadosACliente(cliente.cliente_id)
        setOriginal(original.sort((a,b)=>a.orden-b.orden))
        
        const entregados    = await DB.ArticulosEntregados.GetArticulosEntregadosACliente(cliente.cliente_id)
        if(entregados.length===0){
            const lista     = await DB.ArticulosDeLista.GetArticulos()
            const precios   = await DB.ListasDePrecios.GetListaDePrecios(cliente.listaDePrecioId)
            const especiales= await DB.PreciosEspeciales.GetPreciosEspecialesXCliente(cliente.clienteFactura_id)
            const abonos    = await DB.ArticulosDeAbonos.GetArticulosXCliente(cliente.cliente_id)
            const stocks    = await DB.StocksDeClientes.GetStocksDeCliente(cliente.cliente_id)            
            await DB.ArticulosEntregados.ActualizarEntrega(
                cliente.cliente_id,
                stocks.map<AppData_ArticulosEntregados>(stock=>{
                    const articulo  = lista.find(articulo=>articulo.id===stock.articuloId)
                    const abono     = abonos.find(abono=>abono.articulo_id===stock.articuloId)
                    const precio    = precios.find(precio=>precio.articuloId===stock.articuloId)
                    const especial  = especiales.find(especial=>especial.articuloId===stock.articuloId)
                    return {
                        articulo_id:                articulo!.id,
                        codigoArticulo:             articulo!.codigoInterno,
                        nombreDelArticulo:          articulo!.nombreArticulo,
                        esRetornable:               articulo!.tipoDeEnvase_ids===1,
                        precioUnitario:             especial?.precio??precio?.precio??articulo!.precio,
                        cantidadDisponibles:        abono?.cantidad??0,
                        clienteId:                  cliente.cliente_id,
                        cantidadEntregada:          0,
                        cantidadEnvasesDevueltos:   0,
                        cantidadEnvasesRelevados:   0,
                        cantidadEnvasesPrestados:   0,
                        descuentoManual:            0,
                        descuentoPorCantidad:       0,
                        subtotal:                   0,
                        orden:                      0,
                        motivoPrestamoDevolucionId: 0,
                        esPredeterminado:           false,
                        envasesARecuperar:          stock.cantidadARecuperar??0,
                        envasesEnCliente:           stock.stockActual??0,
                        envasesPermitidos:          stock.cantidadPermitida??0,
                        id:                         0
                    }
                })
            )
        }
        const articulos=await DB.ArticulosEntregados.GetArticulosEntregadosACliente(cliente.cliente_id)
        setArticulos(articulos.sort((a,b)=>a.orden-b.orden))
        setDescXCant(await DB.DescuentosPorCantidad.GetDescuentosPorCantidad(cliente.listaDePrecioId))
        const p = new Promise<void>((resolve, reject)=>{
            setCallbacks({resolve, reject})
        })
        const itemsRecibos = await DB.ItemsDeRecibos.GetItemsDeReciboCliente(cliente.cliente_id)
        setCobrado(itemsRecibos.find(item=>item.formaDePagoId===1)?.importe??0)
        setTotal(0)
        p.finally(()=>setCallbacks(undefined))
        return p
    },[])
    useImperativeHandle(ref, ()=>({nueva}),[nueva])
    const refreshList = useCallback(async ()=>{
        if(!cliente) return
        const articulos=await DB.ArticulosEntregados.GetArticulosEntregadosACliente(cliente.cliente_id)
        setArticulos(articulos.sort((a,b)=>a.orden-b.orden))
        setTotal(articulos.reduce((total, articulo)=>total+articulo.subtotal,0))
    },[cliente])
    const saveEntrega = useCallback(async()=>{
        if(!callbacks) return
        if(!cliente) return callbacks.reject()
        try{
            if(cobrado && contadoRef.current?.checked) throw new Error("No se puede registrar un cobro y marcar contado a la vez")
            const articulos = await DB.ArticulosEntregados.GetArticulosEntregadosACliente(cliente.cliente_id)
            const _articulos = articulos.filter(art=>(0<
                art.cantidadEnvasesRelevados + art.cantidadEnvasesPrestados +
                art.cantidadEnvasesDevueltos + art.cantidadEntregada
            )).map<AppData_ArticulosEntregados>((articulo, index)=>({...articulo,orden:index+1}))
            if(_articulos.length===0) throw new Error("No se registraron articulos con gestion en la venta")
            await DB.ArticulosEntregados.ValidarArticulos(cliente.cliente_id)
            const confirmacion=await confirmarRef.current!.confirmar(
                cliente,
                _articulos,
                cobrado>0?cobrado:contadoRef.current?.checked??false).catch<false>((_reason)=>false) 
            if(!confirmacion) return            
            await DB.ArticulosEntregados.ActualizarEntrega(cliente.cliente_id,_articulos)
            await DB.Clientes.MarcarEntregado(
                cliente.cliente_id, 
                total
            )
            const recibo = (await DB.Recibos.GetReciboCliente(cliente.cliente_id)) || (await DB.Recibos.CreateRecibo(cliente.cliente_id))
            const recibo_items = await DB.ItemsDeRecibos.GetItemsDeReciboCliente(cliente.cliente_id)
            const recibo_item_efectivo = recibo_items.find(item=>item.formaDePagoId===1)
            const contadoChecked = contadoRef.current?.checked??false
            const cobradoFinal   = cobrado??0
            const importeCobrado = cobradoFinal>0?cobradoFinal:contadoChecked?total:undefined
            if(!importeCobrado){
                if(recibo_item_efectivo) await DB.ItemsDeRecibos.DeleteItemDeRecibo(recibo_item_efectivo)
            }else{
                if(!recibo_item_efectivo){
                    await DB.ItemsDeRecibos.AddItemDeRecibo({
                        reciboId:recibo.id,
                        importe:importeCobrado,
                        formaDePagoId:1,
                        descripcion:null,
                        retencionId:null,
                        chequeId:null,
                        tarjetaDeCreditoId:null,
                        tarjetaDeDebitoId:null
                    } as AppData_ItemsDeRecibos)
                }else{
                    await DB.ItemsDeRecibos.EditItemDeRecibo({
                        ...recibo_item_efectivo,
                        importe:importeCobrado
                    })
                }
            }
            callbacks.resolve()
        }catch(error){ToastError((error as Error).message)}
    },[callbacks, cobrado, ToastError, cliente, total])
    const askNewArticulo=useCallback(async()=>{
        if(!addArticuloDialog.current || !cliente) return
        try{
            await addArticuloDialog.current.agregar(cliente,articulos.map<number>(ae=>ae.articulo_id))
            refreshList()
        }catch(_error){}
    },[articulos, cliente, refreshList])
    const askEfectivo=useCallback(async()=>{
        if(!efectivoDialog?.current) return 
        const monto=await efectivoDialog.current.cobrar()
        setCobrado(monto)
    },[efectivoDialog])
    const close=useCallback(async()=>{
        if(!callbacks) return
        if(!cliente) return
        const articulos= await DB.ArticulosEntregados.GetArticulosEntregadosACliente(cliente.cliente_id)
        if(JSON.stringify(original)===JSON.stringify(articulos)) return callbacks.resolve()
        if(!ConfirmRef.current) return
        if(await ConfirmRef.current.prompt("YesNo","Desea revertir los cambios realizados en la gestion?","Revertir cambios?")){
            await DB.ArticulosEntregados.ActualizarEntrega(cliente.cliente_id,original)
            callbacks.resolve()
        }
    },[callbacks, ConfirmRef, cliente, original])
    const updateTotal=useCallback(async()=>{
        if(!cliente) return
        const articulos= await DB.ArticulosEntregados.GetArticulosEntregadosACliente(cliente.cliente_id)
        setTotal(articulos.reduce((total, articulo)=>total+articulo.subtotal,0))
    },[cliente])
    return (
        <Modal show={!!callbacks} size="sm" onHide={close} backdrop="static" centered keyboard={false} className="entregaDialog">
            <AddArticulo ref={addArticuloDialog}/>
            <ConfirmarVenta ref={confirmarRef} />
            <Modal.Header closeButton>
                <Form.Label>
                    Entrega Actual: <Badge>$ {total}</Badge><br/> 
                    Saldo total: <Badge>$ {((cliente?.saldoConsumos??0) + (cliente?.saldoFacturacion??0) + total).toFixed(2)}</Badge>
                </Form.Label>
            </Modal.Header>
            <Modal.Body>
                {articulos.map(articulo=>(
                    <ArticuloEntregado 
                        onDelete={refreshList}
                        onSubtotalChanged={updateTotal}
                        key={articulo.articulo_id} 
                        articuloEntregado={articulo} 
                        descuentosPorCantidad={descXCant.filter(dpc=>dpc.articuloId===articulo.articulo_id)}
                    />
                ))}
            </Modal.Body>
            <Modal.Footer>
                <Form.Row>
                    <Button onClick={askNewArticulo} variant="info" >+ Articulo</Button>
                </Form.Row>
                <Form.Row>
                    <Button variant="toolbar" onClick={askEfectivo} style={{color:"darkgreen"}}>{cobrado?`$ ${cobrado}`:"Efectivo"}</Button>
                    <Form.Check label="Contado" ref={contadoRef}/>
                    <Button variant="toolbar" onClick={saveEntrega} style={{color:"darkgreen"}}><FiCheckCircle/> Guardar</Button>
                </Form.Row>
            </Modal.Footer>
        </Modal>
    )
})