import { FC, useState, useEffect }  from 'react'
import { useCallback, useRef }      from 'react'
import { Card, Form, Button, Col }  from 'react-bootstrap'
import Select                       from 'react-select'
import { ImLoop2, ImBin }           from 'react-icons/im'

import { NumericField, INumericField }      from 'components/numericField'
import { AppData_ArticulosEntregados }      from 'entities/app'
import { AppData_DescuentosPorCantidad }    from 'entities/app'
import { AppData_ValoresSatelites }         from 'entities/app'
import * as DB                              from 'services/DB'
import { SelectOption }                     from 'types'


type MotivoOption = SelectOption<AppData_ValoresSatelites>

type ArticuloEntregadoProps={
    articuloEntregado:AppData_ArticulosEntregados
    descuentosPorCantidad?:AppData_DescuentosPorCantidad[]
    onDelete?:()=>Promise<void>
    onSubtotalChanged?:()=>Promise<void>
}
export const ArticuloEntregado:FC<ArticuloEntregadoProps> = (props)=>{
    const { articuloEntregado }     = props
    const { descuentosPorCantidad } = props
    const { onDelete }              = props
    const { onSubtotalChanged }     = props
    
    //#region Referencias y estados
    const entregados    = useRef<INumericField>(null)
    const prestados     = useRef<INumericField>(null)
    const relevados     = useRef<INumericField>(null)
    const devueltos     = useRef<INumericField>(null)
    const descManual    = useRef<INumericField>(null)

    const [ showMore, setShowMore ] = useState<boolean>(false)
    const [ motivos,  setMotivos  ] = useState<MotivoOption[]>([])
    const [ subTotal, setSubTotal ] = useState<number>(articuloEntregado.subtotal)
    const [ precioUn, setPrecioUn ] = useState<number>(articuloEntregado.precioUnitario)
    const [ desTotal, setDesTotal ] = useState<number>(0)
    const [ error,    setError    ] = useState<string>()
    useEffect(()=>{
        if(onSubtotalChanged) onSubtotalChanged()
    },[subTotal, onSubtotalChanged])
    //#endregion
    const validar=useCallback(()=>{
        const _entregados = entregados.current?.getValue()??0
        const _devueltos  = devueltos.current?.getValue()??0
        const _prestados  = prestados.current?.getValue()??0
        const _relevados  = relevados.current?.getValue()??0
        const _descManual = descManual.current?.getValue()??0
        const _stock      = articuloEntregado.envasesEnCliente
        const _envasesFinalesEnCliente = _prestados + (_relevados>0?_relevados:_stock) - _devueltos
        try{
            setError("")
            if(_entregados+_devueltos+_prestados+_relevados===0)
                throw new Error(`No posee ninguna gestión`)

            if(articuloEntregado.esRetornable){
                if(_devueltos && _prestados) 
                    throw new Error(`No es posible registrar devoluciones de envases y préstamos a la vez`)
                if(_entregados>_envasesFinalesEnCliente) 
                    throw new Error(`La cantidad entregada supera a la cantidad de envases que tiene el cliente`)
                
            }else{
                if(_relevados || _devueltos || _prestados)
                    throw new Error(`Los envases no retornables no pueden ser prestados, devueltos o relevados`)
            }

            if(!subTotal && _descManual)
                throw new Error(`No puede cargar descuentos si el subtotal es $0`)
            if(_relevados && articuloEntregado.motivoPrestamoDevolucionId===0){
                throw new Error(`Debe seleccionar el Motivo del Relevamiento`)
            }
        }catch(error){setError((error as Error).message)}
    },[articuloEntregado, subTotal])
    const loadData=useCallback(async()=>{
        const motivos=await DB.ValoresSatelites.GetTablas([DB.ValoresSatelites.T_MOTIVOS_RELEVAMIENTO_ENVASE])
        const opciones=motivos.map<MotivoOption>(motivo=>({
            value:motivo,
            label:motivo.valor_texto
        }))
        setMotivos(opciones)
    },[])
    useEffect(()=>{loadData()},[loadData])
    
    const guardarCambios=useCallback(async()=>{    
        const _entregados       = entregados.current?.getValue()??0
        const _descManual       = descManual.current?.getValue()??0
        const _relevados        = relevados.current?.getValue()??0
        const cantidadACobrar   = Math.max(_entregados-articuloEntregado.cantidadDisponibles,0)
        const _descCantidad     = descuentosPorCantidad?.reduce<number>((desc, dpc)=>dpc.cantidad<=cantidadACobrar?Math.max(desc,dpc.descuento):desc,0)??0
        const _precioUn         = articuloEntregado.precioUnitario
        const _subTotal         = parseFloat((cantidadACobrar*_precioUn*(1-((_descManual+_descCantidad)/100))).toFixed(2))

        articuloEntregado.cantidadEntregada         = _entregados
        articuloEntregado.descuentoManual           = _descManual
        articuloEntregado.cantidadEnvasesRelevados  = _relevados
        articuloEntregado.descuentoPorCantidad      = _descCantidad
        articuloEntregado.precioUnitario            = _precioUn
        articuloEntregado.subtotal                  = _subTotal
        articuloEntregado.cantidadEnvasesDevueltos  = devueltos.current?.getValue()??0
        articuloEntregado.cantidadEnvasesPrestados  = prestados.current?.getValue()??0
        if(_relevados===0) articuloEntregado.motivoPrestamoDevolucionId=0
        
        setPrecioUn(_precioUn)
        setDesTotal(_descManual+_descCantidad)
        setSubTotal(_subTotal)
        
        await DB.ArticulosEntregados.GuardarArticuloEntregado(articuloEntregado)
        validar()
        //if(onUpdate) onUpdate()
    },[articuloEntregado, descuentosPorCantidad, validar])
    const deleteArticulo=useCallback(async()=>{
        await DB.ArticulosEntregados.EliminarArticuloEntregado(articuloEntregado.clienteId, articuloEntregado.articulo_id)
        if(!onDelete)return
        onDelete()
    },[articuloEntregado, onDelete])
    
    return (
        <Card className="articuloEntregado">
            <Form.Label column xs={12}>
                {props.articuloEntregado.nombreDelArticulo}
                {articuloEntregado.esRetornable &&  (<><br/><ImLoop2 color="green"/></>)}
            </Form.Label>
            <Form.Row>
                <Form.Label column xs={{offset:0, span:4}}>
                    Entregados<br/> 
                    <NumericField ref={entregados} defaultValue={articuloEntregado.cantidadEntregada} onChange={guardarCambios} />
                </Form.Label>
                <Form.Label column xs={{offset:0, span:4}}>
                    Env Prestados<br/>
                    <NumericField ref={prestados} defaultValue={articuloEntregado.cantidadEnvasesPrestados} onChange={guardarCambios}/>
                </Form.Label>
                <Form.Label column xs={{offset:0, span:4}}>
                    Env Devueltos<br/>
                    <NumericField ref={devueltos} defaultValue={articuloEntregado.cantidadEnvasesDevueltos} onChange={guardarCambios}/>
                </Form.Label>
            </Form.Row>
            <Form.Row>
                <Form.Label column xs={{offset:0, span:4}} className="align-self-end">
                    <Button size="sm" onClick={()=>setShowMore(!showMore)}>Ver {showMore?"Menos":"Más"}</Button>
                </Form.Label>
                <Form.Label column xs={{offset:0, span:4}} hidden={!showMore}>
                    Descuento %<br/>
                    <NumericField ref={descManual} defaultValue={articuloEntregado.descuentoManual} onChange={guardarCambios}/>
                </Form.Label>
                <Form.Label column xs={{offset:0, span:4}} hidden={!showMore}>
                    Relevamiento<br/>
                    <NumericField ref={relevados} defaultValue={articuloEntregado.cantidadEnvasesRelevados} onChange={guardarCambios}/>
                </Form.Label>
            </Form.Row>
            <Form.Row hidden={articuloEntregado.cantidadEnvasesRelevados===0 || !showMore }>
                <Col xs={12}>
                    Motivo de Relevamiento
                </Col>
                <Col xs={{offset:1, span:10}}> 
                    <Select<MotivoOption> size="sm" 
                        id="motivoRelev"
                        onChange={motivo=>{
                            articuloEntregado.motivoPrestamoDevolucionId=motivo?.value.valor_id??0
                            guardarCambios()
                        }}
                        options={motivos}
                        value={motivos.find(mot=>mot.value.valor_id===articuloEntregado.motivoPrestamoDevolucionId)}
                    />
                </Col>
            </Form.Row>
            <Form.Row hidden={!error} style={{color:"red"}}>
                <Form.Label column xs={{offset:1, span:10}}>{error}</Form.Label>
            </Form.Row>
            <Form.Row>
                <Form.Label column xs={4}>Stock<br/>{articuloEntregado.envasesEnCliente}</Form.Label>
                <Form.Label column xs={4}>Permitidos<br/>{articuloEntregado.envasesPermitidos}</Form.Label>
                <Form.Label column xs={4}>A Recuperar<br/>{articuloEntregado.envasesARecuperar}</Form.Label>
            </Form.Row>
            <Form.Row>
                <Form.Label column xs={4}>Precio<br/>$ {precioUn.toFixed(2)} {desTotal>0?`(-${desTotal}%)`:''}</Form.Label>
                <Form.Label column xs={4}>Subtotal<br/>$ {subTotal.toFixed(2)}</Form.Label>
                <Form.Label column xs={4}><ImBin color="red" onClick={deleteArticulo}/></Form.Label>
            </Form.Row>
        </Card>
    )
}