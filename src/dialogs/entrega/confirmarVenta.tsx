import React, { useCallback, useState, useRef }     from 'react'
import { forwardRef, useImperativeHandle }          from 'react'
import { Modal, Form, Button, Badge, Card }         from 'react-bootstrap'

import { PromiseCallbacks }                         from 'types'
import { AppData_Clientes }                         from 'entities/app'
import { AppData_ArticulosEntregados}               from 'entities/app'
import { AppData_ComprobantesFisicosUtilizados }    from 'entities/app'
import { AppData_FirmasRemitos }                    from 'entities/app'
import { Signature, ISignature}                     from 'dialogs/signature'
import { useHojaDeRutaContext }                     from 'hooks'
import { FirmasRemitos }                            from 'services/DB'

export type ConfirmarReturn={
    comprobanteFisico?:AppData_ComprobantesFisicosUtilizados
    firmaRemito?:AppData_FirmasRemitos
}
export interface IConfirmarVenta{
    confirmar:(
            cliente:AppData_Clientes, 
            articulos:AppData_ArticulosEntregados[],
            contadoEfectivo:number | boolean
        )=>Promise<ConfirmarReturn>
}
export const ConfirmarVenta = forwardRef<IConfirmarVenta>((_props, ref)=>{
    const [ callbacks, setCallbacks ]               = useState<PromiseCallbacks<ConfirmarReturn>>()
    const [ contadoEfectivo, setContadoEfectivo ]   = useState<number|boolean>(false)
    const [ cliente, setCliente ]                   = useState<AppData_Clientes>()
    const [ articulos, setArticulos ]               = useState<AppData_ArticulosEntregados[]>([])
    const { comprobanteFisicoRef }                  = useHojaDeRutaContext()
    const signatureRef                              = useRef<ISignature>(null)

    useImperativeHandle(ref, ()=>({
        confirmar:async (
                cliente:AppData_Clientes, 
                articulos:AppData_ArticulosEntregados[],
                contadoEfectivo:number | boolean
            )=>{
                setContadoEfectivo(contadoEfectivo)
                setArticulos(articulos)
                setCliente(cliente)
                const p=new Promise<ConfirmarReturn>((resolve, reject)=>setCallbacks({resolve, reject}))
                p.finally(()=>setCallbacks(undefined))
                return p
            }
    }))
    const volver=useCallback(()=>{
        if(!callbacks) return
        callbacks.reject()
    },[callbacks])
    const firmar=useCallback(async()=>{
        if(!signatureRef.current || !callbacks || !cliente) return
        const firma=await signatureRef.current.show().catch<false>(()=>false)
        if(!firma) return
        callbacks.resolve({
            firmaRemito:await FirmasRemitos.AddFirmaRemito({
                firmante:firma.aclaracion,
                firmaRemito:firma.firma,
                cliente_id:cliente.cliente_id, 
                esServicioTecnico:0,
                fechaHora:new Date().toLocaleString()
            })
        })
    },[callbacks, cliente]) 
    const confirmarSinRemito = useCallback(()=>{
        if(!callbacks)return
        callbacks.resolve({})
    },[callbacks])
    const remitoFisico = useCallback(async ()=>{
        if(!comprobanteFisicoRef?.current || !callbacks || !cliente) return
        try{
            callbacks.resolve({
                comprobanteFisico:await comprobanteFisicoRef.current.completar(1,cliente.cliente_id)
            })
        }catch(error){}
    },[comprobanteFisicoRef, callbacks, cliente])
    return (
        <Modal show={!!callbacks} size="sm" backdrop="static" centered keyboard={false} className="confirmarVenta">
            <Signature ref={signatureRef} />
            <Modal.Header>
                <Card>
                    <Form.Row><Form.Label>Confirmacion de entregas</Form.Label></Form.Row>
                    <Form.Row>
                        <Form.Label>Total venta: <Badge>$ {
                            articulos.reduce((total,art)=>total+(art.precioUnitario*art.cantidadEntregada),0)
                        }</Badge></Form.Label>
                        <Form.Label>Contado Efectivo: <Badge>{contadoEfectivo===false?"NO":contadoEfectivo===true?"SI":`$ ${contadoEfectivo}`}</Badge></Form.Label>
                    </Form.Row>
                </Card>
            </Modal.Header>
            <Modal.Body>
                {articulos.map(art=>(
                    <Card key={art.articulo_id}>
                        <Form.Row><Form.Label>{art.nombreDelArticulo}</Form.Label></Form.Row>
                        <Form.Row>
                            <Form.Label>Cantidad Entregada: <Badge>{art.cantidadEntregada || "-"}</Badge></Form.Label>
                            <Form.Label>Envases Prestados: <Badge>{art.cantidadEnvasesPrestados || "-"}</Badge></Form.Label>
                        </Form.Row>
                        <Form.Row>
                            <Form.Label>Descuentos: <Badge>
                                {art.descuentoManual+art.descuentoPorCantidad===0?"-":`
                                    ${art.descuentoManual>0?`${art.descuentoManual} %`:""}
                                    ${art.descuentoManual>0 && art.descuentoPorCantidad>0?" + ":""}
                                    ${art.descuentoPorCantidad>0?`${art.descuentoPorCantidad} %`:""}
                                    `
                                }
                            </Badge></Form.Label>
                            <Form.Label>Envases Devueltos: <Badge>{art.cantidadEnvasesDevueltos || "-"}</Badge></Form.Label>
                        </Form.Row>
                        <Form.Row>
                            <Form.Label>Precio Unitario: <Badge>$ {art.precioUnitario}</Badge></Form.Label>
                            <Form.Label>Envases Relevados: <Badge>{art.cantidadEnvasesRelevados || "-"}</Badge></Form.Label>
                        </Form.Row>
                        <Form.Row><Form.Label>Subtotal <Badge>$ {art.subtotal}</Badge></Form.Label></Form.Row>
                    </Card>
                ))}
            </Modal.Body>
            <Modal.Footer>
                <Button size="sm" variant="toolbar" hidden={cliente?.requiereRemito} onClick={confirmarSinRemito}>Confirmar Sin Remito</Button>
                <Button size="sm" variant="toolbar" onClick={remitoFisico}>Remito Fisico</Button>
                <Button size="sm" variant="toolbar" onClick={firmar}>Firma Cliente</Button>
                <Button size="sm" variant="toolbar" onClick={volver}>Volver</Button>
            </Modal.Footer>
        </Modal>
    )
})