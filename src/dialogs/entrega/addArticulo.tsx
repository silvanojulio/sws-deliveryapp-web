import { forwardRef, useImperativeHandle }   from 'react'
import { useState, useCallback, useRef }            from 'react'
import { Modal, Form, Col, Button, Badge, Card }    from 'react-bootstrap'
import { ImLoop2, ImStarEmpty }                     from 'react-icons/im'
import { FiCheck }                                  from 'react-icons/fi'
import { FcSearch }                                 from 'react-icons/fc'


import { AppData_Clientes }         from 'entities/app'
import { AppData_StocksDeClientes } from 'entities/app'
import { PromiseCallbacks }         from 'types'
import { ArticulosDeLista }         from 'services/DB'
import * as DB                      from 'services/DB'


export type AddArticuloReturn={
    nombreArticulo:string
    articuloId:number
    codigoInterno:string
    precio:number
    cantidadDisponiblePorAbono:number
    esDestacado:boolean
    esRetornable:boolean
}
export interface IAddArticulo{
    agregar:(cliente:AppData_Clientes, already_added_ids:number[])=>Promise<void>
}
export const AddArticulo = forwardRef<IAddArticulo>((_, ref)=>{
    const [ callbacks,  setCallbacks ]  = useState<PromiseCallbacks<void>>()
    const [ articulos,  setArticulos ]  = useState<AddArticuloReturn[]>([])
    const [ txtFiltro,  setTxtFiltro ]  = useState<string>("")
    const [ cliente,    setCliente   ]  = useState<AppData_Clientes>()
    const [ stocks,     setStocks    ]  = useState<AppData_StocksDeClientes[]>([])
    const filtroRef = useRef<HTMLInputElement>(null)
    const agregar = useCallback(async (cliente:AppData_Clientes, already_added_ids:number[])=>{
        setCliente(cliente)
        setArticulos([])
        const ArticulosDeAbonosCliente  = await DB.ArticulosDeAbonos.GetArticulosXCliente(cliente.cliente_id)
        const p = new Promise<void>((resolve, reject)=>setCallbacks({resolve, reject}))
        p.finally(()=>setCallbacks(undefined))
        const PreciosEspecialesCliente  = await DB.PreciosEspeciales.GetPreciosEspecialesXCliente(cliente.clienteFactura_id)
        const ListaDePrecios            = await DB.ListasDePrecios.GetListaDePrecios(cliente.listaDePrecioId)
        const StockDeCliente            = await DB.StocksDeClientes.GetStocksDeCliente(cliente.cliente_id)
        setStocks(StockDeCliente)
        setArticulos((await ArticulosDeLista.GetArticulos())
            .filter(adl=>!already_added_ids.some(aai=>aai===adl.id))
            .map<AddArticuloReturn>(adl=>{
                const ada   = ArticulosDeAbonosCliente.find(ada=>ada.articulo_id===adl.id)
                const pes   = PreciosEspecialesCliente.find(pes=>pes.articuloId ===adl.id)
                const sdc   = StockDeCliente.find( sdc=>sdc.articuloId ===adl.id)
                const ldp   = ListaDePrecios.find(  ldp=>ldp.articuloId ===adl.id)
                return ({
                    esDestacado:!!ada||!!pes||!!sdc,
                    articuloId:adl.id,
                    nombreArticulo:adl.nombreArticulo,
                    codigoInterno:adl.codigoInterno,
                    cantidadDisponiblePorAbono:ada?ada.cantidad:0,
                    esRetornable:adl.tipoDeEnvase_ids===1,
                    precio:pes?pes.precio:ldp?ldp.precio:adl.precio
                } as AddArticuloReturn)
            })
            .sort((a,b)=>{
                switch(true){
                    case a.esDestacado && !b.esDestacado: return -1
                    case b.esDestacado && !a.esDestacado: return 1
                    case a.nombreArticulo < b.nombreArticulo: return -1
                    case b.nombreArticulo < a.nombreArticulo: return 1
                    default: return 0
                }
            })
        )
        return p
    },[])
    useImperativeHandle(ref,()=>({agregar}),[agregar])
    const selectArticulo=useCallback(async(articulo:AddArticuloReturn)=>{
        if(!callbacks) return
        const stockDeCliente=stocks.find(stock=>stock.articuloId===articulo.articuloId)
        await DB.ArticulosEntregados.GuardarArticuloEntregado({
            articulo_id:                articulo.articuloId,
            codigoArticulo:             articulo.codigoInterno,
            nombreDelArticulo:          articulo.nombreArticulo,
            esRetornable:               articulo.esRetornable,
            precioUnitario:             articulo.precio,
            cantidadDisponibles:        articulo.cantidadDisponiblePorAbono,
            clienteId:                  cliente!.cliente_id,
            cantidadEntregada:          0,
            cantidadEnvasesDevueltos:   0,
            cantidadEnvasesRelevados:   0,
            cantidadEnvasesPrestados:   0,
            descuentoManual:            0,
            descuentoPorCantidad:       0,
            subtotal:                   0,
            orden:                      0,
            motivoPrestamoDevolucionId: 0,
            esPredeterminado:           false,
            envasesARecuperar:          stockDeCliente?.cantidadARecuperar??0,
            envasesEnCliente:           stockDeCliente?.stockActual??0,
            envasesPermitidos:          stockDeCliente?.cantidadPermitida??0,
        })
        callbacks.resolve()

    },[callbacks, cliente, stocks])
    const cancelar=useCallback(async()=>{
        if(!callbacks) return
        callbacks.resolve()
    },[callbacks])
    const filtro = useCallback((articulo:AddArticuloReturn):boolean=>{
        if(txtFiltro==="") return true
        return articulo.nombreArticulo.toLowerCase().indexOf(txtFiltro.toLowerCase())>=0
    },[txtFiltro])
    return (
        <Modal show={!!callbacks} onHide={cancelar} size="sm" backdrop="static" centered keyboard={false} className="addArticulo" >
            <Modal.Header closeButton>Seleccionar articulo</Modal.Header>
            <Modal.Body>
                <Form.Row>
                    <Col xs={12}>
                        <FcSearch/>
                        <Form.Control type="text" size="sm" ref={filtroRef} onChange={()=>setTxtFiltro(filtroRef.current?filtroRef.current.value:"")}/>
                    </Col>
                </Form.Row>
                {articulos.filter(filtro).map(art=>(
                    <Card key={art.articuloId}>
                        <Col xs={12}>
                            <Badge>{art.articuloId}</Badge>
                            <Form.Label>{art.nombreArticulo}</Form.Label>
                            <Badge>${art.precio}</Badge>
                        </Col>
                        <Col xs={12}>
                            <Badge>
                                {art.esDestacado && <ImStarEmpty color="yellow" className="star"/>}&nbsp;
                                {art.esRetornable && <ImLoop2 color="green" className="loop"/> }
                            </Badge>
                            <Button variant="toolbar" onClick={()=>selectArticulo(art)}><FiCheck color="green"/>Seleccionar</Button>
                            <Badge>Disponibles: {art.cantidadDisponiblePorAbono}</Badge>
                        </Col>
                    </Card>
                ))}
            </Modal.Body>
        </Modal>
    )
})