import { DB } from 'utils'

import { AppData_GastosDeReparto} from 'entities/app'

export class GastosDeReparto{
    public static async getGastosDeReparto():Promise<AppData_GastosDeReparto[]>{
        return DB.GetAll<AppData_GastosDeReparto>("GastosDeReparto")
    }
    public static async addGastosDeReparto(gasto:Partial<AppData_GastosDeReparto>):Promise<AppData_GastosDeReparto>{
        delete gasto.id
        const gasto_id=await DB.Put("GastosDeReparto",gasto)
        return this.getGastoDeReparto(gasto_id)
    }
    public static async updateGastosDeReparto(gasto:AppData_GastosDeReparto):Promise<AppData_GastosDeReparto>{
        const gasto_id = await DB.Put("GastosDeReparto",gasto)
        return this.getGastoDeReparto(gasto_id)
    }
    public static async getGastoDeReparto(id:any):Promise<AppData_GastosDeReparto>{
        const gasto=await DB.Get<AppData_GastosDeReparto>("GastosDeReparto","id",id)
        if(!gasto) throw new Error("Error al obtener el gasto")
        return gasto
    }
}