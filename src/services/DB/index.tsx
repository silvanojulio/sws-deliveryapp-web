import { ClientePedidoMobile } from 'entities/sync'
import { Clientes }                         from './clientes'
import { ArticulosDeAbonos }                from './articulosDeAbonos'
import { AlertasDeCliente }                 from './alertasDeCliente'
import { Facturas }                         from './facturas'
import { StocksDeClientes }                 from './stocksDeClientes'
import { PreciosEspeciales }                from './preciosEspeciales'
import { Dispensers }                       from './dispensers'
import { ArticulosDeComodatos }             from './articulosDeComodatos'
import { OrdenesDeTrabajo }                 from './ordenesDeTrabajo'
import { DispensersAsociados }              from './dispensersAsociados'

export { UsuariosApp }                      from './usuariosApp'
export { Clientes }                         from './clientes'
export { ArticulosDeLista }                 from './articulosDeLista'
export { ArticulosEntregados }              from './articulosEntregados'
export { AlertasDeCliente }                 from './alertasDeCliente'
export { DescuentosPorCantidad }            from './descuentosPorCantidad'
export { StocksDeClientes }                 from './stocksDeClientes'
export { ArticulosDeAbonos }                from './articulosDeAbonos'
export { PreciosEspeciales }                from './preciosEspeciales'
export { ListasDePrecios }                  from './listasDePrecios'
export { ComprobantesFisicosUtilizados }    from './comprobantesFisicosUtilizados'
export { FirmasRemitos }                    from './firmasRemitos'
export { ValoresSatelites }                 from './valoresSatelites'
export { ItemsDeRecibos }                   from './itemsDeRecibos'
export { Recibos }                          from './recibos'
export { Cheques }                          from './cheques'
export { Retenciones }                      from './retenciones'
export { TarjetasDeCredito }                from './tarjetasDeCredito'
export { TarjetasDeDebito }                 from './tarjetasDeDebito'
export { Facturas }                         from './facturas'
export { DevolucionesArticulos }            from './devolucionesArticulos'
export { OrdenesDeTrabajo }                 from './ordenesDeTrabajo'
export { Configuraciones }                  from './configuraciones'
export { HojasDeRuta }                      from './hojasDeRuta'
export { GastosDeReparto }                  from './gastosDeReparto'
export { DeclaracionesEfectivo }            from './declaracionesEfectivo'
export { Declaraciones }                    from './declaraciones'
export { Dispensers }                       from './dispensers'
export { DispensersAsociados }              from './dispensersAsociados'
export { ArticulosDeComodatos }             from './articulosDeComodatos'
export { LimitesDeClientes }                from './limitesDeClientes'
export { ResumenClientes }                  from './resumenClientes'
export { ComprobantesFisicos }              from './comprobantesFisicos'
export { NumerosDeComprobantesDisponibles } from './numerosDeComprobantesDisponibles'
export { Contactos }                        from './contactos'
export { Incidentes }                       from './incidentes'
export { Logs }                             from './logs'



export const AgregarPedido=async (pedido:ClientePedidoMobile)=>{
    await ArticulosDeAbonos.AgregarPedidoArticuloDeAbono(pedido)
    await AlertasDeCliente.AgregarPedidioAlertas(pedido)
    await Facturas.AgregarPedidoFacturas(pedido)
    await StocksDeClientes.AgregarPedidoStockDeCliente(pedido)
    await PreciosEspeciales.AgregarPedidoPreciosEspeciales(pedido)
    await Dispensers.AgregarPedidoDispensers(pedido)
    await ArticulosDeComodatos.AgregarPedidoArticulosDeComodatos(pedido)
    await OrdenesDeTrabajo.AgregarPedidoOrdenesDeTrabajo(pedido)
    await DispensersAsociados.AgregarPedidoDispensers(pedido)
    await Clientes.AregarPedidoCliente(pedido)
}