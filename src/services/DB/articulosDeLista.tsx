import { DB } from 'utils'
import { AppData_ArticulosDeLista } from "entities/app";
export class ArticulosDeLista{
    private static StoreName="ArticulosDeLista"
    public static async GetArticulos():Promise<AppData_ArticulosDeLista[]>{
        const articulos = await DB.GetAll<AppData_ArticulosDeLista>(this.StoreName)
        return articulos.filter(art=>art.tipoArticulo_ids===1)
    }
    public static async GetRepuestos():Promise<AppData_ArticulosDeLista[]>{
        const articulos = await DB.GetAll<AppData_ArticulosDeLista>(this.StoreName)
        return articulos.filter(art=>art.tipoArticulo_ids===3)
    }
}
export default ArticulosDeLista