import moment from 'moment'

import { DB } from 'utils'

import { AppData_DevolucionesArticulos }    from 'entities/app'
import { Transaccion }                      from 'types'
import { DevolucionArticuloMobile_DTO }     from 'entities/articulos'
import { TiposOperacionesEnvasesMobile }    from 'entities/envases'
import { Clientes }                         from './clientes'

export class DevolucionesArticulos{
    public static async Get(cliente_id:number):Promise<AppData_DevolucionesArticulos[]>{
        return DB.GetAll<AppData_DevolucionesArticulos>("DevolucionesArticulos","cliente_id", cliente_id)
    }
    public static async Save(devolucion:AppData_DevolucionesArticulos):Promise<AppData_DevolucionesArticulos[]>{
        await DB.Put("DevolucionesArticulos",devolucion)
        await Clientes.MarcarVisitado(devolucion.cliente_id)
        return this.Get(devolucion.cliente_id)
    }
    public static async Delete(devolucion:AppData_DevolucionesArticulos):Promise<AppData_DevolucionesArticulos[]>{
        await DB.Delete("DevolucionesArticulos", devolucion.id!)
        return this.Get(devolucion.cliente_id)
    }
    public static async GetMobile_DTO(cliente_id:number):Promise<DevolucionArticuloMobile_DTO[]>{
        const devoluciones = await this.Get(cliente_id)
        return devoluciones.map<DevolucionArticuloMobile_DTO>(devolucion=>({
            articulo_id:devolucion.articulo_id,
            cantidad:devolucion.cantidad,
            cliente_id:devolucion.cliente_id,
            esCambioDirecto:devolucion.esCambioDirecto,
            esReutilizable:devolucion.esReutilizable,
            fecha:moment(new Date()).format("DD/MM/YYYY"),
            id:devolucion.id!,
            motivoDolucion_ids:devolucion.motivoDevolucion_ids,
            tipoOperacion:TiposOperacionesEnvasesMobile.Devolucion
        }))
    }
    public static async GetTransacciones(cliente_id:number):Promise<Transaccion[]>{
        const devoluciones = await this.Get(cliente_id)
        return devoluciones.map<Transaccion>(devolucion=>{
            return ({
                descripcion:`PNC ${devolucion.nombreArticulo}`,
                tipo:"pnc",
                cantidad:devolucion.cantidad
            })
        })
    }
    public static async EliminarTransacciones(cliente_id:number){
        const devoluciones = await this.Get(cliente_id)
        for(let devolucion of devoluciones){
            await DB.Delete("DevolucionesArticulos", devolucion.id!)
        }
    }
}