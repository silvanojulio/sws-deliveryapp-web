import { DB } from 'utils'
import { AppData_Dispensers } from "entities/app";
import { ClientePedidoMobile } from 'entities/sync';

export class Dispensers{
    public static async GetDispensersXCliente(cliente_id:number):Promise<AppData_Dispensers[]>{
        return DB.GetAll<AppData_Dispensers>("Dispensers","cliente_id",cliente_id)
    }
    public static async AgregarPedidoDispensers(pedido:ClientePedidoMobile):Promise<AppData_Dispensers[]>{
        return Promise.all(pedido.dispensers.map<Promise<AppData_Dispensers>>(async(dispenser)=>{
            const id = await DB.Put("Dispensers",dispenser)
            const n_dispenser = await DB.Get<AppData_Dispensers>("Dispensers","id",id)
            if(!n_dispenser) throw new Error(`Error al agregar dispenser al cliente ID Nº ${dispenser.cliente_id}`)
            return n_dispenser
        }))
    }
}