import { DB } from 'utils'

import { Clientes }         from './clientes'
import { Facturas }         from './facturas'
import { ItemsDeRecibos }   from './itemsDeRecibos'
import { Configuraciones }  from './configuraciones'
import { AlertasDeCliente } from './alertasDeCliente'

import { AppData_LimitesDeClientes }    from 'entities/app'

export class LimitesDeClientes{
    public static async GetLimite(clienteId:number):Promise<AppData_LimitesDeClientes>{
        const limite = await DB.Get<AppData_LimitesDeClientes>("LimitesDeClientes","clienteId",clienteId)
        if(!limite) return {
            id:0,
            clienteId,
            limiteDeFacturas:0,
            limiteDeSaldo:0,
            validaLimiteDeSaldo:false,
            validaLimiteFacturas:false
        }
        return limite
    }
    public static async ValidarLimites(clienteId:number):Promise<boolean>{
        const BLOQUEO_DE_CUENTA = await Configuraciones.GetConfig("BLOQUEO_DE_CUENTA","FALSE") 
        if(BLOQUEO_DE_CUENTA==="FALSE") return true
        const limite    = await this.GetLimite(clienteId)
        const cliente   = await Clientes.GetCliente(clienteId)
        const facturas  = await Facturas.GetVencidas(clienteId)
        const cobros    = await ItemsDeRecibos.GetItemsDeReciboCliente(clienteId)
        if(!cliente) throw new Error(`Cliente inexistente (ID: ${clienteId})`)
        return (
            (!limite.validaLimiteFacturas || facturas.length===0 || limite.limiteDeFacturas>facturas.filter(factura=>factura.imputado<factura.saldoActual).length) &&
            (!limite.validaLimiteDeSaldo  || limite.limiteDeSaldo > cliente.saldoFacturacion-cobros.reduce((total, cobro)=>total+cobro.importe,0))
        )
    }
    public static async Desbloquear(clienteId:number):Promise<void>{
        const limite = await this.GetLimite(clienteId)
        limite.validaLimiteDeSaldo  = false
        limite.validaLimiteFacturas = false
        await DB.Put("LimitesDeClientes",limite)
        await AlertasDeCliente.DesbloquearCliente(clienteId)
    }
}