import { DB }                       from 'utils'
import { AppData_OrdenesDeTrabajo } from 'entities/app'
import { ClientePedidoMobile } from 'entities/sync'

export class OrdenesDeTrabajo{
    public static async Get(cliente_id:number):Promise<AppData_OrdenesDeTrabajo[]>{
        return DB.GetAll<AppData_OrdenesDeTrabajo>("OrdenesDeTrabajo","cliente_id",cliente_id)
    }
    public static async Save(orden:AppData_OrdenesDeTrabajo):Promise<void>{
        await DB.Put("OrdenesDeTrabajo",orden)
    }
    public static async AgregarPedidoOrdenesDeTrabajo(pedido:ClientePedidoMobile):Promise<AppData_OrdenesDeTrabajo[]>{
        if(!pedido.ordenesDeTrabajo) return []
        return Promise.all(pedido.ordenesDeTrabajo.map<Promise<AppData_OrdenesDeTrabajo>>(async(orden)=>{
            const id = await DB.Put("OrdenesDeTrabajo",orden as AppData_OrdenesDeTrabajo)
            const n_orden = await DB.Get<AppData_OrdenesDeTrabajo>("OrdenesDeTrabajo","id",id)
            if(!n_orden) throw new Error(`Error al agregar orden de trabajo ID Nº ${orden.ordenDeTrabajoServer_id}`)
            return n_orden
        }))
    }
}