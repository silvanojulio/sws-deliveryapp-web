import { DB } from 'utils'


import { AppData_Declaraciones } from 'entities/app'

export class Declaraciones{
    public static T_Cheque=1
    public static T_Retencion=2
    public static T_Tarjetas=3
    public static async GetDeclaracion(id:any):Promise<AppData_Declaraciones>{
        const declaracion = await DB.Get<AppData_Declaraciones>("Declaraciones","id",id)
        if(!declaracion)throw new Error("No se encuentra la declaracion")
        return declaracion
    }
    public static async GetTipo(tipo:number):Promise<AppData_Declaraciones[]>{
        const declaraciones = await this.GetAll()
        return declaraciones.filter(d=>d.tipo===tipo)
    }
    public static async GetAll():Promise<AppData_Declaraciones[]>{
        return DB.GetAll<AppData_Declaraciones>("Declaraciones")
    }
    public static async Upsert(declaracion:Partial<AppData_Declaraciones>):Promise<AppData_Declaraciones>{
        if(!declaracion.id) delete declaracion.id
        const id = await DB.Put("Declaraciones",declaracion)
        return this.GetDeclaracion(id)
    }
    public static async Delete(id:any):Promise<AppData_Declaraciones>{
        return DB.Delete<AppData_Declaraciones>("Declaraciones",id)
    }
}