import { DB } from 'utils'

import { AppData_TarjetasDeDebito }     from 'entities/app'
import { ReciboMobile_DTO }             from 'entities/recibos'
import { TarjetaDeDebitoMobile_DTO }    from 'entities/recibos'

export class TarjetasDeDebito{
    public static async Get(id:number):Promise<AppData_TarjetasDeDebito|undefined>{
        return DB.Get<AppData_TarjetasDeDebito>("TarjetasDeDebito", "id", id)
    }
    public static async Save(tarjeta:AppData_TarjetasDeDebito):Promise<AppData_TarjetasDeDebito>{
        const tar = await DB.Get<AppData_TarjetasDeDebito>("TarjetasDeDebito","id",await DB.Put("TarjetasDeDebito",tarjeta))
        if(!tar) throw new Error("Error al registrar cobro con tarjeta de debito")
        return tar
    }
    public static async Delete(id:number):Promise<AppData_TarjetasDeDebito>{
        return DB.Delete<AppData_TarjetasDeDebito>("TarjetasDeDebito", id)
    }
    public static async GetMobile_DTO(clienteId:number):Promise<TarjetaDeDebitoMobile_DTO[]>{
        const recibos = await DB.GetAll<ReciboMobile_DTO>("Recibos","clienteId", clienteId)
        return (await Promise.all(
            recibos
            .map<Promise<TarjetaDeDebitoMobile_DTO[]>>(recibo=>DB.GetAll<TarjetaDeDebitoMobile_DTO>("TarjetasDeDebito", "reciboId", recibo.id))))
            .reduce((resultado,tarjetas)=>[...resultado, ...tarjetas], [])
    }
}