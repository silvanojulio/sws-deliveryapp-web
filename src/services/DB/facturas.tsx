import { DB } from 'utils'

import { AppData_Facturas }             from 'entities/app'
import { AppData_Recibos }              from 'entities/app'
import { ImputacionFacturaMobile_DTO }  from 'entities/facturas'
import { ClientePedidoMobile } from 'entities/sync'

export class Facturas{
    public static async Get(cliente_id:number):Promise<AppData_Facturas[]>{
        return DB.GetAll<AppData_Facturas>("Facturas","cliente_id",cliente_id)
    }
    public static async GetVencidas(cliente_id:number):Promise<AppData_Facturas[]>{
        const facturas=await this.Get(cliente_id)
        const fechaActual = new Date()
        return facturas.filter(factura=>{
            const vencimiento=new Date(parseInt(factura.fechaVencimiento1.substring(6, factura.fechaVencimiento1.length-2)))
            return vencimiento<=fechaActual
        })
    }
    public static async Save(factura:AppData_Facturas):Promise<AppData_Facturas>{
        const factura_id = await DB.Put("Facturas",factura)
        if(!factura_id) throw new Error("Error al guardar cambios en factura")
        const fact = await DB.Get<AppData_Facturas>("Facturas","id",factura_id)
        if(!fact) throw new Error("Error al guardar cambios en factura")
        return fact
    }
    public static async GetImputaciones(cliente_id:number):Promise<ImputacionFacturaMobile_DTO[]>{
        const facturas = await this.Get(cliente_id)
        const recibos = await DB.GetAll<AppData_Recibos>("Recibos","clienteId",cliente_id)
        if(!recibos[0]?.id) return []
        return facturas.filter(factura=>factura.imputado>0).map<ImputacionFacturaMobile_DTO>((factura, index)=>({
            cliente_id:cliente_id,
            factura_id:factura.id!,
            imputado:factura.imputado,
            recibo_id:recibos[0].id!
        }))
    }
    public static async AgregarPedidoFacturas(pedido:ClientePedidoMobile):Promise<AppData_Facturas[]>{
        return Promise.all(pedido.facturas.map<Promise<AppData_Facturas>>(async(fac)=>{
            const factura:AppData_Facturas = {
                ...fac,
                id:fac.factura_id,
                imputado:0
            }
            const id = await DB.Put("Facturas",factura)
            const n_factura = await DB.Get<AppData_Facturas>("Facturas","id",id)
            if(!n_factura) throw new Error(`Error al agregar factura a cliente ID Nº ${fac.cliente_id}`)
            return n_factura
        }))
    }
}
