import { DB } from 'utils'
import { AppData_StocksDeClientes } from "entities/app";
import { ClientePedidoMobile } from 'entities/sync';
export class StocksDeClientes{
    public static async GetStocksDeCliente(clienteId:number):Promise<AppData_StocksDeClientes[]>{
        return DB.GetAll<AppData_StocksDeClientes>("StocksDeClientes", "clienteId",clienteId)
    }
    public static async AgregarPedidoStockDeCliente(pedido:ClientePedidoMobile):Promise<AppData_StocksDeClientes[]>{
        return Promise.all(pedido.stock.map<Promise<AppData_StocksDeClientes>>(async(stock)=>{
            const stockDeCliente:AppData_StocksDeClientes = {
                ...stock,
                articuloId:stock.articulo_id
            }
            const id=await DB.Put("StocksDeClientes", stockDeCliente)
            const n_stockDeCliente = await DB.Get<AppData_StocksDeClientes>("StocksDeClientes","id",id)
            if(!n_stockDeCliente) throw new Error(`Error al agregar el stock al cliente ID Nº ${stock.clienteId}`)
            return n_stockDeCliente
        }))
    }
}