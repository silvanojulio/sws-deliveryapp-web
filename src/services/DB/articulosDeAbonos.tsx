import { DB } from 'utils'
import { AppData_ArticulosDeAbonos } from "entities/app";
import { ClientePedidoMobile } from 'entities/sync';

export class ArticulosDeAbonos{
    private static StoreName="ArticulosDeAbonos"
    public static async GetArticulosXCliente(cliente_id:number):Promise<AppData_ArticulosDeAbonos[]>{
        try{
            await DB.GetAll<AppData_ArticulosDeAbonos>(this.StoreName,"cliente_id",cliente_id)
        }catch(error){}
        return DB.GetAll<AppData_ArticulosDeAbonos>(this.StoreName,"cliente_id",cliente_id)
    }
    public static async AgregarPedidoArticuloDeAbono(pedido:ClientePedidoMobile):Promise<AppData_ArticulosDeAbonos[]>{
        const articulos=await Promise.all(pedido.articulosDeAbonos.map<Promise<AppData_ArticulosDeAbonos>>(async(art)=>{
            const id = await DB.Put(this.StoreName, art)
            const articulo = await DB.Get<AppData_ArticulosDeAbonos>(this.StoreName, "id",id)
            if(!articulo) throw new Error(`Error al crear el articulo de abono - ID Nº${art.articulo_id}`)
            return articulo
        }))
        return articulos
    }
}