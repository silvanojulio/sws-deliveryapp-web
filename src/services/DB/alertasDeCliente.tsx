import { DB } from 'utils'
import { AppData_AlertasDeCliente } from "entities/app";
import { ClientePedidoMobile } from 'entities/sync';
export class AlertasDeCliente{
    public static async GetAlertas(cliente_id:number):Promise<AppData_AlertasDeCliente[]>{
        const alertas = await DB.GetAll<AppData_AlertasDeCliente>("AlertasDeCliente")
        return alertas.filter(alerta=>alerta.cliente_id===cliente_id)
    }
    public static async GetAll():Promise<AppData_AlertasDeCliente[]>{
        return DB.GetAll<AppData_AlertasDeCliente>("AlertasDeCliente")
    }
    public static async AgregarPedidioAlertas(pedido:ClientePedidoMobile){
        return Promise.all(pedido.alertasDeCliente.map<Promise<AppData_AlertasDeCliente>>(async(al)=>{
            const id = await DB.Put("AlertasDeCliente",al)
            const alerta = await DB.Get<AppData_AlertasDeCliente>("AlertasDeCliente","id",id)
            if(!alerta)throw new Error(`Error al agregar alerta al cliente ID Nº ${al.cliente_id}`)
            return alerta
        }))
    }
    public static async DesbloquearCliente(cliente_id:number){
        const alertas = await this.GetAlertas(cliente_id)
        for(let alerta of alertas){
            alerta.esBloqueante=false
            await DB.Put("AlertasDeCliente",alerta)
        }
    }
}