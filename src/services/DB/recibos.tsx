import { DB } from 'utils'

import { AppData_Recibos }          from 'entities/app'
import { AppData_Clientes }         from 'entities/app'
import { ReciboMobile_DTO }         from 'entities/recibos'
import { ItemDeReciboMobile_DTO }   from 'entities/recibos'


export class Recibos{
    public static async GetReciboCliente(clienteId:number):Promise<AppData_Recibos | undefined>{
        return DB.Get<AppData_Recibos>("Recibos", "clienteId", clienteId)        
    }
    public static async CreateRecibo(clienteId:number):Promise<AppData_Recibos>{
        const cliente=await DB.Get<AppData_Clientes>("Clientes","cliente_id",clienteId)
        if(!cliente) throw new Error("Cliente inexistente")
        const recibo = await DB.Get<AppData_Recibos>("Recibos","id",await DB.Put("Recibos",{
            clienteId:clienteId,
            clienteFacturaId:clienteId,
            fechaHora:"",
            nombreCliente:cliente.nombreCliente, 
            cerrado:false,
            comprobanteFisicoNro:null,
            comprobanteFisicoPrefijo:null,
        } as AppData_Recibos))
        if(!recibo) throw new Error("Error al crear el recibo")
        return recibo
    }
    public static async GetReciboClienteMobile(clienteId:number):Promise<ReciboMobile_DTO[]>{
        const recibos = await DB.GetAll<ReciboMobile_DTO>("Recibos", "clienteId", clienteId)        
        const items   = await DB.GetAll<ItemDeReciboMobile_DTO>("ItemsDeRecibos")
        return recibos.filter(recibo=>items.some(item=>item.reciboId===recibo.id))

    }
}