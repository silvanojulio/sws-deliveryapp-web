import { DB }       from 'utils'
import AppStorage   from 'utils/appStorage'

import { AppData_UsuariosApp }  from 'entities/app'
import { HojasDeRuta }          from './hojasDeRuta'

export class UsuariosApp{
    public static async Login(user:string, pass:string){
        const hdr = await HojasDeRuta.GetActual()
        if(!hdr) throw new Error("Hoja de ruta no descargada")
        if(hdr.cerrada) throw new Error("Hoja de ruta Cerrada")
        const usuario = await DB.Get<AppData_UsuariosApp>("UsuariosApp","username",user)
        if(!usuario) throw new Error("Usuario inexistente")
        if(usuario.password!==pass) throw new Error("Contraseña incorrecta")
        AppStorage.setValue("CURRENT_LOCAL_SESSION",{usuario:user, expire:Date.now()+86400000, userApp:usuario})
        return usuario
    }
    public static async GetUsuarioById(usuario_id:number):Promise<AppData_UsuariosApp>{
        const usuario = await DB.Get<AppData_UsuariosApp>("UsuariosApp","usuario_id",usuario_id)
        if(!usuario) throw new Error("Usuario inexistente")
        return usuario
    }
}