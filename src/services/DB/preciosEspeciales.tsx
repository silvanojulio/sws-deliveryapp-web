import { DB } from 'utils'
import { AppData_PreciosEspeciales } from "entities/app";
import { ClientePedidoMobile } from 'entities/sync';

export class PreciosEspeciales{
    public static async GetPreciosEspecialesXCliente(clienteId:number):Promise<AppData_PreciosEspeciales[]>{
        return DB.GetAll<AppData_PreciosEspeciales>("PreciosEspeciales","clienteId",clienteId)
    }
    public static async AgregarPedidoPreciosEspeciales(pedido:ClientePedidoMobile):Promise<AppData_PreciosEspeciales[]>{
        return Promise.all(pedido.preciosEspeciales.map<Promise<AppData_PreciosEspeciales>>(async(precio)=>{
            const precioEspecial:AppData_PreciosEspeciales = {
                ...precio,
                articuloId:precio.articulo_id,
                clienteId:precio.cliente_id
            }
            const id = await DB.Put("PreciosEspeciales",precioEspecial)
            const n_precioEspecial = await DB.Get<AppData_PreciosEspeciales>("PreciosEspeciales","id",id)
            if(!n_precioEspecial) throw new Error(`Error al agregar precio especial al cliente ID Nº ${precio.cliente_id}`)
            return n_precioEspecial
        }))
    }
}