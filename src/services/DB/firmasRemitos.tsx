import { DB }                       from 'utils'
import { AppData_FirmasRemitos }    from "entities/app";
import { FirmaRemitoMobile_DTO }    from 'entities/remitos'

export class FirmasRemitos{
    private static StoreName="FirmasRemitos"
    public static async GetFirmasRemitos(cliente_id:number){
        return await DB.GetAll<AppData_FirmasRemitos>(this.StoreName, "cliente_id", cliente_id)
    }
    public static async AddFirmaRemito(firmaRemito:AppData_FirmasRemitos){
        const firmas=await this.GetFirmasRemitos(firmaRemito.cliente_id)
        await Promise.all(firmas.map(f=>DB.Delete(this.StoreName,f.id)))
        delete firmaRemito.id
        return DB.Get<AppData_FirmasRemitos>(this.StoreName,"id", await DB.Put(this.StoreName, firmaRemito))
    }
    public static async GetFirmasRemitosMobile(cliente_id:number):Promise<FirmaRemitoMobile_DTO[]>{
        const firmas=await this.GetFirmasRemitos(cliente_id)
        return firmas.map<FirmaRemitoMobile_DTO>(firma=>({
            cliente_id:cliente_id,
            fechaHora:firma.fechaHora,
            firmante:firma.firmante,
            firmaRemito:firma.firmaRemito
        }))
    }
}