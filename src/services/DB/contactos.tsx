import { DB } from 'utils'

import { AppData_Contactos } from 'entities/app'

type ContactosCliente={
    principal?:     AppData_Contactos
    alternativo?:   AppData_Contactos
}
export class Contactos{
    public static async GetAll():Promise<AppData_Contactos[]>{
        return DB.GetAll<AppData_Contactos>("Contactos")
    }
    public static async GetContactosCliente(cliente_id:number):Promise<ContactosCliente>{
        const contactos = await DB.GetAll<AppData_Contactos>("Contactos", "cliente_id",cliente_id)
        return ({
            principal:contactos.find(con=>con.tipoContacto==="Primer contacto"),
            alternativo:contactos.find(con=>con.tipoContacto==="Contacto alternativo")
        })
    }
    public static async GetAllCliente(cliente_id:number):Promise<AppData_Contactos[]>{
        return DB.GetAll<AppData_Contactos>("Contactos", "cliente_id",cliente_id)
    }
}