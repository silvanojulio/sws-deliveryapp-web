import { DB } from 'utils'
import { AppData_ValoresSatelites } from 'entities/app'

export class ValoresSatelites{    
    
    public static T_MOTIVOSDEDEVOLUCION:number = 280
    public static T_FORMASDEPAGO:number = 380
    public static T_BANCOS:number = 370
    public static T_TIPOSDEGASTOS:number = 390
    public static T_MOTIVOSDESOLICITUDDESTOCK:number = 790
    public static T_SINTOMASSERVICIOTECNICO:number = 550
    public static T_MOTIVOS_DE_CIERRE_ST:number = 580
    public static T_ACCIONES_DISPENSERS_ST:number = 860
    public static T_MOTIVOS_RELEVAMIENTO_ENVASE:number = 1000
    public static T_MARCAS_TARJETAS:number = 990
    public static T_TIPOS_RETENCIONES:number = 400
    
    private static StoreName="ValoresSatelites"
    public static async GetTablas(tabla_ids:number[]):Promise<AppData_ValoresSatelites[]>{
        const valores = await DB.GetAll<AppData_ValoresSatelites>(this.StoreName)
        return valores.filter(val=>tabla_ids.some(tbl_id=>val.tabla_id===tbl_id))
    }
}