import { DB } from 'utils'
import { AppData_ArticulosEntregados }  from "entities/app";
import { ArticuloEntregadoMobile_DTO }  from 'entities/articulos'
import { Transaccion }                  from 'types'
import { StocksDeClientes }             from './stocksDeClientes'
import { Clientes }                     from './clientes'

type ArticulosEntregadosAgrupados={
    cliente_id:number,
    articulos:AppData_ArticulosEntregados[]
}
export class ArticulosEntregados{
    public static async ActualizarEntrega(clienteId:number, articulos:AppData_ArticulosEntregados[]):Promise<void>{
        await this.LimpiarEntrega(clienteId)
        await Promise.all(articulos.map(art=>{
            delete art.id;
            return DB.Put("ArticulosEntregados", {...art, clienteId})
        }))
        if(articulos.length>0){
            await Clientes.MarcarEntregado(clienteId, articulos.reduce((total, art)=>total+art.subtotal, 0))
        }
    }
    public static async GuardarArticuloEntregado(articulo:AppData_ArticulosEntregados):Promise<void>{
        await DB.Put("ArticulosEntregados",articulo)
    }
    public static async EliminarArticuloEntregado(clienteId:number,articulo_id:number):Promise<AppData_ArticulosEntregados[]>{
        const articulos = await DB.GetAll<AppData_ArticulosEntregados>("ArticulosEntregados","clienteId",clienteId)
        const eliminados=await Promise.all(articulos.filter(art=>art.articulo_id===articulo_id).map(art=>{
            return DB.Delete<AppData_ArticulosEntregados>("ArticulosEntregados",art.id)
        }))
        return eliminados
    }
    public static async LimpiarEntrega(clienteId:number){
        const articulos=await DB.GetAll<AppData_ArticulosEntregados>("ArticulosEntregados","clienteId",clienteId)
        for(let articulo of articulos){
            await DB.Delete("ArticulosEntregados",articulo.id)
        }
    }
    public static async GetArticulosEntregadosACliente(clienteId:number){
        return DB.GetAll<AppData_ArticulosEntregados>("ArticulosEntregados","clienteId",clienteId)
    }
    public static async ValidarArticulos(clienteId:number){
        const articulos = await this.GetArticulosEntregadosACliente(clienteId)
        const stocks    = await StocksDeClientes.GetStocksDeCliente(clienteId)
        articulos.forEach(art=>{
            const stock = stocks.find(stk=>stk.articuloId===art.articulo_id)?.stockActual??0
            const _envasesFinalesEnCliente = art.cantidadEnvasesPrestados + (art.cantidadEnvasesRelevados>0?art.cantidadEnvasesRelevados:stock) - art.cantidadEnvasesDevueltos
            if(art.esRetornable){
                if(art.cantidadEnvasesDevueltos && art.cantidadEnvasesPrestados) 
                    throw new Error(`No es posible registrar devoluciones de envases y préstamos a la vez`)
                if(art.cantidadEntregada>_envasesFinalesEnCliente) 
                    throw new Error(`La cantidad entregada supera a la cantidad de envases que tiene el cliente`)
            }else{
                if(art.cantidadEnvasesRelevados || art.cantidadEnvasesDevueltos || art.cantidadEnvasesPrestados)
                    throw new Error(`Los envases no retornables no pueden ser prestados, devueltos o relevados`)
            }
            if(art.cantidadEnvasesRelevados && art.motivoPrestamoDevolucionId===0)
                throw new Error(`Debe seleccionar el Motivo del Relevamiento`)
        })
    }
    public static async GetArticulosEntregadosMobile(cliente_id:number):Promise<ArticuloEntregadoMobile_DTO[]>{
        return (await this.GetArticulosEntregadosACliente(cliente_id)).map<ArticuloEntregadoMobile_DTO>(art=>(
            {
                articulo_id:art.articulo_id,
                cantidadEntregada:art.cantidadEntregada,
                cantidadEnvasesDevueltos:art.cantidadEnvasesDevueltos,
                cantidadEnvasesPrestados:art.cantidadEnvasesPrestados,
                cantidadEnvasesRelevados:art.cantidadEnvasesRelevados,
                clienteId:cliente_id,
                descuentoManual:art.descuentoManual,
                descuentoPorCantidad:art.descuentoPorCantidad,
                id:art.id!,
                motivoPrestamoDevolucionId:art.motivoPrestamoDevolucionId,
                precioUnitario:art.precioUnitario
            }
        ))
    }
    public static async GetTransacciones(cliente_id:number):Promise<Transaccion[]>{
        const transacciones:Transaccion[] = []
        const articulos = await DB.GetAll<AppData_ArticulosEntregados>("ArticulosEntregados","clienteId",cliente_id)
        articulos.forEach(articulo=>{
            if(articulo.cantidadEntregada>0) transacciones.push({
                descripcion:articulo.nombreDelArticulo,
                tipo:"entrega",
                cantidad:articulo.cantidadEntregada
            })
            if(articulo.cantidadEnvasesPrestados>0) transacciones.push({
                descripcion:articulo.nombreDelArticulo,
                tipo:"prestamo",
                cantidad:articulo.cantidadEnvasesPrestados
            })
            if(articulo.cantidadEnvasesDevueltos>0) transacciones.push({
                descripcion:articulo.nombreDelArticulo,
                tipo:"devolucion",
                cantidad:articulo.cantidadEnvasesDevueltos
            })
        })
        return transacciones
    }
    public static async GetArticulosEntregadosAgrupados():Promise<ArticulosEntregadosAgrupados[]>{
        const articulos = await DB.GetAll<AppData_ArticulosEntregados>("ArticulosEntregados")
        const ret = articulos.reduce<ArticulosEntregadosAgrupados[]>((ret:ArticulosEntregadosAgrupados[],art)=>{
            const curr=ret.find(r=>r.cliente_id===art.clienteId)
            if(!curr){
                ret.push({cliente_id:art.clienteId,articulos:[art]})
            }else{
                curr.articulos.push(art)
            }
            return ret
        },[])
        return ret
    }
}