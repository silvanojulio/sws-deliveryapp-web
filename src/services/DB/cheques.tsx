import { DB } from 'utils'


import { AppData_Cheques }          from 'entities/app'
import { AppData_Recibos }          from 'entities/app'
import { AppData_ItemsDeRecibos }   from 'entities/app'
import { ChequeMobile_DTO }         from 'entities/recibos'


export class Cheques{
    /*public static async AddCheque(cheque:AppData_Cheques):Promise<AppData_Cheques>{
        delete cheque.id
        return DB.Get<AppData_Cheques>("Cheques","id",await DB.Put("Cheques", cheque))
    }*/
    public static async SaveCheque(cheque:AppData_Cheques):Promise<AppData_Cheques>{
        const newcheque = await DB.Get<AppData_Cheques>("Cheques","id",await DB.Put("Cheques", cheque))
        if(!newcheque) throw new Error("Error al registrar el cheque")
        return newcheque
    }
    public static async GetCheque(id:number):Promise<AppData_Cheques | undefined>{
        return DB.Get<AppData_Cheques>("Cheques","id",id)
    }
    public static async DeleteCheque(id:number):Promise<AppData_Cheques>{
        return DB.Delete<AppData_Cheques>("Cheques",id)
    }
    public static async GetByCliente(clienteId:number):Promise<AppData_Cheques[]>{
        const recibo = await DB.Get<AppData_Recibos>("Recibos","clienteId",clienteId)
        if(!recibo?.id) return []
        const items  = await DB.GetAll<AppData_ItemsDeRecibos>("ItemsDeRecibos","reciboId",recibo.id)
        if(!items.some(item=>!!item.chequeId)) return []
        return DB.GetAll<AppData_Cheques>(
            "Cheques",
            "id",
            items.map<number | null>(item=>item.chequeId)
        )
    }
    public static async GetByClienteMobile(clienteId:number):Promise<ChequeMobile_DTO[]>{
        const recibo = await DB.Get<AppData_Recibos>("Recibos","clienteId",clienteId)
        if(!recibo?.id) return []
        const items  = await DB.GetAll<AppData_ItemsDeRecibos>("ItemsDeRecibos","reciboId",recibo.id)
        return Promise.all(items.filter(item=>!!item.chequeId).map<Promise<ChequeMobile_DTO>>(async item=>{
            const cheque=await DB.Get<ChequeMobile_DTO>("Cheques","id",item.chequeId)
            return cheque!
        }))
        /*
        if(!items.some(item=>!!item.chequeId)) return []
        return DB.GetAll<ChequeMobile_DTO>(
            "Cheques",
            "id",
            items.map<number | null>(item=>item.chequeId)
        )*/
    }
}