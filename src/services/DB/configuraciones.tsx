import { DB } from 'utils'

import { AppData_Configuraciones } from 'entities/app'

export class Configuraciones{
    public static async GetAll():Promise<AppData_Configuraciones[]>{
        return DB.GetAll<AppData_Configuraciones>("Configuraciones")
    }
    public static async GetConfig(clave:string, defaultValue:string):Promise<string>{
        const configs = await this.GetAll()
        const config  = configs.find(c=>c.clave===clave)
        return config?.valor??defaultValue
    }
}