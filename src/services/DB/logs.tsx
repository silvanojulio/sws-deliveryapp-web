import { DB } from 'utils'
import moment from 'moment'
import { AppData_Logs } from 'entities/app'

export class Logs{
    public static async AddLog(log:Partial<AppData_Logs> & {mensaje:string, funcionalidad:string}){
        log.idEntidad=log.idEntidad??0
        log.entidad=log.entidad??""
        log.fechaHora=moment().format("YYYY-MM-DD hh:mm:ss")
        DB.Put("Logs",log)
    }
}
export default Logs