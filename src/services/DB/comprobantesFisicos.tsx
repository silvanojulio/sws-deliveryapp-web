import { DB } from 'utils'

import { AppData_ComprobantesFisicos } from 'entities/app'


export class ComprobantesFisicos{
    public static async GetPrefijoDeRango(rango:number):Promise<number>{
        const comprobanteFisico = await DB.Get<AppData_ComprobantesFisicos>("ComprobantesFisicos","idRango",rango)
        if(!comprobanteFisico) throw new Error("Rango invalido")
        return comprobanteFisico.prefijo
    }
}