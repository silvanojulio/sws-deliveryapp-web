import { DB } from 'utils'
import { AppData_ListasDePrecios } from "entities/app";

export class ListasDePrecios{
    private static StoreName="ListasDePrecios"
    public static async GetListaDePrecios(listaId:number|null):Promise<AppData_ListasDePrecios[]>{
        return !listaId?[]:DB.GetAll<AppData_ListasDePrecios>(this.StoreName,"listaId",listaId)
    }
}