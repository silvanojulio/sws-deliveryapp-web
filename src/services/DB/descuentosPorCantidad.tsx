import { DB } from 'utils'
import { AppData_DescuentosPorCantidad } from "entities/app";
export class DescuentosPorCantidad{
    private static StoreName="DescuentosPorCantidad"
    public static async GetDescuentosPorCantidad(listaDePrecioId:number|null):Promise<AppData_DescuentosPorCantidad[]>{
        if(!listaDePrecioId) return []
        const descuentos = await DB.GetAll<AppData_DescuentosPorCantidad>(this.StoreName)
        return descuentos.filter(descuento=>descuento.listaDePrecioId===listaDePrecioId)
    }
}