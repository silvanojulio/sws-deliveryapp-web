import { DB } from 'utils'

import { AppData_NumerosDeComprobantesDisponibles } from 'entities/app'
import { AppData_ComprobantesFisicos } from 'entities/app'

export class NumerosDeComprobantesDisponibles{

    public static async NumeroDisponible(prefijo:number, numero:number, tipo:number):Promise<boolean>{
        const disponibles   = (await DB.GetAll<AppData_NumerosDeComprobantesDisponibles>("NumerosDeComprobantesDisponibles")).filter(d=>d.nroComprobante===numero && !d.utilizado)
        const comprobantes  = (await DB.GetAll<AppData_ComprobantesFisicos>("ComprobantesFisicos")).filter(c=>c.prefijo===prefijo && c.idTipoComprobante===tipo)
        const comprobante   = disponibles.find(d=>comprobantes.some(com=>com.idRango===d.idRango))
        return !!comprobante
    }
    public static async MarcarUtilizado(prefijo:number, numero:number){
        const disponibles   = (await DB.GetAll<AppData_NumerosDeComprobantesDisponibles>("NumerosDeComprobantesDisponibles")).filter(d=>d.nroComprobante===numero && !d.utilizado)
        const comprobantes  = (await DB.GetAll<AppData_ComprobantesFisicos>("ComprobantesFisicos")).filter(c=>c.prefijo===prefijo)
        const comprobante   = disponibles.find(d=>comprobantes.some(com=>com.idRango===d.idRango))
        if(!comprobante) throw new Error(`Comprobante ${prefijo}-${numero} no esta disponible`)
        comprobante.utilizado=true
        DB.Put("NumerosDeComprobantesDisponibles", comprobante)
    }
}