import { DB } from 'utils'

import { AppData_HojaDeRuta }               from 'entities/app'
import { AppData_GastosDeReparto }          from 'entities/app'
import { AppData_DeclaracionesEfectivo }    from 'entities/app'
import { AppStorage }                       from 'utils'
import { SessionService }                   from 'services/session'
import { HojasDeRutaSerivce }               from 'services/hojasDeRuta'
import { Clientes }                         from './clientes'

export class HojasDeRuta{
    public static async Get(id:number):Promise<AppData_HojaDeRuta | undefined>{
        return DB.Get<AppData_HojaDeRuta>("HojasDeRuta","id",id)
    }
    public static async GetActual():Promise<AppData_HojaDeRuta | undefined>{
        return DB.Get<AppData_HojaDeRuta>("HojasDeRuta","id",parseInt(await AppStorage.getString("HojaDeRutaId")))
    }
    public static async CerrarHojaDeRutaActual():Promise<void>{
        if((await Clientes.GetPendientesSync()).length>0) throw new Error("Existen clientes pendientes de sincronizar")
        const codigoMovil           = await AppStorage.getString("MOVILID")
        const hojaDeRutaId          = await AppStorage.getString("HojaDeRutaId")
        const hdr                   = await this.GetActual()
        const usuarioId             = (await SessionService.GetCurrentLocalSession())?.userApp.usuario_id
        const declaracionesEfectivo = await DB.GetAll<AppData_DeclaracionesEfectivo>("DeclaracionesEfectivo")
        const gastos                = await DB.GetAll<AppData_GastosDeReparto>("GastosDeReparto")
        if(!codigoMovil || !hojaDeRutaId || !usuarioId) return
        await HojasDeRutaSerivce.CerrarHojaDeRuta(
            codigoMovil, 
            hojaDeRutaId, 
            usuarioId, 
            gastos, 
            declaracionesEfectivo
        )
        await DB.Put("HojasDeRuta",{...hdr, cerrada:true})
    }
}