import { AppData_ResumenClientes } from 'entities/app';
import { DB } from 'utils/indexedDB'




export class ResumenClientes{
    public static async BuscarClientes(texto:string):Promise<AppData_ResumenClientes[]>{
        const clientes = await DB.GetAll<AppData_ResumenClientes>("ResumenClientes")
        return clientes.filter(cliente=>
            (cliente.nombreCliente && cliente.nombreCliente.toLowerCase().indexOf(texto)    >= 0) || 
            (cliente.domicilioCompleto && cliente.domicilioCompleto.toLowerCase().indexOf(texto)>= 0)
        )
    }
    public static async GetResumenCliente(clienteId:number){
        return DB.Get<AppData_ResumenClientes>("ResumenClientes","clienteId",clienteId)
    }
}