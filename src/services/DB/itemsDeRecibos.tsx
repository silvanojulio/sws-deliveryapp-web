import { DB } from 'utils'
import { AppData_ItemsDeRecibos }   from 'entities/app'
import { AppData_Recibos }          from 'entities/app'
import { ReciboMobile_DTO }         from 'entities/recibos'
import { ItemDeReciboMobile_DTO}    from 'entities/recibos'
import { Transaccion }              from 'types'

import { Clientes }                 from './clientes'
type ItemsDeReciboAgrupados={
    cliente_id:number,
    itemsDeRecibos:AppData_ItemsDeRecibos[]
}
export class ItemsDeRecibos{
    public static async GetAll():Promise<AppData_ItemsDeRecibos[]>{
        return DB.GetAll<AppData_ItemsDeRecibos>("ItemsDeRecibos")
    }
    public static async GetItemsDeReciboCliente(cliente_id:number):Promise<AppData_ItemsDeRecibos[]>{
        const recibo = await DB.Get<AppData_Recibos>("Recibos","clienteId",cliente_id)
        if(!recibo) return []
        return DB.GetAll<AppData_ItemsDeRecibos>("ItemsDeRecibos","reciboId",recibo.id)
    }
    public static async AddItemDeRecibo(item:AppData_ItemsDeRecibos):Promise<AppData_ItemsDeRecibos[]>{
        const recibo = await DB.Get<AppData_Recibos>("Recibos","id",item.reciboId)
        if(!recibo) throw new Error("No se puede agregar un item de recibo sin recibo asociado")
        await Clientes.MarcarVisitado(recibo.clienteId)
        if(item.formaDePagoId===1){
            const items = await DB.GetAll<AppData_ItemsDeRecibos>("ItemsDeRecibos","reciboId", item.reciboId)
            const efectivo = items.find(i=>i.formaDePagoId===1)
            if(efectivo) item.id=efectivo.id
        }        
        await DB.Put("ItemsDeRecibos",item)
        const items=await DB.GetAll<AppData_ItemsDeRecibos>("ItemsDeRecibos","reciboId", item.reciboId)
        return items
    }
    public static async EditItemDeRecibo(item:AppData_ItemsDeRecibos):Promise<AppData_ItemsDeRecibos[]>{
        await DB.Put("ItemsDeRecibos",item) 
        return DB.GetAll<AppData_ItemsDeRecibos>("ItemsDeRecibos","reciboId", item.reciboId)
    }
    public static async DeleteItemDeRecibo(item:AppData_ItemsDeRecibos):Promise<AppData_ItemsDeRecibos[]>{
        if(item.id) await DB.Delete<AppData_ItemsDeRecibos>("ItemsDeRecibos",item.id)
        return DB.GetAll<AppData_ItemsDeRecibos>("ItemsDeRecibos","reciboId", item.reciboId)
    }
    public static async GetMobile_DTO(cliente_id:number):Promise<ItemDeReciboMobile_DTO[]>{
        const recibo = await DB.Get<ReciboMobile_DTO>("Recibos","clienteId",cliente_id)
        if(!recibo) return []
        return DB.GetAll<ItemDeReciboMobile_DTO>("ItemsDeRecibos","reciboId",recibo.id)
    }
    public static async GetTransacciones(cliente_id:number):Promise<Transaccion[]>{
        const items = await this.GetItemsDeReciboCliente(cliente_id)
        return items.map<Transaccion>(item=>{
            var descripcion=""
            switch(item.formaDePagoId){
                case 1: descripcion="Cobro en efectivo"; break;
                case 2: descripcion="Cobro con cheque"; break;
                case 8: descripcion="Cobro con tarjeta de crédito"; break;
                case 9: descripcion="Retención"; break;
                case 10: descripcion="Cobro con tarjeta de débito"; break;
            }
            return {
                descripcion,
                tipo:"cobro",
                cantidad:item.importe
            }
        })
    }
    public static async GetItemsDeReciboAgrupados():Promise<ItemsDeReciboAgrupados[]>{
        const recibos   = await DB.GetAll<AppData_Recibos>("Recibos")
        const items     = await DB.GetAll<AppData_ItemsDeRecibos>("ItemsDeRecibos")
        const ret       = items.reduce<ItemsDeReciboAgrupados[]>((ret,item)=>{
            const recibo = recibos.find(rec=>rec.id===item.reciboId)!
            const agrupados=ret.find(r=>r.cliente_id===recibo.clienteId)
            if(!agrupados){
                ret.push({cliente_id:recibo.clienteId,itemsDeRecibos:[item]})
            }else{
                agrupados.itemsDeRecibos.push(item)
            }
            return ret
        },[])
        return ret
    }
    public static async EliminarTransacciones(cliente_id:number){
        const items = await this.GetItemsDeReciboCliente(cliente_id)
        for(let item of items){
            await DB.Delete("ItemsDeRecibos", item.id!)
        }
        
        const recibos = await DB.GetAll<ReciboMobile_DTO>("Recibos", "clienteId", cliente_id)        
        for(let recibo of recibos){
            await DB.Delete("Recibos",recibo.id)
        }
    }

}