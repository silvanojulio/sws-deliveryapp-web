import { DB } from 'utils'

import { AppData_Retenciones }      from 'entities/app'
import { ReciboMobile_DTO }         from 'entities/recibos'
import { RetencionesMobile_DTO }    from 'entities/recibos'

export class Retenciones{
    public static async Save(retencion:AppData_Retenciones):Promise<AppData_Retenciones>{
        const ret = await DB.Get<AppData_Retenciones>("Retenciones","id",await DB.Put("Retenciones",retencion))
        if(!ret) throw new Error("Error al registrar la retencion")
        return ret
    }
    public static async GetRetencion(id:number):Promise<AppData_Retenciones |undefined>{
        return DB.Get<AppData_Retenciones>("Retenciones","id",id)
    }
    public static async DeleteRetencion(id:number):Promise<AppData_Retenciones>{
        return DB.Delete<AppData_Retenciones>("Retenciones",id)
    }
    public static async GetMobile_DTO(clienteId:number):Promise<RetencionesMobile_DTO[]>{
        const recibos = await DB.GetAll<ReciboMobile_DTO>("Recibos","clienteId", clienteId)
        if(!recibos || recibos.length===0) return []
        return DB.GetAll<RetencionesMobile_DTO>("Retenciones","reciboId", recibos.map<number>(recibo=>recibo.id))
        /*return (await Promise.all(
            recibos
            .map<Promise<RetencionesMobile_DTO[]>>(recibo=>DB.GetAll<RetencionesMobile_DTO>("Retenciones", "reciboId", recibo.id))))
            .reduce((resultado,tarjetas)=>[...resultado, ...tarjetas], [])
        */
    }
}