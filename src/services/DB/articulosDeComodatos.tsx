import { DB } from 'utils'
import { AppData_ArticulosDeComodatos } from "entities/app";
import { ClientePedidoMobile } from 'entities/sync';

export class ArticulosDeComodatos{
    public static async GetArticulosXCliente(cliente_id:number):Promise<AppData_ArticulosDeComodatos[]>{
        return DB.GetAll<AppData_ArticulosDeComodatos>("ArticulosDeComodatos","cliente_id",cliente_id)
    }
    public static async AgregarPedidoArticulosDeComodatos(pedido:ClientePedidoMobile):Promise<AppData_ArticulosDeComodatos[]>{
        return await Promise.all(pedido.articulosDeAbonos.map<Promise<AppData_ArticulosDeComodatos>>(async(art)=>{
            const articulo:AppData_ArticulosDeComodatos = {
                ...art,
                precio:art.precioExcendente
            }
            const id = await DB.Put("ArticulosDeComodatos", articulo)
            const n_articulo = await DB.Get<AppData_ArticulosDeComodatos>("ArticulosDeComodatos", "id",id)
            if(!n_articulo) throw new Error(`Error al agregar articulo de comodato al clietne ID Nº ${art.cliente_id}`)
            return n_articulo
        }))
    }
}