import { DB } from 'utils'
//import { StoreTriggers }        from './store'
import { VIEW_Incidente_DTO }   from 'entities/incidentes'

export class Incidentes{
    public static async UpdateList(newIncidentes:VIEW_Incidente_DTO[]):Promise<void>{
        const oldIncidentes = await DB.GetAll<VIEW_Incidente_DTO>("Incidentes")
        await Promise.all(oldIncidentes.filter(o=>!newIncidentes.some(n=>n.id===o.id)).map(i=>DB.Delete("Incidentes",i.id)))
        await Promise.all(newIncidentes.map(i=>DB.Put("Incidentes",i)))
        //this.Trigger()
    }
    public static async GetList():Promise<VIEW_Incidente_DTO[]>{
        return DB.GetAll<VIEW_Incidente_DTO>("Incidentes")
    }
}