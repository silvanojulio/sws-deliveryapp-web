import { DB } from 'utils'

import { AppData_ComprobantesFisicosUtilizados }    from "entities/app";
import { ComprobanteFisicoUtilizadoMobile_DTO }     from 'entities/comprobantes'
import { NumerosDeComprobantesDisponibles }         from './numerosDeComprobantesDisponibles'

export class ComprobantesFisicosUtilizados{
    private static StoreName="ComprobantesFisicosUtilizados"
    public static async GetComprobantesFisicosUtilizadosXCliente(cliente_id:number):Promise<AppData_ComprobantesFisicosUtilizados[]>{
        return DB.GetAll<AppData_ComprobantesFisicosUtilizados>("ComprobantesFisicosUtilizados","cliente_id",cliente_id)
    }
    public static async AddComprobanteFisicoUtilizado(comprobante:Partial<AppData_ComprobantesFisicosUtilizados>):Promise<AppData_ComprobantesFisicosUtilizados>{
        delete comprobante.id
        if(!comprobante.nroPrefijo || !comprobante.nroComprobante || !comprobante.tipoDeComprobanteFisico_ids) 
            throw new Error("Especifique un numero de prefijo y comprobante")
        if(!await NumerosDeComprobantesDisponibles.NumeroDisponible(comprobante.nroPrefijo, comprobante.nroComprobante, comprobante.tipoDeComprobanteFisico_ids))
            throw new Error(`Comprobante ${comprobante.nroPrefijo}-${comprobante.nroComprobante} no disponible`)
        const _comprobante = await DB.Get<AppData_ComprobantesFisicosUtilizados>(this.StoreName,"id", await DB.Put(this.StoreName, comprobante))
        if(!_comprobante) throw new Error("Error al registrar comprobante")
        NumerosDeComprobantesDisponibles.MarcarUtilizado(_comprobante.nroPrefijo, _comprobante.nroComprobante)
        return _comprobante
    }
    public static async GetComprobantesFisicosUtilizadosMobile(cliente_id:number):Promise<ComprobanteFisicoUtilizadoMobile_DTO[]>{
        return (await this.GetComprobantesFisicosUtilizadosXCliente(cliente_id)).map<ComprobanteFisicoUtilizadoMobile_DTO>(comp=>(
            {
                cliente_id:cliente_id,
                id:comp.id!,
                nroComprobante:comp.nroComprobante,
                nroPrefijo:comp.nroPrefijo,
                tipoDeComprobanteFisico_ids:comp.tipoDeComprobanteFisico_ids
            }
        ))
    }
}