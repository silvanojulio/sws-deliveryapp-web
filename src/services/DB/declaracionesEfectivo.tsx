import { DB } from 'utils'

import { AppData_DeclaracionesEfectivo } from 'entities/app'

export class DeclaracionesEfectivo{
    public static async GetAll():Promise<AppData_DeclaracionesEfectivo[]>{
        return DB.GetAll<AppData_DeclaracionesEfectivo>("DeclaracionesEfectivo")
    }
    public static async Update(id:number, cantidad:number):Promise<AppData_DeclaracionesEfectivo>{
        const declaracion = await DB.Get<AppData_DeclaracionesEfectivo>("DeclaracionesEfectivo","id",id)
        if(!declaracion) throw new Error("No existe la declaracion de efectivo")
        await DB.Put("DeclaracionesEfectivo",{...declaracion, cantidad})
        return {...declaracion, cantidad}
    }
}