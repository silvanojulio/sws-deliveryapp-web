import { DB } from 'utils'

import { AppData_DispensersAsociados }              from "entities/app";
import { DispenserAsociadoAOrdenDeTrabajoMobile }   from 'entities/dispensers';
import { ClientePedidoMobile }                      from 'entities/sync';

export class DispensersAsociados{
    public static async GetDispensersXCliente(ordenDeTrabajoId:number):Promise<AppData_DispensersAsociados[]>{
        return DB.GetAll<AppData_DispensersAsociados>("DispensersAsociados","ordenDeTrabajoId",ordenDeTrabajoId)
    }
    public static async AgregarPedidoDispensers(pedido:ClientePedidoMobile):Promise<AppData_DispensersAsociados[]>{
        if(!pedido.ordenesDeTrabajo) return []
        const dispensers=pedido.ordenesDeTrabajo.reduce<DispenserAsociadoAOrdenDeTrabajoMobile[]>((arr,orden)=>[...orden.dispensers],[])
        return Promise.all(dispensers.map<Promise<AppData_DispensersAsociados>>(async(disp)=>{
            const dispenser:AppData_DispensersAsociados = {
                ...disp,
                OrdenDeTrabajoServer_id:disp.ordenDeTrabajoServer_id,
                ordenDeTrabajoId:disp.ordenDeTrabajoServer_id,
                mantemientoCargado:false

            }
            const id = await DB.Put("DispensersAsociados",dispenser)
            const n_dispenser = await DB.Get<AppData_DispensersAsociados>("DispensersAsociados","id",id)
            if(!n_dispenser) throw new Error(`Error al agregar dispenser asociado ID Nº ${dispenser.dispenser_id}`)
            return n_dispenser
        }))
    }
}