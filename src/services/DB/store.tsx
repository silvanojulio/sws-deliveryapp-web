export class StoreTriggers{
    private static Subscriptions:(undefined | (()=>void))[]=[]
    protected static Trigger(){        
        this.Subscriptions.forEach(fn=>!!fn && fn())
    }
    public static Subscribe(fn:()=>void){
        this.Subscriptions.push(fn)
        fn()
        return this.Subscriptions.length-1
    }
    public static Unsubscribe(id:number){
        this.Subscriptions[id]=undefined
    }
}