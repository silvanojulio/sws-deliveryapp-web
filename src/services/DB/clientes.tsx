import moment                           from 'moment'

import { AppData_Clientes }             from "entities/app";
import { DB }                           from 'utils'
import { PrinterTemplates }             from 'utils'
import { AppStorage }                   from 'utils'
import { Transaccion }                  from 'types'
import { Receipt }                      from 'types'
import { ClienteVisitadoMobile_DTO }    from "entities/clientes";
import { ClientePedidoMobile }          from "entities/sync";
import { ArticulosEntregados }          from './articulosEntregados'
import { ItemsDeRecibos }               from './itemsDeRecibos'
import { DevolucionesArticulos }        from './devolucionesArticulos'
import { Configuraciones }              from './configuraciones'
import { HojasDeRuta }                  from './hojasDeRuta'
import { ResumenClientes }              from './resumenClientes'
import { AlertasDeCliente }             from './alertasDeCliente'
import { LimitesDeClientes }            from './limitesDeClientes'
import { Facturas }                     from './facturas'
import { Printer }                      from 'services/printer'


export class Clientes{
    public static async SaveCliente(cliente:Partial<AppData_Clientes>){
        cliente.relevamientoCoordenadas_latitud=cliente.relevamientoCoordenadas_latitud??""
        cliente.relevamientoCoordenadas_longitud=cliente.relevamientoCoordenadas_longitud??""
        cliente.altitud=cliente.altitud??""
        cliente.longitud=cliente.longitud??""        
        await DB.Put("Clientes", cliente)
    }
    public static async GetClientes():Promise<AppData_Clientes[]>{
        const clientes      = await DB.GetAll<AppData_Clientes>("Clientes")
        return clientes.sort((cl1, cl2)=>cl1.orden-cl2.orden)
    }
    public static async GetCliente(cliente_id:number):Promise<AppData_Clientes | undefined>{
        return DB.Get<AppData_Clientes>("Clientes","cliente_id",cliente_id)
    }
    public static async MarcarVisitado(cliente_id:number){
        const cliente=await this.GetCliente(cliente_id)
        if(!cliente) throw new Error("Cliente no encontrado")
        const itemsDeRecibos = await ItemsDeRecibos.GetItemsDeReciboCliente(cliente_id)
        cliente.totalCobradoActual=itemsDeRecibos.reduce((total, item)=>total+item.importe, 0)
        cliente.esRepaso=false
        cliente.visitado=true
        cliente.visita_fechaHora=moment(new Date()).format("DD/MM/YYYY HH:mm")
        cliente.visita_longitud=await AppStorage.getString("CURRENTLONGITUD","")
        cliente.visita_altitud=await AppStorage.getString("CURRENTLATITUD","")
        this.SaveCliente(cliente)
        return cliente
    }
    public static async MarcarRepaso(cliente_id:number){
        const cliente=await this.GetCliente(cliente_id)
        if(!cliente) throw new Error("Cliente no encontrado")
        cliente.esRepaso=true
        this.SaveCliente(cliente)
        return cliente
    }
    public static async MarcarAusente(cliente_id:number){
        const cliente=await this.MarcarVisitado(cliente_id)
        cliente.ausente=true
        this.SaveCliente(cliente)
        return cliente
    }
    public static async MarcarEntregado(cliente_id:number, totalEntregadoActual:number){
        const cliente=await this.MarcarVisitado(cliente_id)
        cliente.ausente=false
        cliente.ventaEntrega=totalEntregadoActual>0
        cliente.totalEntregadoActual=totalEntregadoActual
        this.SaveCliente(cliente)
        return cliente
    }
    public static async MarcarCobrado(cliente_id:number, totalCobradoActual:number){
        const cliente=await this.MarcarVisitado(cliente_id)
        cliente.ausente=false
        cliente.totalCobradoActual=totalCobradoActual
        this.SaveCliente(cliente)
        return cliente
    }
    public static async MarcarCerrado(cliente_id:number){
        const cliente=await this.MarcarVisitado(cliente_id)
        cliente.cerrado=true
        cliente.esSync=true
        cliente.fechaHoraTransferido=null
        this.SaveCliente(cliente)
        return cliente
    }
    public static async GetClienteVisitado(cliente_id:number):Promise<ClienteVisitadoMobile_DTO>{
        const cliente       = await this.GetCliente(cliente_id)
        if(!cliente) throw new Error("Cliente no encontrado")
        const transacciones = await this.GetTransacciones(cliente_id)
        const imputaciones  = await Facturas.GetImputaciones(cliente_id)
        const cobroConsumo  = transacciones
                                .filter(tr=>tr.tipo==="cobro")
                                .reduce<number>((total, tr)=>total+(tr.cantidad??0), 0)
                            > imputaciones
                                .reduce<number>((total,imp)=>total+imp.imputado, 0)
        const cobroFacturas = imputaciones.some(imp=>imp.imputado>0)
        
        return {
            cliente_id:cliente.cliente_id,
            ausente:cliente.ausente,
            visitado:cliente.visitado,
            ventaEntrega:cliente.ventaEntrega               || transacciones.some(tr=>tr.tipo==="entrega"),
            devolucionArticulo:cliente.devolucionArticulo   || transacciones.some(tr=>tr.tipo==="pnc"),
            devolucionEnvases:cliente.devolucionEnvases     || transacciones.some(tr=>tr.tipo==="devolucion"),
            prestamoEnvases:cliente.prestamoEnvases         || transacciones.some(tr=>tr.tipo==="prestamo"),
            cobroConsumo:cliente.cobroConsumo               || cobroConsumo,
            cobroFactura:cliente.cobroFactura               || cobroFacturas,
            descargado:cliente.descargado,
            visita_fechaHora:cliente.visita_fechaHora,
            latitud:cliente.altitud,
            longitud:cliente.longitud,
            relevamientoCoordenadas_latitud:cliente.relevamientoCoordenadas_latitud,
            relevamientoCoordenadas_longitud:cliente.relevamientoCoordenadas_longitud,
            visita_altitud:cliente.visita_altitud,
            visita_longitud:cliente.visita_longitud
        }
    }
    public static async GetTransacciones(cliente_id:number):Promise<Transaccion[]>{
        return [
            ...await ArticulosEntregados.GetTransacciones(cliente_id),
            ...await ItemsDeRecibos.GetTransacciones(cliente_id),
            ...await DevolucionesArticulos.GetTransacciones(cliente_id)
        ]
    }
    public static async ImprimirComprobante(cliente_id:number):Promise<void>{
        const cliente           = await this.GetCliente(cliente_id)
        const resumen           = await ResumenClientes.GetResumenCliente(cliente_id)
        if(!cliente) throw new Error(`Imprimir Comprobante: No existe el cliente con id: ${cliente_id}`)
        const hojaDeRuta        = await HojasDeRuta.GetActual()
        if(!hojaDeRuta) throw new Error(`Imprimir Comprobante: No hay informacion de la hoja de ruta`)
        const configuraciones   = await Configuraciones.GetAll()
        const cobros            = await ItemsDeRecibos.GetItemsDeReciboCliente(cliente_id)
        const articulos         = await ArticulosEntregados.GetArticulosEntregadosACliente(cliente_id)
        const alertas           = await AlertasDeCliente.GetAlertas(cliente_id)
        const limites           = await LimitesDeClientes.GetLimite(cliente_id)
        const facturas          = await Facturas.Get(cliente_id)
        const web_empresa       = configuraciones.find(config=>config.clave==="WEB_EMPRESA")
        const nombre_fantasia   = configuraciones.find(config=>config.clave==="NOMBRE_FANTASIA")
        const telefono          = configuraciones.find(config=>config.clave==="TELEFONO")

        const receipt:Receipt   = [
            ...PrinterTemplates.cabecera(nombre_fantasia?.valor??"",web_empresa?.valor??"",telefono?.valor??""),
            ...PrinterTemplates.clienteInfo(cliente, hojaDeRuta),
        ]
        if(cliente.ausente){
            receipt.push(
                ...PrinterTemplates.mensaje("Estimado cliente: Visitamos su domicilio, pero nadie nos atendio. En caso de necesitar alguno de nuestros productos por favor contactenos.")
                )
        }else{
            if(articulos.some(art=>art.cantidadEntregada>0))        receipt.push(...PrinterTemplates.articulosEntregados(articulos))
            if(articulos.some(art=>art.cantidadEnvasesPrestados>0)) receipt.push(...PrinterTemplates.envasesPrestados(articulos))
            if(articulos.some(art=>art.cantidadEnvasesDevueltos>0)) receipt.push(...PrinterTemplates.envasesDevueltos(articulos))
            if(cobros.some(cobro=>cobro.importe>0))                 receipt.push(...PrinterTemplates.cobros(cobros))
            
        }
        const fechaActual=new Date()
        receipt.push(...PrinterTemplates.saldos(cliente, cobros.reduce((t,c)=>t+c.importe, 0),articulos.reduce((t,a)=>t+a.subtotal,0)))
        switch(true){ //imprimir alertas de bloqueo o suspension
            case alertas.some(alerta=>alerta.tipoDeAlerta_ids===1):
                receipt.push(...PrinterTemplates.cuentaSuspendidaSaldos({
                    limite:limites.limiteDeSaldo,
                    saldo:cliente.saldoFacturacion+cliente.saldoConsumos
                }))
                break;
            case alertas.some(alerta=>alerta.tipoDeAlerta_ids===2):
                receipt.push(...PrinterTemplates.cuentaSuspendidaFacturas({
                    cantVencidos:facturas.filter(factura=>{
                        const vencimiento=new Date(parseInt(factura.fechaVencimiento1.substring(6, factura.fechaVencimiento1.length-2)))
                        return vencimiento<fechaActual
                    }).length,
                    cantVigente:facturas.filter(factura=>{
                        const vencimiento=new Date(parseInt(factura.fechaVencimiento1.substring(6, factura.fechaVencimiento1.length-2)))
                        return vencimiento>=fechaActual
                    }).length,
                    desde:moment(facturas
                            .map(f=>new Date(parseInt(f.fechaVencimiento1.substring(6, f.fechaVencimiento1.length-2))))
                            .reduce((newer,vencimiento)=>vencimiento<fechaActual && vencimiento>newer?vencimiento:newer,new Date(0))
                        ).format("DD/MM/YYYY"),
                    montoVencido:facturas.filter(factura=>{
                        const vencimiento=new Date(parseInt(factura.fechaVencimiento1.substring(6, factura.fechaVencimiento1.length-2)))
                        return vencimiento<fechaActual
                    }).reduce((t,f)=>t+f.saldoActual,0),
                    montoVigente:facturas.filter(factura=>{
                        const vencimiento=new Date(parseInt(factura.fechaVencimiento1.substring(6, factura.fechaVencimiento1.length-2)))
                        return vencimiento>=fechaActual
                    }).reduce((t,f)=>t+f.saldoActual,0)
                }))
                break;
            case alertas.some(alerta=>alerta.tipoDeAlerta_ids===3):
                receipt.push(...PrinterTemplates.cuentaPreventivoSaldo({
                    limite:limites.limiteDeSaldo,
                    saldo:cliente.saldoFacturacion+cliente.saldoConsumos
                }))
                break;
            case alertas.some(alerta=>alerta.tipoDeAlerta_ids===4):
                receipt.push(...PrinterTemplates.cuentaPreventivoFacturas({
                    cantVencidos:facturas.filter(factura=>{
                        const vencimiento=new Date(parseInt(factura.fechaVencimiento1.substring(6, factura.fechaVencimiento1.length-2)))
                        return vencimiento<fechaActual
                    }).length,
                    cantVigente:facturas.filter(factura=>{
                        const vencimiento=new Date(parseInt(factura.fechaVencimiento1.substring(6, factura.fechaVencimiento1.length-2)))
                        return vencimiento>=fechaActual
                    }).length,
                    desde:moment(facturas
                            .map(f=>new Date(parseInt(f.fechaVencimiento1.substring(6, f.fechaVencimiento1.length-2))))
                            .reduce((newer,vencimiento)=>vencimiento>=fechaActual && (newer<fechaActual || vencimiento<=newer)?vencimiento:newer,new Date(0))
                        ).format("DD/MM/YYYY"),
                    montoVencido:facturas.filter(factura=>{
                        const vencimiento=new Date(parseInt(factura.fechaVencimiento1.substring(6, factura.fechaVencimiento1.length-2)))
                        return vencimiento<fechaActual
                    }).reduce((t,f)=>t+f.saldoActual,0),
                    montoVigente:facturas.filter(factura=>{
                        const vencimiento=new Date(parseInt(factura.fechaVencimiento1.substring(6, factura.fechaVencimiento1.length-2)))
                        return vencimiento>=fechaActual
                    }).reduce((t,f)=>t+f.saldoActual,0)
                }))
                break;
        }
        receipt.push(...PrinterTemplates.pie(await AppStorage.getString("SERVERDOMAIN"), resumen?.usuario, resumen?.clave))
        await Printer.PrintReceipt(receipt)
    }
    public static async AregarPedidoCliente(pedido:ClientePedidoMobile):Promise<AppData_Clientes>{
        const cliente:AppData_Clientes = { 
            ...pedido.cliente,
            esPedido:true,
            esRepaso:false,
            esSync:false,
            requiereRemito:pedido.cliente.conRemito??false,
            pendiente:true,
            descargado:false,
            tieneAlertas:pedido.alertasDeCliente.length>0,
            haVistoAlertas:false,
            clienteFactura_id:pedido.cliente.clientePadre??pedido.cliente.cliente_id,
            listaDePrecioId:pedido.cliente.listaDePrecios_id,
            cerrado:false,
            fechaHoraTransferido:null,
            relevamientoCoordenadas_latitud:"",
            relevamientoCoordenadas_longitud:"",
            desbloqueado:false,
            totalEntregadoActual:0,
            totalCobradoActual:0
        } as AppData_Clientes

        await this.SaveCliente(cliente)
        const result = await DB.Get<AppData_Clientes>("Clientes","cliente_id",cliente.cliente_id)
        if(!result)throw new Error("Error al agregar cliente de pedido")
        return result
    }
    public static async Buscar(texto:string):Promise<AppData_Clientes[]>{
        const clientes = await this.GetClientes()
        return clientes.filter(cl=>
            (cl.nombreCliente && cl.nombreCliente.toLowerCase().indexOf(texto)     >=  0)   ||
            (cl.domicilioCompleto && cl.domicilioCompleto.toLowerCase().indexOf(texto) >=  0)
        )
    }
    public static async GetPendientesSync():Promise<AppData_Clientes[]>{
        return (await this.GetClientes()).filter(cliente=>cliente.esSync && cliente.fechaHoraTransferido===null)
    }
    public static async ResetCliente(cliente_id:number){

        const cliente=await this.GetCliente(cliente_id)
        if(!cliente) throw new Error("Cliente no encontrado")
        await ArticulosEntregados.LimpiarEntrega(cliente_id)
        await DevolucionesArticulos.EliminarTransacciones(cliente_id)
        await ItemsDeRecibos.EliminarTransacciones(cliente_id)
        
        cliente.totalCobradoActual=0
        cliente.visitado=false
        cliente.visita_fechaHora=""
        cliente.visita_longitud=""
        cliente.visita_altitud=""
        this.SaveCliente(cliente)

    }
}
