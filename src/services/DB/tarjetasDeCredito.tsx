import { DB } from 'utils'

import { AppData_TarjetasDeCredito }    from 'entities/app'
import { ReciboMobile_DTO }             from 'entities/recibos'
import { TarjetaDeCreditoMobile_DTO}    from 'entities/recibos'

export class TarjetasDeCredito{
    public static async Get(id:number):Promise<AppData_TarjetasDeCredito|undefined>{
        return DB.Get<AppData_TarjetasDeCredito>("TarjetasDeCredito", "id", id)
    }
    public static async Save(tarjeta:AppData_TarjetasDeCredito):Promise<AppData_TarjetasDeCredito>{
        const tar = await DB.Get<AppData_TarjetasDeCredito>("TarjetasDeCredito","id",await DB.Put("TarjetasDeCredito",tarjeta))
        if(!tar) throw new Error("Error a registrar cobro con tarjeta de credito")
        return tar
    }
    public static async Delete(id:number):Promise<AppData_TarjetasDeCredito>{
        return DB.Delete<AppData_TarjetasDeCredito>("TarjetasDeCredito", id)
    }
    public static async GetMobile_DTO(clienteId:number):Promise<TarjetaDeCreditoMobile_DTO[]>{
        const recibos = await DB.GetAll<ReciboMobile_DTO>("Recibos", "clienteId",clienteId)
        return (await Promise.all(
            recibos
            .map<Promise<TarjetaDeCreditoMobile_DTO[]>>(recibo=>DB.GetAll<TarjetaDeCreditoMobile_DTO>("TarjetasDeCredito", "reciboId", recibo.id))))
            .reduce((resultado,tarjetas)=>[...resultado, ...tarjetas],[])
    }
}