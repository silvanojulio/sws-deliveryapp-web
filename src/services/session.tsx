import ApiClient    from 'utils/apiClient'
import AppStorage   from 'utils/appStorage'

import { User_DTO } from 'entities/usuarios'
import { CurrentLocalSession } from 'entities/session'
import { LoginResponse }        from 'entities/session'

export type LoginSuccess={usuario:User_DTO, tokenValido:string, session_id:number}
export class SessionService{

    public static async IniciarSesionControladorPlaya(
            username:string, 
            password:string
        ):Promise<LoginSuccess>{
        const res = await ApiClient.call<LoginSuccess>({
            url:`Session/IniciarSesionControladorPlaya`,
            method:"post",
            responseType:"json",
            data:{username, password}
        })
        if(res.error) throw new Error(res.message)
        return res as LoginSuccess
    }
    public static async IniciarSessionExterno(username:string, password:string):Promise<LoginResponse>{
        const res = await ApiClient.call<LoginResponse>({
            url:`Session/IniciarSesionExterno`,
            method:"post",
            responseType:"json",
            data:{username, password}
        })
        if(res.error) throw res.error
        await AppStorage.setValue("lastRemoteLogin",res)
        return res as LoginResponse
    }
    public static async getValidToken(username?:string, password?:string):Promise<string>{
        const usuario = await AppStorage.getObject<CurrentLocalSession>("CURRENT_LOCAL_SESSION")
        if(usuario?.token) return usuario.token
        const lastLogin=await AppStorage.getObject<LoginResponse>("lastRemoteLogin")
        if(lastLogin?.tokenValido && lastLogin.usuario.usuario_id===usuario?.userApp.usuario_id){

            return lastLogin.tokenValido
        }
        if(!username || !password) throw new Error("Datos de logueo no ingresados")
        const loginResult = await this.IniciarSessionExterno(username,password)
        return loginResult.tokenValido
    }
    public static async GetCurrentLocalSession():Promise<CurrentLocalSession | undefined>{
        const currentSession = await AppStorage.getObject<CurrentLocalSession>("CURRENT_LOCAL_SESSION")
        if(!currentSession) return undefined
        if(currentSession.expire<=Date.now()) return undefined
        return currentSession
    }
}