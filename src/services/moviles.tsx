import ApiClient from 'utils/apiClient'

import { VIEW_Moviles_DTO } from 'entities/moviles'
export class MovilesServices{
    public static async ObtenerMoviles():Promise<VIEW_Moviles_DTO[]>{
        const res = await ApiClient.call<{moviles:VIEW_Moviles_DTO[]}>({
            url:`Moviles/ObtenerMoviles`,
            method:"get",
            responseType:"json"
        })
        if(res.error) throw res.message
        return res.moviles
    }
    public static async InformarUbicacion(
        hojaDeRuta_id:number,
        codigoIdentificacionMovil:string,
        latitud:string, longitud:string
    ):Promise<void>{
        const data={
            codigoIdentificacionMovil,
            ubicacion:{latitud,longitud,hojaDeRuta_id,estadoMovi_ids:1}
        }
        const res = await ApiClient.call<{}>({
            url:"Moviles/InformarUbicacion",method:"post",data,responseType:'json'
        })
        if(res.error) throw new Error(`Error '${res.error}':'${typeof(res.error)}'`)
        return
    }

}