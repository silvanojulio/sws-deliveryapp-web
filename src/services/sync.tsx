import ApiClient                                from 'utils/apiClient'
import moment                                   from 'moment'
import { RegistrarVisitaClienteMobileRequest, 
    RegistrarServicioTecnicoRequest, 
    SyncNuevosPedidos,
    ConfirmarClienteAgregadoALaRutaRequest, 
    AgregarClienteAHojaDeRutaRequest}           from 'entities/sync'
import { ComandoMobile }                        from 'entities/mongo'
import * as DB                                  from 'services/DB'
import { AppStorage }                           from 'utils'


export class SyncServices{ 
    private static apiUrl='Sync'
    public static async RegistrarVisitaClienteMobile_DTO(request:RegistrarVisitaClienteMobileRequest):Promise<Date>{
        const res=await ApiClient.call<{fecha:string}>({
            url:`${this.apiUrl}/RegistrarVisitaClienteMobile`,
            method:"post",
            responseType:"json",
            data:{request},
        })        
        if(res.error) throw new Error(res.message)
        return (moment(res.fecha)).toDate()
    }
    public static async RegistrarVisitaClienteMobile(cliente_id:number):Promise<Date>{
        const hojaDeRutaId=parseInt(await AppStorage.getString("HojaDeRutaId"))
        const codigoMovil=await AppStorage.getString("MOVILID")
        if(!codigoMovil) throw new Error(`Codigo Movil no configurado`)
        if(!hojaDeRutaId) throw new Error(`Hoja de Ruta no seleccionada`)
        const firmas=await DB.FirmasRemitos.GetFirmasRemitosMobile(cliente_id)
        const firmaBase64=firmas[0]?.firmaRemito??null
        const request:RegistrarVisitaClienteMobileRequest={ 
            visita:{
                ClienteVisitado:await DB.Clientes.GetClienteVisitado(cliente_id),
                ArticulosEntregados:await DB.ArticulosEntregados.GetArticulosEntregadosMobile(cliente_id),
                Cheques:await DB.Cheques.GetByClienteMobile(cliente_id),
                ComprobantesEntregados:[],
                ComprobantesFisicosUtilizados:await DB.ComprobantesFisicosUtilizados.GetComprobantesFisicosUtilizadosMobile(cliente_id),
                DevolucionesArticulos:await DB.DevolucionesArticulos.GetMobile_DTO(cliente_id),
                FirmasRemito:firmas.map(firma=>{
                    delete firma.firmaRemito
                    return firma
                }),
                Imputaciones:await DB.Facturas.GetImputaciones(cliente_id),
                ItemsDeRecibos:await DB.ItemsDeRecibos.GetMobile_DTO(cliente_id),
                Recibos:await DB.Recibos.GetReciboClienteMobile(cliente_id),
                Retenciones:await DB.Retenciones.GetMobile_DTO(cliente_id),
                TarjetaDeDebito:await DB.TarjetasDeDebito.GetMobile_DTO(cliente_id),
                TarjetasDeCredito:await DB.TarjetasDeCredito.GetMobile_DTO(cliente_id),
                hojaDeRutaId:hojaDeRutaId,
            },
            codigoMovil,
            hojaDeRutaId,
            firmaBase64,
            garbage:""
        } 
        //return new Date()
        return this.RegistrarVisitaClienteMobile_DTO(request)
    }
    public static async RegistrarServicioTecnico(request:RegistrarServicioTecnicoRequest):Promise<Date>{
        const res=await ApiClient.call<{fecha:string}>({
            url:`${this.apiUrl}/RegistrarServicioTecnico`,
            method:"post",
            responseType:"json",
            data:{request},
        })
        
        if(res.error) throw res.message
        return (moment(res.fecha)).toDate()
    }
    public static async ObtenerNuevosPedidos(hojaDeRutaId:number, codigoDeMovil:string):Promise<SyncNuevosPedidos>{
        const res=await ApiClient.call<{nuevosPedidos:SyncNuevosPedidos}>({
            url:`${this.apiUrl}/ObtenerNuevosPedidos`,
            method:"get",
            responseType:"json",
            params:{ hojaDeRutaId, codigoDeMovil },
        })
        
        if(res.error) throw res.message
        return res.nuevosPedidos
    }
    public static async ConfirmarClienteAgregadoALaRuta(request:ConfirmarClienteAgregadoALaRutaRequest):Promise<void>{
        const res=await ApiClient.call<{}>({
            url:`${this.apiUrl}/ConfirmarClienteAgregadoALaRuta`,
            method:"post",
            responseType:"json",
            data:{ request },
        })
        
        if(res.error) throw res.message
        return
    }
    public static async QuitarAusenteNoCompraDeClienteEnMobile(hojaDeRutaId:number, clienteId:number):Promise<void>{
        const res=await ApiClient.call<{}>({
            url:`${this.apiUrl}/QuitarAusenteNoCompraDeClienteEnMobile`,
            method:"post",
            responseType:"json",
            data:{ hojaDeRutaId, clienteId },
        })
        
        if(res.error) throw res.message
        return
    }
    public static async VerificarConfiguracion(domain:string, codigoIdentificacion:string):Promise<string>{
        const res=await ApiClient.call<{message:string}>({
            headers:{
                urldestiny:`http://${domain}/${this.apiUrl}/VerificarConfiguracion`
            },
            method:"get",
            responseType:"json",
            params:{ codigoIdentificacion },
            data:{ codigoIdentificacion },
        })
        
        if(res.error) throw new Error(res.message)
        return res.message
    }
    public static async AgregarClienteAHojaDeRuta(request:AgregarClienteAHojaDeRutaRequest):Promise<void>{
        const res=await ApiClient.call<{message:string}>({
            url:`${this.apiUrl}/AgregarClienteAHojaDeRuta`,
            method:"post",
            responseType:"json",
            data:{ request },
        })
        
        if(res.error) throw res.message
        return 
    }
    public static async ObtenerComandosParaEjecutar(codiIdentificacion:string):Promise<ComandoMobile[]>{
        const res=await ApiClient.call<{comandos:ComandoMobile[]}>({
            url:`${this.apiUrl}/ObtenerComandosParaEjecutar`,
            method:"get",
            responseType:"json",
            params:{ codiIdentificacion },
        })
        
        if(res.error) throw new Error(res.message)
        return res.comandos
    }
    public static async ConfirmarComandosMobile(idsComandos:string[]){
        const res = await ApiClient.call<{}>({
            url:'Sync/ConfirmarComandosMobile',
            method:"post",
            responseType:"json",
            data:{idsComandos}
        })
        if(res.error) throw new Error(res.message)
    }
}
export default SyncServices