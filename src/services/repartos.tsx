import ApiClient from 'utils/apiClient'

import { RepartoDTO } from 'entities/repartos'
export default class RepartosServices{
    private static apiUrl='Repartos'

    public static async ObtenerRepartosDisponibles():Promise<RepartoDTO[]>{
        const res = await ApiClient.call<{repartos:RepartoDTO[]}>({
            url:`${this.apiUrl}/ObtenerRepartosDisponibles`,
            method:"get"
        })
        if(res.error) throw res.message
        return res.repartos
    }
}