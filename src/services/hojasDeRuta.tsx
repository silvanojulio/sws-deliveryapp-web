import ApiClient    from 'utils/apiClient'
import moment       from 'moment'

import { AppData }                          from 'entities/app'
import { AppData_GastosDeReparto }          from 'entities/app'
import { AppData_DeclaracionesEfectivo }    from 'entities/app'
import { VIEW_HojasDeRutas_DTO }            from 'entities/hojasDeRuta'
import { HojaDeRutaExportableDto }          from 'entities/hojasDeRuta'
import { AppStorage, DB }                   from 'utils'

export class HojasDeRutaSerivce{
    public static async ObtenerHojaDeRutaYClientes(fecha:string, reparto:number):Promise<VIEW_HojasDeRutas_DTO[]>{
        const res = await ApiClient.call<VIEW_HojasDeRutas_DTO[]>({
            url:`HojasDeRuta/ObtenerHojaDeRutaYClientes`,
            method:"get",
            responseType:"json",
            params:{
                fecha, 
                reparto
            }
        })
        return res
    }
    public static async ObtenerDetallesDeClientes(hojaDeRutaId:number):Promise<HojaDeRutaExportableDto>{
        const res = await ApiClient.call<{detalles:HojaDeRutaExportableDto}>({
            url:`HojasDeRuta/ObtenerDetallesDeClientes`,
            method:'get',
            responseType:'json',
            params:{hojaDeRutaId}
        })
        if(res.error) throw res.message
        return res.detalles
    }    
    public static async DescargarBaseDeDatosEnJSON(date:Date):Promise<AppData>{ 
        const fecha=moment(date).format("YYYYMMDD")
        const codigoIdentificacionMovil=await AppStorage.getString("MOVILID")
        const res = await ApiClient.call<{db:AppData}>({
            url:`HojasDeRuta/DescargarBaseDeDatosEnJSON`,
            method:"get",
            responseType:"json",
            params:{codigoIdentificacionMovil,fecha}
        })
        if(res.error) throw res.message
        await AppStorage.setValue("HojaDeRutaId", res.db.HojasDeRuta[0].id)
        await DB.Fill(res.db).catch(error=>{})
        return res.db 
    }
    public static async CerrarHojaDeRuta(
        codigoMovil:string, 
        hojaDeRutaId:string,
        usuarioId:number,
        gastos:AppData_GastosDeReparto[],
        declaracionesEfectivo:AppData_DeclaracionesEfectivo[]
    ):Promise<{[key:string]:any}>{
        const res=await ApiClient.call<{[key:string]:any}>({
            url:`HojasDeRuta/CerrarHojaDeRutaMobile`,
            method:"post",
            responseType:"json",
            data:{codigoMovil,hojaDeRutaId,gastos,declaracionesEfectivo,usuarioId}
        })
        if(res.error) throw new Error(res.message)
        return res
    }
}