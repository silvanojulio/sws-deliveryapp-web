import { ApiClient } from 'utils'
import { VIEW_Dispensers_DTO } from 'entities/dispensers'

export default class DispensersService{
    private static apiUrl='/Dispensers'

    public static async ObtenerDispenserPorNumeroInterno(numero:string):Promise<VIEW_Dispensers_DTO>{
        const res=await ApiClient.call<{dispenser:VIEW_Dispensers_DTO}>({
            url:`${this.apiUrl}/ObtenerDispenserPorNumeroInterno`,
            method:"get",
            params:{numero}
        })
        if(res.error) throw res.error
        if(!res.dispenser) throw new Error("Dispenser no encontrado")
        return res.dispenser
    }

}