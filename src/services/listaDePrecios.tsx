import { ApiClient } from 'utils'
import { VIEW_ListaDePrecios_DTO } from 'entities/listaDePrecios'

export default class ListaDePreciosServices{
    private static apiUrl='ListaDePrecios'

    public static async ObtenerListasDePreciosVigentes():Promise<VIEW_ListaDePrecios_DTO[]>{
        const res = await ApiClient.call<{listas:VIEW_ListaDePrecios_DTO[]}>({
            url:`${this.apiUrl}/ObtenerListasDePreciosVigentes`
        })
        if(res.error) throw res.message
        return res.listas
    }
}