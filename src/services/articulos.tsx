import ApiClient from 'utils/apiClient'
import AppStorage from 'utils/appStorage'
import { VIEW_Articulos_DTO } from 'entities/articulos'

export default class ArticulosServices{
    private static apiUrl="Articulos"
    public static async ObtenerArticulosActivos():Promise<VIEW_Articulos_DTO[]>{
        const res=await ApiClient.call<{articulos:VIEW_Articulos_DTO[]}>({
            url:`${this.apiUrl}/ObtenerArticulosActivos`,
            method:"get",
            responseType:"json",
            params:{},
            data:{},
            headers:{}
        })
        
        if(res.error) throw res.message
        AppStorage.setValue("ObtenerArticulosActivos", res.articulos)
        return res.articulos
    }
}