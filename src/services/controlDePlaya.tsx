import { ApiClient } from 'utils'
import { ControlDePlaya } from 'entities/controlDePlaya'

export default class ControlesDePlayaService{
    private static apiUrl = "/ControlesDePlaya"

    public static async GuardarNuevoControlMobile(
        control:ControlDePlaya, tokenAprobador:string
    ):Promise<void>{
        const {hojaDeRutaId, tipoEventoId, kilometros, usuarioControlaId}=control
        const idsDispensers:number[]|null=control.articulos.filter(art=>!!art.dispenser && !art.articulo).map(art=>art.dispenser_id)
        const articulos=control.articulos.reduce<{
            articulo_id:number,
            cantidadVacios:number,
            cantidadLlenos:number,
            cantidadFallados:number
        }[]>((result, art)=>{
            if(!!art.articulo && !art.dispenser) return [...result, {
                articulo_id:art.articulo_id,
                cantidadVacios:art.cantidadVaciosPack*art.articulo!.cantidadPorPack + art.cantidadVaciosUni,
                cantidadLlenos:art.cantidadLlenosPack*art.articulo!.cantidadPorPack + art.cantidadLlenosUni,
                cantidadFallados:art.cantidadFalladosPack*art.articulo!.cantidadPorPack + art.cantidadFalladosUni                
            }]
            return result
        },[])

        /*Guardar Control de Playa con token aprobador*/
        
        const res = await ApiClient.call<{}>({
            url:`${this.apiUrl}/GuardarNuevoControlMobile`,
            method:"post",
            headers:{CURRENTTOKENVALUE:tokenAprobador},
            data:{
                hojaDeRutaId, tipoEventoId, kilometros,
                usuarioControlaId, articulos, idsDispensers
            }
        })
        if(res.error) throw res.message
        
    }
}