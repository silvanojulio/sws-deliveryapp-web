import { ApiClient }                from 'utils'
import { Incidentes_Totales_DTO }   from 'entities/incidentes'
import { VIEW_Incidente_DTO }       from 'entities/incidentes'
import { UsuariosApp }              from './DB/usuariosApp'
import { SessionService}            from './session'
import moment                       from 'moment'

export class IncidentesService{
    public static async ObtenerIncidentesAbiertosPorUsuario():Promise<Incidentes_Totales_DTO>{
        const usuario = await SessionService.GetCurrentLocalSession()
        if(!usuario)throw new Error("Usuario No Logueado")
        const res = await ApiClient.call<Incidentes_Totales_DTO>({
            url:'Incidentes/ObtenerIncidentesAbiertosPorUsuario',
            params:{
                usuarioId:usuario.userApp.usuario_id
            }
        })
        if(res.error) throw new Error(res.message)
        return res
    }
    public static async ObtenerIncidentesCliente(usuarioId:number):Promise<VIEW_Incidente_DTO[]>{
        const usuario = await UsuariosApp.GetUsuarioById(usuarioId)
        const validToken =await SessionService.getValidToken(usuario.username, usuario.password)

        const res = await ApiClient.call<{incidentes:VIEW_Incidente_DTO[]}>({
            url:'Incidentes/ObtenerIncidentesCliente',
            method:'post',
            params:{
                usuarioId:usuarioId,
                estadoIncidente:1,
                fechaDesde:moment(new Date()).format("DD/MM/YYYY"),
                fechaHasta:moment(new Date()).format("DD/MM/YYYY"),
            },
            headers:{
                CURRENTTOKENVALUE:validToken
            }
        })
        if(res.error) throw new Error(res.message)
        return res.incidentes
    }
}