import { Receipt, ReceiptText }              from 'types/receipt'
import * as Utils               from 'utils'

class PrinterCommands{
    public static ESC_INIT      = new Uint8Array([0x1B, 0x40])
    public static ESC_CHAR_MODE = new Uint8Array([0x1B, 0x21, 0x00])
    public static ESC_IMAGE_MODE= new Uint8Array([0x1B, 0x2A, 33, 124, 1])
    public static LINE_FEED     = new Uint8Array([10])
}
export class Printer{
    private static device:any=null
    private static printer:any=null
    private static async PrintIMG(base64:string):Promise<Uint8Array>{
        const {width, dots} = await Utils.ImageUtil.ConvertToDotsArray(base64)
        const chunk_maxSize=width*24
        const chunk_count=Math.ceil(dots.length/chunk_maxSize)
        const firma=new Array<Uint8Array>()
        for(let ch_index=0;ch_index<chunk_count;ch_index++){
            const chunk=Utils.transpose(dots.slice(chunk_maxSize*ch_index,chunk_maxSize*(ch_index+1)),width)
            const buffer=new Uint8Array(380*3)
            for(let b=0;b<chunk.length;b+=8){
                buffer[b/8]=parseInt(chunk.slice(b,b+8).join(""),2)
            }
            firma.push(Utils.concatUint8Arrays(PrinterCommands.ESC_IMAGE_MODE,buffer, PrinterCommands.LINE_FEED))
        }
        return Utils.concatUint8Arrays(new Uint8Array([0x1B, 0x33, 24]),...firma,new Uint8Array([0x1B, 0x33, 30, 0x1B, 0x40]))
    }
    private static async PrintArrayBuffer(buffer:Uint8Array, start:number=0):Promise<void>{
        if(!this.device || !this.printer) await this.FindPrinter()
        if(!this.device || !this.printer) throw new Error("Error al conectar con la impresora") 
        for(var offset=0;offset<buffer.length;offset+=20) 
        await this.printer.writeValueWithoutResponse(buffer.slice(offset, offset+20))
        this.device.gatt.disconnect()
    }
    private static async FindPrinter(){
        
        let mobileNavigatorObject: any = window.navigator;
        if(mobileNavigatorObject && mobileNavigatorObject.bluetooth) {
            if(!this.device){
                this.device=await mobileNavigatorObject.bluetooth.requestDevice({
                    filters:[{
                        services:["e7810a71-73ae-499d-8c15-faa9aef0c3f2"]
                    }]
                })
                
                this.device.ongattserverdisconnected=()=>{
                    this.printer=null
                }
            }
            if(!this.device.gatt.connected) await this.device.gatt.connect()                
            const service = await this.device.gatt.getPrimaryService("e7810a71-73ae-499d-8c15-faa9aef0c3f2")
            const characs = await service.getCharacteristics()
            if(characs.length===0) throw new Error("El dispositivo no tiene caracteristicas para usar")
            const charac=characs[0]
            if(!charac.properties.writeWithoutResponse) throw new Error("El dispositivo no puede recibir comando de escritura")
            this.printer=charac
                
        }else{
            throw new Error("El navegador no soporta la conexion BLE")
        }  
    }
    public static async PrintReceipt(receipt:Receipt):Promise<void>{
        receipt.push(
            ReceiptText.Feed,
            ReceiptText.Separador
        )
        await this.PrintArrayBuffer(Utils.concatUint8Arrays(
            ...await Promise.all<Uint8Array>(receipt.map<Promise<Uint8Array>>(async (text)=>{
                switch(text.type){
                    case "text": return text.toUint8Array()
                    case "image":return this.PrintIMG(text.content)
                    case "qr": 
                        const len   = text.content.length+3
                        const sL    = len%256
                        const sH    = (len-sL)/256
                        return Utils.concatUint8Arrays(
                            ReceiptText.Aligns[text.align],
                            new Uint8Array([29, 40, 107, 4, 0, 49, 65, 50, 0]),
                            new Uint8Array([29, 40, 107, 3, 0, 49, 67, 8]),
                            new Uint8Array([29, 40, 107, 3, 0, 49, 69, 48]),
                            new Uint8Array([29, 40, 107, sL, sH, 49, 80, 48]),
                            new TextEncoder().encode(text.content),
                            new Uint8Array([29, 40, 107, 3, 0, 49, 81, 48]),
                            )
                    default: return new Uint8Array()
                }
            })),
            new Uint8Array([10,10,10])
        ))
    }
}