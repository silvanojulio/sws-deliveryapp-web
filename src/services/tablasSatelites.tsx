import ApiClient from 'utils/apiClient'
import { ValoresTablasSateliteDTO } from 'entities/tablasSatelites'

export default class TablasSatelites{
    private static apiUrl:string = "TablasSatelites"
    
    public static T_MOTIVOSDEDEVOLUCION:number = 280
    public static T_FORMASDEPAGO:number = 380
    public static T_BANCOS:number = 370
    public static T_TIPOSDEGASTOS:number = 390
    public static T_MOTIVOSDESOLICITUDDESTOCK:number = 790
    public static T_SINTOMASSERVICIOTECNICO:number = 550
    public static T_MOTIVOS_DE_CIERRE_ST:number = 580
    public static T_ACCIONES_DISPENSERS_ST:number = 860
    public static T_MOTIVOS_RELEVAMIENTO_ENVASE:number = 1000
    public static T_MARCAS_TARJETAS:number = 990
    public static T_TIPOS_RETENCIONES:number = 400

    public static async ObtenerValores(idsTablas:number[]):Promise<ValoresTablasSateliteDTO[]>{
        const res=await ApiClient.call<{valores:ValoresTablasSateliteDTO[]}>({
            url:`${this.apiUrl}/ObtenerValores`,
            method:"GET",
            data:{idsTablas}
        })
        if(res.error) throw res.message
        return res.valores
    }
    public static async ObtenerTodasLasTablas():Promise<ValoresTablasSateliteDTO[]>{
        return this.ObtenerValores([
            this.T_MOTIVOSDEDEVOLUCION,
            this.T_FORMASDEPAGO,
            this.T_BANCOS,
            this.T_TIPOSDEGASTOS,
            this.T_MOTIVOSDESOLICITUDDESTOCK,
            this.T_SINTOMASSERVICIOTECNICO,
            this.T_MOTIVOS_DE_CIERRE_ST,
            this.T_ACCIONES_DISPENSERS_ST,
            this.T_MOTIVOS_RELEVAMIENTO_ENVASE,
            this.T_MARCAS_TARJETAS,
            this.T_TIPOS_RETENCIONES
        ])
    }
}