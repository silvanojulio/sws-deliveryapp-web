import { ApiClient }                from 'utils'
import { VIEW_ArticulosXAbonoDTO }  from 'entities/articulos'

export class AbonosServices{
    private static apiUrl="Abonos"

    public static async ObtenerArticulosDeAbonosVigentes(clienteId:number):Promise<VIEW_ArticulosXAbonoDTO[]>{
        const res=await ApiClient.call<{articulos:VIEW_ArticulosXAbonoDTO[]}>({
            url:`${this.apiUrl}/ObtenerArticulosDeAbonosVigentes`,
            data:{clienteId}
        })
        if(res.error) throw res.message
        return res.articulos
    }
}