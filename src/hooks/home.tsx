import { useContext }   from 'react'
import { HomeContext }  from 'contexts/homeContext'

export const useHomeContext = ()=>useContext(HomeContext)