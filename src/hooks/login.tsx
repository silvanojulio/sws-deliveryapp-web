import { useContext }   from 'react'
import { LoginContext } from 'contexts/loginContext'

export const useLoginContext  = ()=>useContext(LoginContext)