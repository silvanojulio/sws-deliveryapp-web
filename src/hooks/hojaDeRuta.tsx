import { useContext } from 'react'
import { HojaDeRutaContext } from 'contexts'

export const useHojaDeRutaContext = ()=>useContext(HojaDeRutaContext)